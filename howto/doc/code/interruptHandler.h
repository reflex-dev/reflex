#include "reflex/interrupts/InterruptHandler.h"
#include "reflex/interrupts/InterruptVector.h"

class EXAMPLE : public InterruptHandler
{
protected:

public:
	EXAMPLE();
	
	virtual void enable();
	
	virtual void disable();
	  
	virtual void handle();
};
