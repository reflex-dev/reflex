#ifndef DATASPACE
#define DATASPACE 10
#endif
class SenseCompute : public Activity{
public:
	SenseCompute();
	virtual void run();
	virtual void compute();
	virtual void doRadio();

	ActivityFunctor<SenseCompute,&SenseCompute::compute> computeFunctor;
	ActivityFunctor<SenseCompute,&SenseCompute::doRadio> radioFunctor;

	void init(Sink1<char>* sensorOut, Sink1<Buffer*>* radioOut, OutputChannel* out);

	Sink1<char> *sensorOut; //pointer to the sensor input
       	OutputChannel* out;
	SingleValue2<char,uint16> adcInput;
	SingleValue1<Buffer*> radioInput;
	Event eventInput;
	Sink1<Buffer*>	*radioOut;

private:  
	uint16 values[DATASPACE];
	int counter;


};
