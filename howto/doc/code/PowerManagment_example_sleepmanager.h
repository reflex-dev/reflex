class SleepManager : public Activity{
public:

	SleepManager();

	/** This is the Activity
	 *  Here the operation mode is changed and the application inwoked
	 */
	virtual void run();

	/** Sets the pointer to subsequent components, which handle events & buffers.
	 *
	 *  @param out: pointer to the Application
	 *  @param out: pointer to the outputchannel
	 */
	void init(Event *eventOut, OutputChannel *out);

	VirtualTimer vtimer;

private:  
	Event *eventOut;	
	Event ready;
	OutputChannel *out;
	uint8 currentMode; //stores current power managment mode
	

};
