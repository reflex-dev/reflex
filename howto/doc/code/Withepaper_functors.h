template <typename T, void (T::* MemFn)()>
class ActivityFunctor : public Activity {
public:
	/** Constructor attaches the functor with an object of template type T
	 */
	ActivityFunctor(T& pObj) :
			pObj(pObj)
	{
	}

	/** In run method the specific memberfunction of the related object
	 *  is called.
	 */
	virtual void run()
	{
		(pObj.*MemFn)();
	}

private:
	/** Reference to the related object.
	 */
	T& pObj;
};
