void TimeTriggeredScheduler::start()
{
	while(1){
		_interruptsDisable();
		if(current == 0){
		  current = list.first();
		}
		//if timeslot of next activity is reached
		if(getSystem().clock.getTime() == current->startTime){

		  //if activity has to be invoked
		  if(current->act->rescheduleCount >0 && !current->act->locked){

        		current->act->rescheduleCount--;
        		scheduleCount--;

        		_interruptsEnable();
        		current->act->run();
        		_interruptsDisable();

		  }

		  //go to next activity
		  current = current->next;
		}

		//sleep till next time interrupt
		getSystem().powerManager.powerDown();

	}
}
