class NodeConfiguration 
	: public System 
{
public:
	NodeConfiguration() 
		: System()
		, pool(0) //the Buffer is only used as FiFo.
		, out(&pool)
		, radio(pool)
		, serial(1,SerialRegisters::B19200)
	{
		//wires components together
		out.init(&serial.input);
		serial.init(0,0);
		adc.init(&senseCompute.adcInput);
		radio.init(&senseCompute.radioInput,0);
		senseCompute.init(&adc.input, &radio.input, &out);
		sleepman.init(&senseCompute.eventInput, &out);
 
		//set groups
		timer.setGroups(SLEEP | AWAKE );
		serial.rxHandler.setGroups( AWAKE );
		//serial TX and ADC are activated on demand,
		//thus put them into a default group
		serial.txHandler.setGroups( NOOPGROUP );
		adc.setGroups( NOOPGROUP );
		//the radio is only active in AWAKE
		radio.setGroups( AWAKE );
				
		//initally (de)-activate groups
		powerManager.enableGroup(SLEEP);
		powerManager.disableGroup(AWAKE);
	}

	PoolManager poolManager;
	SizedPool<IOBufferSize,NrOfStdOutBuffers> pool;
	OutputChannel out;
	ADConverter12 adc;
	Serial serial;
	TMoteRadio radio;
	SenseCompute senseCompute;
	SleepManager sleepman;
};
