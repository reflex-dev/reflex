//define power groups
enum PowerGroup {
	AWAKE = reflex::PowerManager::GROUP1, 
	SLEEP = reflex::PowerManager::GROUP2,
	NOOPGROUP = reflex::PowerManager::NOTHING
};


class NodeConfiguration 
	: public System 
{
public:
	NodeConfiguration() 
		: System()
		, pool(0) //the Buffer is only used as FiFo. 
			  //So stacksize is 0 @see Buffer
		, out(&pool)
		, serial(1,SerialRegisters::B19200)
	{
		//wires components together
		out.init(&serial.input);
		serial.init(&senseCompute.serialInput,0);
		adc.init(&senseCompute.adcInput);
		senseCompute.init(&adc.input, &out);
		sleepman.init(&senseCompute.eventInput, &out);
	
		//set groups
		timer.setGroups(SLEEP | AWAKE );
		serial.rxHandler.setGroups( AWAKE );
		//serial TX and ADC are activated on demand,
		//thus put them into a default group
		serial.txHandler.setGroups( NOOPGROUP );
		adc.setGroups( NOOPGROUP );

		//initally (de)-activate groups
		powerManager.enableGroup(SLEEP);
		powerManager.disableGroup(AWAKE); 
		
	}

	PoolManager poolManager;
	SizedPool<IOBufferSize,NrOfStdOutBuffers> pool; 
	OutputChannel out;

	ADConverter12 adc;
	Serial serial;
	SenseCompute senseCompute;
       	SleepManager sleepman;
};
