#include "HelloWorld.h"

using namespace reflex;

HelloWorld::HelloWorld() : vtimer(VirtualTimer::PERIODIC)
{
	ready.init(this);
	vtimer.init(ready);
	vtimer.set(1000);
}


void HelloWorld::init(OutputChannel* out)
{
	this->out = out;
}

void HelloWorld::run()
{		
  out->write("HelloWorld!");
  out->writeln();
}
