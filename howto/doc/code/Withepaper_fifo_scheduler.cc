//dispatching of the first element in the readyList
void FifoScheduler::dispatch()
{
	_interruptsEnable(); // interrupts are enabled during execution
	first->run();
	_interruptsDisable();

	first->rescheduleCount--;
	if((first->rescheduleCount) && (!first->locked)){ //schedule again
		last->next = first;
		last = first;
		first = first->next;
		last->next = 0;
	}else{                                            //don't schedule again
		first->status = FifoActivity::IDLE;
		first = first->next;
	}
}
