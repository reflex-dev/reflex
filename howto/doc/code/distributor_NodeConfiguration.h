#include "reflex/misc/Distributor.h"

class NodeConfiguration : public System {
public:
	NodeConfiguration()
	{
		clock.init(&distributor);	
		distributor.registerSink(A.timer);
		distributor.registerSink(B.timer);
		distributor.registerSink(C.timer);		
	}
	Application A, B, C;
	Distributor<3> distributor;
};
