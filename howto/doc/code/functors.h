#include "reflex/scheduling/Activity.h"
#include "reflex/scheduling/ActivityFunctor.h"

using namespace reflex;

#ifndef DATASPACE
#define DATASPACE 10
#endif

class SenseCompute : public Activity{
public:
	SenseCompute();

	/** This is the Activity
	 *  Here the sensor is inwoked
	 */
	virtual void run();

	/** This is the other Activity
	 *  Here the data is written to standard output
	 */
	virtual void compute();

	/** This is the Activity Functor
	 *  Ponting to a class and to a member function of
	 *  this class
	 */
	ActivityFunctor<SenseCompute,&SenseCompute::compute> computeFunctor;

	/** Sets the pointer to a subsequent component, which handle buffers.
	 *
	 *  @param out: pointer to the sensor input
	 *  @param out: pointer to the outputchannel
	 */
	void init(Sink1<char>* sensorOut, OutputChannel* out);

	Sink1<char> *sensorOut; //pointer to the sensor input
       	OutputChannel* out;
	SingleValue2<char,uint16> input;
	Event ready;
	VirtualTimer vtimer;

private:  
	uint16 values[DATASPACE];
	int counter;
};
