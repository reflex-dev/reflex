class HelloWorld : public Activity{
public:
	HelloWorld();

	/** This is the Activity
	 *  Here the famous message is written to standard output
	 */
	virtual void run();


	/** Sets the pointer to a subsequent component, which handle buffers.
	 *
	 *  @param device : pointer to a subsequent component
	 *  @param pool : pointer to the memory pool
	 */
	void init(Sink1<Buffer*>* output, Pool* pool);


	Sink1<Buffer*> *output; //pointer to subseqent component
	Event ready;
	VirtualTimer vtimer;
	Pool *pool; //pointer to the pool for buffer allocation
};
