
#ifndef NodeConfiguration_h
#define NodeConfiguration_h
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(ses):	 NodeConfiguration
 *
 *	Author:		 Karsten Walther
 *
 *	Description: Implements the System
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */
#include "conf.h"

#include "reflex/System.h"
#include "reflex/io/Serial.h"
#include "reflex/io/Led.h"
#include "reflex/io/OutputChannel.h"

#include "reflex/memory/PoolManager.h"
#include "reflex/memory/SizedPool.h"

#include "SenseCompute.h"
#include "SleepManager.h"
#include "reflex/io/ADConverter.h"

namespace reflex {

enum PowerGroups {
	AWAKE = reflex::PowerManager::GROUP1, 
	SLEEP = reflex::PowerManager::GROUP2,
	NOOPGROUP = reflex::PowerManager::NOTHING
};

/**
 * This is the assembly of the components for the helloworld application.
 */
class NodeConfiguration : public System {
public:
	/** Binding components together and initialize them
	 */
	NodeConfiguration() 
		: System()
		, pool(0)
		, out(&pool)
		, serial(0,Serial::B19200)
		, adc(0)
	{
		out.init(&serial.input);

		serial.init(&senseCompute.serialInput,0);
		adc.init(&senseCompute.adcInput);
		senseCompute.init(&adc, &out);
		sleepman.init(&senseCompute.eventInput, &out);

		timer.setGroups(AWAKE | SLEEP); //timer is part of groups SLEEP and group AWAKE
		serial.setGroups(AWAKE); // serial is bound to group AWAKE


		powerManager.enableGroup(SLEEP); //this enables all registered entities (obj of type PowerManageAble) which are part of the group SLEEP
		powerManager.disableGroup(AWAKE); //that disables all entities which are part of the group SLEEP and NOT part of group AWAKE.
		// Note: The timer will stay enabled here. Cause it is part of the group AWAKE, which has been enabled.

		// trigger manually for the first time

	}

	PoolManager poolManager;
	SizedPool<IOBufferSize, NrOfStdOutBuffers> pool;

	/** Provides formatted output
	 */
	OutputChannel out;

private:
	/** Serial interface
	 */
	Serial serial;

	ADConverter adc;
	SenseCompute senseCompute;
	SleepManager sleepman;
};

inline
NodeConfiguration& getApplication()
{
	extern NodeConfiguration system;
	return system;
}

} //reflex

#endif
