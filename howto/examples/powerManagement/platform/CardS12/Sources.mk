#/*
# *	REFLEX - Real-time Event FLow EXecutive
# *
# *	A lightweight operating system for deeply embedded systems.
# *
#  
# *	Author:		 Karsten Walther
# */

CC_SOURCES_CONTROLLER += \
	io/Serial.cc \
	io/ADConverter.cc

CC_SOURCES_PLATFORM += \

