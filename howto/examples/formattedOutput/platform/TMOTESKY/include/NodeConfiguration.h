#ifndef NodeConfiguration_h
#define NodeConfiguration_h
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(ses):	 Application
 *
 *	Author:		 Karsten Walther
 *
 *	Description: Implements the System
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */

#include "reflex/types.h"
#include "reflex/io/Serial.h"
#include "reflex/io/OutputChannel.h"
#include "reflex/memory/PoolManager.h"
#include "reflex/memory/SizedPool.h"

#include "HelloWorld.h"
#include "reflex/System.h"

namespace reflex {

/**Define different powergroups for an application. To use the available groups provided by the powermanager are highly 
 * recommended.
 */
enum PowerGroups {
	AWAKE = reflex::PowerManager::GROUP1, 
	SLEEP = reflex::PowerManager::GROUP2,
	NOOPGROUP = reflex::PowerManager::NOTHING
};


class NodeConfiguration 
	: public System 
{
public:
	NodeConfiguration() 
		: System()
		, pool(0) //the Buffer is only used as FiFo. So stacksize is 0 @see Buffer
		, out(&pool)
		, serial(1,SerialRegisters::B19200) //initialize the serial interface on USART1 with 19200 baud
	{
		//wires components together
		out.init(&serial.input);
		hello.init(&out);

		timer.setGroups(SLEEP | AWAKE ); //timer is part of groups SLEEP and group AWAKE
		serial.rxHandler.setGroups( NOOPGROUP ); //not used here
		//NOTE: set txhandler to a noopgroup but it will work anyway
		serial.txHandler.setGroups( NOOPGROUP );
		//serial.txHandler must not be put in a group because secondary interrupt are managed by the corresponding software

		powerManager.enableGroup(AWAKE); //this enables all registered entities (obj of type PowerManageAble) which are part of the group SLEEP
		powerManager.disableGroup(SLEEP); //that disables all entities which are part of the group SLEEP and NOT part of group AWAKE. 
		// Note: The timer will stay enabled here. Cause it is part of the group AWAKE, which has been enabled.

	}

	PoolManager poolManager; ///< managed different pools
	SizedPool<IOBufferSize,NrOfStdOutBuffers> pool; ///< a pool of bufferobject with static size
	OutputChannel out; ///< an object that provides easy formated output. 

	Serial serial;	///< the driver for the serial interface
	HelloWorld hello; ///< here the famous HelloWorld message is produzed, repeatedly.
};


inline NodeConfiguration& getApplication()
{
	extern NodeConfiguration system;
	return system;
}

} //reflex

#endif
