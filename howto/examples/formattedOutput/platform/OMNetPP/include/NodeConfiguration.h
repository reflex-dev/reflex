#ifndef NodeConfiguration_h
#define NodeConfiguration_h
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(ses):	 NodeConfiguration
 *
 *	Author:		 Sören Höckner, Karsten Walther
 *
 *	Description: Implements the System
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */

#include "reflex/System.h"
#include "reflex/io/OmnetConsole.h"
#include "reflex/io/OutputChannel.h"
#include "reflex/memory/PoolManager.h"
#include "reflex/memory/SizedPool.h"
#include "HelloWorld.h"

#include "reflex/emulator/NetworkNode.h"
#include "reflex/debug/opp_dbg.h"

enum {
	AWAKE = reflex::PowerManager::GROUP1,
	SLEEP = reflex::PowerManager::GROUP2
};

namespace reflex {

/**
 * This is the assembly of the components for the helloworld application.
 */
class NodeConfiguration : public System {
public:
	/** Binding components together and initialize them
	 */
	NodeConfiguration():
		pool(0),	//specify internal Stacksize
		out(&pool)
	{
		timer.setGroups((AWAKE | SLEEP));
	}

	void init()
	{
		opp_dbg.prefix() << "NodeConfiguration.init()\n";

	
		out.init(&console);
		hello.init(&out);

		powerManager.enableGroup( PowerManager::NOTHING );
		powerManager.disableGroup(SLEEP);
		powerManager.enableGroup(AWAKE);

		//trigger manual for the first time
		hello.ready.notify();
	}


	void start()
	{
		getSystem().scheduler.start();
		//tc.start();
	}

	/**
	*	collect statistik data
	*/
	void finish()
	{
		opp_dbg < "NodeConfiguration.finish()\n";
	}

	void toggleLed()
	{
	}


	PoolManager poolManager;
	SizedPool<IOBufferSize, NrOfStdOutBuffers> pool;

	/** Provides formatted output
	 */
	OutputChannel out;

private:
	/** Serial interface
	 */
	OmnetConsole console;

	/** the main component for helloworld
	 */
	HelloWorld hello;
};

inline
NodeConfiguration& getApplication()
{
	extern NodeConfiguration *system; ///< the global system obj
	return *system;
}

} //reflex

#endif
