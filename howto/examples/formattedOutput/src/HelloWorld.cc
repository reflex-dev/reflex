/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
  
 *	Author:		 Karsten Walther
 */
#include "HelloWorld.h"


HelloWorld::HelloWorld() : vtimer(VirtualTimer::PERIODIC)
{
	ready.init(this);
	vtimer.init(ready);
	vtimer.set(1000);
}


void HelloWorld::init(OutputChannel* out)
{
	this->out = out;
}

void HelloWorld::run()
{		
  out->write("HelloWorld!");
  out->writeln();
}

