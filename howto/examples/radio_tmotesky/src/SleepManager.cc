/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
  
 *	Author:		 Karsten Walther
 */
#include "SleepManager.h"
#include "NodeConfiguration.h"
using namespace reflex;

SleepManager::SleepManager()
       	  : vtimer(VirtualTimer::PERIODIC)
{
	ready.init(this);
	vtimer.init(ready);
	vtimer.set(1500);
	currentMode = SLEEP;
}


void SleepManager::init(Event *eventOut, OutputChannel *out)
{
	this->eventOut = eventOut;
	this->out = out;
}


void SleepManager::run()
{
	if (currentMode == SLEEP)
	  {	//change mode to awake
		getApplication().powerManager.switchMode((powerManagement::PowerGroups) AWAKE,
						  (powerManagement::PowerGroups) SLEEP);
		currentMode = AWAKE;
		out->write("AWAKE");
    	} else {
	  //change mode to sleep
		getApplication().powerManager.switchMode((powerManagement::PowerGroups) SLEEP,
						  (powerManagement::PowerGroups) AWAKE);
		currentMode = SLEEP;
   		eventOut->notify();
		out->write("SLEEP");
  	}
  	out->writeln();
}

