/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
  
 *	Author:		 Karsten Walther
 */
#include "SenseCompute.h"
#include "NodeConfiguration.h"
using namespace reflex;

SenseCompute::SenseCompute()
	: Activity()
	, computeFunctor(*this)
	, radioFunctor(*this)
{
	eventInput.init(this);
	adcInput.init(&computeFunctor);
	radioInput.init(&radioFunctor);
	counter = 0;
}


void SenseCompute::init(Sink1<char>* sensorOut, Sink1<Buffer*>* radioOut, OutputChannel* out)
{
	this->sensorOut = sensorOut;
	this->radioOut = radioOut;
	this->out = out;
}


void SenseCompute::run()
{
  	sensorOut->assign('5');
	
}

void SenseCompute::compute()
{
	char channel;
    	adcInput.get(channel,values[counter]); //get data from eventbuffer
	counter++;
	if (counter == DATASPACE) 
	{ 
	  	counter = 0;
		uint16 mean = 0;
		for(int i = 0; i < DATASPACE; i++) //calculate mean
		{
			out->write(values[i]);
			out->write(":");
			mean += values[i];
		}
		mean = mean / DATASPACE;
		out->write(" MEAN "); //write result
       		out->write(mean);
  		out->writeln();
		//getApplication().radio.recvHandler.switchOn(); //make sure radio is on
	 	Buffer *buf2 = new(&getApplication().pool) Buffer(&getApplication().pool);
 		buf2->write(mean);		
  		radioOut->assign(buf2);	
	}
}


void SenseCompute::doRadio()
{

  Buffer *buf =  (Buffer*)radioInput.get();
  int MSG;
	buf->read(MSG);	// the message, shuld be one int
	buf->downRef(); //buffer is not needed anymore
	out->write("Got from differend Node: "); //write result
	out->write(MSG);
  	out->writeln();
}
