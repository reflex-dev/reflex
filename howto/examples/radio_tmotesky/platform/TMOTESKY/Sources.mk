#/*
# *	REFLEX - Real-time Event FLow EXecutive
# *
# *	A lightweight operating system for deeply embedded systems.
# *
#  
# *	Author:		 Carsten Schulze, Karsten Walther
# */

CC_SOURCES_CONTROLLER += \
	io/Serial.cc \
	io/ADConverter12.cc \

CC_SOURCES_PLATFORM += \
	io/TMoteRadio.cc \
	io/CC2420.cc \
	io/SPI.cc



