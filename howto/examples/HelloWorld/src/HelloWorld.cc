/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
  
 *	Author:		 Karsten Walther
 */
#include "HelloWorld.h"

using namespace reflex;

HelloWorld::HelloWorld() : vtimer(VirtualTimer::PERIODIC)
{
	ready.init(this);
	vtimer.init(ready);
	vtimer.set(1000);
}


void HelloWorld::init(Sink1<Buffer*>* output, Pool* pool)
{
	this->output = output;
	this->pool = pool;
}

void HelloWorld::run()
{
  if (output)
    {		
      Buffer* buffer = new (pool) Buffer(pool);
      if(buffer)
		  {
			  buffer->write("HelloWorld!\n");
			  output->assign(buffer);
			  buffer = 0;
		  }
    }
}
