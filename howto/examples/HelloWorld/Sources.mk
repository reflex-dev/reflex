#/*
# *	REFLEX - Real-time Event FLow EXecutive
# *
# *	A lightweight operating system for deeply embedded systems.
# *
#  
# *	Author:		 Karsten Walther
# */

CC_SOURCES_APPLICATION += \
	src/HelloWorld.cc \
	src/main.cc

CC_SOURCES_LIB += \
	memory/Buffer.cc \
	memory/FreeList.cc \
	memory/Pool.cc \
	memory/PoolManager.cc \
	io/OutputChannel.cc


CC_SOURCES_CONTROLLER += \


sinclude $(APPLICATIONPATH)/platform/$(PLATFORM)/Sources.mk
