#ifndef NodeConfiguration_h
#define NodeConfiguration_h
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(ses):	 NodeConfiguration
 *
 *	Author:		 Karsten Walther
 *
 *	Description: Implements the System
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */

#include "reflex/debug/Assert.h"
#include "reflex/System.h"

#include "reflex/io/Serial.h"
#include "reflex/io/PortRegisters.h"
#include "reflex/io/OutputChannel.h"
#include "reflex/io/ADConverter.h"

#include "reflex/memory/PoolManager.h"
#include "reflex/memory/SizedPool.h"

#include "Sense.h"
#include "Compute.h"

namespace reflex {
/**Define different powergroups for an application. To use the available groups provided by the powermanager are highly 
 * recommended.
 */
enum PowerGroups {
	AWAKE = reflex::PowerManager::GROUP1, 
	SLEEP = reflex::PowerManager::GROUP2,
	NOOPGROUP = reflex::PowerManager::NOTHING
};

/**
 * This is the assembly of the components for the application.
 */
class NodeConfiguration : public System 
{
public:	
	PoolManager poolManager; ///< A manager which manages multiple pools
	SizedPool<IOBufferSize, NrOfStdOutBuffers> pool; ///< a pool

	OutputChannel out; ///< formated output

protected:
	//the atmega128 has 2 serial modules. here the second one on port 1 is used, port 0 is also available
	Serial<1> serial; ///< Serial interface on port 1
	ADConverter adc;
	Sense sense;
	Compute compute;

public:
	NodeConfiguration() 
		: System()
		, pool(0) //the Buffer is only used as FiFo. So stacksize is 0 @see Buffer
		, out(&pool)
		, serial(B19200)
	{
		out.init(&(serial.input)); //formated output to serial interface

		sense.init(&adc.input);
		compute.init(&out);
		adc.init(&compute.input);

		timer.setGroups(SLEEP | AWAKE ); //timer is part of groups SLEEP and group AWAKE
		serial.receiverHandler.setGroups(NOOPGROUP);
		serial.transmitterHandler.setGroups(NOOPGROUP);

		adc.setGroups( AWAKE );


		powerManager.enableGroup(AWAKE); //this enables all registered entities (obj of type PowerManageAble) which are part of the group SLEEP
		powerManager.disableGroup(SLEEP); //that disables all entities which are part of the group SLEEP and NOT part of group AWAKE.
		// Note: The timer will stay enabled here. Cause it is part of the group AWAKE, which has been enabled.
		out.write("Reflex initialized");
		out.writeln();

	}
	
};

inline
NodeConfiguration& getApplication()
{
	extern NodeConfiguration system;
	return system;
}

} //reflex

#endif

