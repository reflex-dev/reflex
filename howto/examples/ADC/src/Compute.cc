/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
  
 *	Author:		 Karsten Walther
 */
#include "Compute.h"

using namespace reflex;

Compute::Compute() //: timer(ready)
{
	input.init(this);
	counter = 0;
}


void Compute::init(OutputChannel* out)
{
	this->out = out;
}

void Compute::run()
{
	char channel;
    	input.get(channel,values[counter]); //get data from eventbuffer
	counter++;
	if (counter == DATASPACE) 
	{ 
	  	counter = 0;
		uint16 mean = 0;
		out->write("\r\nADC\'s:");
		for(int i = 0; i < DATASPACE; i++) //calculate mean
		{
			out->write(values[i]);
			out->write(":");
			mean += values[i];
		}
		mean = mean / DATASPACE;
		out->write(" MEAN ");	
		out->write(mean);
  		out->writeln();
	}
}

