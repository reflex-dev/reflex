import qbs 1.0
import ReflexPackage

ReflexPackage {
    name: "virtualtimer"
    files: [
        "sources/reflex/timer/VirtualizedTimer.cc",
        "sources/reflex/timer/VirtualTimer.cc"
    ]

    Depends { name : "core" }
    Depends { name : "platform" }

    Group {
        files: "sources/reflex/timer/*.h"
        qbs.install: true
        qbs.installDir: "include/reflex/timer"
    }
}
