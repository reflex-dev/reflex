#ifndef VirtualizedTimer_h
#define VirtualizedTimer_h
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *	Class(es):	VirtualizedTimer
 *
 *	Author:		Stefan Nuernberger
 *
 *	Description:	Class that virtualizes one hardware timer to several
 *			virtual timers.
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 **/

#include "reflex/types.h"
#include "reflex/timer/VTimerBase.h"
#include "reflex/timer/HardwareTimer.h"
//#include "conf.h" // FIXME: current configuration (may override VIRTUAL_TIMERS)

#ifndef VIRTUAL_TIMERS
#define VIRTUAL_TIMERS 16
#endif

namespace reflex {

/**
 * This class virtualizes one hardware timer and provides up to 255
 * instances of VirtualTimer with timer interrupts. The hardware timer
 * is set dynamically according to the next counter instead of generating
 * ticks at a fixed rate.
 */
class VirtualizedTimer : public Sink0 {
public:

	/**
	 * constructor
	 */
	VirtualizedTimer();

	/**
	 * setHardwareTimer
	 * @param timer HardwareTimer to be virtualized
	 */
	void setHardwareTimer(HardwareTimer *timer);

	void registerTimer(VTimerBase *timer);
	void removeTimer(VTimerBase *timer);

	/**
	 * set
	 * update a value for a VirtualTimer
	 */
	void set(VTimerBase *timer);

	/**
	 * run
	 * main method of Activity. Get current time
	 * and update timers accordingly.
	 */
	void notify();

	/**
	 * disableAll
	 * switch off all registered virtual timers
	 * @param prioritized whether prioritized timers are switched off, too
	 */
	void disableAll(bool prioritized = false);

private:

	enum timer_constants {
		MAX_DELTA = (sizeof(Time) < sizeof(int32)) ? (Time) -1 : (1UL << 31) - 1 /* MAX_INT32 */
	};

	VTimerBase *timers[VIRTUAL_TIMERS]; ///< array of virtual timers
	HardwareTimer *hwtimer; ///< hardware timer to virtualize
	uint8 registered; ///< number of registered timers
	bool active; ///< whether at least one timer is currently running

	Time last_update; ///< time of last update
	Time min_remaining; ///< interval time set on last update
};

} // reflex

#endif
