import qbs 1.0
import ReflexPackage

ReflexPackage {
    name: "devices"

    Depends { name : "buffer" }
    Depends { name : "core" }

    files: [
        "sources/reflex/io/EEPROM_24xx512.cc"
    ]

    Group {
        files: "sources/reflex/io/*.h"
        qbs.install: true
        qbs.installDir: "include/reflex/io"
    }
}
