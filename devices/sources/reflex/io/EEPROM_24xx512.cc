/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *	Author:		Stefan Nuernberger
 *
 *	Description: Driver for 512kBit EEPROM 24AA512/24LC512/24FC512
 */

#include "reflex/io/EEPROM_24xx512.h"

namespace reflex {
namespace devices {

/* constructor
 * set default I2C address and stuff...
 */
EEPROM_24xx512::EEPROM_24xx512()
    :readFunc(*this) // init ActivityFunctor for read
    ,writeFunc(*this) // init ActivityFunctor for write
{
    // connect events to activities
    read.init(&readFunc);
    write.init(&writeFunc);

    success = NULL;
    error = NULL;

    // set default address
    // last 3 bits may vary, set correct address in NodeConfiguration if needed
    setAddress(DEFAULT_I2C_ADDRESS);
}

/* read_bytes
 * Read bytes from the EEPROM. This uses random access/sequential read.
 * NOTE: The rwpos automatically wraps around after MAX_ADDRESS has been
 *       reached. Reading continues from 0x0000 in EEPROM address space.
 *
 * @param buf - pointer to buffer where the bytes will be stored
 * @param len - number of bytes to read from device
 */
void EEPROM_24xx512::read_bytes()
{
    unsigned char *buf;
    uint16 len;
    uint16 offset = rseek.getOffset();

    /* get parameters */
    read.get(buf, len);
    if (len == 0) goto err_out;
    // start write (for random offset),
    // use poll to wait for previous data writes to finish
    if (!start_write_poll()) goto err_out;

    // send read offset high byte
    i2c_send((uint8)(offset >> 8));
    if (!i2c_command(CHECK_ACK)) goto err_out;
    // send read offset low byte
    i2c_send((uint8)(offset));
    if (!i2c_command(CHECK_ACK)) goto err_out;
    // (re-)start read operation
    i2c_command(START_READ);
    if (!i2c_command(CHECK_ACK)) goto err_out;

    // read bytes (all but last)
    for (uint16 i = 1; i != len; ++i, ++buf) {
        *buf = i2c_recv(ACK); // read byte
    }
    // last byte is finished with NACK+STOP
    *buf = i2c_recv(STOP);
    // update offset
    rseek.assign(offset + len);

    // success!
    if (success) success->notify();
    return;

err_out:
    // something went wrong, issue STOP and signal error
    i2c_command(STOP);
    if (error) error->notify();
}

/* write_bytes
 * Write bytes into EEPROM. The device can only handle PAGE_SIZE bytes writes
 * in a single sequential write operation, but do not worry because this method
 * is aware of the problem and will handle all the inconvenient stuff for you.
 * Just as with reading, writing will continue at the start of EEPROM memory when
 * the highest byte has been written.
 */
void EEPROM_24xx512::write_bytes()
{
    unsigned char *buf;
    uint16 len;
    uint16 offset;
    /* get parameters */
    write.get(buf, len);

    while (len != 0) {
        // get current write offset
         offset = wseek.getOffset();
        // start WRITE operation, use polling to wait for previous write
        if (!start_write_poll()) {
            // no ACK?! no device? signal error...
            if (error) error->notify();
            return;
        }

        // send write offset high byte
        i2c_send((uint8)(offset >> 8));
        if (!i2c_command(CHECK_ACK)) goto err_out;
        // send write offset low byte
        i2c_send((uint8)(offset));
        if (!i2c_command(CHECK_ACK)) goto err_out;

        // compute number of bytes up to PAGE_SIZE boundary
        uint8 bytes_to_write = (uint8) (PAGE_SIZE - (offset & (PAGE_SIZE - 1)));
        if (len < bytes_to_write) {
            bytes_to_write = (uint8) len;
        }

        // write bytes
        for (uint8 i = 0; i != bytes_to_write; ++i, ++buf) {
            i2c_send(*buf); // write byte
            if (!i2c_command(CHECK_ACK)) goto err_out;
        }
        // STOP triggers memory write on device
        i2c_command(STOP);

        // adjust offset
        wseek.assign(offset + bytes_to_write);
        len -= bytes_to_write;
    }

    // success!
    if (success) success->notify();
    return;

err_out:
    // something went wrong. issue STOP and signal error
    i2c_command(STOP);
    if (error) error->notify();
}

/* start_write_poll
 * implements device acknowledge polling before the next write operation.
 * This maximizes bus throughput as described in the data sheet.
 * NOTE: To avoid deadlocks if there is no memory device, the polling
 *       will stop after a predefined number of tries.
 * @return bool - true if acknowledge received, false otherwise (#tries exceeded)
 */
bool EEPROM_24xx512::start_write_poll()
{
    uint8 tries = 0;
    do {
        i2c_command(START_WRITE);
        if (i2c_command(CHECK_ACK)) {
            return true; // device is ready for next operation
        }
    } while (++tries < MAX_POLL_COUNT);
    // we did not receive an ACK...
    return false;
}

} // ns devices
} // ns reflex

