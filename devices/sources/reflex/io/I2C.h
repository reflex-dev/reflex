/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(es):	 I2C_Master, I2C_Device
 *
 *	Author:	     Stefan Nuernberger
 *
 *	Description: Abstraction of an I2C Bus. These classes should be
 *               used by bus driver implementations and slave drivers.
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and
 *    Operating Systems. All rights reserved.
 *
 *    Redistribution and use in source and binary forms, with or without
 *    modification, are permitted provided that the following conditions
 *    are met:
 *
 *       1. Redistributions of source code must retain the above copyright
 *          notice, this list of conditions and the following disclaimer.
 *
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the
 *          distribution.
 *
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *    The views and conclusions contained in the software and documentation
 *    are those of the authors and should not be interpreted as representing
 *    official policies, either expressed or implied, of BTU Cottbus,
 *    Department for Distributed Systems and Operating Systems.
 *
 **/

#ifndef REFLEX_DEVICES_I2C_H
#define REFLEX_DEVICES_I2C_H

#include "reflex/types.h"
#include "reflex/powerManagement/PowerManageAble.h"

namespace reflex{
namespace devices {

/* i2c_control_t
 * control signals on SDA
 * NOTE: the symbols mark single bits in the control byte. Yet, combinations
 * of bits are not supported.
 */
enum i2c_control_t {
    START_READ = 0x1   /// transmit a (RE)START condition with address and READ flag
    ,START_WRITE = 0x2 /// transmit (RE)START condition with address and WRITE flag
    ,STOP = 0x04       /// transmit a STOP condition (preceded with NACK on i2c_recv)
    ,ACK = 0x08        /// send ACK after received byte
    ,CHECK_ACK = 0x10  /// check for acknowledgement
    ,RESTART_READ = START_READ      /// for convenience
    ,RESTART_WRITE = START_WRITE    /// for convenience
};


/* I2C_Master
 * Interface implemented by I2C master bus drivers
 * Every I2C Master should be PowerManageAble.
 */
class I2C_Master : public PowerManageAble
{
public:
    /* constructor
     * every I2C master is a primary PowerManageAble device
     */
    I2C_Master() : PowerManageAble(PowerManageAble::PRIMARY) { }

    /* i2c_command
     * issue bus control command
     * @param addr, slave address
     * @param cmd, bus command
     * @return bool, true if ack received (only on CHECK_ACK), false else
     */
    virtual bool i2c_command(uint8 addr, i2c_control_t cmd) = 0;

    /* i2c_send
     * send a byte to the device
     * @param byte, the byte to send
     */
    virtual void i2c_send(uint8 byte) = 0;

    /* i2c_recv
     * receive a byte from the device
     * @param addr, slave address for RESTART
     * @param finish, send ACK, (NACK+)STOP, (NACK+RE)[START_WRITE|START_READ]
     * @return uint8, received byte
     */
    virtual uint8 i2c_recv(uint8 addr, i2c_control_t finish) = 0;

    /* features of PowerManageAble */
    virtual void enable() = 0;
    virtual void disable() = 0;
};


/* I2C_Device
 * Base class for I2C device drivers
 */
class I2C_Device
{
public:

    /* constructor */
    I2C_Device()
    {
        master = NULL;
        addr = 0;
    }

    /* setAddress
     * set 7 bit slave address of this device
     */
    void setAddress(uint8 address)
    {
        this->addr = address;
    }

    /* setMaster
     * set the connected master bus driver
     */
    void setMaster(I2C_Master *master)
    {
        if (!master) return;

        this->master = master;
        bool enabled = master->isEnabled();

        if (!enabled) {
            /* master has to be enabled in PM for this to work! */
            master->enable();
            i2c_configure(); // reconfigure device
            master->disable();
        } else {
            i2c_configure(); // reconfigure device
        }
    }

    /* i2c_configure
     * must be implemented by derived class
     * Called after the master has changed.
     * Here the device initialization should happen...
     */
    virtual void i2c_configure() = 0;

    /* i2c_command
     * issue bus control command
     * @return bool - true if ack received (only CHECK_ACK), false else
     */
    bool i2c_command(i2c_control_t cmd)
    {
        if (!master) return false;
        return master->i2c_command(addr, cmd);
    }

    /* i2c_send
     * send a byte to the device
     * @param byte, the byte to send
     */
    void i2c_send(uint8 byte)
    {
        if (!master) return;
        master->i2c_send(byte);
    }

    /* i2c_recv
     * receive a byte from the device
     * @param finish, send ACK, (NACK+)STOP, (NACK+RE)[START_WRITE|START_READ]
     * @return uint8, received byte
     */
    uint8 i2c_recv(i2c_control_t finish)
    {
        if (!master) return 0;
        return master->i2c_recv(addr, finish);
    }

private:
    uint8 addr; /// i2c address of slave device
    I2C_Master *master; /// connected I2C master bus driver
};

} // ns devices
} // ns reflex
#endif // I2C_H
