import qbs 1.0
import ReflexPackage

ReflexPackage {
    name: "buffer"

    Depends { name : "core" }

    cpp.defines : [
        "MAX_POOL_COUNT=" + project.buffer_maxPoolCount,
        "BUFFER_SIZE_TYPE=" + project.buffer_sizeType
    ]

    files: [
        "sources/reflex/**/*.cc"
    ]

    Group {
        files: "sources/reflex/io/*.h"
        qbs.install: true
        qbs.installDir: "include/reflex/io"
    }

    Group {
        files: "sources/reflex/memory/*.h"
        qbs.install: true
        qbs.installDir: "include/reflex/memory"
    }

    Export {
        Depends { name : "cpp" }

        cpp.defines : [
            "MAX_POOL_COUNT=" + project.buffer_maxPoolCount,
            "BUFFER_SIZE_TYPE=" + project.buffer_sizeType
        ]
        cpp.includePaths : "sources"
    }
}
