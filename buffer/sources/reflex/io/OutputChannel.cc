/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
  
 *	Author:		 Karsten Walther
 */
#include "reflex/io/OutputChannel.h"
#include "reflex/interrupts/InterruptLock.h"
#include "reflex/types.h"

using namespace reflex;

static const char *lookup = "0123456789abcdef";

void OutputChannel::init(Sink1<Buffer*>* device)
{
	this->device = device;
}

void OutputChannel::getBuffer()
{
	//create new buffer
	//Seems to be short but leads to code bloat if inlined
	buffer = new (pool) Buffer(pool);
}

void OutputChannel::sendBuffer()
{
    if(buffer)
    {
		if (device)
		{
			device->assign(buffer);
			buffer = 0;
        }
        else
        {
			buffer->downRef();
            // FIXME: why u no "buffer = 0"?
        }
	}
}


void OutputChannel::write(char c)
{
	InterruptLock l;

    if(!buffer)
    {
		getBuffer();
    }

    if(buffer)
    {
		buffer->write(c);
	}
}

void OutputChannel::write(const char* data)
{
	InterruptLock l;
    if(!buffer)
    {
		getBuffer();
    }

    if(buffer)
    {
		char* pos = (char*)data;
        while(*pos != 0)
        {
			buffer->write(*pos++);
		}
	}
}

void OutputChannel::write(const void* data, unsigned size)
{
	InterruptLock l;

    if(!buffer)
    {
		getBuffer();
    }

    if(buffer)
    {
		char* ptr = (char*)buffer->allocData(size);
        if(ptr)
        {
			char* end = ptr + size;
			char* ptrSource = (char*) data;
            while(ptr < end)
            {
				*ptr++ = *ptrSource++;
			}
		}
	}
}

void OutputChannel::write(unsigned data)
{
	char result[MAX_INT_DIGITS];
	char* pos = result;
    do {
		*pos++ = data % 10 + '0';
		data /= 10;
    } while(data);

	InterruptLock l;
    if(!buffer)
    {
		getBuffer();
    }

    if(buffer)
    {
		char* dest = (char*)buffer->allocData(pos-result);
        if(dest)
        {
            while(pos > result)
            {
				*dest++ = *--pos;
			}
		}
	}
}

void OutputChannel::write(unsigned value, char digits)
{
	InterruptLock l;
    if(!buffer)
    {
		getBuffer();
    }

    if(buffer)
    {
		char* start = (char*)buffer->allocData(digits);
        if(start)
        {
			char* ptr = start + digits;
            while(ptr-- > start)
            {
				*ptr = value % 10 + '0';
				value /= 10;
			}
		}
	}
}

void OutputChannel::write(int data)
{
    if(data < 0)
    {
		write('-');
		data = 0 - data;
	}
	write((unsigned)data);
}

void OutputChannel::write(void* address)
{
	InterruptLock l;

    if(!buffer)
    {
		getBuffer();
    }

    if(buffer)
    {
		char* ptr = (char*)buffer->allocData(sizeof(void*) * 2 + 2);
        if(ptr)
        {
			*ptr++ = '0';
			*ptr++ = 'x';
			char* ptr2 = ptr + sizeof(void*) * 2;
			//ptrdiff_t represents an integer with the length of a pointer (e.g 32 Bit vs 64 Bit)
			//yes it's a signed type but thats is not the problem here.
			ptrdiff_t tmp = (ptrdiff_t)address;
            while(ptr2-- > ptr)
            {
				*ptr2 = lookup[tmp & 0xf];
				tmp >>= 4;
			}
		}
	}
}

void OutputChannel::writeln()
{
	InterruptLock l;

    if(!buffer)
    {
		getBuffer();
    }

    if(buffer)
    {
		buffer->write((char)13);
		buffer->write((char)10);
		sendBuffer();
	}
	//getBuffer();
}

void OutputChannel::flush()
{
	InterruptLock l;
	//if(buffer){
	sendBuffer();
	//}
	//getBuffer();
}


