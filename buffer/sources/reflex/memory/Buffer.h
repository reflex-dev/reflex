/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(ses):	Buffer
 *
 *	Author:		Sören Höckner, Karsten Walther, Olaf Krause
 *
 *	Description:	This is a buffer which I/O. Where are two write pointers
 *					associated with it, one for writing data in fifo-style
 *                  (writeOffset - write position) and one for putting header
 *					information in front to the buffer (tosOffset - top of stack).
 *					Both pointers are offsets to the start address of the
 *					available memory and are initialized with the same value.
 *					The tos offset is decremented by the push() operation and
 *					incremented by pop() and read(). The tos offset points to the
 *					last written byte and equals to the predecrement manner. The wpos is increment by
 *                  write() and points after a write() operation to the next byte after
 *					the written data. That means it is used in a postincrement manner.
 *					The initial offset is determined by the the pool, from
 *					which the buffer is allocated.
 *					This way buffers can simply be used by applications without
 *					knowing which network stages a message must pass on its
 *					way. But buffers can be used also in other contexts.
 *
 *					Note that the managed memory is not within the buffer object.
 *					The managed memory is placed directly behind the buffer object.
 *
 *					The layout of the memory managed by a buffer is as follows:
 *
 *					----------------------------------------------------
 *					|    Free   <-|  Header   |      Data    |->       |
 *					----------------------------------------------------
 *                start       tosOffset initialOffset    writeOffset  size
 *                                |---------length---------->|
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */

#ifndef Buffer_h
#define Buffer_h

//#include "conf.h"
#include "reflex/types.h"
#include "reflex/memory/memcpy.h"
#include "reflex/data_types/ChainLink.h"

#include "reflex/debug/Assert.h"

namespace reflex
{

class Pool;

/** Implements basic buffer usages and is chained to a pool.
 *	moreover it is enqueueable and referencecountable
 *
 */
class Buffer
	: public data_types::ChainLink
{
public:
/**
 * The maximum size of a buffer is defined by the internal
 * type Buffer::SizeT and set to uint8 by default.
 * You can override it in the application config file
 * conf.h by adding the following line.
 *
 * \code
 * #define BUFFER_SIZE_TYPE uint16
 * \endcode
 *
 *
 */
#ifdef BUFFER_SIZE_TYPE
		typedef BUFFER_SIZE_TYPE SizeT;
#else
		typedef uint8 SizeT;
#endif

	/** Initialize members.
	 *
	 *  @param pool pointer to the associated pool
	 */
	Buffer(Pool* pool)
	{
		Assert(pool);
		initialize(pool);
	}

	~Buffer()
	{
		this->downRef();
	}


	/** returns the address of the first written byte of the buffer, this
	 *  is either start of the header if already a push operation was performed
	 *  or the address of the first byte in the datasection.
	 *
	 *	@return startaddress of datafield
	 */
	const uint8* getStart() const {return getTOSPos();}
	uint8* getStart() {return getTOSPos();}

	/**	returns the currently utilized buffer length
	 *
	 * 	@return content length
	 */
	SizeT getLength() const;

	/**	returns the (fixed) buffer size.
	 *	buffersize - tosOffset is the free size of the stack part
	 *	buffersize - writeoffset is the free size of the fifo part
	 * 	@return buffer size
	 */
	SizeT getBufferSize() const {return bufferSize;}

	/**
	 * @return returns the available space in the FIFO part of the buffer in bytes
	 */
	SizeT getFreeFIFOSpace()
	{
		// is shouldn't be possible that writeoffset is > bufferSize
		return bufferSize - writeOffset;
	}

	/**
	 * @return returns the available space in the stack part of the buffer in bytes
	 */
	SizeT getFreeStackSpace()
	{
		return tosOffset;
	}


	/** set the offset values tos and wpos to the given value. Is used
	 *  for example for receiving packets from the network, while data
	 *  are written from the beginning.
	 *
	 *  @param offset offsetvalue for tos and wpos
	 */
	void initOffsets(SizeT offset)
	{
		tosOffset = offset;
		writeOffset = offset;
	}

	/** resets the Buffer to its initial state
	 */
	void reset();

	/** duplicate the Buffer.
	 *  The new Buffer becomes a Duplicate.
	 *
	 *  @param  val the new Buffer where the data are copied to
	 */
	void copyTo(Buffer *val) const;

	//////////////////////////////////////////////////////////////////////
	//Methods for buffer reference management
	//////////////////////////////////////////////////////////////////////

	/** reference count increment.
	 *	signals additional reference to the buffer
	 */
	void upRef();

	/** reference count decrement.
	 *	puts buffer back into the related pool, if not referenced anymore
	 */
	void downRef();

	uint8 getPoolId() const
	{
		return this->poolId;
	}

	////////////////////////////////////////////////////////////////////////////
	// Methods for writing and reading the buffer
	////////////////////////////////////////////////////////////////////////////
	//template<typename BufferedT>
	//Buffer& operator <<(const BufferedT& obj) {this->write(obj);return *this;}

	//template<typename BufferedT>
	//Buffer& operator >>(BufferedT& obj) {this->read(obj);return *this;}

	/** puts a character into the buffer behind already written data
	 *
	 *  @param  item object to be stored in
	 *  @return returns true if object has been stored
	 */
	template<typename T>
	bool write(const T& item);
	bool write(const uint8 item);
	void write(const void* data, SizeT size) {writeBuffer<true>(data,size);}


	/** gets the next unread item from the buffer, if
	 *  there is no unread one 0 is returned. destructive read
	 *
	 *	@param	item	read from buffer and stored inside it
	 *	@return	false on empty buffer
	*/
	template<typename T>
	void read(T& item);
	void read(uint8 &item);
	void read(void* data, SizeT size) {readBuffer<true>(data,size);}

	/** pushes data on the header stack.
	 *
	 *  @param  item object to be stored in
	 *  @return returns true if object has been stored
	 */
	template<typename T>
	bool push(const T &item);
	bool push(const uint8 item);
	void push(const void* data, SizeT size) {pushBuffer<true>(data,size);}


	/** removes data from the header stack and stores them in item
	 *
	 *  @param  item object to be stored in
	 *  @return returns true if object has been stored
	 */
	template<typename T>
	void pop(T &item);
	void pop(uint8 &item);


	/** copies data from the header stack
	 *
	 *  @param  item object to be stored in
	 *  @return returns true if object has been stored
	 */
	template<typename T>
	void peek(T &item) const;
	void peek(uint8 &item) const;

	/** Increases the header stack by size bytes
	 *
	 *  @param size bytes to allocate
	 *  @return the start address of the allocated memory
	 */
	void* allocHeader(SizeT size);

	/** Increases the data part in the buffer
	 *
	 *  @param size bytes to allocate
	 *  @return the start address of the allocated memory
	 */
	void* allocData(SizeT size);

public:
	/**
	 */
	uint8* getTOSPos() const{ return const_cast<uint8*>(getReadPos());}
	const uint8* getReadPos() const;
	uint8* getWritePos() const;

protected:
	/* implementation details: cause of the usage of templatefunctions for
	 * writing or reading of data a general method for that job is necessary.
	 * Due to the prevention against code bloat, this method should be located inside
	 * the cc-file. But we've got two implementations of an Buffer in one class
	 * (e.g stack and fifo-semantic). So sometime the push method isn't used when the
	 * buffer is used as an fifo-buffer. The compiler won't generate a template method in
	 * the compilation unit if not referenced. But only if the -frepo switch is used.
	 * The plain signature of those methods looks like "read<true>(const void*,size_t)"
	 * read<false>(...) will not be implemented and shall not be implemented in future.
	 */
	template<bool used>
	void writeBuffer(const void*, SizeT);
	template<bool used>
	void pushBuffer(const void*, SizeT);
	template<bool used>
	void readBuffer(void*, SizeT);

protected:
	friend class PoolManager;

public:
	//a bitset is used to save a byte per buffer and to get an even object size
	unsigned poolId:4;			///<	related pool, it is set	while the constructor is called
	unsigned referenceCount:4;  ///< 	reference counter

	//the offsets these are byte values instead of pointers this saves 3bytes
	//per buffer
	SizeT	tosOffset;	///< points on valid data
	SizeT	writeOffset; ///< points to the nex free space behind the data region
	SizeT	bufferSize; ///< the maximal length of buffer.

	/** Used for initialization of members after construction or reset.
	 *
	 *	@param pool pointer to the associated pool
	 */
	void initialize(Pool *pool);

};//class Buffer

inline
Buffer::SizeT Buffer::getLength() const
{
	return writeOffset - tosOffset;
}

inline
void Buffer::upRef()
{
	referenceCount++;
}

template<bool Used>
void Buffer::writeBuffer(const void* src, SizeT size)
{
//	Assert(writeOffset+size <= bufferSize);
	//caddr_t pos = ((caddr_t)(this+1)) + writeOffset;
	memcpy(getWritePos(),src,size);
	writeOffset += size;
}

template<typename T>
inline
bool Buffer::write(const T& item)
{
	if (writeOffset + sizeof(T) > bufferSize) return false;

	this->write(&item,sizeof(T));
	return true;
}

inline
bool Buffer::write(const uint8 item)
{
//	Assert(writeOffset + 1 <= bufferSize);

//	if (writeOffset + 1 > bufferSize) return false;

	uint8* pos =  getWritePos();
	*pos = item;
	writeOffset += 1;

	return true;
}

template<bool Used>
void Buffer::readBuffer(void* data,SizeT size)
{
//	Assert(tosOffset + size <= writeOffset);

	//	/caddr_t pos = ((caddr_t)(this+1)) + tosOffset;
	const void* pos =  getReadPos();
	memcpy(data,pos,size);
	tosOffset += size;
}

template<typename T>
inline
void Buffer::read(T &item)
{
	this->read(&item, sizeof(T));
}

inline
void Buffer::read(uint8 &item)
{
//	Assert(tosOffset + 1 <= writeOffset);

	//caddr_t pos = ((caddr_t)(this+1)) + tosOffset;
	const uint8* pos = getReadPos();
	item = *pos;
	tosOffset++;
}


template<bool used>
void Buffer::pushBuffer(const void* src,SizeT size)
{
	tosOffset -= size;
	//caddr_t pos = ((caddr_t)(this+1)) + tosOffset;
	uint8* pos = getTOSPos();
	memcpy( pos, src, size);
}


template<typename T>
inline
bool Buffer::push(const T &item)
{
//	Assert(sizeof(T) <= tosOffset);

	if (sizeof(T) > tosOffset) return false;

	push(&item,sizeof(T));
	return true;
}


inline
bool Buffer::push(const uint8 item)
{
//	Assert(1 <= tosOffset);

	if (1 > tosOffset) return false;

	tosOffset--;
	//caddr_t pos = ((caddr_t)(this+1)) + tosOffset;
	uint8* pos = getTOSPos();
	*pos = item;

	return true;
}

template<typename T>
inline
void Buffer::pop(T &item)
{
	read(item);
}


inline
void Buffer::pop(uint8 &item)
{
	//caddr_t pos = ((caddr_t)(this+1)) + tosOffset;
	const uint8* pos = getReadPos();
	item = *pos;
	tosOffset++;
}


template<typename T>
void Buffer::peek(T &item) const
{
	Assert(tosOffset + sizeof(T) <= writeOffset);

	//caddr_t pos = ((caddr_t)(this+1)) + tosOffset;
	memcpy(&item,getReadPos(),sizeof(T));
}


inline
void Buffer::peek(uint8 &item) const
{
	Assert(tosOffset + 1 < writeOffset);

	//caddr_t pos = ((caddr_t)(this+1)) + tosOffset;
	const uint8* pos=getReadPos();
	item = *pos;
}

//protected
inline
const uint8* Buffer::getReadPos() const {
	return reinterpret_cast<const uint8*>(this+1) + tosOffset;
}

inline
uint8* Buffer::getWritePos()  const {
	return 	const_cast<uint8*>(reinterpret_cast<const uint8*>(this+1) + writeOffset);
}

} //namespace reflex

#endif
