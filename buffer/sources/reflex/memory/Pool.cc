/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
  
 *	Author:		 Karsten Walther
 */
#include "reflex/memory/Pool.h"

using namespace reflex;

Pool::Pool()
{
	id = poolManager->registerPool(this);
}

