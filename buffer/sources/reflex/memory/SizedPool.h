#ifndef SizedPool_h
#define SizedPool_h
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(ses):	SizedPool
 *
 *	Author:		Karsten Walther
 *
 *	Description:	A pool object contains all the memory it can manage.
 *					Therefore it uses an array of PoolObjects, these either
 *					contain a buffer object and data space if they are in use
 *					or PoolObjects contain a next pointer if they are free.
 *					This way the PoolObjects can be stored in a FreeList, which
 *					is already a base class of Pool.
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */

#include "reflex/types.h"
#include "reflex/memory/Pool.h"
#include "reflex/debug/Assert.h"
#include "reflex/data_types/ChainLink.h"

namespace reflex {

/** This class reserves bufferspace for a given count of typed elements. If
 *  it is used with objects, new operator must be used for allocation for
 *  proper initialization of objects. Alignment is this of a pointer.
 */
template <Buffer::SizeT elementSize, size_t itemCount>
class SizedPool : public Pool {
	/** Each elements must be able to contain a buffer and its related memory
	 *  or a pointer. Further a poolObject can be used as a freelist element
	 *  inside this sized pool.
	 */
	union PoolObject {
		char item[elementSize + sizeof(Buffer)]; //one element contains Buffer and data
		data_types::ChainLink* next;
	};
public:
	/** Puts all poolObjects into the freelist and initializes the Pool base
	 *  class.
	 */
	SizedPool(const Buffer::SizeT initialOffset);

private:
	/** The bufferspace
	 */
	PoolObject objects[itemCount];
};

template<Buffer::SizeT elementSize, size_t itemCount>
SizedPool<elementSize,itemCount>::SizedPool(const Buffer::SizeT initialOffset)
{
	Assert(elementSize >= initialOffset);

	//put the poolobjects in the freelist ( indirect base class)
	for(size_t i = 0; i <= itemCount-1; i++){
		FreeList::free(&objects[i]);
	}

	//initialize direct base pool
	this->initialOffset = initialOffset;
	this->elementLength = elementSize;
}

} //end namespace

#endif
