/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
  
 *	Author:		 Soeren Hoeckner, Karsten Walther
 */

#include "reflex/data_types/Singleton.h"
#include "reflex/debug/Assert.h"
#include "reflex/memory/Buffer.h"
#include "reflex/memory/Pool.h"
#include "reflex/memory/PoolManager.h"
#include "reflex/memory/memcpy.h"

using namespace reflex;

void Buffer::downRef()
{
	Assert((referenceCount-1) >= 0 );

	referenceCount--;
	if(referenceCount==0){
		data_types::Singleton<PoolManager>()->getPool(poolId)->free(this);
	}
}

void* Buffer::allocHeader(Buffer::SizeT size)
{
	Assert( tosOffset >= size );
	tosOffset -= size;
	return this->getStart();
}

void* Buffer::allocData(Buffer::SizeT size)
{
	Assert( writeOffset + size <= bufferSize );
	uint8* wpos = getWritePos();
	writeOffset += size;
	return wpos;
}


void Buffer::reset()
{
	Pool *pool = data_types::Singleton<PoolManager>()->getPool(poolId);
	this->initialize(pool);
}

void Buffer::copyTo(Buffer *val) const
{
	val->bufferSize = this->bufferSize;
	val->tosOffset = this->tosOffset;
	val->writeOffset = this->writeOffset;
    memcpy(val->getStart(),this->getStart(),this->getLength());
}


void Buffer::initialize(Pool *pool)
{
	referenceCount = 1;
	poolId = pool->id;
	bufferSize = pool->elementLength;
	tosOffset = pool->initialOffset;
	writeOffset = tosOffset;
}

