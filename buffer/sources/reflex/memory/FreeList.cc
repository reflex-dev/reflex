/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
  
 *	Author:		 Karsten Walther
 */
#include "reflex/memory/FreeList.h"

using namespace reflex;
using namespace data_types;

void* FreeList::alloc()
{
	InterruptLock lock;
	ChainLink* tmp = first;
	if(first){
		first = first->next;
	}
	return tmp;
}
