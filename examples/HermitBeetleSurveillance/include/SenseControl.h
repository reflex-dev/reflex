#ifndef SENSECONTROL_H
#define SENSECONTROL_H
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(ses):	SenseControl
 *
 *	Author:		Stefan Nuernberger
 *
 *	Description: This component controls the sampling. It requests
 *               the values from two sensors and returns the result
 *               as a Sample struct.
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */

#include "reflex/types.h"
#include "reflex/scheduling/ActivityFunctor.h"
#include "reflex/sinks/Sink.h"
#include "reflex/sinks/Event.h"
#include "reflex/sinks/SingleValue.h"

#include "definitions.h"

using reflex::ActivityFunctor;
using reflex::SingleValue1;
using reflex::Sink0;
using reflex::Sink1;
using reflex::Event;

class SenseControl {
private:
    Sink0 *innerSensor; // inner sensor reader
    Sink0 *outerSensor; // outer sensor reader
    Sink1<Sample> *output; // subsequent sink

    Sample currentSample; // current sample

public:
    Event sampleSensors; // get sensor values from first sensor
    SingleValue1<SensorValues> innerSensorResult; // first sensor ready, sample second sensor
    SingleValue1<SensorValues> outerSensorResult; // second sensor ready, read all results

    /** constructor
     */
    SenseControl();

    /** init
     *  connect sensors and output
     *  @param innerSensor inner sensor reader
     *  @param outerSensor outer sensor reader
     *  @param output subsequent sink for gathered sample
     */
    void init(Sink0 *innerSensor, Sink0 *outerSensor, Sink1<Sample> *output);

private:
    /** sample first sensor */
    void sampleFirstSensor();

    /** sample second sensor */
    void sampleSecondSensor();

    /** read results */
    void readResults();

    /** Activity Functors for methods */
    ActivityFunctor<SenseControl, &SenseControl::sampleFirstSensor> firstSensorFunctor;
    ActivityFunctor<SenseControl, &SenseControl::sampleSecondSensor> secondSensorFunctor;
    ActivityFunctor<SenseControl, &SenseControl::readResults> readResultsFunctor;
};

#endif // SENSECONTROL_H
