#ifndef SENSORREADER_H
#define SENSORREADER_H
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(ses):	SensorReader
 *
 *	Author:		Stefan Nuernberger
 *
 *	Description: This component commands a hardware sensor (SHTxx) to
 *               sample temperature and humidity when triggered and
 *               returns the result as a SensorValues struct.
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */

#include "reflex/types.h"
#include "reflex/scheduling/ActivityFunctor.h"
#include "reflex/sinks/Sink.h"
#include "reflex/sinks/SingleValue.h"

#include "definitions.h"

using reflex::Sink0;
using reflex::Sink1;
using reflex::SingleValue1;
using reflex::ActivityFunctor;

class SensorReader : public Sink0 {
private:
    Sink1<SensorValues> *output; // subsequent output for sensor results
    Sink1<uint8> *sensor; // hardware sensor command input

    SensorValues current; // last received values

    /** private class to buffer a sensor result.
     *      *  used for temperature result, since SingleValue won't accept
     *           *  values if no activity is registered
     *                */
    class ResultBuffer : public Sink1<uint16> {	    
	uint16 result;
    public:
	void assign(uint16 value) { result = value; }
	uint16 get() { return result; }
	void get(uint16& value) { value = result; }
    };

public:
    ResultBuffer temperature; // sensor temperature result input
    SingleValue1<uint16> humidity; // sensor humidity result input

    /** constructor */
    SensorReader();

    /** init
     *  connect outputs of component
     *  @param sensor the hardware sensor (SHTxx)
     *  @param output subsequent output for sensor result
     */
    void init(Sink1<uint8> *sensor, Sink1<SensorValues> *output);

    /** notify
     * implements Sink0
     * triggers sensor reading
     */
    void notify();

private:

    /** readResults
     *  read the values from temperature and humidity input and copy to
     *  subsequent sink.
     */
    void readResults();

    /** NOTE: the temperature input does not trigger an activity. We always request
     *  temperature and humidity. The sensor driver queue ensures that the temperature
     *  result is available before the humidity result (FCFS order of requests). So
     *  we read both results when the humidity result is available.
     */
    ActivityFunctor<SensorReader, &SensorReader::readResults> readResultFunctor;
};

#endif // SENSORREADER_H
