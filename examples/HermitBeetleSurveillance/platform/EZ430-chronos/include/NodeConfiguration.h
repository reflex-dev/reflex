#ifndef NodeConfiguration_h
#define NodeConfiguration_h
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(ses):	 Application
 *	Author:		 Karsten Walther
 *
 *	Description: Implements the System
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */

#include "reflex/types.h"
#include "reflex/io/Ports.h"
#include "reflex/io/SHT7x.h"
#include "reflex/io/Button.h"
#include "reflex/io/Display.h"
#include "reflex/sinks/Sink.h"

#include "reflex/System.h"
#include "SenseControl.h"
#include "SensorReader.h"

using namespace reflex;

enum PowerGroups {
 	DEFAULT = reflex::PowerManager::GROUP1,
 	DISABLED = reflex::PowerManager::NOTHING
};

class NodeConfiguration : public System {
public:

    /* typedefs for the connected sensors (pin definition) */
    typedef SHT7x<mcu::Port2, buttons::M1, buttons::S1, buttons::S2> InnerSensor;
    typedef SHT7x<mcu::Port2, buttons::M1, buttons::S1, buttons::M2> OuterSensor;

    /** ValueDisplay
     *  a transparent interceptor of uint16 (sensor readings) that
     *  prints the value on the EZ430 Display (upper line or lower line)
     */
    template<typename TDisplayLine>
    class ValueDisplay : public Sink1<uint16> {
        Sink1<uint16> *consumer;
        TDisplayLine *display;
    public:
        /** init */
        void init(Sink1<uint16> *con, TDisplayLine* dis) {
            consumer = con;
            display = dis;
        }

        /** assign (sensor reading interceptor) */
        void assign(uint16 val) {
            if (display)
                *display = val;
            if (consumer)
                consumer->assign(val); // propagate result
        }
    };

    NodeConfiguration() : System()
	{
        // connect inner sensor to sensorReader; pass temperature through display
        innerSensor.init(&innerSensorDisplay, &innerSensorReader.humidity);
        innerSensorDisplay.init(&innerSensorReader.temperature, &display.getUpperLine());
        innerSensorReader.init(&innerSensor, &senseControl.innerSensorResult);

        // connect outer sensor to sensorReader; pass temperature through display
        outerSensor.init(&outerSensorDisplay, &outerSensorReader.humidity);
        outerSensorDisplay.init(&outerSensorReader.temperature, &display.getLowerLine());
        outerSensorReader.init(&outerSensor, &senseControl.outerSensorResult);

        // connect senseControl outputs (no output for sample)
        senseControl.init(&innerSensorReader, &outerSensorReader, NULL);

        // connect button to trigger sensor reading
        button.init(&senseControl.sampleSensors);

        // assign groups for primary powermanageable devices
        timer.setGroups(DEFAULT); // put system timer in default group
        button.setGroups(DEFAULT); // put button in default group
        display.setGroups(DEFAULT); // put LCD display in default group.
        // sensors are secondary (self managed) and do not need a power group

        // set initial power group, system timer is started implicitely here
		powerManager.enableGroup(DEFAULT); // enable all entities in default group
		powerManager.disableGroup(~DEFAULT); // disable all others
	}

private:

    /* sense */
    SenseControl senseControl; // controller for the sense operation
    InnerSensor innerSensor; // SHT7x temperature sensor (inside)
    OuterSensor outerSensor; // SHT7x temperature sensor (outside)
    SensorReader innerSensorReader; // reader for values of inner sensor
    SensorReader outerSensorReader; // reader for values of outer sensor

    /* send */

    /* debug output */
    Display display; // the EZ430 LCD Display
    ValueDisplay<display::UpperLine> innerSensorDisplay; // display for inner sensor temperature
    ValueDisplay<display::LowerLine> outerSensorDisplay; // display for outer sensor temperature

    /* others */
    Button<buttons::BL, 1000> button; // button that triggers sensor reading (debounced 1sec)
};

inline NodeConfiguration& getApplication() 
{
	extern NodeConfiguration system;
	return system;
}

#endif
