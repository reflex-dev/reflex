/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *	Class(ses):	 SenseControl
 *	Author:		 Stefan Nuernberger
 */

#include "SenseControl.h"

using namespace reflex;

/** constructor
 */
SenseControl::SenseControl() :
        firstSensorFunctor(*this),
        secondSensorFunctor(*this),
        readResultsFunctor(*this)
{
    sampleSensors.init(&firstSensorFunctor); // triggers reading first sensor
    innerSensorResult.init(&secondSensorFunctor); // triggers reading second sensor
    outerSensorResult.init(&readResultsFunctor); // triggers reading all results
}

/** init
 *  connect sensors and output
 *  @param innerSensor inner sensor reader
 *  @param outerSensor outer sensor reader
 *  @param output subsequent sink for gathered sample
 */
void SenseControl::init(Sink0 *innerSensor, Sink0 *outerSensor, Sink1<Sample> *output) {
    this->innerSensor = innerSensor;
    this->outerSensor = outerSensor;
    this->output = output;
}

/** sample first sensor */
void SenseControl::sampleFirstSensor() {
    innerSensor->notify();
}

/** sample second sensor */
void SenseControl::sampleSecondSensor() {
    outerSensor->notify();
}

/** read results */
void SenseControl::readResults() {
    currentSample.sensor1 = innerSensorResult.get();
    currentSample.sensor2 = outerSensorResult.get();

    // get current time (or let another component fill it in)
    currentSample.month = 0;
    currentSample.day = 0;
    currentSample.hour = 0;
    currentSample.minute = 0;

    if (output)
        output->assign(currentSample);
}
