#ifndef COMMAND_H
#define COMMAND_H
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(ses): ---
 *	Author:		Richard Weickelt <richard@weickelt.de>
 *                      Andre Sieber <as@informatik.tu-cottbus.de>
 *
 *	Description: Commands for component manager and linker
 *
 *    This file is part of REFLEX.
 *
 *    REFLEX is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as
 *    published by the Free Software Foundation, either version 3 of the
 *    License, or (at your option) any later version.
 *
 *    REFLEX is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with REFLEX.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace reflex
{

enum Command
{
	RECEIVE = 1,
	INSTALL = 2,
	UNINSTALL = 3,
	STARTUP = 4,
	SHUTDOWN = 5,
	LIST = 6,
	STARTUP_ALL = 7,
	RECEIVE_HEADER = 8,
	RECEIVE_DATA = 9,
	ABORT_INSTALL = 10
};

}
#endif
