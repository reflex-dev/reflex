/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 */
#include "msgInterpreter.h"
#include "NodeConfiguration.h"

using namespace reflex;

msgInterpreter::msgInterpreter(Pool& pool)
	: timeoutFunctor(*this)
	, timer(VirtualTimer::PERIODIC)
	, pool(pool)
{
	timeoutEvent.init(&timeoutFunctor);
	input.init(this);
	timer.init(timeoutEvent);

	timer.set(1000);

//	numberOfwrittenData = 0;  
//	Display::LowerLine& lLine= getApplication().display.getLowerLine();
//	lLine=(uint16) numberOfwrittenData;


   
}


void msgInterpreter::init(Sink1<Buffer*>* application, Sink1<Buffer*>* radio, Sink1<Buffer*>* componentManager)
{
	this->application = application;
	this->radio = radio;
	this->componentManager = componentManager;
}


void msgInterpreter::timeout()
{
	getApplication().display.getSymbols().toggle(display::SEG_L2_ONE);
}


void msgInterpreter::run()
{
  	Buffer* msgData = input.get();
	uint8 type;

	msgData->read(type);
	if (type == 0x11)
	  {
	    
	    componentManager->assign(msgData);

	  }
}