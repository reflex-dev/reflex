/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 */
#include "radioDelay.h"
#include "NodeConfiguration.h"

using namespace reflex;

radioDelay::radioDelay()
        : delayFunctor(*this)
	, delayTimer(VirtualTimer::ONESHOT)
	, pool(pool)
{
	delayEvent.init(&delayFunctor);
	input.init(this);
	delayTimer.init(delayEvent);

	isDelaying = false;

}


void radioDelay::init(Sink1<Buffer*>* radio, uint16 delayTime)
{
	this->radio = radio;
	this->delayTime = delayTime;
}



void radioDelay::run()
{

	Buffer* radioData = input.get();
	Sink1<Buffer*>* delayer;
	delayer = &delayInput;
	delayer->assign(radioData);
	if (!isDelaying) {
		isDelaying = true;
		delayTimer.set(delayTime);
	}
}




void radioDelay::delayDone()
{

	Buffer* delayedData = delayInput.get();
	if (delayedData) {
		radio->assign(delayedData);
		delayTimer.set(delayTime);
	} else {
		isDelaying = false;
	}

}
