/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *

 *	Author:		 Karsten Walther
 */
#include "Complex.h"
#include "NodeConfiguration.h"
#include "conf.h"



using namespace reflex;


Complex::Complex(Pool& pool): 
  timer(VirtualTimer::ONESHOT)
  , pool(pool)
  , sendFunctor(*this)
{
  ready.init(this);
  timer.init(ready);
  timer.set(5000);
  ADCin.init(&sendFunctor);
}

void Complex::init(reflex::Sink1<reflex::mcu::ADC12_A::Request>* ADCout, reflex::Sink1<reflex::Buffer*>* RADIOout)	
{
  this->ADCout = ADCout;
  this->RADIOout = RADIOout;
}

void Complex::run()
{
  timer.set(1000);
  mcu::ADC12_A::Request request(ADC12_A::Ref_Vref_AVss,ADC12_A::InCh_AVccDIV,ADC12_A::CH1);
  ADCout->assign(request);
}

void Complex::send()
{
  uint16 adcVal =  (ADCin.get()<<2)/41;
  Buffer* buffer = new(&pool)  Buffer(&pool); // get a new buffer
  buffer->write(adcVal);
  RADIOout->assign(buffer);
}

/**
 * Connect this component to others and start it.
 */
void* startup()
{
  Complex* comp = new (getApplication().memoryManager) Complex(getApplication().pool);
  comp->init(&getApplication().adc.input, &getApplication().radioDelayer.input);
  getApplication().adc.init(&comp->ADCin);
  return (void*) comp;
}

/**
 * Disconnect this component from system
 */
bool shutdown(void* addr)
{
  Complex* comp = (Complex*) addr;
  comp->timer.set(0);
  getApplication().adc.init(0);
  if (comp->triggered())
    return false;
  if (comp->sendFunctor.triggered())
    return false;
  MemoryManager::freeRam(addr);
  delete (getApplication().memoryManager, comp);
  return true;
}
