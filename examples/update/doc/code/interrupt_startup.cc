void* startup()
{
	InterruptTest* comp = new (getApplication().memoryManager) InterruptTest();
	comp->enable();
	return (void*) comp;
}