bool shutdown(void* addr)
{
	Complex* comp = (Complex*) addr;
	comp->timer.set(0);
	getApplication().adc.init(0);
	if (comp->triggered())
		return false;
	if (comp->sendFunctor.triggered())
		return false;
	MemoryManager::freeRam(addr);
	delete (getApplication().memoryManager, comp);
	return true;
}
