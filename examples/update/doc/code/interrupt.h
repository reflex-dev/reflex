class InterruptTest : public reflex::InterruptHandler
{
public:	
	InterruptTest();
	
	void removeInterrupt();
	
	void enable();
	void disable();
	
protected:
	virtual void handle();
	
	uint16 numberOfInterrupts;
};

void* startup();
bool shutdown(void*);