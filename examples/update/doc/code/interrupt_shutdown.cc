bool shutdown(void* addr)
{
	InterruptTest* comp = (InterruptTest*) addr;
	comp->removeInterrupt();
	MemoryManager::freeRam(addr);
	delete (getApplication().memoryManager, comp);
	return true;
}