class Complex : public reflex::Activity
{
public:
	Complex(reflex::Pool& pool);
	
	virtual void run();
	
	void init(reflex::Sink1<reflex::mcu::ADC12_A::Request>* ADCout,
			  reflex::Sink1<reflex::Buffer*>* RADIOout);
		
	void send();
	reflex::ActivityFunctor<Complex,&Complex::send> sendFunctor;
	
	reflex::SingleValue1<uint16> ADCin;
	reflex::VirtualTimer timer;	
protected:
	reflex::Event ready;
	reflex::Sink1<reflex::mcu::ADC12_A::Request>* ADCout;
	reflex::Sink1<reflex::Buffer*>* RADIOout;
	reflex::Pool& pool;
};

void* startup();

bool shutdown(void*);