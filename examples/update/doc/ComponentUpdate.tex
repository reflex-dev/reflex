%
%
% REFLEX - Real-time Event FLow EXecutive
%
% A lightweight operating system for deeply embedded systems.
%

% Author:	Andre Sieber
%
% Description: LaTex file for the Reflex componnet update
%
%%%%%%%%%%%%%%%%% Formatierungen %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\documentclass[10pt,oneside,english]{scrartcl}
\usepackage{multicol}
\setlength{\columnseprule}{0.3pt}
\setlength{\columnsep}{1cm}
\usepackage{rotating}


%\usepackage[pdftex]{graphics}
\usepackage{graphicx}
\usepackage{listings,color}   %% color für Farbmarkierungen
   \lstset{language=C++}
\usepackage[american,english]{babel}
\usepackage[latin1]{inputenc} %%direkte eingabe von umlauten
%\usepackage{hyperref} %% erstellt PDF-Inhaltsverzeichnis
\usepackage{varioref}
\usepackage{float}
\usepackage{subfigure}
\usepackage{placeins}


\usepackage[pdftex, bookmarks = true]{hyperref} %% PDF- und HTML-Funktionen
\hypersetup{
colorlinks = true,
linkcolor = black,
citecolor = black,
urlcolor = black,
pdfstartview = FitV,
pdfauthor = {},
pdftitle = {},
pdfsubject = {},
pdfkeywords = {},
pdfcreator = {},
pdfproducer = {}
}


%\typearea[current]{last}

\title{The {\sc Reflex} Component Update Manual}
\author{Andr\'{e} Sieber}

%\makeindex

%%%%%%%%%%%%%%%%% Das Dokument %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}
\linespread{1.2}

\lstset{% general command to set parameter(s)
basicstyle=\scriptsize\ttfamily, % print whole listing small
keywordstyle=\color{blue}\bfseries,
identifierstyle=\color{black}, % nothing happens
commentstyle=\color{green}, % white comments
stringstyle=\color{red}, % typewriter type for strings
showstringspaces=false,
frame=topline|bottomline,
backgroundcolor=\color{white},  % no special string spaces
captionpos=b,
tabsize=4,
aboveskip=10pt,
belowskip=10pt,
numbers=left,
float
}



\floatplacement{figure}{htb}
\floatplacement{lstlisting}{htb}



\maketitle

\tableofcontents

\section{Introduction}

This document is an introduction of the component update for the {\sc Reflex} ({\bf R}eal-Time {\bf E}vent {\bf FL}ow {\bf EX}ecutive) operating system for embedded systems.
Starting with two simple examples, the characteristics of the {\sc Reflex} wireless update approach are explained. Both examples are available in the Repository within the examples folder.

\vspace{0.5cm}
%Since updates in productive wireless sensor network environments mostly make only small changes or add additionally functionality, replacing the whole program image have high costs in terms of bandwidth and energy. 
Updates in productive wireless sensor network environments mostly make only small changes or add additionally functionality, thus replacing the whole program image have high costs in terms of bandwidth and energy. To reduce costs this update mechanism works on the scale of components. The update mechanism is capable of installing, running and uninstalling  pre-linked {\sc Reflex} components on a sensor node. It consists of two parts, the {\sc ComponentManager} and the {\sc MemoryManager}. The first reacts to commands and receives, starts, stops and removes components. The second is responsible for memory handling in RAM and FLASH.

\vspace{0.5cm}

The messages of the update system are intended to be delivered through the sensor node network using the application mac/routing mechanisms and hence the system does not implement such methods. In the case of single hop communication without mac/routing the {\sc ComponentManager} is capable of sending acknowledgments for each command and implements a basic duplicate suppression for the component code. In general there are two communication scenarios intended. Delivering components to a singe node using unicast mechanisms of the network and multicast to reach the whole network.  


The overall workflow of the component update is summed up in figure \ref{fig::workflow}.

\begin{figure}[hbt]
         \centering
         \includegraphics[width=0.8\textwidth]{figures/workflow.pdf}
         \caption{{\sc Reflex} Component Update Workflow}
         \label{fig::workflow}
 \end{figure}

\section{Example}

The first example consists of a simple component that interacts with the event-flow and includes two activities. The second example registers an interrupt handler.

Listing \ref{list::structure} shows the directory structure of the example and all other relevant files of the component update.

\lstinputlisting[language=c++,
caption={Directory Structure of Component Update system and example}
,label=list::structure]
{code/structure.txt}

\subsection{Compiling}


\subsection{Example One - Component-integration into the event-flow}

In this example a component is sampling a value using the ADC on the node and sends the result to another component, e.g. the radio. Listing \ref{list::sense_send_h} shows the structure of the component.
Besides the {\it startup()} and {\it shutdown()} methods (lines 47 and 56) the component does not differ from any other {\sc Reflex} components. These methods are used to start or stop the component and are called by the {\sc ComponentManager}. 

\lstinputlisting[language=c++,
caption={Example Component Structure}
,label=list::sense_send_h]
{code/sense_send.h}

The startup code of the component is shown in listing \ref{list::sense_startup_cc}. In Line 3 the component object is created with the  {\sc MemoryManager} {\it new()} and could be accessed with created the {\it comp} pointer. After that the {\it init()} method of the component is called to connect the outputs of the component to other components in the event-flow (in this example the ADC and radio). Then the ADC {\it init()} method is called to connect its output to the example component input (line 7). Last, the pointer to the created object is returned to the {\sc ComponentManager}.

\lstinputlisting[language=c++,
caption={Example Component Startup Method}
,label=list::sense_startup_cc]
{code/sense_startup.cc}


The shutdown code of the component is shown in listing  \ref{list::sense_shutdown_cc}. Using the passed pointer address a pointer to the example component object is created in line 3. 
Then the connections to the component are removed to prevent the components activities from being scheduled again. 
If one of the activities is already scheduled, the {\it shutdown()} method must be aborted (lines 6-10). In this case the 
{\it shutdown()} has to be scheduled again after the components activities where executed and are not scheduled anymore. 
If the component is not scheduled, the used space can be freed (line 10 and 11).

\lstinputlisting[language=c++,
caption={Example Component Shutdown Method}
,label=list::sense_shutdown_cc]
{code/sense_shutdown.cc}

\subsection{Compiling}

To build the component it has to be integrated into the applications Sources.mk first. Listing \ref{list::sense_sources_mk} shows an example. In lines 6 and 7 the sources needed by the component are defined. In this example only the component code itself is needed. The naming scheme is always CC\_SOURCES\_COMPONENT\_TYPE\_NAME with TYPE specifying the type (and thus the location) of source (APPLICATION, PLATFORM, CONTROLLER or LIB) and NAME the desired component name. 

\lstinputlisting[language=make,
caption={Example Component Sources.mk for the Application}
,label=list::sense_sources_mk]
{code/sense_Sources.mk}

Next the base application (for this example the UpdateTest-{\sc Reflex} application) must be built and all symbols extracted to be able to compile and link components for this system. To be able to handle components the application must contain and connect  the  {\sc ComponentManager} and the {\sc MemoryManager}. 
The application is built as usual with the {\it make} command but with the {\it COMPONENT\_UPDATE=1} parameter. 
After the application was successfully compiled, the symbols can be extracted with {\it make kernel\_symbols}. This uses the linker and a php script to extract the symbols into {\it UpdateTest.sym.filtered.txt} which is used to link the component. Without this file no components can be linked for this application. 
The component {\sc COMPLEX} is build with {\it make COMPLEX TEXT=0xF000} and linked to address 0xF000 which is located within the flash memory\footnote{Note that flash memory is asymmetric. Writing a bit is only possible in one direction 1 to 0, resetting a bit to 1 is only possible with an erase cycle which affects a whole segment. It is advised to separate components to individual segments to prevent failures.}. To be able to start and stop the component the locations of the {\it startup()} and {\it shutdown()} methods must be known. This can be achieved by using the toolchains {\it nm} command.  In this example the location of the {\it startup()} method is 0xf168 and the {\it shutdown()} method is located at the address 0xf050.

\lstinputlisting[language=bash]
{code/comandline.txt}

The component is now ready to be uploaded to a node running the UpdateTest application.

\subsection{Remote Programming}

The {\sc ComponentManager} is controlled by various commands (see section \ref{commandos}). The tools directory in the UpdateTest/platform/EZ430Chronos/ contains example python scripts \footnote{python version 2.5.x, the upload script requires the intelhex package, see http://bialix.com/intelhex/} to demonstrate the interaction with an on-node {\sc ComponentManager} using a modified EZ430-Chronos wireless access point. 
Listing \ref{list::upload} shows an example upload command and the resulting output. Besides the component hex file and the startup and shutdown addresses it is necessary to specify several flags and the component ID.  

\begin{itemize}
\item The persistent  flag defines if a component is located in ROM, or volatile in RAM. 
\item The autostart flag specifies if the component should start automatically after installation and at system startup. 
\item The acknowledgement flag defines if acknowledgements for the individual data packets should be sent by the node. 
\end{itemize}

\lstinputlisting[language=bash,
caption={Example Upload of a Component}
,label=list::upload]
{code/uploadCMD.txt}

\subsection{Example Two - Interrupts}
The structure of {\sc Reflex} allows the registration of interrupt handlers at any time, which can be used to install components containing interrupt processing, e.g. device drivers. In this example an interrupt handler for {\it Port 2} of the EZ430-Chronos which displays the number of interrupts on the integrated display is packed as component. In listing \ref{list::interrupt_h} the structure of the example component is shown. The component derivates from {\it InterruptHandler} and thus needs {\it enable()}, {\it disable()} and the {\it handle()}-method. Additionally, the already known  {\it startup()} and {\it shutdown()}-methods are needed. Furthermore, the component needs a method (line 6) to unregister the interrupt handler in the system when the component is stopped and/or uninstalled. The registration of the interrupt within the {\it InterruptGuardian} is done automatically during the component object is creation.

\lstinputlisting[language=c++,
caption={Example Interrupt Component Header}
,label=list::interrupt_h]
{code/interrupt.h}

The {\it startup()}-method is shown in listing \ref{list::interrupt_startup_cc}. It creates the object (line 3) and then enables the interrupt (line 4). 

\lstinputlisting[language=c++,
caption={Example Interrupt Component startup()-Method}
,label=list::interrupt_startup_cc]
{code/interrupt_startup.cc}

To stop the component it is necessary to disable the interrupt and unregister the handler from the system (listing  \ref{list::interrupt_shutdown_cc}) . This is done by calling the corresponding method of the object (line 4). Since the component has no activity it can not already be scheduled and is removed from memory after the interrupt was unregistered and deactivated.

\lstinputlisting[language=c++,
caption={Example Interrupt Component shutdown()-Method}
,label=list::interrupt_shutdown_cc]
{code/interrupt_shutdown.cc}

The method which is responsible for removing the interrupt handler from the system in shown in listing \ref{list::interrupt_remove_cc}. It first disables the interrupt by calling the {\it disable()}-method. Then overwrites the entry of the handler within the InterruptGuardian with the default invalidHandler.

\lstinputlisting[language=c++,
caption={Example Interrupt Component removeInterrupt()-Method}
,label=list::interrupt_remove_cc]
{code/interrupt_remove.cc}

\section{Internals}
As result of the availability of the system symbols via extraction while building the base system, it is possible to compile and link components later, and run their activities and interrupt handler even after the nodes are deployed. 
The memory layout  (figure \ref{fig::memory}) shows the created structure of the {\sc Reflex} component update. Following the {\sc Reflex}-kernel and base application the information of every permanently installed component is stored. This information (called header, see table \ref{header}) is necessary to identify and manage components. Each component has a unique ID(at least for this application) and the addresses of the {\it startup()} and {\it shutdown()}-methods. As described before, these methods are used to construct the component object and integrate it to the {\sc Reflex} event flow, or remove it from the event flow and RAM. Above the header space the components are located. Since the flash memory is asymmetric, writing a bit is only possible in one direction and resetting needs an erase cycle which affects a whole segment. It is advised to separate components to individual segments to prevent failures due to collateral partial removing components.


%The overall idea of the {\sc Reflex} component update is shown in figure \ref{fig::memory} by the memory layout of the system. Following the {\sc Reflex}-kernel and base application the information of every permanently installed component are stored. These informations (called header) contain the component ID, the info byte, the size and the addresses of the {\it startup()} and {\it shutdown()}-methods. 
\begin{table}[htdp]
\caption{Component Header}
\begin{center}
\begin{tabular}{|c|c|l|}
\hline
Item & Type & Short Description\\
\hline
\hline
id & uint16 & component ID \\
info & uint16 & flags how to treat the component \\
startup & void* & location of  startup method \\
shutdown & void* & location of shutdown method \\
file & caddr\_t & address of component \\
size & uint16 & size of component's binary data \\
\hline
\end{tabular}
\end{center}
\label{header}
\end{table}



 \begin{figure}[hbt!]
         \centering
         \includegraphics[width=1\textwidth]{figures/memory.pdf}
         \caption{{\sc Reflex} Component Update Memory Layout}
         \label{fig::memory}
 \end{figure}

The  {\sc Reflex} component update can be divided into platform dependent and platform independent parts. The whole component management logic is platform independent, while the memory management depends on the used microcontroller.

\subsection{Platform independent Parts}

The {\sc ComponentManager} (located in lib/include/reflex/componentUpdate/ComponentManager.h) reacts to commands according to its state machine (shown in figure \ref{fig::statemachine}). The commands and their parameters are shown in table \ref{commands}. 

Some commands may influence the internal state. Most commands execute within the {\it Idle}-state. It is only left when a component should be installed. Except the component installation process, all commands are idempotent and thus not affected by message duplicates.
To install a component the {\it command receive})-command is used. The next  command has to be the  {\it receive\_header}-command to reach the  {\it Receive Component}-state. If the component header shows that it could not be installed (e.g. because of the target memory location is  already occupied) the state changes to  {\it Error} which only could be left with the  {\it abort\_install command}.  Within the {\it Receive Component}-state the data attached to the commands is written to the specified location. Commands other than {\it receive\_data} or {\it abort\_install} will be ignored. The {\sc ComponentManager} checks every component packet if the data was already written and ignores them if so, to prevent a wrong byte count in case of message duplicates. If all data was received (the byte count is equal to the component size) the component is installed.

\vspace{0.5cm}

To connect the {\sc ComponentManager}  the packet input is available through the  {\it get\_packet\_input()}-method. The sink of type  {\it Buffer*} triggers an activity. Commands then trigger an internal activity which handles them asynchronous to increase the reactivity of the system.

\begin{figure}[hbt]
         \centering
         \includegraphics[width=0.5\textwidth]{figures/statemachine.pdf}
         \caption{ComponentManager state machine}
         \label{fig::statemachine}
 \end{figure}

The {\it system/ComponentMake.mk} contains the commands needed by {\sc make} to build the components, including the extraction of the symbols using a simple perl script (utils/componentUpdate/extractSymbolsFromELF.pl). 

\subsection{Platform dependent Parts}
 
The main platform dependent part is the  {\sc MemoryManager} (e.g. located in controller/MSP430X/include/reflex/flash/MemoryManager.h) and contain the memory handling for the components of a platform. It consists mainly of methods for allocation and freeing memory within the flash and RAM of the micro controller. The {\sc MemoryManager} implements a FLASH memory driver including methods for writing and erasing parts of the memory.
Within the {\sc MemoryManager}, assumptions about the structure of the RAM are made. An important is the {\it MM\_MAX\_STACKSIZE}, which plays a role when RAM is allocated. 
Additionally, the platform {\sc Linkerscript} has to be modified.  Markings for the header space must be placed. For example a specific {\sc Linkerscript}  could be written and used when the component update mechanism is used, e.g. using the make system.
 

\subsection{{\sc ComponentManager} Commands}
\label{commandos}

The commands and their responses are shown in table \ref{commands}. The responses are listed in table \ref{responses}. Commands issued at the wrong state will trigger different responses. Within the {\it Error}-state only the {\it abort\_install}-command is accepted, every other command is responded by {\it ERROR\_STATE}. While receiving data  commands other than {\it receive\_data} or {\it abort\_install} will result in a {\it  COMMAND\_INVALID\_RECEIVE}-response.

\vspace{0.5cm}

%The header data structure (shown in table \ref{header}) is used as parameter for the {\it receive\_header} command. Additionally, this structure is returned for each component (with added uint8 if the component is currently running) when the {\it list}-command is issued. The first message returned after that command contains the  number of installed components and the begin and end of the component header space.
The header data structure (shown in table \ref{header}) is used as parameter for the {\it receive\_header} command. Additionally, this structure is returned for each component  when the {\it list}-command is issued. 

\begin{sidewaystable}[ht]
\caption{{\sc ComponentManager} Commands}
\begin{small}
\begin{tabular}{|c|c|c|c|l|l|}
\hline
Command & Opcode & Argument & Argument Type & Short Description & responce code\\
\hline
\hline
receive & 0x01 & send packet ACK & uint16 & initialize component installation &  \\
\hline
install & 0x02 & - & - 				        & installes received component & 1, 3, 4, 5, 249, 253\\
&&&                                                                 & - only internally used & \\
\hline
uninstall & 0x03 & component ID & uint16     & stops component and removes & 11, 12, 249, 253\\
&&&                                                                 & it from node &\\
\hline
startup  & 0x04 & component ID & uint16       & starts component if not running & 6, 7, 252, 249, 253\\
\hline
shutdown & 0x05 & component ID & uint16   & stops component if running & 8, 9, 249, 252, 253\\
\hline
list & 0x06 & -& -                                                   & sends number of installed components,  & 10, 249\\
&&&                                                                 & begin and end of the component header space &\\
&&&                                                                 & and &\\
&&&                                                                 & header and the running status (as uint8) of &\\
&&&                                                                 & each component in individual messages &\\

\hline
startup\_all & 0x07 & - & -                                   & starts all persistent components &\\
\hline
receive\_header & 0x08 & header & headersize & allocate memory for component &2, 3, 248, 249. 251\\
&&&                                                                 & and prepers component receprion & \\
\hline
receive\_data & 0x09 & adress \& data bytes  & uint16 + x & stores x data bytes at given & 13, 25, 247 \\
&&&                                                                 & address, issue installation if all data received &\\
\hline
abort\_install & 0x0A & - & -                               & clean up component memory from & 1, 4, 251\\
&&&                                                                 &partial data, return to IDLE state & \\
\hline
\end{tabular}
\end{small}
\label{commands}
\end{sidewaystable}%

%
%\begin{sidewaystable}[htdp]
%\caption{{\sc ComponentManager} Commands}
%\begin{small}
%\begin{tabular}{|c|c|c|c|l|}
%\hline
%Command & Opcode & Argument & Argument Type & Short Description\\
%\hline
%\hline
%receive & 1 & send packet ACK & uint16 & initialize component installation\\
%\hline
%install & 2 & - & - 				        & installes received component \\
%&&&                                                                 & - only internally used\\
%\hline
%uninstall & 3 & component ID & uint16     & stops component and removes \\
%&&&                                                                 & it from node\\
%\hline
%startup  & 4 & component ID & uint16       & starts component if not running \\
%\hline
%shutdown & 5 & component ID & uint16   & stops component if running \\
%\hline
%list & 6 & -& -                                                   & sends number of installed components and \\
%&&&                                                                 & header of each in individual messages\\
%\hline
%startup\_all & 7 & - & -                                   & starts all persistent components\\
%\hline
%receive\_header & 8 & header & headersize & allocate memory for component \\
%&&&                                                                 & and prepers component receprion \\
%\hline
%receive\_data & 9 & adress \& data bytes  & uint16 + x & stores x data bytes at given\\
%&&&                                                                 & address, issue installation if all data received\\
%\hline
%abort\_install & 10 & - & -                               & clean up component memory from \\
%&&&                                                                 &partial data, return to IDLE state\\
%\hline
%\end{tabular}
%\end{small}
%\label{commands}
%\end{sidewaystable}%


%\begin{table}[ht]
%\caption{{\sc ComponentManager} Responses}
%\begin{small}
%\begin{tabular}{|c|c|}
%\hline
%Name & Code \\
%\hline
%\hline
%		NO\_SPACE\_FOR\_HEADER &  1 \\
%		NO\_SPACE\_FOR\_COMPONENT & 2 \\ 
%		NO\_SLOT\_FOR\_COMPONENTHEADER & 3 \\
%		INSTALLATION\_FAILED & 4 \\
%		INSTALLATION\_SUCCESSFUL & 5 \\
%		STARTUP\_FAILED & 6 \\
%		STARTUP\_SUCCESSFUL & 7 \\
%		SHUTDOWN\_FAILED & 8 \\
%		SHUTDOWN\_SUCCESSFUL & 9 \\ 
%		COMPONENT\_LIST & 10 \\
%		UNINSTALL\_FAILED & 11 \\
%		UNINSTALL\_SUCCESSFUL & 12 \\
%		DATA\_RECEIVED & 13 \\
%		ABORT\_SUCCESSFUL & 14 \\
%		DATA\_ALREADY\_RECEIVED & 247 \\
%		ALREADY\_INSTALLED & 248 \\
%		COMMAND\_INVALID\_RECEIVE & 249 \\
%		PACKET\_INVALID & 250 \\
%		COMMAND\_INVALID & 251 \\
%		ALREADY\_RUNNING & 252 \\
%		UNKNOWN\_COMPONENT & 253 \\
%		ERROR\_STATE & 254 \\
%\hline
%\end{tabular}
%\end{small}
%\label{responses}
%\end{table}%

\begin{table}[ht]
\caption{{\sc ComponentManager} Responses}
\begin{small}
\begin{tabular}{|c|c|}
\hline
Name & Code \\
\hline
\hline
		NO\_SPACE\_FOR\_HEADER &  0x01 \\
		NO\_SPACE\_FOR\_COMPONENT & 0x02 \\ 
		NO\_SLOT\_FOR\_COMPONENTHEADER & 0x03 \\
		INSTALLATION\_FAILED & 0x04 \\
		INSTALLATION\_SUCCESSFUL & 0x05 \\
		STARTUP\_FAILED & 0x06 \\
		STARTUP\_SUCCESSFUL & 0x07 \\
		SHUTDOWN\_FAILED & 0x08 \\
		SHUTDOWN\_SUCCESSFUL & 0x09 \\ 
		COMPONENT\_LIST & 0x0A \\
		UNINSTALL\_FAILED & 0x0B \\
		UNINSTALL\_SUCCESSFUL & 0x0C \\
		DATA\_RECEIVED & 0x0D \\
		ABORT\_SUCCESSFUL & 0x0E \\
		DATA\_ALREADY\_RECEIVED & 0xF7 \\
		ALREADY\_INSTALLED & 0xF8 \\
		COMMAND\_INVALID\_RECEIVE & 0xF9 \\
		PACKET\_INVALID & 0xFA \\
		COMMAND\_INVALID & 0xFB \\
		ALREADY\_RUNNING & 0xFC \\
		UNKNOWN\_COMPONENT & 0xFD \\
		ERROR\_STATE & 0xFF \\
\hline
\end{tabular}
\end{small}
\label{responses}
\end{table}%

\end{document}
