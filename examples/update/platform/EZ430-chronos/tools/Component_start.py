#start component on Chronos watch with running component update.
#
#
# Copyright (c) 2011 Andre Sieber 
#
# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation
# files (the "Software"), to deal in the Software without
# restriction, including without limitation the rights to use,
#
# copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following
# conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
#



import serial
import array
import sys
import platform
from struct import *
from time import *


#komandozeilen parameter
from optparse import OptionParser 
parser = OptionParser()
parser.add_option("-I", "--ID", type="int",dest="id", help="component id")
(options, args) = parser.parse_args()
header_id = options.id

##################### commands and constants ###############################


#info data
PERSISTENT = 0x04
AUTOSTART = 0x10


# comands
RECEIVE = 1
INSTALL = 2
UNINSTALL = 3
STARTUP = 4
SHUTDOWN = 5
LIST = 6
STARTUP_ALL = 7
RECEIVE_HEADER = 8
RECEIVE_DATA = 9
ABORT_INSTALL = 10


NO_SPACE_FOR_HEADER = 1
NO_SPACE_FOR_COMPONENT = 2
NO_SLOT_FOR_COMPONENTHEADER = 3
INSTALLATION_FAILED = 4
INSTALLATION_SUCCESSFUL = 5
STARTUP_FAILED = 6
STARTUP_SUCCESSFUL = 7
SHUTDOWN_FAILED = 8
SHUTDOWN_SUCCESSFUL = 9
COMPONENT_LIST = 10
UNINSTALL_FAILED = 11
UNINSTALL_SUCCESSFUL = 12
DATA_RECEIVED = 13
ABORT_SUCCESSFUL = 14
DATA_ALREADY_RECEIVED = 247
ALREADY_INSTALLED = 248
COMMAND_INVALID_RECEIVE = 249
PACKET_INVALID = 250
COMMAND_INVALID = 251
ALREADY_RUNNING = 252
UNKNOWN_COMPONENT = 253
ERROR_STATE = 254


def printResponce(responce):
    if  responce == NO_SPACE_FOR_HEADER :
        print "NO_SPACE_FOR_HEADER"
    elif responce == NO_SPACE_FOR_COMPONENT :
        print "NO_SPACE_FOR_COMPONENT"
    elif responce == NO_SLOT_FOR_COMPONENTHEADER :
        print "NO_SLOT_FOR_COMPONENTHEADER"
    elif  responce == INSTALLATION_FAILED :
        print "INSTALLATION_FAILED"
    elif  responce == INSTALLATION_SUCCESSFUL :
        print "INSTALLATION_SUCCESSFUL"
    elif  responce == STARTUP_FAILED :
        print "STARTUP_FAILED"
    elif  responce == STARTUP_SUCCESSFUL :
        print "STARTUP_SUCCESSFUL"
    elif  responce == SHUTDOWN_FAILED :
        print "SHUTDOWN_FAILED"
    elif  responce == SHUTDOWN_SUCCESSFUL :
        print "SHUTDOWN_SUCCESSFUL"
    elif  responce == COMPONENT_LIST :
        print "COMPONENT_LIST"
    elif  responce == UNINSTALL_FAILED :
        print "UNINSTALL_FAILED"
    elif  responce == UNINSTALL_SUCCESSFUL :
        print "UNINSTALL_SUCCESSFUL"
    elif  responce == ABORT_SUCCESSFUL :
        print "ABORT_SUCCESSFUL"
    elif responce == DATA_RECEIVED :
        print "DATA_RECEIVED"
    elif responce == DATA_ALREADY_RECEIVED :
        print "DATA_ALREADY_RECEIVED"
    elif responce == ALREADY_INSTALLED :
        print "ALREADY_INSTALLED"
    elif responce == COMMAND_INVALID_RECEIVE :
        print "COMMAND_INVALID_RECEIVE"
    elif  responce == ALREADY_RUNNING :
        print "ALREADY_RUNNING"
    elif  responce == PACKET_INVALID :
        print "PACKET_INVALID"
    elif  responce == COMMAND_INVALID :
        print "COMMAND_INVALID"
    elif responce == UNKNOWN_COMPONENT :
        print "UNKNOWN_COMPONENT"
    elif responce == ERROR_STATE :
        print "ERROR_STATE"
    else :
        print "responce unknown - %d" %(responce)




#######################################AP Commands##################################



def startAccessPoint():
    return array.array('B', [0xFF, 0x07, 0x03]).tostring()# startMarker, Command, PacketSize

def stopAccessPoint():
    return array.array('B', [0xFF, 0x09, 0x03]).tostring()

def switchChannel(channel):
    return array.array('B', [0xFF, 0x074, 0x04, channel]).tostring()# startMarker, Command, PacketSize, channelNo

def startTX():
    return array.array('B', [0xFF, 0x75, 0x03]).tostring()# startMarker, Command, PacketSize

def stopTX():
    return array.array('B', [0xFF, 0x76, 0x03]).tostring()# startMarker, Command, PacketSize
    
def changeTXPower(power):
    return array.array('B', [0xFF, 0x77, 0x04, power]).tostring()# startMarker, Command, PacketSize, power

def sendData(data):
    x = array.array('B', [0xFF, 0x78, 0x03 + len(data)])# startMarker, Command, PacketSize, data (n bytes)
    for i in range(len(data)):
        x.append(data[i])
#    i = 0
#    y = x.tostring()
#    for i in range(len(y)):
#        print (y[i], ", ")
    return x.tostring()


def startOwnProtocol():
    return array.array('B', [0xFF, 0x7A, 0x03]).tostring()# startMarker, Command, PacketSize, 

def stopOwnProtocol():
    return array.array('B', [0xFF, 0x7B, 0x03]).tostring()# startMarker, Command, PacketSize, 

def getData():
    return array.array('B', [0xFF, 0x79, 0x03]).tostring()# startMarker, Command, PacketSize, 


#######################################"connect to accesspoint"##################################

done = 0

def destPort():
    global done
    ser.write(startAccessPoint())
    tester = ser.read(100) 
    if len(tester)  == 0 :
        done = 0
    else:
        done = 1

if platform.system() == "Darwin":
 #   global done
#while not done:
    try:
        ser = serial.Serial("/dev/tty.usbmodem001",115200,timeout=0.0025)
        #done = 1
        destPort()
    except serial.serialutil.SerialException:
        pass
    if done == 0:
        try:
            ser = serial.Serial("/dev/tty.usbmodem002",115200,timeout=0.0025)
            #done = 1
            destPort()
        except serial.serialutil.SerialException:
            pass  
elif platform.system() == "Linux":
 #   global done
    try:
        ser = serial.Serial("/dev/ttyACM0",115200,timeout=0.0025)
        #done = 1
        destPort()
    except serial.serialutil.SerialException:
        pass
    if done == 0:
        try:
            ser = serial.Serial("/dev/ttyACM1",115200,timeout=0.0025)
            #done = 1
            destPort()
        except serial.serialutil.SerialException:
            pass  
elif platform.system() == "Windows":
  #  global done
    try:
        ser = serial.Serial(0,115200,timeout=0.0025)
        destPort()
        #done = 1
    except serial.serialutil.SerialException:
        pass
    if done == 0:
        try:
            ser = serial.Serial(1,115200,timeout=0.0025)
            destPort()
            #done = 1
        except serial.serialutil.SerialException:
            pass  

if done == 0:
    print "ERROR NO ACCESS POINT FOUND!"
    sys.exit(0)
else:
    print "connected to access point"
accel = ser.read(100) #clear serial port



#######################################"start acces point"##################################
#Start access point
ser.write(stopAccessPoint()) #wichtig!
ser.write(startAccessPoint())
ser.write(startOwnProtocol())


#######################################"main"##################################


data2sendCommand = [
    STARTUP, 
    STARTUP>>8,
    (header_id & 0x00FF),
    (header_id>>8)
    ]


data2sendCommand.insert(0, 0x11)

print "starting component...",
sys.stdout.flush()

###send data
ser.write(sendData(data2sendCommand))
sleep(0.1)
TempData = ser.read(100)   #read pending data
sleep(2.0)
#get data
ser.write(getData())
data = ser.read(100)
if len(data) > 4 :
    print "answer from node:", #auspacken der daten) 
    printResponce(ord(data[5]))



sys.exit(0)

