#ifndef NodeConfiguration_h
#define NodeConfiguration_h
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(es):	 Application
 *
 *	Author:		 Andre Sieber
 *
 *	Description: Implements the System
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */

#include "reflex/types.h"

//#include "reflex/io/Serial.h"
//#include "reflex/io/Led.h"

//#include "reflex/io/OutputChannel.h"
#include "reflex/memory/PoolManager.h"
#include "reflex/memory/SizedPool.h"
#include "reflex/power/Battery.h"
#include "reflex/adc/ADC12_A.h"

#include "reflex/io/Button.h"


//////////update
#include "reflex/flash/MemoryManager.h"
#include "reflex/componentUpdate/ComponentManager.h"


#include "msgInterpreter.h"

#include "reflex/System.h"
#include "reflex/wdt/WDT_A.h"
#include "reflex/rf/RADIO.h"
#include "reflex/io/Display.h"


#include "reflex/data_types.h"

#include "reflex/debug/SizeOf.h"
#include "reflex/sys/SYS.h"

#include "radioDelay.h"





namespace reflex {




/**Define different powergroups for an application. To use the available groups provided by the powermanager are highly 
 * recommended.
 */
enum PowerGroups {
	DEFAULT = reflex::PowerManager::GROUP1,
	DISABLED = reflex::PowerManager::NOTHING
};



class NodeConfiguration 
	: public System 
{
public:


	NodeConfiguration() 
		: System()
		, pool(0) //the Buffer is only used as FiFo. So stacksize is 0 @see Buffer
		, radio(pool)
		, interpreter(pool)
                ,componentManager(&memoryManager, &pool)
	{

		RadioConfiguration cfg(pool);
		cfg.setCCA(true);
		cfg.setLogicalChannel(0);
		cfg.setSendingTOS(false);
		cfg.setSignalAttenuation(0);
		cfg.setWhitening(false);
		cfg.setTransmissionTime(150);
		cfg.setContinuousTransmission(false);
		cfg.performConfiguration(radio.get_in_confData());

		radio.connect_out_data(&interpreter.input);
		interpreter.init(0, radio.get_in_input(),componentManager.get_packet_input());
	       
		timer.setGroups(DEFAULT); // put system timer in default group
		display.setGroups(DEFAULT);
		radio.setGroups(DEFAULT);


		//////update
		radioDelayer.init(radio.get_in_input(), 30);
		componentManager.set_outgoingPacket_receiver(&radioDelayer.input);

		powerManager.enableGroup(DEFAULT); // this enables all registered entities in DEFAULT group (starts system timer)
		iterator = 0;
	}




	void incIt()
	{
		iterator++;
	}


	mcu::SYS sys;
	mcu::RADIO radio;


	mcu::ADC12_A adc;
	Battery battery;

	mcu::WDT_A watchdog;

	uint16 iterator;


	PoolManager poolManager;
	SizedPool<IOBufferSize,NrOfStdOutBuffers> pool; ///< a pool of bufferobject with static size
	Display display;


	msgInterpreter interpreter;
	radioDelay radioDelayer;

	/////////////////////update
	MemoryManager memoryManager;
	ComponentManager componentManager;

};


inline NodeConfiguration& getApplication()
{
	extern NodeConfiguration system;
	return system;
}




} //reflex


#endif
