#ifndef TESTCASE_H
#define TESTCASE_H

#include "reflex/memory/new.h"
#include "reflex/scheduling/ActivityFunctor.h"
#include "reflex/sinks/Event.h"
#include "reflex/sinks/SingleValue.h"
#include "reflex/sinks/Sink.h"
#include "reflex/timer/VirtualTimer.h"
#include "reflex/types.h"
#include "TestEnvironment.h"

#include <stdio.h>

namespace unitTest
{

/**
\ingroup unitTest
\brief Base class for component tests in REFLEX.

A test suite implements a set of test cases for component and driver tests in
REFLEX. All custom tests must derived from this class. Typically, one would
implement multiple, but related tests and distinguish them by an identifier
(integer value).

Test execution is handled by the TestEnvironment object. For each test case
it calls the constructor of the custom test suite with \a id as identifier.
The identifier can also be retrieved calling TestSuite::currentTestId() during
execution.

A test case is finished when one of the result methods
TestSuite::setResultOk(), TestSuite::setResultFailed() or
TestSuite::setResultSkipped() is called. Additionally, the test environment
sets up a timeout, after which the test case is aborted. The timeout is
configurable through TestSuite::setTimeoutMs(). The default timeout behaviour
can be changed by overloading the virtual method TestSuite::timeout(). Sometimes
one would like to perform a chain of tests, where the first faulty one will
cause the test case to fail. This is possible with the macro VERIFY() which can
take additional text for logging purpose.

After finishing the test, TestEnvironment calls the class destructor. The user
must ensure, that every previously allocated resource is unlocked by that time.
Otherwise succeeding test cases might show unexpected behaviour.

Access to the environment is possible through TestSuite::environment(). It
provides additional resources.

\see \ref unitTest for code examples.

 */
class TestSuite
{
public:
    //! Constructs a new test case with a given id
	TestSuite(uint16 testCaseId);

    //! Destroys the test case and frees all resources.
	virtual ~TestSuite();

    //! Returns the current test case id
	inline uint16 currentTestId() const;

    //! Returns the test environment which provides additional tools.
	inline TestEnvironment& environment();

protected:
    //! Marks the test as failed.
	void setResultFailed();

    //! Marks the test as successul.
	void setResultOk();

    //! Marks the test as being skipped.
	void setResultSkipped();

    //! Sets the test timeout to \a milliseconds.
    inline void setTimeoutMs(uint16 milliseconds);

    //! Handles timeout events.
	virtual void timeout();

	enum
	{
		DefaultTimeoutMs = 1000, MsgSize = 100
	};

private:
    TestEnvironment _environment;
	uint16 _currentTestCase;

	reflex::VirtualTimer timer_timeout;
	reflex::Event in_timeout;

protected:
	reflex::ActivityFunctor<TestSuite, &TestSuite::timeout> act_timeout;
};

uint16 TestSuite::currentTestId() const { return _currentTestCase; }

TestEnvironment& TestSuite::environment() { return _environment; }

void TestSuite::setTimeoutMs(uint16 milliseconds) { timer_timeout.set(milliseconds); }

}

/*!
\ingroup unitTest
\brief Convenience macro for test assertion from within unitTest::TestSuite.

This macro verifies \a expr and aborts test execution if \a expr is false. On
success, the test execution is not influenced. This is especially helpful, when
implementing a test-chain. This avoids nested if constructs. Additional text
might be provided, as VERIFY() forwards all trailing parameters to `printf()`.

\see \ref unitTest for example code.
*/
#define VERIFY(expr, ...) \
if (!(expr)) \
{ \
	setResultFailed(); \
	char buffer[TestSuite::MsgSize]; \
	sprintf(buffer, __VA_ARGS__); \
	environment().outputChannel().write(buffer); \
	return; \
}

#define SKIP() \
setResultSkipped(); \
return;
#endif /* TESTCASE_H */
