#ifndef EDFSCHEDULERTEST_H
#define EDFSCHEDULERTEST_H

#include "TestSuite.h"
#include "reflex/scheduling/ActivityFunctor.h"
#include "reflex/sinks/Event.h"
#include "reflex/timer/VirtualTimer.h"

namespace unitTest
{

/*!
\ingroup unitTest
\brief Tests EDF scheduling schemes.

Several tests similar to those defined in PrioritySchedulerTest are performed.

\note
Until now all test cases are executed with a halted timer which keeps
EDF scheduler similar to a fixed priority scheduler. More test cases are
needed!
*/
class EdfSchedulerTest: public TestSuite, public reflex::Sink0
{
public:
    enum TestCases
    {
        ExecutionOrder = 0,
        MultipleExecution,
        Lock,
        Unlock,
        TriggerTightDeadlineAfterLoose,
        TriggerLooseDeadlineAfterTight,
        SchedulingMonitor,
        // For preemptive scheduling only
        LooseDL_Interrupt_TightDL,
        TightDL_Interrupt_LooseDL
    };

    enum {
        DefaultTimeoutMs = 10,
    };

    EdfSchedulerTest(uint16 id);
    ~EdfSchedulerTest();

    virtual void notify();

private:
    void run_1();
    void run_2();
    void timeout();

    reflex::Event in_1;
    reflex::Event in_2;

    reflex::ActivityFunctor<EdfSchedulerTest, &EdfSchedulerTest::run_1> act_1;
    reflex::ActivityFunctor<EdfSchedulerTest, &EdfSchedulerTest::run_2> act_2;

    uint8 execLog[2];
    uint8 expectedLog[2];
    uint8 currentExecCount;

    reflex::VirtualTimer timer;
};

}


#endif // EDFSCHEDULERTEST_H
