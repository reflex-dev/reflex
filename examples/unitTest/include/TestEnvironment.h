#ifndef TESTENVIRONMENT_H
#define TESTENVIRONMENT_H

#include "reflex/data_types/Singleton.h"
#include "reflex/memory/SizedPool.h"
#include "reflex/io/OutputChannel.h"
#include "reflex/scheduling/ActivityFunctor.h"
#include "reflex/sinks/Event.h"
#include "reflex/sinks/SingleValue.h"
#include "reflex/timer/VirtualTimer.h"
#include "reflex/types.h"

#include "TestCase.h"

/*!
\defgroup unitTest
\brief Components and test cases for a unit-testing.

The namespace unitTest contains components and helper classes to create
comprehensive self-test applications. Central component is
unitTest::TestEnvironment, which executes a set of unitTest::TestCase objects
and returns the result to the console or a serial interface.

Tests are implemented in groups by deriving from unitTest::TestSuite. Each test
case in a test suite carries its own identifier, by which it is then accessed
during run-time. A minimal test case may look like this:

\code
class MyTestSuite: public unitTest::TestSuite
{
public:
    enum TestCases
    {
        TestCase0 = 0,
        TestCase1
    };

    MyTestSuite(uint16 id);
};
\endcode

The enum TestCases in the above example defines the test case identifiers. The
test functionality itself can be implemented in the constructor:

\code
using namespace unitTest;

MyTestSuite::MyTestSuite(uint16 id) : TestSuite(id)
{
    switch(currentTestId())
    {
    case TestCase0:
        VERIFY((1+1) == 2, "Weird arithmetics: 1+1!=2?");
        VERIFY((1*1) == 2, "Weird arithmetics: 1*1!=1?");
        setResultOk();
        break;
    case TestCase1:
        if (1*1 == 1)
        {
            setResultOk();
        }
        else
        {
            setResultFailed();
        }
        break;
    default:
        setResultSkipped();
    }
}
\endcode

More complex examples may contain activities and event channels, but this is
not necessary. The \a id parameter is set by the test environment and decides
which test case is to be executed. As long as the test is active, the id can
be retrieved calling unitTest::TestSuite::currentTestId(). Several functions
control the result: unitTest::TestSuite::setResultOk(),
unitTest::TestSuite::setResultFailed() and
unitTest::TestSuite::setResultSkipped(). The VERIFY() macro is a more convenient
way, to perform multiple tests in a series. If VERIFY() fails, the test will
abort immediately and the result is set to fail. The macro maps directly to
`printf()` and can transport a note to the user, e.g. the reason for failing.

Test cases are executed in a test application:

\code
class NodeConfiguration : public System
{
    NodeConfiguration();

    unitTest::TestEnvironment testEnvironment;
    static const unitTest::TestCase testList[];
    //...
};
\endcode

The \a testEnvironment object is resposible for correct test construction and
termination. The \a testList array contains all test cases and is located
directly in flash during compile-time. A list with both test cases from above
would look like:

\code
using namespace unitTest;

const TestCase NodeConfiguration::testList[] =
{
    // name                  , id                    , stub
    { "MyTestSuite::TestCase0", MyTestSuite::TestCase0, &create<MyTestCase> },
    { "MyTestSuite::TestCase1", MyTestSuite::TestCase1, &create<MyTestCase> }
};

// or alternatively
const TestCase NodeConfiguration::testList[] =
{
    TESTCASE(MyTestSuite, TestCase0),
    TESTCASE(MyTestSuite, TestCase1),
};
\endcode

Each test case defines a \a name, an \a id and a generator \a stub. The \a name
can be freely chosen and only represents the test suite in the output log. The
\a id is the test case identifier and corresponds to the implemented test
cases. The generator \a stub is a function, that constructs the test case
during run-time in RAM. For each test case, the compiler emits another instance
of this stub. A default implementation is the create() function.

The example above defines test cases in two different ways: The long form where
every parameter is adjustable and the convenient short form using the macro
TESTCASE(). Once, all test cases are defined, the application can be started:
\code
NodeConfiguration::NodeConfiguration()
{
    testEnvironment.setOut_message(&console);
    console.out_txFinished = testEnvironment.in_txFinished();
    testEnvironment.setTestList(&testList[0], sizeof(testList)/sizeof(unitTest::TestCase));

    timer.switchOn();

    testEnvironment.start();
}
\endcode

The application now iterates over every entry in the \a testList and executes
them in ascending order. If a test case fails, the next one is taken. This is
the output of the above application:

\verbatim
# name;	id;	comment;	result
MyTestSuite::TestCase0;    0; "Weird arithmetics: 1+1!=2?" ; fail
MyTestSuite::TestCase1;    1; ""                           ; ok
# FINISHED
\endverbatim


Special care is required when using time triggered scheduling. The test
environment does not come with a timer that could clock the scheduler. Instead,
the test environment guarantees proper test creation. But to run the test, a
periodic timer has to be defined and connected to the scheduler in the test suite
constructor. After running the test, this timer can be safely stopped and
destroyed together with the test case.

 */
namespace unitTest
{

/*!
\ingroup unitTest
\brief Result of a test case execution.

Test results are sent from within TestSuite to the
unitTest::TestEnvironment::in_result which converts them into a string
representation.
 */
enum TestResult
{
    TestOk, TestFailed, TestSkipped
};

/*!
\ingroup unitTest
\brief Environment to construct and execute test cases.

TestEnvironment is the central application component that executes test cases
and provides additional tools. It implements the singleton pattern, which means
that only one instance exists in the whole application. Like all reflex
components, it relies on a working event flow.

Inputs:
  - TestEnvironment::in_txFinished()

Outputs:
  - TestEnvironment::setOut_message()

TestEnvironment expects the user to connect all inputs and outputs as well as
a list of test cases with TestEnvironment::setTestList(). After successful
setup, the component can be started by calling TestEnvironment::start().

During test execution, TestEnvironment::outputChannel() provides access to the
logging console.

\see \ref unitTest for example code.

 */
class TestEnvironment
{
    friend class TestSuite;

public:
    enum
    {
        DefaultEdfResponseTime = 1000 //! Response time for activities in
                                      //! EDF scheduling
    };

    //! Input for transmission notifications.
    inline reflex::Event* in_txFinished();

    //! Returns the system output channel for logging.
    inline reflex::OutputChannel& outputChannel();

    //! Connects \a input to logging output.
    inline void setOut_message(reflex::Sink1<reflex::Buffer*>* input);

    //! Setup a \a list of test cases with a fixed \a size.
    inline void setTestList(const TestCase* list, uint16 size);

    //! Initiates the test execution.
    inline void start();

private:
    inline reflex::Sink1<TestResult>& in_result();

    class TestEnvironmentImplementation
    {
    public:
        reflex::SingleValue1<TestResult> in_result;
        reflex::Event in_txFinished;

        TestEnvironmentImplementation();
        inline reflex::OutputChannel& outputChannel();
        inline void setOut_message(reflex::Sink1<reflex::Buffer*>* input);
        void setTestList(const TestCase* list, uint16 size);
        void start();

    private:
        /*!
         Memory placeholder for a TestSuite object. A struct is used to ensure
         greatest alignment.
         */
        template<uint16 size>
        class DataBuffer
        {
            uint8 data[size];
        };

        enum
        {
            TestBufferSize = 1024, MsgSize = 128, PoolSize = 1
        };

        void run_result();
        void run_txFinished();

        inline TestSuite* currentTestSuite();

        const TestCase* testList;
        uint16 sizeOfTestList;
        uint16 currentIndex;

        reflex::SizedPool<MsgSize, PoolSize> _communicationPool;
        reflex::OutputChannel _outputChannel;
        DataBuffer<TestBufferSize> _testSuiteBuffer;

        reflex::ActivityFunctor<TestEnvironmentImplementation, &TestEnvironmentImplementation::run_result> act_result;
        reflex::ActivityFunctor<TestEnvironmentImplementation, &TestEnvironmentImplementation::run_txFinished> act_txFinished;
    };

    reflex::data_types::Singleton<TestEnvironmentImplementation> p;
};

TestSuite* TestEnvironment::TestEnvironmentImplementation::currentTestSuite() { return reinterpret_cast<TestSuite*> (&_testSuiteBuffer); }

reflex::OutputChannel& TestEnvironment::TestEnvironmentImplementation::outputChannel() { return _outputChannel; }

void TestEnvironment::TestEnvironmentImplementation::setOut_message(reflex::Sink1<reflex::Buffer*>* input) { _outputChannel.init(input); }

inline reflex::Sink1<TestResult>& TestEnvironment::in_result() { return p->in_result; }

inline reflex::Event* TestEnvironment::in_txFinished() { return &p->in_txFinished; }

inline reflex::OutputChannel& TestEnvironment::outputChannel() { return p->outputChannel(); }

inline void TestEnvironment::setOut_message(reflex::Sink1<reflex::Buffer*>* input) { p->setOut_message(input); }

inline void TestEnvironment::setTestList(const TestCase* list, uint16 size) { p->setTestList(list, size); }

inline void TestEnvironment::start() { p->start(); }



}

#endif /* TESTENVIRONMENT_H */
