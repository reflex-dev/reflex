#ifndef TESTCASE_H_
#define TESTCASE_H_

#include "reflex/types.h"
#include "Create.h"

namespace unitTest
{

class TestSuite;

/*!
\ingroup unitTest
\brief Defines a single entry in the test list.

This macro is for convenience only to make test case definition shorter. It
maps \a type to a test suite and \a name to an enumeration within \a type,
which typically represents a test case id.

The code
\code
TESTCASE(FifoSchedulerTest, MultipleExecution),
\endcode

expands to
\code
{ "FifoSchedulerTest::MultipleExecution", FifoSchedulerTest::MultipleExecution, &create<FifoSchedulerTest> }
\endcode

\see unitTest::TestCase
\hideinitializer
*/
#define TESTCASE(type, name)\
{ #type "::" #name, type::name, &create<type> }

/*!
\ingroup unitTest
\brief Pointer to a test case generator function.

\see unitTest::create().
*/
typedef TestSuite* (*GeneratorStub)(void*, uint16);


/*!
\ingroup unitTest
\brief A single entry in a list of test cases.

A test case is in an instance of a TestSuite object in combination with a
certain test identifier, since every test suite typically contains multiple
test cases. Every test case that is executed, has to be placed in a table
during compile-time which is then assigned to the test environment.

\see \ref unitTest for example code.

*/
struct TestCase
{
private:
    enum
    {
        MaxNameLength = 64 //!< maximum name length
    };

public:
    char name[MaxNameLength]; //!< Full name of the test case
    uint16 id; //!< Test case identifier
    GeneratorStub stub; //!< Pointer to a function that constructs the test case.
};


}


#endif /* TESTLIST_H_ */
