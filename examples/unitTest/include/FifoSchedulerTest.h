#ifndef FIFOSCHEDULERTEST_H_
#define FIFOSCHEDULERTEST_H_

#include "TestSuite.h"
#include "reflex/scheduling/ActivityFunctor.h"
#include "reflex/sinks/Event.h"

namespace unitTest
{

/*!
\ingroup unitTest
\brief Tests correct behaviour of FifoScheduler and FifoActivity.

This class tests FifoScheduler and FifoActivity on the event-flow level.
It performs several tests regarding execution order, reschedule-count
and (un-)locking.

 */
class FifoSchedulerTest: public TestSuite
{
public:
    enum TestCases
    {
        ExecutionOrder = 0,
        MultipleExecution,
        Lock,
        Unlock
    };

    FifoSchedulerTest(uint16 id);

private:
    void run_1();
    void run_2();
    void timeout();

    reflex::Event in_1;
    reflex::Event in_2;

    reflex::ActivityFunctor<FifoSchedulerTest, &FifoSchedulerTest::run_1> act_1;
    reflex::ActivityFunctor<FifoSchedulerTest, &FifoSchedulerTest::run_2> act_2;

    uint8 execLog[2];
    uint8 currentExecCount;

};

}

#endif /* INITIALTEST_H_ */
