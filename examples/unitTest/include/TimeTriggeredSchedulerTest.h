#ifndef TIMETRIGGEREDSCHEDULERTEST_H
#define TIMETRIGGEREDSCHEDULERTEST_H

#include "TestSuite.h"
#include "reflex/scheduling/ActivityFunctor.h"
#include "reflex/sinks/Event.h"

namespace unitTest
{

/*!
\ingroup unitTest
\brief Tests correct behaviour of TimeTriggeredScheduler.

This class tests TimeTriggeredScheduler on the event-flow level.
It performs several tests regarding execution order, reschedule-count
and (un-)locking.

 */
class TimeTriggeredSchedulerTest: public TestSuite
{
public:
    enum TestCases
    {
        ExecutionOrder = 0,
        SingleTriggerSingleSlot,
        SingleTriggerMultiSlot,
        MultiTriggerSingleSlot,
        MultiTriggerMultiSlot,
        Lock,
        Unlock
    };

    TimeTriggeredSchedulerTest(uint16 id);

private:
    void run_1();
    void run_2();
    void timeout();
    void setTimeoutMs(uint16 milliseconds);

    reflex::Event in_1;
    reflex::Event in_2;

    reflex::ActivityFunctor<TimeTriggeredSchedulerTest,
        &TimeTriggeredSchedulerTest::run_1> act_1;
    reflex::ActivityFunctor<TimeTriggeredSchedulerTest,
        &TimeTriggeredSchedulerTest::run_2> act_2;

    uint8 execLog[2];
    uint8 currentExecCount;
    reflex::VirtualTimer schedulingTimer;
};

}

#endif // TIMETRIGGEREDSCHEDULERTEST_H
