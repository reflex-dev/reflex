#ifndef CREATE_H
#define CREATE_H

#include "reflex/types.h"

namespace unitTest
{

class TestSuite;

/*!
\ingroup unitTest
\brief Function that constructs a test case in RAM.

Unlike other REFLEX components, test cases are created dynamically before
execution. This function serves as a generator and places a new instance of
\a TestType at address \a addr. The test case identifier \a id specifies which
test case is to be executed.

The compiler creates one instance for each test suite, that is referenced in
the test list. The default implementation accepts only one template parameter
-- the test suite class \a type and passes the test case \a id to its
constructor. But it is possible to overload it by implementations with more
template parameters:

\code
template<template<IOPorts, IOPorts> class PortTest,
    IOPorts Port1,
    IOPorts Port2,
    uint8 pins>
TestSuite* create(void* addr, uint16 id)
{
    return new (addr) PortTest<Port1, Port2>(id, pins);
}
\endcode

The above example creates a test suite that consumes two template parameters
\a Port1, \a Port2 and passes one additional parameter \a pins to its
constructor.

\see \ref unitTest for example code.

 */
template<typename TestType>
TestSuite* create(void* addr, uint16 id) { return new (addr) TestType(id); }

}


#endif // CREATE_H
