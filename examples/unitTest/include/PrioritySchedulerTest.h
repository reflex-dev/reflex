#ifndef PRIORITYSCHEDULERTEST_H_
#define PRIORITYSCHEDULERTEST_H_

#include "TestSuite.h"
#include "reflex/scheduling/ActivityFunctor.h"
#include "reflex/sinks/Event.h"
#include "reflex/timer/VirtualTimer.h"

namespace unitTest
{

/*!
\ingroup unitTest
\brief Tests reflex::PrioritySchedulerNonpremptive and reflex::PriorityActivity.

This class tests reflex::PrioritySchedulerNonpreemptive and
reflex::PriorityActivity on the event-flow level. It performs several tests
regarding execution order and priority levels, reschedule-count and
(un-)locking.
Preemption is tested using a VirtualTimer and a synchronous input.
 */
class PrioritySchedulerTest: public TestSuite, public reflex::Sink0
{
public:
    enum TestCases
    {
        ExecutionOrder = 0,
        MultipleExecution,
        Lock,
        Unlock,
        TriggerHighPriorityAfterLow,
        TriggerLowPriorityAfterHigh,
        SchedulingMonitor,
        // For preemptive scheduling only
        LowPrioAct_HighPrioAct,
        LowPrioAct_Interrupt_HighPrioAct,
        HighPrioAct_Interrupt_LowPrioAct
    };

    enum {
        DefaultTimeoutMs = 10
    };

    PrioritySchedulerTest(uint16 id);
    ~PrioritySchedulerTest();

    virtual void notify();

private:
    void run_1();
    void run_2();
    void timeout();

    reflex::Event in_1;
    reflex::Event in_2;

    reflex::ActivityFunctor<PrioritySchedulerTest, &PrioritySchedulerTest::run_1> act_1;
    reflex::ActivityFunctor<PrioritySchedulerTest, &PrioritySchedulerTest::run_2> act_2;

    uint8 execLog[2];
    uint8 expectedLog[2];
    uint8 currentExecCount;

    reflex::VirtualTimer timer;
};

}

#endif /* PRIORITYSCHEDULERTEST_H_ */
