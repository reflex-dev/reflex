import qbs 1.0
import ReflexApplication
import ReflexProject

ReflexProject {
    buffer_maxPoolCount : 1
    core_nrOfSchedulingSlots : 10
    core_ticksPerRound : 100

    ReflexApplication {
        name: "unittest"

        files: [
            "src/TestSuite.cc",
            "src/TestEnvironment.cc",
            "src/LinkedListTest.cc",
            "platform/" + reflex_platform + "/src/*.cc"
        ]

        cpp.includePaths : [
            "include",
            "platform/" + reflex_platform + "/include"
        ]

        Group {
            files : {
                return  {
                    EDF_SCHEDULING : [
                        "src/EdfSchedulerTest.cc"
                    ],
                    FIFO_SCHEDULING : [
                        "src/FifoSchedulerTest.cc"
                    ],
                    EDF_SCHEDULING_SIMPLE : [
                        "src/EdfSchedulerTest.cc"
                    ],
                    EDF_SCHEDULING_NONPREEMPTIVE : [
                        "src/EdfSchedulerTest.cc"
                    ],
                    PRIORITY_SCHEDULING : [
                        "src/PrioritySchedulerTest.cc"
                    ],
                    PRIORITY_SCHEDULING_SIMPLE : [
                        "src/PrioritySchedulerTest.cc"
                    ],
                    PRIORITY_SCHEDULING_NONPREEMPTIVE : [
                        "src/PrioritySchedulerTest.cc"
                    ],
                    TIME_TRIGGERED_SCHEDULING : [
                        "src/TimeTriggeredSchedulerTest.cc"
                    ]
                }[project.core_schedulingScheme];
            }
        }
    }
}
