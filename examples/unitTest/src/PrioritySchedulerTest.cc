#include "PrioritySchedulerTest.h"

using namespace unitTest;
using namespace reflex;

PrioritySchedulerTest::PrioritySchedulerTest(uint16 id) : TestSuite(id),
    act_1(*this), act_2(*this), execLog{0}, expectedLog{0},
        timer(VirtualTimer::ONESHOT)
{
    in_1.init(&act_1);
    in_2.init(&act_2);
    timer.connect_output(this);

    currentExecCount = 0;

    switch (currentTestId())
    {
    /* act_1 comes first, then _act2 */
    case ExecutionOrder:
        expectedLog[0] = 1;
        expectedLog[1] = 2;
        in_1.notify();
        in_2.notify();
        setTimeoutMs(DefaultTimeoutMs);
        break;
    /* exec act_1 twice */
    case MultipleExecution:
        expectedLog[0] = 1;
        expectedLog[1] = 1;
        in_1.notify();
        in_1.notify();
        setTimeoutMs(DefaultTimeoutMs);
        break;
    /* Locked activity must not be scheduled */
    case Lock:
        expectedLog[0] = 0;
        expectedLog[1] = 0;
        act_1.lock();
        in_1.notify();
        setTimeoutMs(DefaultTimeoutMs);
        break;
    /* Locked activity must be scheduled after being unlocked*/
    case Unlock:
        expectedLog[0] = 1;
        expectedLog[1] = 0;
        act_1.lock();
        in_1.notify();
        act_1.unlock();
        setTimeoutMs(DefaultTimeoutMs);
        break;
    case TriggerHighPriorityAfterLow:
        expectedLog[0] = 1;
        expectedLog[1] = 2;
        act_1.setPriority(PriorityActivity::DefaultPriority - 1);
        in_2.notify();
        in_1.notify();
        setTimeoutMs(DefaultTimeoutMs);
        break;
    case TriggerLowPriorityAfterHigh:
        expectedLog[0] = 1;
        expectedLog[1] = 2;
        act_1.setPriority(PriorityActivity::DefaultPriority - 1);
        in_1.notify();
        in_2.notify();
        setTimeoutMs(DefaultTimeoutMs);
        break;
    // Activities triggered by an interrupt handler must be dispatched after
    // the interrupt handler has been finished. This tests correct usage of
    // the scheduling monitor (issue #6).
    case SchedulingMonitor:
        expectedLog[0] = 3;
        expectedLog[1] = 1;
        timer.set(1);
        setTimeoutMs(DefaultTimeoutMs);
        break;
    case LowPrioAct_HighPrioAct:
        expectedLog[0] = 2;
        expectedLog[1] = 1;
        act_2.setPriority(PriorityActivity::DefaultPriority - 1);
        act_1.setPriority(PriorityActivity::DefaultPriority);
        in_1.notify();
        in_2.notify();
        setTimeoutMs(DefaultTimeoutMs);
        break;
    // A high priority activity must preempt a low priority activity.
    case LowPrioAct_Interrupt_HighPrioAct:
        expectedLog[0] = 1;
        expectedLog[1] = 0;
        act_1.setPriority(PriorityActivity::DefaultPriority - 1);
        in_1.notify();
        execLog[currentExecCount] = 0;
        currentExecCount++;
        setTimeoutMs(DefaultTimeoutMs);
        break;
    // A low priority activity must NOT preempt a high priority activity.
    case HighPrioAct_Interrupt_LowPrioAct:
        expectedLog[0] = 0;
        expectedLog[1] = 1;
        act_1.setPriority(PriorityActivity::DefaultPriority + 1);
        in_1.notify();
        execLog[currentExecCount] = 0;
        currentExecCount++;
        setTimeoutMs(DefaultTimeoutMs);
        break;
    default:
        setResultSkipped();
    }
}

PrioritySchedulerTest::~PrioritySchedulerTest()
{
    timer.stop();
}

void PrioritySchedulerTest::run_1()
{
    execLog[currentExecCount] = 1;
    currentExecCount++;
}

void PrioritySchedulerTest::run_2()
{
    execLog[currentExecCount] = 2;
    currentExecCount++;
}

void PrioritySchedulerTest::timeout()
{
    switch(currentTestId())
    {
    case ExecutionOrder:
    case MultipleExecution:
    case Lock:
    case Unlock:
    case TriggerHighPriorityAfterLow:
    case TriggerLowPriorityAfterHigh:
    case SchedulingMonitor:
    case LowPrioAct_HighPrioAct:
    case LowPrioAct_Interrupt_HighPrioAct:
    case HighPrioAct_Interrupt_LowPrioAct:
        VERIFY((execLog[0] == expectedLog[0]) && (execLog[1] == expectedLog[1]),
                "log: (%u,%u), expected (%u, %u)",
                execLog[0], execLog[1], expectedLog[0], expectedLog[1]);
        setResultOk();
        break;
    default:
        TestSuite::timeout();
    }
}

/*!
This is an synchronous method, called directly from a VirtualTimer on interrupt
level. This is necessary to test preemption.
 */
void PrioritySchedulerTest::notify()
{
    switch(currentTestId())
    {
    case SchedulingMonitor:
        in_1.notify();
        execLog[currentExecCount] = 3;
        currentExecCount++;
        break;
    default:
        VERIFY(false, "timer must NOT fire in this testcase" );
    }
}
