#include "TestSuite.h"

using namespace reflex;
using namespace unitTest;

TestSuite::TestSuite(uint16 testCaseId) :
	timer_timeout(VirtualTimer::ONESHOT), act_timeout(*this)
{
	_currentTestCase = testCaseId;
	in_timeout.init(&act_timeout);
	timer_timeout.connect_output(&in_timeout);
	timer_timeout.set(DefaultTimeoutMs);
}

TestSuite::~TestSuite()
{
	timer_timeout.stop();
}

/*!
This will set the test result to 'fail'.

This method can be overloaded for custom behaviour.
 */
void TestSuite::timeout()
{
	setResultFailed();
	environment().outputChannel().write("timeout");
}

void TestSuite::setResultFailed()
{
	timer_timeout.stop();
    environment().in_result().assign(TestFailed);
}

void TestSuite::setResultOk()
{
	timer_timeout.stop();
    environment().in_result().assign(TestOk);
}

void TestSuite::setResultSkipped()
{
	timer_timeout.stop();
    environment().in_result().assign(TestSkipped);
}

