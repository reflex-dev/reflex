#include "TimeTriggeredSchedulerTest.h"
#include "reflex/System.h"

using namespace unitTest;
using namespace reflex;

TimeTriggeredSchedulerTest::TimeTriggeredSchedulerTest(uint16 id) :
    TestSuite(id), act_1(*this), act_2(*this),
    schedulingTimer(VirtualTimer::PERIODIC)
{
    schedulingTimer.connect_output(getSystem().scheduler.in_tick());
    schedulingTimer.set(1);

    in_1.init(&act_1);
    in_2.init(&act_2);

    execLog[0] = 0;
    execLog[1] = 0;
    currentExecCount = 0;

    switch (currentTestId())
    {
    /* act_1 comes first, then _act2 */
    case ExecutionOrder:
        getSystem().scheduler.registerActivity(&act_1, 1);
        getSystem().scheduler.registerActivity(&act_2, 1);
        in_1.notify();
        in_2.notify();
        setTimeoutMs(2);
        break;
    /* Trigger act_1 once, but wait two cycles */
    case SingleTriggerSingleSlot:
        getSystem().scheduler.registerActivity(&act_1, 1);
        in_1.notify();
        setTimeoutMs(TICKS_PER_ROUND+1);
        break;
    /* Register act_1 to multiple time slots */
    case SingleTriggerMultiSlot:
        getSystem().scheduler.registerActivity(&act_1, 1);
        getSystem().scheduler.registerActivity(&act_1, 2);
        in_1.notify();
        setTimeoutMs(3);
        break;
    /* Trigger act_1 twice */
    case MultiTriggerSingleSlot:
        getSystem().scheduler.registerActivity(&act_1, 1);
        in_1.notify();
        in_1.notify();
        setTimeoutMs(TICKS_PER_ROUND+1);
        break;
    /* Trigger act_1 twice */
    case MultiTriggerMultiSlot:
        getSystem().scheduler.registerActivity(&act_1, 1);
        getSystem().scheduler.registerActivity(&act_1, 2);
        in_1.notify();
        in_1.notify();
        setTimeoutMs(3);
        break;
    /* Locked activity must not be scheduled */
    case Lock:
        getSystem().scheduler.registerActivity(&act_1, 1);
        act_1.lock();
        in_1.notify();
        setTimeoutMs(3);
        break;
    /* Locked activity must be scheduled after being unlocked*/
    case Unlock:
        getSystem().scheduler.registerActivity(&act_1, 1);
        act_1.lock();
        in_1.notify();
        act_1.unlock();
        setTimeoutMs(3);
        break;
    default:
        setResultSkipped();
    }
}


void TimeTriggeredSchedulerTest::run_1()
{
    execLog[currentExecCount] = 1;
    currentExecCount++;
}

void TimeTriggeredSchedulerTest::run_2()
{
    execLog[currentExecCount] = 2;
    currentExecCount++;
}

void TimeTriggeredSchedulerTest::timeout()
{
    switch(currentTestId())
    {
    case ExecutionOrder:
        VERIFY((execLog[0] == 1) && (execLog[1] == 2), "order: %u, %u", execLog[0], execLog[1]);
        setResultOk();
        break;
    case SingleTriggerSingleSlot:
        VERIFY((execLog[0] == 1) && (execLog[1] == 0), "order: %u, %u", execLog[0], execLog[1]);
        setResultOk();
        break;
    case SingleTriggerMultiSlot:
        VERIFY((execLog[0] == 1) && (execLog[1] == 0), "order: %u, %u", execLog[0], execLog[1]);
        setResultOk();
        break;
    case MultiTriggerSingleSlot:
        VERIFY((execLog[0] == 1) && (execLog[1] == 1), "order: %u, %u", execLog[0], execLog[1]);
        setResultOk();
        break;
    case MultiTriggerMultiSlot:
        VERIFY((execLog[0] == 1) && (execLog[1] == 1), "order: %u, %u", execLog[0], execLog[1]);
        setResultOk();
        break;
    case Lock:
        VERIFY((execLog[0] == 0), "execLog[0]=%u", execLog[0]);
        setResultOk();
        break;
    case Unlock:
        VERIFY((execLog[0] == 1), "execLog[0]=%u", execLog[0]);
        setResultOk();
        break;
    default:
        TestSuite::timeout();
    }
}

/*!
Overloads TestSuite::setTimeoutMs() so that TestSuite::act_timeout is
scheduled immediately after timeout.
 */
void TimeTriggeredSchedulerTest::setTimeoutMs(uint16 milliseconds)
{
    getSystem().scheduler.removeActivity(&this->TestSuite::act_timeout);
    getSystem().scheduler.registerActivity(&this->TestSuite::act_timeout, milliseconds % TICKS_PER_ROUND);
    TestSuite::setTimeoutMs(milliseconds);
}
