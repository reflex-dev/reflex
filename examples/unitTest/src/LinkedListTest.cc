#include "LinkedListTest.h"

using namespace unitTest;
using namespace reflex;
using namespace data_types;

LinkedListTest::LinkedListTest(uint16 id) : TestSuite(id)
{
    switch(currentTestId())
    {
    case BasicTests:
        VERIFY(list.isEmpty(), "List must be empty");
        VERIFY(list.first() == 0, "Empty list must start with 0");
        VERIFY(list.last() == 0, "Empty list must end with 0");
        break;
    case Append:
        list.append(&elements[0]);
        VERIFY(!list.isEmpty(), "List must contain 1 element");
        VERIFY(list.first() == &elements[0], "Element 0 must be first");
        VERIFY(list.last() == &elements[0], "Element 0 must be last");
        list.append(&elements[1]);
        VERIFY(elements[0].next == &elements[1], "Elements 0 and 1 not in a chain");
        VERIFY(list.first() == &elements[0], "Element 0 must still be first");
        VERIFY(list.last() == &elements[1], "Element 1 must be last");
        break;
    case Remove:
        list.append(&elements[0]);
        list.append(&elements[1]);
        list.append(&elements[2]);
        list.remove(&elements[1]);
        /* Remove middle */
        VERIFY(!elements[1].isLinked(), "Element 1 must not be linked");
        VERIFY(elements[0].isLinked(), "Element 0 must still be linked");
        VERIFY(list.first() == &elements[0], "Element 0 must be first");
        VERIFY(list.last() == &elements[2], "Element 2 must be last");
        VERIFY(elements[0].next == &elements[2], "Elements 0 and 2 not in a chain");
        /* Remove last */
        list.remove(&elements[2]);
        VERIFY(!elements[2].isLinked(), "Element 2 must not be linked");
        /* Remove first */
        list.remove(&elements[0]);
        VERIFY(!elements[0].isLinked(), "Element 0 must not be linked");
        VERIFY(list.isEmpty(), "List not empty");
        break;
    case Insert:
        list.insert(&elements[1]);
        list.insert(&elements[0]);
        VERIFY(list.first() == &elements[0], "Element 0 is not first");
        VERIFY(list.last() == &elements[1], "Element 1 is not last");
        VERIFY(elements[0].next == &elements[1], "Element 0 is not before element 1");
        list.insert(&elements[2]);
        VERIFY(list.last() == &elements[2], "Element 2 is not last");
        VERIFY(elements[1].next == &elements[2], "Element 1 is not before element 2");
        break;
    case InsertBefore:
        list.insertBefore(0, &elements[0]);
        VERIFY(!list.isEmpty(), "List must not be empty");
        VERIFY(list.first() == &elements[0], "Item 0 must be first");
        VERIFY(list.last() == &elements[0], "Item 0 must also be last");
        list.insertBefore(list.first(), &elements[1]);
        VERIFY(list.first() == &elements[1], "Item 1 must be first");
        VERIFY(list.last() == &elements[0], "Item 0 must be last");
        VERIFY(list.first()->isLinked(), "Item 1 must be linked");
        VERIFY(list.first()->next == &elements[0], "Item 1 and 0 must be in a chain");
        list.insertBefore(list.last(), &elements[2]);
        VERIFY(elements[2].isLinked(), "Item 2 must be linked");
        VERIFY(list.first() == &elements[1], "Item 1 must still be first");
        VERIFY(list.last() == &elements[2], "Item 2 must still be last");
        break;
    case TakeFirst:
        list.append(&elements[0]);
        list.append(&elements[1]);
        ChainLink* item = list.takeFirst();
        VERIFY(item == &elements[0], "Item 0 must be first");
        VERIFY(!item->isLinked(), "Item 0 must not be linked");
        VERIFY(list.first() == &elements[1], "Item 1 must still be in list");
        item = list.takeFirst();
        VERIFY(item == &elements[1], "Item 1 must be first");
        VERIFY(!item->isLinked(), "Item 1 must not be linked");
        VERIFY(list.isEmpty(), "List must be empty");
        item = list.takeFirst();
        VERIFY(item == 0, "Empty list must return 0");
        break;
    }

    setResultOk();
}
