#include "FifoSchedulerTest.h"

using namespace unitTest;
using namespace reflex;

FifoSchedulerTest::FifoSchedulerTest(uint16 id) : TestSuite(id), act_1(*this), act_2(*this)
{
    in_1.init(&act_1);
    in_2.init(&act_2);

    execLog[0] = 0;
    execLog[1] = 0;
    currentExecCount = 0;

    switch (currentTestId())
    {
    /* act_1 comes first, then _act2 */
    case ExecutionOrder:
        in_1.notify();
        in_2.notify();
        setTimeoutMs(50);
        break;
    /* exec act_1 twice */
    case MultipleExecution:
        in_1.notify();
        in_1.notify();
        setTimeoutMs(50);
        break;
    /* Locked activity must not be scheduled */
    case Lock:
        act_1.lock();
        in_1.notify();
        setTimeoutMs(25);
        break;
    /* Locked activity must be scheduled after being unlocked*/
    case Unlock:
        act_1.lock();
        in_1.notify();
        act_1.unlock();
        setTimeoutMs(25);
        break;
    default:
        setResultSkipped();
    }
}


void FifoSchedulerTest::run_1()
{
    execLog[currentExecCount] = 1;
    currentExecCount++;
}

void FifoSchedulerTest::run_2()
{
    execLog[currentExecCount] = 2;
    currentExecCount++;
}

void FifoSchedulerTest::timeout()
{
    switch(currentTestId())
    {
    case ExecutionOrder:
        VERIFY((execLog[0] == 1) && (execLog[1] == 2), "execLog[0]=%u, execLog[1]=%u", execLog[0], execLog[1]);
        setResultOk();
        break;
    case MultipleExecution:
        VERIFY((execLog[0] == 1) && (execLog[1] == 1), "execLog[0]=%u, execLog[1]=%u", execLog[0], execLog[1]);
        setResultOk();
        break;
    case Lock:
        VERIFY((execLog[0] == 0), "execLog[0]=%u", execLog[0]);
        setResultOk();
        break;
    case Unlock:
        VERIFY((execLog[0] == 1), "execLog[0]=%u", execLog[0]);
        setResultOk();
        break;
    default:
        TestSuite::timeout();
    }
}
