#include "NodeConfiguration.h"
#include "reflex/MachineDefinitions.h"

namespace reflex
{
NodeConfiguration system;
}

using namespace reflex;

int main()
{
    system.scheduler.start();
    return (0);
}
