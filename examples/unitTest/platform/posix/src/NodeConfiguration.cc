#include "NodeConfiguration.h"

using namespace reflex;
using namespace unitTest;

#if defined FIFO_SCHEDULING

#include "LinkedListTest.h"
#include "FifoSchedulerTest.h"

const TestCase NodeConfiguration::testList[] =
{
    TESTCASE(LinkedListTest, BasicTests),
    TESTCASE(LinkedListTest, Append),
    TESTCASE(LinkedListTest, Remove),
    TESTCASE(LinkedListTest, Insert),
    TESTCASE(LinkedListTest, TakeFirst),
    TESTCASE(FifoSchedulerTest, ExecutionOrder),
    TESTCASE(FifoSchedulerTest, MultipleExecution),
    TESTCASE(FifoSchedulerTest, Lock),
    TESTCASE(FifoSchedulerTest, Unlock)
};

#elif defined EDF_SCHEDULING_NONPREEMPTIVE

#include "EdfSchedulerTest.h"

const TestCase NodeConfiguration::testList[] =
{
    TESTCASE(EdfSchedulerTest, ExecutionOrder),
    TESTCASE(EdfSchedulerTest, MultipleExecution),
    TESTCASE(EdfSchedulerTest, Lock),
    TESTCASE(EdfSchedulerTest, Unlock),
    TESTCASE(EdfSchedulerTest, TriggerTightDeadlineAfterLoose),
    TESTCASE(EdfSchedulerTest, TriggerLooseDeadlineAfterTight),
    TESTCASE(EdfSchedulerTest, SchedulingMonitor)
};

#elif defined EDF_SCHEDULING  || defined EDF_SCHEDULING_SIMPLE

#include "EdfSchedulerTest.h"

const TestCase NodeConfiguration::testList[] =
{
    TESTCASE(EdfSchedulerTest, ExecutionOrder),
    TESTCASE(EdfSchedulerTest, MultipleExecution),
    TESTCASE(EdfSchedulerTest, Lock),
    TESTCASE(EdfSchedulerTest, Unlock),
    TESTCASE(EdfSchedulerTest, TriggerTightDeadlineAfterLoose),
    TESTCASE(EdfSchedulerTest, TriggerLooseDeadlineAfterTight),
    TESTCASE(EdfSchedulerTest, SchedulingMonitor),
    TESTCASE(EdfSchedulerTest, LooseDL_Interrupt_TightDL),
    TESTCASE(EdfSchedulerTest, TightDL_Interrupt_LooseDL)
};

#elif defined PRIORITY_SCHEDULING_NONPREEMPTIVE

#include "PrioritySchedulerTest.h"

const TestCase NodeConfiguration::testList[] =
{
    TESTCASE(PrioritySchedulerTest, ExecutionOrder),
    TESTCASE(PrioritySchedulerTest, MultipleExecution),
    TESTCASE(PrioritySchedulerTest, Lock),
    TESTCASE(PrioritySchedulerTest, Unlock),
    TESTCASE(PrioritySchedulerTest, TriggerHighPriorityAfterLow),
    TESTCASE(PrioritySchedulerTest, TriggerLowPriorityAfterHigh),
    TESTCASE(PrioritySchedulerTest, SchedulingMonitor)
};

#elif defined PRIORITY_SCHEDULING_NONPREEMPTIVE \
        || defined PRIORITY_SCHEDULING \
        || defined PRIORITY_SCHEDULING_SIMPLE

#include "PrioritySchedulerTest.h"

const TestCase NodeConfiguration::testList[] =
{
    TESTCASE(PrioritySchedulerTest, ExecutionOrder),
    TESTCASE(PrioritySchedulerTest, MultipleExecution),
    TESTCASE(PrioritySchedulerTest, Lock),
    TESTCASE(PrioritySchedulerTest, Unlock),
    TESTCASE(PrioritySchedulerTest, TriggerHighPriorityAfterLow),
    TESTCASE(PrioritySchedulerTest, TriggerLowPriorityAfterHigh),
    TESTCASE(PrioritySchedulerTest, SchedulingMonitor),
    TESTCASE(PrioritySchedulerTest, LowPrioAct_HighPrioAct),
    TESTCASE(PrioritySchedulerTest, LowPrioAct_Interrupt_HighPrioAct),
    TESTCASE(PrioritySchedulerTest, HighPrioAct_Interrupt_LowPrioAct)
};

#elif defined TIME_TRIGGERED_SCHEDULING

#include "TimeTriggeredSchedulerTest.h"

const TestCase NodeConfiguration::testList[] =
{
    TESTCASE(TimeTriggeredSchedulerTest, ExecutionOrder),
    TESTCASE(TimeTriggeredSchedulerTest, SingleTriggerSingleSlot),
    TESTCASE(TimeTriggeredSchedulerTest, SingleTriggerMultiSlot),
    TESTCASE(TimeTriggeredSchedulerTest, MultiTriggerSingleSlot),
    TESTCASE(TimeTriggeredSchedulerTest, MultiTriggerMultiSlot),
    TESTCASE(TimeTriggeredSchedulerTest, Lock),
    TESTCASE(TimeTriggeredSchedulerTest, Unlock)
};

#endif

namespace reflex {

NodeConfiguration::NodeConfiguration()
{
    testEnvironment.setOut_message(&console);
    console.set_out_txFinished(testEnvironment.in_txFinished());
    testEnvironment.setTestList(&testList[0], sizeof(testList)/sizeof(unitTest::TestCase));

    testEnvironment.start();
}

}
