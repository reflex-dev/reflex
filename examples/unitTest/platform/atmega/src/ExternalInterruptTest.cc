#include "ExternalInterruptTest.h"
#include "reflex/memory/Flash.h"

using namespace reflex;
using namespace atmega;
using namespace unitTest;

ExternalInterruptTest::ExternalInterruptTest(uint16 id, InterruptPin intPin, PortPin portPin) :
	TestSuite(id), act(*this)
{
	input.init(&act);
	interrupt.out_event = &input;
	counts = 0;
	this->portPin = portPin;

	if (currentTestId() >= NrOfTestCases)
	{
		setResultSkipped();
		return;
	}

	setTimeoutMs(500);
	switch (currentTestId())
	{
	case Registers:
		interrupt.setPin(intPin);
		VERIFY(interrupt.pin() == intPin, READ_FLASH("setPin()"));
		interrupt.setEvent(ExternalInterrupt::AnyEdge);
		VERIFY(interrupt.event() == ExternalInterrupt::AnyEdge, READ_FLASH("AnyEdge"))
		interrupt.setEvent(ExternalInterrupt::FallingEdge);
		VERIFY(interrupt.event() == ExternalInterrupt::FallingEdge, READ_FLASH("FallingEdge"))
		interrupt.setEvent(ExternalInterrupt::RisingEdge);
		VERIFY(interrupt.event() == ExternalInterrupt::RisingEdge, READ_FLASH("RisingEdge"))
		interrupt.setEvent(ExternalInterrupt::LowLevel);
		VERIFY(interrupt.event() == ExternalInterrupt::LowLevel, READ_FLASH("LowLevel"))
		VERIFY(interrupt.pin() == intPin, READ_FLASH("setPin() after levels"))
		setResultOk();
		break;
	case RisingEdge:
		interrupt.setPin(intPin);
		interrupt.setEvent(ExternalInterrupt::RisingEdge);
		InterruptPort()->DDR |= portPin;
		InterruptPort()->PORT &= ~portPin;
		//interrupt.switchOn();
		InterruptPort()->PORT &= ~portPin;
		InterruptPort()->PORT &= ~portPin;
		InterruptPort()->PORT &= ~portPin;
		InterruptPort()->PORT |= portPin;
		InterruptPort()->PORT |= portPin;
		InterruptPort()->PORT |= portPin;
		break;
	case FallingEdge:
		interrupt.setPin(intPin);
		interrupt.setEvent(ExternalInterrupt::FallingEdge);
		InterruptPort()->DDR |= portPin;
		InterruptPort()->PORT |= portPin;
		interrupt.switchOn();
		InterruptPort()->PORT |= portPin;
		InterruptPort()->PORT |= portPin;
		InterruptPort()->PORT |= portPin;
		InterruptPort()->PORT &= ~portPin;
		InterruptPort()->PORT &= ~portPin;
		InterruptPort()->PORT &= ~portPin;
		break;
	case AnyEdge:
		interrupt.setPin(intPin);
		interrupt.setEvent(ExternalInterrupt::AnyEdge);
		InterruptPort()->DDR |= portPin;
		InterruptPort()->PORT &= ~portPin;
		interrupt.switchOn();
		InterruptPort()->PORT &= ~portPin;
		InterruptPort()->PORT |= portPin;
		InterruptPort()->PORT |= portPin;
		InterruptPort()->PORT |= portPin;
		InterruptPort()->PORT &= ~portPin;
		InterruptPort()->PORT &= ~portPin;
		InterruptPort()->PORT &= ~portPin;
		break;
	case LowLevel:
		interrupt.setPin(intPin);
		interrupt.setEvent(ExternalInterrupt::FallingEdge);
		InterruptPort()->DDR |= portPin;
		InterruptPort()->PORT |= portPin;
		interrupt.switchOn();
		InterruptPort()->PORT |= portPin;
		InterruptPort()->PORT |= portPin;
		InterruptPort()->PORT |= portPin;
		InterruptPort()->PORT &= ~portPin;
		InterruptPort()->PORT &= ~portPin;
		InterruptPort()->PORT &= ~portPin;
		InterruptPort()->PORT |= portPin;
		break;
	default:
		SKIP()
		;
		break;
	}
}

ExternalInterruptTest::~ExternalInterruptTest()
{
	interrupt.switchOff();
	InterruptPort()->PORT &= ~portPin;
	InterruptPort()->DDR &= ~portPin;
}

void ExternalInterruptTest::run_event()
{
	switch (currentTestId())
	{
	case AnyEdge:
		counts++;
		if (counts == 2)
		{
			setResultOk();
		}
		break;
	case LowLevel:
		counts++;
		break;
	case Registers:
		VERIFY(false, READ_FLASH("unexpected interrupt"))
		break;
	default:
		setResultOk();
	}
}

void ExternalInterruptTest::timeout()
{
	if ((currentTestId() == LowLevel) && (counts > 0))
	{
		setResultOk();
	}
	else
	{
		TestSuite::timeout();
	}
}
