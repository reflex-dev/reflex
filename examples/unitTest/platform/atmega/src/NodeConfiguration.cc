#include "NodeConfiguration.h"
#include "ExternalInterruptTest.h"
#include "PinChangeInterruptTest.h"
#include "PortTest.h"
#include "SpiTest.h"
#include "TestCase.h"

#include "reflex/memory/Flash.h"

using namespace reflex;
using namespace mcu;
using namespace unitTest;

typedef PinChangeInterruptTest<Core::PortB> PCInt0To7Test;

#if defined FIFO_SCHEDULING

#include "FifoSchedulerTest.h"

// Selftest for atmega328 on the arduino
template<>
const TestCase NodeConfiguration<Atmega328>::testList[] IN_FLASH =
{
    TESTCASE(FifoSchedulerTest, ExecutionOrder),
    TESTCASE(FifoSchedulerTest, MultipleExecution),
    TESTCASE(FifoSchedulerTest, Lock),
    TESTCASE(FifoSchedulerTest, Unlock)
};

// Selftest for atmega2560 on the STK600
template<>
const TestCase NodeConfiguration<Atmega2560>::testList[] IN_FLASH =
{
    TESTCASE(FifoSchedulerTest, ExecutionOrder),
    TESTCASE(FifoSchedulerTest, MultipleExecution),
    TESTCASE(FifoSchedulerTest, Lock),
    TESTCASE(FifoSchedulerTest, Unlock),
    { "SpiTest::Registers", 	SpiTest::RegisterTest, 		&create<SpiTest> },
    { "SpiTestLoopback",		SpiTest::LoopbackTest,		&create<SpiTest> },
    { "PortTest::ABRegisters", 	0 ,							&create<PortTest, Core::PortA, Core::PortB, AllPins> },
    { "PortTest::AB0x01Port1ToPort2", 1 , 					&create<PortTest, Core::PortA, Core::PortB, 0x01> },
    { "PCInt0::Registers",      PCInt0To7Test::Registers,
            &create<PinChangeInterruptTest, Core::PortB, PinChangeInterrupt::PinGroup0, AllPins>  },
    { "PCInt0::SinglePinChange", PCInt0To7Test::SinglePinChange,
            &create<PinChangeInterruptTest, Core::PortB, PinChangeInterrupt::PinGroup0, Pin0>  }
};


// Selftest for atmega644 / atmega1284 on the pollin board http://www.pollin.de/shop/downloads/D810038B.PDF
template<>
const TestCase NodeConfiguration<Atmega644>::testList[] IN_FLASH =
{
    TESTCASE(FifoSchedulerTest, ExecutionOrder),
    TESTCASE(FifoSchedulerTest, MultipleExecution),
    TESTCASE(FifoSchedulerTest, Lock),
    TESTCASE(FifoSchedulerTest, Unlock),
    { "PortAB::Registers", 	PortTest<Core::PortA, Core::PortB>::Registers,
            &create<PortTest, Core::PortA, Core::PortB, AllPins> },
    // Test all pins but RXD and TXD
    { "PortCD::Registers", 	PortTest<Core::PortC, Core::PortD>::Registers,
            &create<PortTest, Core::PortC, Core::PortD, AllPins & ~Pin0 & ~Pin1> },
    { "PCInt0..7::Registers",      PCInt0To7Test::Registers,
            &create<PinChangeInterruptTest, Core::PortB, PinChangeInterrupt::PinGroup0, AllPins>  },
    { "PCInt0::SinglePinChange", PCInt0To7Test::SinglePinChange,
            &create<PinChangeInterruptTest, Core::PortB, PinChangeInterrupt::PinGroup0, Pin0>  },
    { "PCInt1::SinglePinChange", PCInt0To7Test::SinglePinChange,
            &create<PinChangeInterruptTest, Core::PortB, PinChangeInterrupt::PinGroup0, Pin1>  },
    { "PCInt2::SinglePinChange", PCInt0To7Test::SinglePinChange,
            &create<PinChangeInterruptTest, Core::PortB, PinChangeInterrupt::PinGroup0, Pin2>  },
    { "PCInt3::SinglePinChange", PCInt0To7Test::SinglePinChange,
            &create<PinChangeInterruptTest, Core::PortB, PinChangeInterrupt::PinGroup0, Pin3>  },
    { "PCInt4::SinglePinChange", PCInt0To7Test::SinglePinChange,
            &create<PinChangeInterruptTest, Core::PortB, PinChangeInterrupt::PinGroup0, Pin4>  },
    { "PCInt5::SinglePinChange", PCInt0To7Test::SinglePinChange,
            &create<PinChangeInterruptTest, Core::PortB, PinChangeInterrupt::PinGroup0, Pin5>  },
    { "PCInt6::SinglePinChange", PCInt0To7Test::SinglePinChange,
            &create<PinChangeInterruptTest, Core::PortB, PinChangeInterrupt::PinGroup0, Pin6>  },
    { "PCInt7::SinglePinChange", PCInt0To7Test::SinglePinChange,
            &create<PinChangeInterruptTest, Core::PortB, PinChangeInterrupt::PinGroup0, Pin7>  },
    { "INT0Test::Registers",    ExternalInterruptTest::Registers,
            &create<ExternalInterruptTest, ExternalInterrupt::INT0, Pin2> },
    { "INT0Test::RisingEdge",   ExternalInterruptTest::RisingEdge,
            &create<ExternalInterruptTest, ExternalInterrupt::INT0, Pin2> },
    { "INT0Test::FallingEdge",   ExternalInterruptTest::FallingEdge,
            &create<ExternalInterruptTest, ExternalInterrupt::INT0, Pin2> },
    { "INT0Test::AnyEdge",   ExternalInterruptTest::AnyEdge,
            &create<ExternalInterruptTest, ExternalInterrupt::INT0, Pin2> },
    { "INT0Test::LowLevel",  ExternalInterruptTest::LowLevel,
            &create<ExternalInterruptTest, ExternalInterrupt::INT0, Pin2> },
    { "INT1Test::Registers",    ExternalInterruptTest::Registers,
            &create<ExternalInterruptTest, ExternalInterrupt::INT1, Pin3> },
    { "INT1Test::RisingEdge",   ExternalInterruptTest::RisingEdge,
            &create<ExternalInterruptTest, ExternalInterrupt::INT1, Pin3> },
    { "INT1Test::FallingEdge",   ExternalInterruptTest::FallingEdge,
            &create<ExternalInterruptTest, ExternalInterrupt::INT1, Pin3> },
    { "INT1Test::AnyEdge",   ExternalInterruptTest::AnyEdge,
            &create<ExternalInterruptTest, ExternalInterrupt::INT1, Pin3> },
    { "INT1Test::LowLevel",  ExternalInterruptTest::LowLevel,
            &create<ExternalInterruptTest, ExternalInterrupt::INT1, Pin3> },
    { "PortTest::MosiToMiso", 	PortTest<Core::PortB, Core::PortB>::Port1ToPort2,
            &create<PortTest, Core::PortB, Core::PortB, Pin5, Pin6> },
    { "SpiTest::Registers", 	SpiTest::RegisterTest, 		&create<SpiTest> },
    { "SpiTest::Loopback",		SpiTest::LoopbackTest,		&create<SpiTest> },
};

#elif defined PRIORITY_SCHEDULING_NONPREEMPTIVE

#include "PrioritySchedulerTest.h"

template<mcu::McuType mcuType>
const TestCase NodeConfiguration<mcuType>::testList[] IN_FLASH =
{
    TESTCASE(PrioritySchedulerTest, ExecutionOrder),
    TESTCASE(PrioritySchedulerTest, MultipleExecution),
    TESTCASE(PrioritySchedulerTest, Lock),
    TESTCASE(PrioritySchedulerTest, Unlock),
    TESTCASE(PrioritySchedulerTest, TriggerHighPriorityAfterLow),
    TESTCASE(PrioritySchedulerTest, TriggerLowPriorityAfterHigh),
    TESTCASE(PrioritySchedulerTest, SchedulingMonitor)
};

#elif defined PRIORITY_SCHEDULING \
        || defined PRIORITY_SCHEDULING_SIMPLE

#include "PrioritySchedulerTest.h"

template<mcu::McuType mcuType>
const TestCase NodeConfiguration<mcuType>::testList[] IN_FLASH =
{
    TESTCASE(PrioritySchedulerTest, ExecutionOrder),
    TESTCASE(PrioritySchedulerTest, MultipleExecution),
    TESTCASE(PrioritySchedulerTest, Lock),
    TESTCASE(PrioritySchedulerTest, Unlock),
    TESTCASE(PrioritySchedulerTest, TriggerHighPriorityAfterLow),
    TESTCASE(PrioritySchedulerTest, TriggerLowPriorityAfterHigh),
    TESTCASE(PrioritySchedulerTest, SchedulingMonitor),
    TESTCASE(PrioritySchedulerTest, LowPrioAct_HighPrioAct),
    TESTCASE(PrioritySchedulerTest, LowPrioAct_Interrupt_HighPrioAct),
    TESTCASE(PrioritySchedulerTest, HighPrioAct_Interrupt_LowPrioAct)
};

#elif defined EDF_SCHEDULING_NONPREEMPTIVE

#include "EdfSchedulerTest.h"

template<mcu::McuType mcuType>
const TestCase NodeConfiguration<mcuType>::testList[] IN_FLASH =
{
    TESTCASE(EdfSchedulerTest, ExecutionOrder),
    TESTCASE(EdfSchedulerTest, MultipleExecution),
    TESTCASE(EdfSchedulerTest, Lock),
    TESTCASE(EdfSchedulerTest, Unlock),
    TESTCASE(EdfSchedulerTest, TriggerTightDeadlineAfterLoose),
    TESTCASE(EdfSchedulerTest, TriggerLooseDeadlineAfterTight),
    TESTCASE(EdfSchedulerTest, SchedulingMonitor)
};

#elif defined EDF_SCHEDULING \
        || defined EDF_SCHEDULING_SIMPLE

#include "EdfSchedulerTest.h"

template<mcu::McuType mcuType>
const TestCase NodeConfiguration<mcuType>::testList[] IN_FLASH =
{
    TESTCASE(EdfSchedulerTest, ExecutionOrder),
    TESTCASE(EdfSchedulerTest, MultipleExecution),
    TESTCASE(EdfSchedulerTest, Lock),
    TESTCASE(EdfSchedulerTest, Unlock),
    TESTCASE(EdfSchedulerTest, TriggerTightDeadlineAfterLoose),
    TESTCASE(EdfSchedulerTest, TriggerLooseDeadlineAfterTight),
    TESTCASE(EdfSchedulerTest, SchedulingMonitor),
    TESTCASE(EdfSchedulerTest, LooseDL_Interrupt_TightDL),
    TESTCASE(EdfSchedulerTest, TightDL_Interrupt_LooseDL)
};

#elif defined TIME_TRIGGERED_SCHEDULING

#include "TimeTriggeredSchedulerTest.h"

template<mcu::McuType mcuType>
const TestCase NodeConfiguration<mcuType>::testList[] IN_FLASH =
{
    TESTCASE(TimeTriggeredSchedulerTest, ExecutionOrder),
    TESTCASE(TimeTriggeredSchedulerTest, SingleTriggerSingleSlot),
    TESTCASE(TimeTriggeredSchedulerTest, SingleTriggerMultiSlot),
    TESTCASE(TimeTriggeredSchedulerTest, MultiTriggerSingleSlot),
    TESTCASE(TimeTriggeredSchedulerTest, MultiTriggerMultiSlot),
    TESTCASE(TimeTriggeredSchedulerTest, Lock),
    TESTCASE(TimeTriggeredSchedulerTest, Unlock)
};

#endif


/* Initialization of NodeConfiguration */
namespace reflex
{
template<>
NodeConfiguration<CurrentMcu>::NodeConfiguration()
{
    for (volatile uint32 i = 0; i < 5*65535; i++)
    {
        // delay
    }
    usart.setBaudRate(57600, 16000000);
	usart.rxHandler().switchOn();
    testEnvironment.setTestList(&testList[0], sizeof(testList) / sizeof(TestCase));
    testEnvironment.setOut_message(usart.get_in_tx());
    usart.set_out_txFinished(testEnvironment.in_txFinished());
    testEnvironment.start();
}
}
