#include "SpiTest.h"

#include "reflex/memory/Flash.h"

using namespace reflex;
using namespace mcu;
using namespace unitTest;

SpiTest::SpiTest(uint16 id) :
	TestSuite(id)
{
	switch (currentTestId())
	{
	case RegisterTest:
		spi.setDivider(spi.OSCdiv128);
		VERIFY(spi.divider() == spi.OSCdiv128, READ_FLASH("OSCdiv128"))
		spi.setDivider(spi.OSCdiv64);
		VERIFY(spi.divider() == spi.OSCdiv64, READ_FLASH("OSCdiv64"))
		spi.setDivider(spi.OSCdiv32);
		VERIFY(spi.divider() == spi.OSCdiv32, READ_FLASH("OSCdiv32"))
		spi.setDivider(spi.OSCdiv16);
		VERIFY(spi.divider() == spi.OSCdiv16, READ_FLASH("OSCdiv16"))
		spi.setDivider(spi.OSCdiv8);
		VERIFY(spi.divider() == spi.OSCdiv8, READ_FLASH("OSCdiv8"))
		spi.setDivider(spi.OSCdiv4);
		VERIFY(spi.divider() == spi.OSCdiv4, READ_FLASH("OSCdiv4"))
		spi.setDivider(spi.OSCdiv2);
		VERIFY(spi.divider() == spi.OSCdiv2, READ_FLASH("OSCdiv2"))
		spi.setDataMode(spi.Mode0);
		VERIFY(spi.dataMode() == spi.Mode0, READ_FLASH("Mode0"))
		spi.setDataMode(spi.Mode1);
		VERIFY(spi.dataMode() == spi.Mode1, READ_FLASH("Mode1"))
		spi.setDataMode(spi.Mode2);
		VERIFY(spi.dataMode() == spi.Mode2, READ_FLASH("Mode2"))
		spi.setDataMode(spi.Mode3);
		VERIFY(spi.dataMode() == spi.Mode3, READ_FLASH("Mode3"))
		setResultOk();
		break;
	case LoopbackTest:
	{
		spi.setDivider(spi.OSCdiv128);
		spi.setDataMode(spi.Mode0);
		uint8 data = DataSet0;
		spi.readWrite(&data, sizeof(data));
		VERIFY(data == DataSet0, READ_FLASH("sent %x, received 0x%X"), DataSet0, data);
		data = DataSet1;
		spi.setCsEnabled(true);
		spi.readWrite(&data, sizeof(data));
		spi.setCsEnabled(false);
		VERIFY(data == DataSet1, READ_FLASH("sent %x, received: 0x%X"), DataSet1, data);
	}
		setResultOk();
		break;
	default:
		setResultSkipped();
		break;
	}
}

SpiTest::~SpiTest()
{

}
