#ifndef EXTERNALINTERRUPTTEST_H_
#define EXTERNALINTERRUPTTEST_H_

#include "TestSuite.h"
#include "reflex/io/ExternalInterrupt.h"
#include "reflex/io/IOPort.h"
#include "reflex/scheduling/ActivityFunctor.h"
#include "reflex/sinks/Event.h"

namespace unitTest
{

typedef reflex::mcu::ExternalInterrupt::Pin InterruptPin;
typedef reflex::mcu::PortPin PortPin;


/*!
 \ingroup unitTest
 \brief Tests the reflex::atmega::ExternalInterrupt driver of the ATMEGA.

 */
class ExternalInterruptTest: public TestSuite
{
public:
	enum TestCases
	{
		Registers = 0, FallingEdge, RisingEdge, AnyEdge, LowLevel, NrOfTestCases
	};

	ExternalInterruptTest(uint16 id,  InterruptPin intPin, PortPin portPin );
	~ExternalInterruptTest();

private:
	void run_event();
	void timeout();

	reflex::atmega::ExternalInterrupt interrupt;
	reflex::Event input;
	reflex::ActivityFunctor<ExternalInterruptTest, &ExternalInterruptTest::run_event> act;

	typedef reflex::atmega::IOPort<reflex::atmega::Core::PortD> InterruptPort;

	uint8 counts;
	PortPin portPin;

};

template<class ExternalInterruptTest, InterruptPin intPin, PortPin portPin>
TestSuite* create(void* addr, uint16 id) {
	return new (addr) ExternalInterruptTest(id, intPin, portPin);
}

}

#endif /* EXTERNALINTERRUPTTEST_H_ */
