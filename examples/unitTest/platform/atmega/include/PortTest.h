#ifndef TESTPORT_H_
#define TESTPORT_H_

#include "reflex/memory/Flash.h"
#include "reflex/io/IOPort.h"

#include "TestSuite.h"

namespace unitTest
{

typedef reflex::atmega::Core::IOPorts IOPorts;
typedef reflex::atmega::PortPins PortPins;
typedef reflex::atmega::PortPin PortPin;

/*!
 \ingroup unitTest
 \brief Tests reflex::atmega::IOPort functionality on the AMTEGA.

 For this test, Port1 must be connected to Port2. Alternating bitmasks are then written to Port1 which
 have to be read successfully by Port2.

 */
template<IOPorts Port1, IOPorts Port2>
class PortTest: public TestSuite
{
public:
	enum TestCases
	{
		Registers = 0, Port1ToPort2, NrOfTestCases
	};

	PortTest(uint16 id, uint8 out, uint8 in);
	~PortTest();
private:
	uint8 out;
	uint8 in;
};

/*!
\cond
\ingroup unitTest
\brief Overloads the default generator function with more parameters.

This makes it possible to configure test cases during compile-time.
\endcond
*/
template<template<IOPorts, IOPorts> class PortTest, IOPorts Port1, IOPorts Port2, uint8 out, uint8 in = out>
TestSuite* create(void* addr, uint16 id) { return new (addr) PortTest<Port1, Port2>(id, out, in); }


template<IOPorts Port1, IOPorts Port2>
PortTest<Port1, Port2>::PortTest(uint16 id, uint8 out, uint8 in) :
	TestSuite(id)
{
	using namespace reflex;
	using namespace atmega;

	this->in = in;
	this->out = out;

	switch (currentTestId())
	{
	case Registers:
		IOPort<Port1>()->DDR |= out;
		VERIFY(((IOPort<Port1>()->DDR & out) == in), READ_FLASH("Port1->DDR expected 0x%X but read 0x%X"), in, IOPort<Port1>()->PIN & in);
		IOPort<Port2>()->DDR |= in;
		VERIFY(((IOPort<Port2>()->DDR & in) == in), READ_FLASH("Port2->DDR expected 0x%X but read 0x%X"), in, IOPort<Port2>()->PIN & in);
		setResultOk();
		break;
	case Port1ToPort2:
		IOPort<Port1>()->DDR |= out;
		IOPort<Port2>()->DDR &= ~in; // configure Port2 as input port
		IOPort<Port1>()->PORT |= out; // write a bitmask to Port1
		VERIFY(((IOPort<Port2>()->PIN & in) == in), READ_FLASH("Port2->PIN expected 0x%X but read 0x%X"), in, IOPort<Port2>()->PIN & in);
		setResultOk();
		break;
	default:
		setResultSkipped();
		break;
	}
}


template<IOPorts Port1, IOPorts Port2>
PortTest<Port1, Port2>::~PortTest()
{
	using namespace reflex;
	using namespace atmega;
	IOPort<Port1>()->PIN &= ~out;
	IOPort<Port1>()->PORT &= ~out;
	IOPort<Port1>()->DDR &= ~out;
	IOPort<Port2>()->PIN &= ~in;
	IOPort<Port2>()->PORT &= ~in;
	IOPort<Port2>()->DDR &= ~in;
}

}

#endif /* TESTPORT_H_ */
