#ifndef Configuration_h
#define Configuration_h

// the type for the network address
typedef unsigned char NetAddress;

// different system constants
enum { 	
	PacketPayload = 80,
	IOBufferSize = 100,   	//used for OutputChannel
	NrOfStdOutBuffers = 20,
	MaxPoolCount=2
	};//amount of buffers in OutputChannel


//! maximum number of nodes in OMNETPP omnetpp.ini
#define NODECOUNT 226

#define TIMERFACTOR 1000

#endif

