/*
 * NodeConfiguration.cpp
 *
 *  Created on: Oct 5, 2010
 *      Author: shoeckne
 */

#include "NodeConfiguration.h"

using namespace reflex;

Define_Module(NodeConfiguration);

NodeConfiguration::NodeConfiguration() : pool(20) //stacksize
    , pingPong(ReflexBaseApp::currentId())
    , radio()
{
    radio.connect_out_upperLayer(pingPong.get_input());
    radio.connect_pool(&pool);

    pingPong.connect_Pool(&pool);
    pingPong.connect_output(radio.get_in_upperLayer());
}

void NodeConfiguration::initialize()
{
    ReflexBaseApp::initialize();
    pingPong.initialize();
}

void NodeConfiguration::finish()
{
    ReflexBaseApp::finish();
    pingPong.finish();
}

void NodeConfiguration::callEndSimulation()
{
    currentModule()->endSimulation();
}

SimTime NodeConfiguration::getSimTime()
{
    return simTime();
}
