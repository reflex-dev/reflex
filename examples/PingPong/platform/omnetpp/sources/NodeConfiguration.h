/*
 * NodeConfiguration.h
 *
 *  Created on: Oct 5, 2010
 *      Author: Soeren Hoeckner
 */

#ifndef NODECONFIGURATION_H_
#define NODECONFIGURATION_H_

#include "reflex/types.h"
#include "omnetpp/ReflexBaseApp.h"

#include "reflex/memory/PoolManager.h"
#include "reflex/memory/SizedPool.h"
#include "reflex/io/OMNeTRadio.h"
#include "PingPong.h"

class NodeConfiguration : public ReflexBaseApp
{
public:
    /**	Memory Pool
     *	< BufferType , Number of Buffers >
     */
    reflex::PoolManager poolManager;
    reflex::SizedPool<IOBufferSize, NrOfStdOutBuffers> pool;

    PingPong pingPong;
    OMNeTRadio radio;

public:
    NodeConfiguration();

    void callEndSimulation();
    void finish();
    void initialize();
    SimTime getSimTime();
};

#endif /* NODECONFIGURATION_H_ */
