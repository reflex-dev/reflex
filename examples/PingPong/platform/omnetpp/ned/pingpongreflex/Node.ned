//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

package pingpongreflex;


import org.mixim.base.modules.BaseBattery;
import org.mixim.modules.mac.csma;
import org.mixim.modules.netw.WiseRoute;
import org.mixim.modules.nic.IWirelessNicUpperCtrl;
import org.mixim.base.modules.IBaseArp;
import org.mixim.base.modules.IBaseApplLayer;
import org.mixim.base.modules.IBaseNetwLayer;
import org.mixim.modules.power.battery.BatteryStats;
import org.mixim.modules.power.battery.SimpleBattery;


//
// TODO auto-generated module
//
module Node
{
    parameters:
        string networkType = default("BaseNetwLayer");       //type of the network layer
        string applicationType = default("BurstApplLayer");  //type of the application layer
        string mobilityType = default("StationaryMobility"); //type of the mobility module
        string arpType = default("BaseArp");                 //type of address resolution module
        string nicType = default("NicCSMA");
        
        @display("bgb=249,439,white;i=device/palm;i2=status/battery;b=40,40,rect");
        @node();
    gates:
        input radioIn; // gate for sendDirect

    submodules:
        arp: <arpType> like IBaseArp {
            @display("p=189,175;i=block/network2");
        }
        mobility: <mobilityType> like inet.mobility.IMobility {
            parameters:
                @display("p=189,115;i=block/cogwheel");
        }
        nic: <nicType> like IWirelessNicUpperCtrl {
            parameters:
                @display("p=120,300;i=block/wrxtx,#008000");
        }
        netwl: <networkType> like IBaseNetwLayer {
            parameters:
                @display("p=120,236;i=block/layer");
        }
        appl: <applicationType> like IBaseApplLayer if applicationType != "" {
            parameters:
                @display("p=119,35;i=app");
        }
        batteryStats: BatteryStats {
            @display("p=189,300;i=block/table,#FF8040");
        }
        battery: SimpleBattery {
            @display("p=189,236;i=block/plug,#FF8000");
        }
    connections:

		// netwl <-->nic
        netwl.lowerLayerOut --> nic.upperLayerIn;
        nic.upperLayerOut --> netwl.lowerLayerIn;
        netwl.lowerControlOut --> nic.upperControlIn;
        nic.upperControlOut --> netwl.lowerControlIn;

		// radio to world
        radioIn --> nic.radioIn;

		// netwl <--> appl
        netwl.upperControlOut --> appl.lowerControlIn;
        netwl.upperLayerOut --> appl.lowerLayerIn;
        appl.lowerLayerOut --> netwl.upperLayerIn;
        appl.lowerControlOut --> netwl.upperControlIn;

}
