/*
 * PingPong.cc
 *
 *  Created on: July 8, 2014
 *        Author: slohs
 */

#include "PingPong.h"
#include "NodeConfiguration.h"

PingPong::PingPong(int nodeId)
	: nodeId( nodeId ) 
{
}

void PingPong::initialize()
{
	timer.set(nodeId + 1 * 1000);
}


void PingPong::timerHandle_Impl()
{
	ping();
}

void PingPong::receiveHandle_Impl()
{
	pong();
}

void PingPong::ping() {

	reflex::Buffer* buffer = new(pool) reflex::Buffer(pool);
	
	if (!buffer)
	{
		return;	
	}

	buffer->write('P');
	buffer->write('i');
	buffer->write('n');
	buffer->write('g');
	buffer->write(nodeId);

	if( output )
	{
		output->assign(buffer);
	}
	else
	{
		buffer->downRef();
	}

}

void PingPong::pong() {
	reflex::Buffer* buffer = input.get();

	if (buffer)
	{
		char ping[4];
		int senderId;

		buffer->read(ping);
		buffer->read(senderId);
		buffer->downRef();

		if (ping[1] == 'i') // received a ping message
		{
			buffer = new (pool) reflex::Buffer(pool);

			buffer->write('P');
			buffer->write('o');
			buffer->write('n');
			buffer->write('g');
			buffer->write(senderId);

			if (output)
			{
				output->assign(buffer);
			}
			else
			{
				buffer->downRef();
			}
		}	
		else if (ping[1] == 'o')
		{
		}
	}
}

void PingPong::finish()
{
}
