import qbs 1.0
import ReflexApplication
import ReflexProject

ReflexProject {
    ReflexApplication {
        name: "HelloWorld"

        files: [
            "src/HelloWorld.cc",
            "src/main.cc",
        ]

        cpp.includePaths : [
            "include",
            "platform/" + reflex_platform + "/include"
        ]
    }
}
