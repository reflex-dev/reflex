#ifndef NodeConfiguration_h
#define NodeConfiguration_h
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(ses):	 Application
 *
 *	Author:		 Karsten Walther
 *
 *	Description: Implements the System
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */

#include "reflex/types.h"

//#include "reflex/io/Serial.h"

#include "reflex/io/OutputChannel.h"
#include "reflex/memory/PoolManager.h"
#include "reflex/memory/SizedPool.h"

#include "reflex/System.h"
#include "reflex/usci/USCI_UART.h"
#include "reflex/io/SoftUART.h"
#include "reflex/io/Display.h"

#include "reflex/timer/Registers.h"
#include "reflex/io/Ports.h"

#include "HelloWorld.h"
#include "BufferSplicer.h"
#include "ScrollText.h"
#include "conf.h"

namespace reflex {

using msp430x::usci::USCI_UART;
using msp430x::usci::RegistersA0;
using msp430x::timer_a::Registers_A3;
using msp430x::Port2;
using msp430x::SoftUART;

/**Define different powergroups for an application. To use the available groups provided by the powermanager are highly 
 * recommended.
 */
enum PowerGroups {
	DEFAULT = reflex::PowerManager::GROUP1,
	DISABLED = reflex::PowerManager::DISABLED
};

class NodeConfiguration 
	: public System
{
public:
	NodeConfiguration() 
		: System()
		, pool(0) ///< the Buffer is only used as FiFo. So stacksize is 0 @see Buffer
		, out(&pool)
        , hello(&out)
	{
		// connection of components
		out.init(splice.get_in_buffer());
		splice.set_out_byte(uart.get_in_tx());
		uart.set_out_tx_ready(splice.get_in_next());
		uart.set_out_rx(screen.get_in_char());
		screen.set_out_display(&display.getLowerLine());

		// power management group assignment
		timer.setGroups(DEFAULT); // put system timer in default group
		display.setGroups(DEFAULT);
		uart.setGroups(DEFAULT); // enable receive

		powerManager.enableGroup(DEFAULT); // this enables all registered entities in DEFAULT group (starts system timer)
		powerManager.disableGroup(~DEFAULT); // disable all others
	}

	void toggleLed()
	{
		static uint16 count=0;
		display.getUpperLine() = count++;
	}

	PoolManager poolManager;
	SizedPool<IOBufferSize,NrOfStdOutBuffers> pool; ///< a pool of bufferobject with static size

	USCI_UART<RegistersA0> uart; ///< USCI in UART mode, 9600 baud serial interface
//	SoftUART<Registers_A3, Port2, 1 /* P2.1 = TX */, 2 /* P2.2 = RX */> uart; ///< Software UART 9600 baud

	BufferSplicer splice; ///< splice buffer into single bytes for serial interface
	ScrollText<Display::LowerLine> screen; ///< output screen scrolling received characters
	Display display; ///< EZ430-Chronos Display

	OutputChannel out; ///< an object that provides easy formated output.
	HelloWorld hello; ///< here the famous HelloWorld message is produced, repeatedly.
};


inline NodeConfiguration& getApplication()
{
	extern NodeConfiguration system;
	return system;
}

} // reflex

#endif
