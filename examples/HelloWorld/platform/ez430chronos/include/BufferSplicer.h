#ifndef BUFFERSPLICER_H
#define BUFFERSPLICER_H

#include "reflex/memory/Buffer.h"
#include "reflex/sinks/Sink.h"
#include "reflex/interrupts/InterruptLock.h"

/*! \brief A component to splice buffers into single characters one by one
 * This component does not use an activity since it is expected to be used
 * in conjunction with UART or similar bytewise interfaces that require
 * fast responses.
 */
class BufferSplicer : private reflex::Sink0, private reflex::Sink1<reflex::Buffer*>
{
private:
	reflex::Sink1<uint8> *out; ///< bytewise output channel
	reflex::Buffer *current; ///< currently spliced buffer
	reflex::Buffer::SizeT length; ///< current buffer length
public:
	/*! initialize members to sane state */
	BufferSplicer()
	{
		out = NULL;
		current = NULL;
		length = 0;
	}

	/*! set bytewise output channel */
	inline void set_out_byte(reflex::Sink1<uint8> *output) { out = output; }
	/*! get input channel to trigger next output */
	inline reflex::Sink0 *get_in_next() { return this; }
	/*! get input channel for new buffer */
	inline reflex::Sink1<reflex::Buffer*> *get_in_buffer() { return this; }

	/*! receive next buffer
	 */
	void assign(reflex::Buffer *buf)
	{
		reflex::InterruptLock lock;

		// free previous buffer
		if (current) current->downRef();
		current = buf;
		length = buf->getLength();

		// trigger splicer
		this->notify();
	}

	/*! splice the next piece of current buffer
	 *  This method is presumably called from interrupt context
	 */
	void notify()
	{
		if (!out) return;
		if (!current) return;

		if (length-- == 0)
		{
			/* end of buffer */
			current->downRef();
			current = NULL;
			return;
		}

		uint8 byte;
		current->read(byte);
		out->assign(byte);
	}
};

#endif // BUFFERSPLICER_H
