import qbs 1.0

// Import REFLEX convenience items. Make sure to set qbsSearchPaths in Your
// profile preferences for this to work.
import ReflexProject
import ReflexApplication

// Main project item for the VirtualTimers example application
// and the REFLEX framework.
ReflexProject {

    ReflexApplication {
        name: "VirtualTimers"

        // Platform-independent source files to compile
        // An explicit list is used here.
        Group
        {
            prefix : "sources/"
            files : [
                "BlinkApplication.cc",
                "main.cc"
            ]
        }

        // Platform-dependent source files
        // This group item shows the usage of variable folders and
        // wild-card matching
        Group
        {
            prefix : "platform/" + project.reflex_platform + "/sources/"
            files : "*.cc"
        }

        // Include paths for the application headers
        cpp.includePaths : [
            "sources",
            "platform/" + project.reflex_platform + "/sources"
        ]
    }
}
