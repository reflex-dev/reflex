#include "DisplayDriver.h"

using namespace examples;
using namespace virtualtimers;
using namespace ez430chronos;

using namespace reflex;

DisplayDriver::DisplayDriver() :
    act_display(*this)
{
    in_state.init(&act_display);
    display.switchOn();

    // The ez430chronos platform needs the system timer explicitly switched
    // on if it is used. This has to be done at least somewhere in the
    // platform-dependent code.
    reflex::mcu::CoreApplication::instance()->timer.switchOn();
}

void DisplayDriver::run_display()
{
    uint8 state = in_state.get();
    (state & (1 << 0)) ? display.getSymbols().set(reflex::display::ICON_ALARM)
                       : display.getSymbols().clear(reflex::display::ICON_ALARM);
    (state & (1 << 1)) ? display.getSymbols().set(reflex::display::ICON_RECORD)
                       : display.getSymbols().clear(reflex::display::ICON_RECORD);
    (state & (1 << 2)) ? display.getSymbols().set(reflex::display::ICON_HEART)
                       : display.getSymbols().clear(reflex::display::ICON_HEART);

    display.getLowerLine() = timer.getSystemTime();
}
