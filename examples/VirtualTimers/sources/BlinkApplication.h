#ifndef BlinkLogic_h
#define BlinkLogic_h
/*
 *    This file is part of REFLEX.
 *
 *    Copyright 2014 BTU Cottbus-Senftenberg, Distributed Systems /
 *    Operating Systems Group. All rights reserved.
 *
 *    Redistribution and use in source and binary forms, with or without
 *    modification, are permitted provided that the following conditions
 *    are met:
 *
 *       1. Redistributions of source code must retain the above copyright
 *          notice, this list of conditions and the following disclaimer.
 *
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the
 *          distribution.
 *
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *    The views and conclusions contained in the software and documentation
 *    are those of the authors and should not be interpreted as representing
 *    official policies, either expressed or implied, of BTU Cottbus,
 *    Department for Distributed Systems and Operating Systems.
 *
 * */

#include "DisplayDriver.h"

#include <reflex/scheduling/ActivityFunctor.h>
#include <reflex/sinks/Event.h>
#include <reflex/sinks/Sink.h>
#include <reflex/timer/VirtualTimer.h>

namespace examples {
namespace virtualtimers {

//! Platform-independent central application logic
class BlinkApplication {
public:
    //! Initializes the application structure
    BlinkApplication();

private:
    // Child components
    reflex::VirtualTimer t0;
    reflex::VirtualTimer t1;
    reflex::VirtualTimer t2;
    platform::DisplayDriver displayDriver;

    // Inputs
    reflex::Event e0;                       // for timer ticks
    reflex::Event e1;
    reflex::Event e2;

    // Output
    reflex::Sink1<uint8>* out_state;      // for the current blink state

    // Activity methods
    void blink0();
    void blink1();
    void blink2();

    // Activity functor objects to encapsulate the activity methods
    reflex::ActivityFunctor<BlinkApplication, &BlinkApplication::blink0> func0;
    reflex::ActivityFunctor<BlinkApplication, &BlinkApplication::blink1> func1;
    reflex::ActivityFunctor<BlinkApplication, &BlinkApplication::blink2> func2;

    // Private component member variables
    uint8 currentState;     // Current blink state encoded as bitmask
};

}
}

#endif

