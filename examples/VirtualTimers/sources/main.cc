#include "BlinkApplication.h"
#include <reflex/CoreApplication.h>

int main()
{
    reflex::mcu::CoreApplication core;
    examples::virtualtimers::BlinkApplication app;
    reflex::mcu::CoreApplication::instance()->scheduler()->start();
    return(0);
}
