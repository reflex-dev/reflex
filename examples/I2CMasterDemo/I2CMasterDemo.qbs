import qbs 1.0
import ReflexApplication
import ReflexProject

ReflexProject {
    components_enabled : true

    ReflexApplication {
        name: "HelloWorld"

        files: [
            "sources/main.cc",
        ]

        Group {
            condition : qbs.architecture == "ez430chronos"
            prefix : "platform/ez430chronos/sources/"
            files : "NodeConfiguration.cc"
        }

        cpp.includePaths : [
            "sources",
            "platform/" + qbs.architecture + "/sources"
        ]
    }
}
