#include "NodeConfiguration.h"

using namespace reflex;
using namespace mcu;
using mcu::I2C_Master;
using devices::EEPROM_24xx512;
using mcu::usci::USCI_I2C_Master;
using mcu::usci::RegistersB0;

NodeConfiguration::NodeConfiguration() : System() // init base system
    , successIndicator(SymbolDisplay::TOGGLE, SymbolDisplay::HEART)
    , errorIndicator(SymbolDisplay::SET, SymbolDisplay::ALARM)
{
    /* FIXME: This should use some sort of port mapping configuration object
     * It is currently aimed at SCL = Button::M1, SDA = Button::M2 on EZChronos */
    /* set up port mapping here... */
    Port2()->map(1, pmc::UCB0SDA); // USCI B0 I2C Data
    Port2()->map(2, pmc::UCB0SCL); // USCI B0 I2C Clock

    // set pins to mapped (USCI-I2C) functionality
    Port2()->DIR |= (0x04 /* Button::M1 */ + 0x02 /* Button::M2 */);
    Port2()->OUT &= ~(0x04 /* Button::M1 */ + 0x02 /* Button::M2 */);
    Port2()->SEL |= (0x04 /* Button::M1 */ + 0x02 /* Button::M2 */);

    // connect button to trigger sensor reading
    button.init(&sensor.measure);
    // connect sensor to distributor
    sensor.init(&distPres, &distTemp);
    // connect distributor to display and aggregator
    distTemp.registerSink(&temperatureDisplay.input);
    distTemp.registerSink(&aggregator.input1);
    distPres.registerSink(&pressureDisplay.input);
    distPres.registerSink(&aggregator.input2);
    // set up data displays, success and error indication
    temperatureDisplay.init(&display.getUpperLine());
    pressureDisplay.init(&display.getLowerLine());
    successIndicator.init(&display.getSymbols());
    errorIndicator.init(&display.getSymbols());
    // connect aggregator with data logger with eeprom
    aggregator.init(&logger.input);
    logger.init(&eeprom.read, &eeprom.write,
            &successIndicator.input, &errorIndicator.input);
    eeprom.init(&logger.success, &logger.error);

    // connect I2C devices to I2C Master, reconfigures devices
    // NOTE: I2C Master is temporarily activated for this to work!
    sensor.setMaster(&i2cmst_sw);
    eeprom.setMaster(&i2cmst_hw);

    // assign groups for primary powermanageable devices
    timer.setGroups(DEFAULT); // put system timer in default group
    button.setGroups(DEFAULT); // put button in default group
    display.setGroups(DEFAULT); // put LCD display in default group.
    i2cmst_sw.setGroups(DEFAULT); // put I2C Master (internal) in default group
    i2cmst_hw.setGroups(DEFAULT); // put I2C Master (external) in default group
    // the sensor is a secondary (self managed) device
    // the eeprom does not support (or need) power management

    // set initial power group, system timer is started implicitely here
    powerManager.enableGroup(DEFAULT); // enable all entities in default group
    powerManager.disableGroup(~DEFAULT); // disable all others

    // we trigger the button once, so the display doesn't stay black after start
    button.notify();
}
