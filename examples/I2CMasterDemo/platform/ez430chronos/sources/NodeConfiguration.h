#ifndef NodeConfiguration_h
#define NodeConfiguration_h
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(ses):	 NodeConfiguration
 *	Author:		 Stefan Nuernberger
 *
 *	Description: Put together all the components for a node.
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and
 *    Operating Systems. All rights reserved.
 *
 *    Redistribution and use in source and binary forms, with or without
 *    modification, are permitted provided that the following conditions
 *    are met:
 *
 *       1. Redistributions of source code must retain the above copyright
 *          notice, this list of conditions and the following disclaimer.
 *
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the
 *          distribution.
 *
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *    The views and conclusions contained in the software and documentation
 *    are those of the authors and should not be interpreted as representing
 *    official policies, either expressed or implied, of BTU Cottbus,
 *    Department for Distributed Systems and Operating Systems.
 *
 **/

#include "reflex/types.h"
#include "reflex/System.h"

#include "reflex/usci/USCI_I2C_Master.h"
#include "reflex/io/I2C_Master.h"
#include "reflex/io/SCP1000_I2C.h"
#include "reflex/io/EEPROM_24xx512.h"
#include "reflex/io/Display.h"
#include "reflex/io/Button.h"
#include "reflex/misc/Distributor.h"

#include "Aggregator.h"
#include "SymbolDisplay.h"
#include "DataDisplay.h"
#include "DataLogCompare.h"

#include "reflex/io/Ports.h"
#include "reflex/pmc/Registers.h"
#include "reflex/usci/Registers.h"

namespace reflex {

enum PowerGroups {
	DEFAULT = reflex::PowerManager::GROUP1,
	DISABLED = reflex::PowerManager::NOTHING
};

/* NodeConfiguration
 * This is the complete configuration of all system components.
 */
class NodeConfiguration : public System {
public:

    NodeConfiguration();
private:
	/* i2c bus master (pure software implementation) for internal bus */
	mcu::I2C_Master<mcu::PortJ, 3 /* PJ.3 = SCL */, 2 /* PJ.2 = SDA */> i2cmst_sw;
	/* i2c bus master for external pins (USCI hardware support) */
	mcu::usci::USCI_I2C_Master<mcu::usci::RegistersB0> i2cmst_hw;
	/* sensor driver for I2C attached slave */
	SCP1000_I2C sensor; // SCP1000-D11 pressure + temp sensor (on board)
	/* EEPROM on external I2C bus */
	devices::EEPROM_24xx512 eeprom; // 512kBit (64kb) EEPROM

	/* sensor value output */
	Display display; // the EZ430 LCD Display
	DataDisplay<uint32, display::LowerLine> pressureDisplay; // display sensor pressure value (19 bits)
	DataDisplay<uint16, display::UpperLine> temperatureDisplay; // display sensor temperature value (14 bits)
	/* success and error indication */
	SymbolDisplay successIndicator;
	SymbolDisplay errorIndicator;

	/* distributors for delivering signals to different consumers */
	Distributor1<uint16, 2> distTemp; // temperature to display and aggregator
	Distributor1<uint32, 2> distPres; // pressure to display and aggregator

	/* data logging */
	typedef Aggregator<uint16,uint32> Aggregator1;
	Aggregator1 aggregator; // aggregator for temperature and pressure values
	DataLogCompare<Aggregator1::ResultT> logger; // write and read sensor values to/from eeprom (verified log).

	/* others */
	Button<buttons::BL, 250> button; // button that triggers sensor reading (debounced 250msec)
};

inline NodeConfiguration& getApplication()
{
	extern NodeConfiguration system;
	return system;
}

} // ns reflex

#endif
