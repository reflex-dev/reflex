#ifndef SignalQuality_h
#define SignalQuality_h
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(ses):	Hannes_send_h
 *
 *	Author:		Karsten Walther
 *
 *	Description: Famous Hannes_send_h Application.
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */
#include "reflex/sinks/Event.h"
#include "reflex/scheduling/Activity.h"
#include "reflex/timer/VirtualTimer.h"
#include "reflex/sinks/Sink.h"
#include "reflex/types.h"
#include "reflex/scheduling/ActivityFunctor.h"
#include "reflex/sinks/SingleValue.h"
#include "reflex/memory/Buffer.h"
#include "reflex/sinks/Queue.h"

#include "reflex/updater/UpdateTypes.h"


using namespace reflex;

/** This class writes its well known message to a typical output, every time
 *  it is notified that the next cycle can begin.
 */
class UpdateDataSend : public reflex::Activity
{
public:

	UpdateDataSend();

	/**
	 * @return sink for incoming data
	 */
	inline Queue<Buffer*>* get_in_receiveDataFromRadio()
	{
		return &receiveDataFromRadio;
	}

	/**
	 * @return sink for receiving a "sending-was-sucessful" notification
	 */
	inline Sink1<bool>* get_in_sendReady()
	{
		return &sendReady;
	}

	/**
	 * connects the data output with a sink
	 * @param sink where the data have to be delivered
	 */
	inline void connect_out_sendToRadio(Queue<Buffer*>* sink)
	{
		sendToRadio = sink;
	}

private:
    /*
     * sensitive to clockEvent
     */
	void run();

	/**
	 * must called in Nodeconfiguration.h
	 * @param sendToRadio signals the radio that data for transmission are available
	 * @param sink for reconfiguring the radio driver
	 */
	//void init(reflex::Sink1<reflex::Buffer*> *sendToRadio, reflex::Sink1<reflex::Buffer*> *confData);


	/**
	 * is called, when sending was finished
	 */
	void send();

	/**
	 * is called, when data were received via radio
	 */
	void receiveData();

	/**
	 * is called, when data from temperature sensor are ready to process
	 */
	void changeSetup();

////////////////////////////////////////////////////
//	Member area
////////////////////////////////////////////////////

	/**
	 * virtual timer
	 */
    reflex::VirtualTimer timer;


	/**
	 * simple clock event
	 */
	reflex::Event clockEvent;


	/**
	 * receive -> if data are present in this object
	 */
	reflex::Queue<reflex::Buffer*> receiveDataFromRadio;


    /*
     * if the data were send, then the radio notifies this Event
     */
	reflex::SingleValue1<bool> sendReady;




////////////////////////////////////////////////////
//	functor area
////////////////////////////////////////////////////
    /**
	*  functor for when sending was successful
	*/
    reflex::ActivityFunctor<UpdateDataSend, &UpdateDataSend::send> sendReadyFunctor;


    /**
     * functor to process incoming data
     */
    reflex::ActivityFunctor<UpdateDataSend, &UpdateDataSend::receiveData> receiveFunctor;



    /**
     * pointer to a sink -> if data are copied into the buffer, then RADIO::transmit is notified
     */
    reflex::Sink1<reflex::Buffer*> *sendToRadio;


	uint16 localCounter, remoteCounter, readPos, noOfPackets, remainder;


	/**
	 * saves the current packet size to transmit
	 */
	uint8 packetSize;

	enum updateState
	{
		  META_INIT_STATE
		, META_INFO_STATE
		, META_UPDATE_STATE

	} state;

};

#endif
