/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 **/

#ifndef TEST_H
#define TEST_H

/*
//namespace test {
//	#include "cc430x613x.h"

	// LCD Segments
	#define LCD_A    BIT0
	#define LCD_B    BIT1
	#define LCD_C    BIT2
	#define LCD_D    BIT3
	#define LCD_E    BIT6
	#define LCD_F    BIT4
	#define LCD_G    BIT5
	#define LCD_H    BIT7

	// LCD Segment Mapping
	const unsigned char  LCD_Char_Map[] =
	{
	  LCD_A+LCD_B+LCD_C+LCD_D+LCD_E+LCD_F,        // '0' or 'O'
	  LCD_B+LCD_C,                                // '1' or 'I'
	  LCD_A+LCD_B+LCD_D+LCD_E+LCD_G,              // '2' or 'Z'
	  LCD_A+LCD_B+LCD_C+LCD_D+LCD_G,              // '3'
	  LCD_B+LCD_C+LCD_F+LCD_G,                    // '4' or 'y'
	  LCD_A+LCD_C+LCD_D+LCD_F+LCD_G,              // '5' or 'S'
	  LCD_A+LCD_C+LCD_D+LCD_E+LCD_F+LCD_G,        // '6' or 'b'
	  LCD_A+LCD_B+LCD_C,                          // '7'
	  LCD_A+LCD_B+LCD_C+LCD_D+LCD_E+LCD_F+LCD_G,  // '8' or 'B'
	  LCD_A+LCD_B+LCD_C+LCD_D+LCD_F+LCD_G,        // '9' or 'g'
	  LCD_A+LCD_B+LCD_C+LCD_E+LCD_F+LCD_G,        // 'A'        10
	  LCD_A+LCD_D+LCD_E+LCD_F,                    // 'C'        11
	  LCD_B+LCD_C+LCD_D+LCD_E+LCD_G,              // 'd'        12
	  LCD_A+LCD_D+LCD_E+LCD_F+LCD_G,              // 'E'        13
	  LCD_A+LCD_E+LCD_F+LCD_G,                    // 'F'        14
	  LCD_B+LCD_C+LCD_E+LCD_F+LCD_G,              // 'H'        15
	  LCD_B+LCD_C+LCD_D+LCD_E,                    // 'J'        16
	  LCD_D+LCD_E+LCD_F,                          // 'L'        17
	  LCD_A+LCD_B+LCD_E+LCD_F+LCD_G,              // 'P'        18
	  LCD_B+LCD_C+LCD_D+LCD_E+LCD_F               // 'U'        19
	};
	volatile unsigned char * P2DIR = (unsigned char *) 0x209;
	volatile unsigned char * P2DS = (unsigned char *) 0x205;
	volatile unsigned char * P2OUT = (unsigned char *) 0x203;

	void initPort () {
		//_interruptsDisable();
		// *P2DIR = 0xFF;
		*((volatile char*)0x209) = 0xFF;
		// *P2DS = 0xFF;
		*((volatile char*)0x205) = 0xFF;
		// *P2OUT = 0xFF;
		*((volatile char*)0x203) = 0x0;

	}

	void setPort () {
		*((volatile char*)0x203) = 0xFF;
	}

	void turnOnLCD() {

		P5SEL |= (BIT5 | BIT6 | BIT7);
		P5DIR |= (BIT5 | BIT6 | BIT7);

		LCDBCTL0 =  (LCDDIV0 + LCDDIV1 + LCDDIV2 + LCDDIV3 + LCDDIV4)| LCDPRE0 | LCD4MUX | LCDON | LCDSON;
		LCDBVCTL = LCDCPEN | VLCD_3_44;
		REFCTL0 &= ~REFMSTR;

		LCDBPCTL0 = 0x03FF;                         //Select LCD Segments 0-9
		LCDBPCTL1 = 0x0000;                         //

		LCDM5 |= LCD_Char_Map[15];     //H
		LCDM4 |= LCD_Char_Map[13];     //E
		LCDM3 |= LCD_Char_Map[17];     //L
		LCDM2 |= LCD_Char_Map[17];     //L
		LCDM1 |= LCD_Char_Map[0];      //0

		// Blink Memory
		LCDBM5 |= LCD_Char_Map[11];     //C
		LCDBM4 |= LCD_Char_Map[11];     //C
		LCDBM3 |= LCD_Char_Map[4];     //4
		LCDBM2 |= LCD_Char_Map[3];     //3
		LCDBM1 |= LCD_Char_Map[0];      //0
	}
*/

	/*void toggleLCD () {
		LCDBMEMCTL ^= LCDDISP;
	}*/
//}

namespace test{

inline void initPort () {
	//_interruptsDisable();
	//*P2DIR = 0xFF;
	*((volatile char*)0x209) = (char)0xFF;
	//*P2DS = 0xFF;
	*((volatile char*)0x205) = (char)0xFF;
	//*P2OUT = 0xFF;
	*((volatile char*)0x203) = (char)0x0;

}

inline void setPort () {
	*((volatile char*)0x203) = (char)0xFF;
}
inline void clearPort () {
	*((volatile char*)0x203) = (char)0x00;
}

inline void togglePort () {

	*((volatile char*)0x203) ^= (char)0xFF;
}

}


#endif // TEST_H
