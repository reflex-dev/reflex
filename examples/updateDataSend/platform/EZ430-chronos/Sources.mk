#/*
# *	REFLEX - Real-time Event FLow EXecutive
# *
# *	A lightweight operating system for deeply embedded systems.
# *
# *	Revision:	 $Id: Sources.mk 1168 2009-08-04 16:28:41Z snu $
# *	Author:		 Soeren Hoeckner
# */

CC_SOURCES_CONTROLLER += \
	lcd/LCD_B.cc \
	rf/RF1A.cc \
	rf/RADIO.cc

CC_SOURCES_PLATFORM += \
	io/Display.cc 


CC_SOURCES_LIB += \
	data_types/BCD.cc
