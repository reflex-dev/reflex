/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *

 *	Author:		Richard Weickelt <richard@weickelt.de>
 *
 */

#include "NodeConfiguration.h"

namespace reflex
{
NodeConfiguration system;
}

using namespace reflex;

int main()
{
	//NodeConfiguration::instance.scheduler.start();
	system.scheduler.start();
	return (0);
}
