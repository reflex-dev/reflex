import qbs 1.0
import ReflexApplication
import ReflexProject

ReflexProject {
    buffer_enabled : true
    buffer_maxPoolCount : 2
    components_enabled : true
    virtualtimer_enabled : true
    xml_enabled : true

    ReflexApplication {
        files: [
            "src/XmlBlinker.cc",
            "src/main.cc",
        ]

        cpp.includePaths : [
            "include",
            "platform/" + reflex_platform + "/include"
        ]
    }
}
