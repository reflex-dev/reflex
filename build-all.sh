#!/usr/bin/env bash

## REFLEX Build Script
##
## Builds the REFLEX library, all examples and executes unit-tests.
## This script serves as a hook for the Jenkins CI.
##
## Usage: usage-example <platform> <example>
##
##       -h     Show help options.

help=$(grep "^## " "${BASH_SOURCE[0]}" | cut -c 4-)
REFLEXPATH="$(pwd)"

printHelp() {
  echo "$help"
  exit 1
}

# If less than two arguments supplied, display usage
if [  $# -le 1 ]; then
    printHelp
    exit 1
fi

platform="$1"
target="$2"

if [ $platform == "guest" ]; then
    platform="posix"
elif [ $platform == "OMNeTPP" ]; then
    platform="omnetpp"
elif [ $platform == "EZ430-chronos" ]; then
    platform="ez430chronos"
elif [ $platform == "atmega_generic" ]; then
    platform="atmega"
elif [ $platform == "TMOTESKY" ]; then
    platform="tmotesky"
fi

echo -e "\e[32mBuild target \"${target}\" for platform \"${platform}\".\e[0m"

if ! hash qbs >/dev/null 2>&1; then
    echo -e "\e[31mThe Qt Build System qbs was not found. Maybe it is not installed.\e[0m"
    exit 1
fi

# Import a clean QBS config. This step might be avoided on other machines
# than the CI server.
if ! qbs config --import ./qbs/config/idun-jenkins.conf >/dev/null; then
    echo -e "\e[31mCould not import QBS configuration from file qbs/config/idun-jenkins.conf\e[0m"
    exit 1
fi

# Workaround for jenkins server, where REFLEX is located at a random location.
# This requires a modification of the qbsSearchPath for each build.
qbs config profiles.gcc.preferences.qbsSearchPaths "${REFLEXPATH}/qbs"

# Omnetpp needs access to its binaries
if [ "$platform" == "omnetpp" ]; then
    export PATH=$PATH:/opt/omnetpp/bin;
fi

case "$target" in
"reflex")
    qbs clean profile:${platform}
    qbs install profile:${platform}
    ;;
"unitTest")
    UNITTESTDIR="${REFLEXPATH}/examples/unitTest"
    TOOLDIR="${UNITTESTDIR}/tools"
    INSTALLROOT="${UNITTESTDIR}/posix-debug/install-root"
    cd "${UNITTESTDIR}"

    SCHEMES="\
        FIFO_SCHEDULING\
        PRIORITY_SCHEDULING\
        PRIORITY_SCHEDULING_SIMPLE\
        PRIORITY_SCHEDULING_NONPREEMPTIVE\
        EDF_SCHEDULING\
        EDF_SCHEDULING_SIMPLE\
        EDF_SCHEDULING_NONPREEMPTIVE\
        TIME_TRIGGERED_SCHEDULING"

    qbs clean profile:${platform}
    for scheme in $SCHEMES; do
        qbs install profile:posix project.core_schedulingScheme:${scheme}

        if [ "$platform" == "posix" ]; then
            echo "Tooldir: $TOOLDIR installroot: $INSTALLROOT"
            ${TOOLDIR}/exectest.py "${INSTALLROOT}/bin/unittest" > "${INSTALLROOT}/testlog_${scheme}.tap"
        fi
    done
    ;;
*)
    qbs install --file ./examples/${target}/ profile:${platform}
    ;;
esac
