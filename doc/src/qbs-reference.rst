=============
Qbs Reference
=============

List of Custom Qbs Items
========================

.. toctree::
   :titlesonly:
   :maxdepth: 1

   reflexapplication-item
   reflexproject-item

List of Custom Qbs Modules
==========================
