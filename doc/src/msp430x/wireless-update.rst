=======================================
Wireless Update on the Msp430x Platform
=======================================
The Msp430x platform provides self-programming capabilities which open the
possibility for application-specific firmware updates. The msp430x REFLEX
platform package comes with a wireless bootloader that supports
over-the-air updates on cc430 controllers. This document describes, how
bootloader and application are built and shows the usage of the whole update
process.

Two scenarios are considered in this document:

 - initial build and flash of bootloader and application
 - wireless over-the-air update of an application


How the Bootloader Works
========================
Functionality
-------------
The bootloader starts right after a reset and performs the following steps.
It shows the current state as a number in the upper line of the ez430-chronos:

1. Try to receive a firmware-update beacon packet.

   This packet contains meta information about a firmware image (version,
   size). It is sent periodically by the uploader script. If a beacon packet is
   received within 1s *and* either the installed firmware version differs from
   the propagated version *or* the installed firmware is invalid, the
   bootloader proceeds with step 2, otherwise step 3.

2. Receive firmware image packets.

   The lower line shows the number of remaining packets as a hex value. When
   the image is complete, calculate a CRC and write it to the firmware
   header.

3. Check the CRC of the installed firmware.

   If the CRC is invalid, proceed with step 1, otherwise step 4.

4. Start the firmware.

   Jump to the firmware start address configured by
   ``msp430x_update_firmwareaadress``.


Memory Layout
-------------
The cc430 controller provides up to 32KiB Flash-ROM. A REFLEX application
with updates enabled would use the following memory regions:

========== ============ ====================================
Address    Section      Purpose
========== ============ ====================================
``0x1800`` ``.infomem`` Meta information (node-id)
``0x8000`` ``.text``    Bootloader
``0x8c00`` ``.text``    Application firmware
========== ============ ====================================

The wireless bootloader reserves 3KiB in the beginning of the main flash
region. This is controlled by the ``msp430x_bootloader_address`` property in
the |ReflexProject| item. The special bsl region of the Msp430x controller
familiy is not used here, although it would be technically possible.

The application firmware has to be linked at least to ``0x8c00`` (default)
or beyond which is controlled by ``msp430x_update_firmwareaddress`` for each
|ReflexApplication| item. Special care is taken for the reset vector
during an update. It is always overwritten by the bootloader.

Each firmware starts with a special header, containing version information
and a placeholder for the checksum. Version information is included during
the build procedure. The checksum is later written by the bootloader. When
a running firmware wants to trigger an update, it just needs to invalidate
the checksum field. This can be achieved by calling ``setFirmwareInvalid()``
in the :cpp:class:`reflex::msp430x::CoreApplication` class.


Building a Project with Update Functionality
============================================
In order to build bootloader and update functionality, set the
``msp430x_update_enabled`` and ``msp430x_bootloader_enabled`` properties in the
|ReflexProject| Qbs item ``true``. Memory addresses for bootloader,
application and meta information can be specified in the |ReflexProject| and
|ReflexApplication| Qbs item if needed.

.. code-block:: qml

    import qbs 1.0
    import ReflexProject

    ReflexProject {
        msp430x_bootloader_enabled : true
        msp430x_update_enabled : true
        // ...

        ReflexApplication {
            name : "Application1"
            version : 2
            // update enabled automatically
        }

        ReflexApplication {
            name : "Application2"
            // update disabled only for this product
            msp430x_update_enabled : false
        }

Both, bootloader and application share a node identifier (node-id). It is not
included in the application, but written manually during the initial
programming procedure into the imfomem area. It is not supposed to be
overwritten.

The `software version`, an integeger number, is specified by the ``version``
property in the |ReflexApplication| product item. The build system
compiles and links it into the binary.

When having the above properties configured correctly, a build and install
command will produce:

- firmware binaries
- meta information as json file(s)
- bootloader binary
- uploader script

::

    $ qbs install profile:<profile>
    $ cd <install-root>
    $ ls bin
    msp430x-bootloader  updatable-app  updatable-app.hex  updatable-app.json
    $ ls utils
    uploader.py



Initial Programming
===================
This section describes the necessary commands for a ez430chronos clock.

1. Perform a mass-erase. Ensure, that the ``.infomem`` region is also clared:

   .. code-block:: sh

       $ mspdebug rf2500 "erase all"
       $ mspdebug rf2500 "erase segment 0x1800"

2. Write a node-id as a two-digit hex number (``01..FE``).
   ``00`` and ``FF`` should be avoided because they might have special
   meaning:

   .. code-block:: sh

       $ mspdebug rf2500 "mw 0x1800 XX"

3. Write the bootloader:

   .. code-block:: sh

       $ mspdebug rf2500 "prog msp430x-bootloader"

The node is now ready to accept updates over-the-air and will remain in the
bootloader until a complete firmware image has been received.


Field Update
============

When building an application project with update enabled, a json-file is
generated along with the firmware binary, containing meta information about
the application firmware. Use the uploader script, that is installed in the
``utils`` sub-directory of the ``install-root`` folder, to start broadcasting:

.. code-block:: sh

       $ cd /install-root
       $ ./utils/uploader.py bin/firmware.json


.. |ReflexApplication| replace:: :ref:`ReflexApplication <reflexapplication>`
.. |ReflexProject| replace:: :ref:`ReflexProject <reflexproject>`
