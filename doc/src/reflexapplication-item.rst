.. _reflexapplication:

======================
ReflexApplication Item
======================

Default Qbs item for REFLEX applications. Inherits
`Product <http://doc.qt.io/qbs/product-item.html>`_

This Qbs item defines an application product within a ReflexProject item.
It builds the application source files and loads all necessary REFLEX
dependencies.

.. code-block:: qml

    import qbs 1.0
    import ReflexApplication

    ReflexApplication {
        name: "myApplication"
        files : [
            // application source files
        ]

        // ...
    }


Properties
==========

+---------------------------------+--------------+----------+---------------------------------------------------------+
| Property                        | Type         | Default  | Description                                             |
+=================================+==============+==========+=========================================================+
| buffer_enabled                  | bool         | |bu|     | Add a dependency to the buffer package for this         |
|                                 |              |          | product.                                                |
+---------------------------------+--------------+----------+---------------------------------------------------------+
| components_enabled              | bool         | |co|     | Add a dependency to the components package for this     |
|                                 |              |          | product.                                                |
+---------------------------------+--------------+----------+---------------------------------------------------------+
| devices_enabled                 | bool         | |de|     | Add a dependency to the devices package for this        |
|                                 |              |          | product.                                                |
+---------------------------------+--------------+----------+---------------------------------------------------------+
| protothread_enabled             | bool         | |pr|     | Add a dependency to the protothread package for this    |
|                                 |              |          | product.                                                |
+---------------------------------+--------------+----------+---------------------------------------------------------+
| reflex_path                     | path         | n/a      | The REFLEX sources root folder. This property is        |
|                                 |              |          | read-only.                                              |
+---------------------------------+--------------+----------+---------------------------------------------------------+
| reflex_platform                 | string       | n/a      | The current platform. The value of this item is deduced |
|                                 |              |          | from ``qbs.architecture`` in the toolchain. This        |
|                                 |              |          | property is read-only.                                  |
+---------------------------------+--------------+----------+---------------------------------------------------------+
| virtualtimer_enabled            | bool         | |vi|     | Add a dependency to the virtualtimer package for this   |
|                                 |              |          | product.                                                |
+---------------------------------+--------------+----------+---------------------------------------------------------+
| xml_enabled                     | bool         | |xm|     | Add a dependency to the xml package for this            |
|                                 |              |          | product.                                                |
+---------------------------------+--------------+----------+---------------------------------------------------------+
| version                         | int          | undefined| Version number of the application. It is linked into    |
|                                 |              |          | application binary. This property has to be set if      |
|                                 |              |          | using the msp430x update functionality.                 |
+---------------------------------+--------------+----------+---------------------------------------------------------+

.. |bu| replace:: ``project.buffer_enabled``
.. |co| replace:: ``project.components_enabled``
.. |de| replace:: ``project.devices_enabled``
.. |pr| replace:: ``project.protothread_enabled``
.. |vi| replace:: ``project.virtualtimer_enabled``
.. |xm| replace:: ``project.xml_enabled``


Msp430x-specific Properties
===========================

+---------------------------------+--------------+------------+---------------------------------------------------------+
| Property                        | Type         | Default    | Description                                             |
+=================================+==============+============+=========================================================+
| msp430x_update_enabled          | bool         | |rep1|     | Overwrites the project-wide configuration of            |
|                                 |              |            | ``msp430x_update_enabled`` for this product item.       |
|                                 |              |            | If false, update functionality is disabled only in this |
|                                 |              |            | specific application product.                           |
+---------------------------------+--------------+------------+---------------------------------------------------------+
| msp430x_update_firmwareaddress  | int          | |rep2|     | Memory address to which the application is being linked |
|                                 |              |            | when update support is enabled. The default value is    |
|                                 |              |            | taken from the |ReflexProject| item                     |
+---------------------------------+--------------+------------+---------------------------------------------------------+
|                                 |              |            |                                                         |
+---------------------------------+--------------+------------+---------------------------------------------------------+

.. |rep1| replace:: ``project.msp430x_update_enabled``
.. |rep2| replace:: ``project.msp430x_update_firmwareaddress``
.. |ReflexProject| replace:: :ref:`ReflexProject <reflexproject>`

