namespace tinydsm {
	class IReplicationCalculator {
	public:
		virtual void setTarget(uint16 target) = 0;
		virtual uint16 getTarget() = 0;
		virtual bool targetReached() = 0;
		virtual void reset(timestamp_t lastAdvertise) {
			this->lastAdvertise = lastAdvertise;
		}
		timestamp_t getLastAdvertise() {
			return lastAdvertise;
		}
		void setReplicationRange(hops_t range) {
			replicationrange = range;
		}
	    hops_t getReplicationRange() {
			return replicationrange;
		}
		virtual void handleAcknowlegde(reflex::Buffer* ack) = 0;
		virtual void getUpdatePDUBuffer(varid_t varid, 
										nodeid_t owner, 
										size_t size, 
										timestamp_t timestamp, 
										SharingType type, 
										reflex::Buffer* destBuf) = 0;
	private:
		timestamp_t lastAdvertise;
		hops_t replicationrange;
	};
}