template<class functionality>
class MinimalFunctionality : public functionality {
public:
	typedef typename functionality::Type Type;

	void start() {
		functionality::start();
	}
	
protected:
	MinimalFunctionality(varid_t id, nodeid_t owner, SharingType sharingType)
		: functionality(id, owner, sharingType) {
	}

	void process(reflex::Buffer* buf) {
		functionality::process(buf);
	}

};