...
enum communicationManagerConstants
{
	/* Maximal number of duplicate surpression entries. */
	DUPLICATE_SUPPRESSION_LIST_LENGTH = 20,
	
	/* Maximal number of routing information entries. */
	ROUTING_INFORMATION_LIST_LENGTH = 5,

	/* The maximal age of a duplication suppression entry. */
	MAX_DUPLICATE_LIST_ENTRY_AGE = 2,

	/* The resolution of the age counter. */
	AGE_COUNTER_INTERVALL = 500
};
...