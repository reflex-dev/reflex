#include "reflex/scheduling/Activity.h"
#include "reflex/sinks/SingleValue.h"
#include "Mappings.h" //contains definitions of COMPRESSOR_ON and COMPRESSOR_OFF

//let FridgeControl be a schedulable entity
class FridgeControl : public Activity {
public:
	FridgeControl(int upperTemp, int lowerTemp) :
					input(this) //let the input trigger this object
	{
		this->upperTemp = upperTemp; //set upper threshhold value
		this->lowerTemp = lowerTemp; //set lower threshhold value
	}

	//Remember run is only called if a value was assigned to the input
	virtual void run()
	{
		int current = input.get(); //read the input
		if(current > upperTemp){
			output->assign(COMPRESSOR_ON); //switch on compressor
		}
		if(current < lowerTemp){
			output->assign(COMPRESSOR_OFF); //switch off compressor
		}
	}

	SingleValue1<int> input;  //the input for the temperatures
	Sink1<char>* output;   //pointer to input of the next processing component
};
