/** Register set for the clock reset generator modul,
 *  refer to the manual for details.
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */
struct Registers
{
    volatile char SYNR;
    volatile char REFDV;
    volatile char CTFLG;
    volatile char CRGFLG;
    volatile char CRGINT;
    volatile char CLKSEL;
    volatile char PLLCTL;
    volatile char RTICTL;
    volatile char COPCTL;
    volatile char FORBYP;
    volatile char CTCTL;
    volatile char ARMCOP;
};

enum ClockResetGeneratorBits {
	//CRGFLG
	SCM = 0x01,
	SCMIF = 0x02,
	TRACK = 0x04,
	LOCK = 0x08,
	LOCKIF = 0x10,
	LVRF = 0x20,
	PORF = 0x40,
	RTIF = 0x80,

	//CRGINT
	SCMIE = 0x02,
	LOCKIE = 0x10,
	RTIE = 0x80,

	//CLKSEL
	COPWAI = 0x01,
	RTIWAI = 0x02,
	CWAI = 0x04,
	PLLWAI = 0x08,
	ROAWAI = 0x10,
	SYSWAI = 0x20,
	PSTP = 0x40,
	PLLSEL = 0x80,

	//PLLCTL
	SCME = 0x01,
	PCE = 0x02,
	PRE = 0x04,
	ACQ = 0x10,
	AUTO = 0x20,
	PLLON = 0x40,
	CME = 0x80,

	//COPCTL
	CR0 = 0x01,
	CR1 = 0x02,
	CR2 = 0x04,
	RSBCK = 0x40,
	WCOP = 0x80,

};

