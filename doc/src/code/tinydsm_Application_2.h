...
#include "tinydsm/SharedVariable.h"
#include "tinydsm/Functionalities.h"
#include "tinydsm/ReplicationCalculator.h"

using namespace tinydsm;
using namespace reflex;

class Application {
public:
	Application() : sharedVariable(1, 0), ... {
	... 
	}	

	void init() {
		sharedVariable.start();

		sharedVariable.getReplicationCalculator()->setParameter(2 /*hops*/, 3 /*replica*/);
		sharedVariable.startReplication();
	}
	...
private:
	SharedVariable<QueryFunctionality<
				   UpdateFunctionality<
				   ReplicationFunctionality<DataType<uint16> > > > sharedVariable;
	...
};