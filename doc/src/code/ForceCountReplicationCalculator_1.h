class ForceCountTargetReplicationCalculator : public IReplicationCalculator {
private:
	uint8 neighbours, replicates, targetCount;
	hops_t range;
public:
	CountTargetReplicationCalculator() {
		neighbours = 0;
		replicates = 0;
		targetCount = 0;
		range = 1;
	}

	void setNumberOfReplica(uint8 target) {
		targetCount = target;
	}

	virtual bool targetReached() {
		return replicates >= targetCount;
	}

	virtual void handleAcknowlegde(Acknowl* ack) {
		++neighbours;
		if(ack->updateDone)	++replicates;
	}
	
	...
