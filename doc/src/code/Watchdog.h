/** This modul is for generation of a reset if, the timer is not reset within
 *  the watchdog timeout - obviously an indicator for a hanging system. Watchdog
 *  is set to maximum timeout which is 2^24 / ClockFrequency => approx. 1s @ 16MHz
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */
class Watchdog : public Sink0, public InterruptHandler {
public:
	/** Starts the watchdog unit
	 */
	Watchdog()
	{
		registers = (ClockResetGeneratorRegisters*)CRG_BASE;
		registers->COPCTL = 7; //set to maximum timeout
	}

	/** Resets timeout counter -> starts a new watchdog cycle
	 */
	virtual void notify()
	{
		registers->ARMCOP = 0x55; //write a 0x55 followed by a 0xaa
		registers->ARMCOP = 0xAA; //this sequence resets the watchdog
	}

	/** Starts system reset
	 */
	virtual void handle()
	{
		extern "C" void _reset();
		_reset();
	}

	/** Re-enables the watchdog after deep sleep.
	 */
	virtual void enable();

	/** Disables watchdog before deep sleep if needed.
	 */
	virtual void disable();


private:
	ClockResetGeneratorRegisters* registers;
};

