#include "reflex/System.h" //defines the base environment for the platform
#include "reflex/io/ADConverter.h"
#include "reflex/io/PortAB.h"
#include "reflex/timer/PeriodicTimer.h"
#include "FridgeControl.h"
#include "SampleControl.h"

//a node configuration is always derived from a base system
class NodeConfiguration : public System {
private:
	//instantiate components
	PeriodicTimer timer;   //a prescaling channel
	SampleControl sample;  //sample as event channel
	ADConverter adc;
	Converter tempConversion;
	FridgeControl fridge;
	PortAB port;

public:
	NodeConfiguration()
	{
		//clock is part of the base system and assumed to
		//tick every ms on this architecture
		clock.connectToOutput(timer);
		//connect the components
		timer.output = sample;
		sample.output = &(adc.input);
		adc.output = &(tempConversion.input);
		tempConversion.output = &(fridge.input);
		fridge.output = &(serial.input);
	}
};
