#include "reflex/scheduling/ActivityFunctor.h"
#include "reflex/sinks/SingleValue.h"
#include "Mappings.h" //contains definitions of COMPRESSOR_ON and COMPRESSOR_OFF

//FridgeControl is not an activity anymore
class FridgeControl{
public:
	FridgeControl(int upperTemp1, int lowerTemp1,
					int upperTemp2, int lowerTemp2 ) :
					fridgeFunctor(*this),   //connect functor to this object
					frosterFunctor(*this)
	{
		...
	}

	//Method checks temperatur for the fridge and act as needed
	void checkFridge();
	//same for the froster
	void checkFroster();

	SingleValue1<int> inputFridge;
	SingleValue1<int> inputFroster;

	//The functors are the scheduled entities, they are simple gateway
	//objects which contain a pointer to an object and call the
	//specified method in their run method.
	ActivityFunctor<FridgeControl,&FridgeControl::checkFridge> fridgeFunctor;
	ActivityFunctor<FridgeControl,&FridgeControl::checkFroster> frosterFunctor;

	Sink1<char>* output;   //pointer to input of the next processing component
	char myState; 	//in most control logic an internal state is useful
};
