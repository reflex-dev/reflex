class NodeConfiguration : public System {
private:
	//instantiate components
	ADConverter adc1;
	ADConverter adc2;
	Converter converter;
	SerialInterface serial;

public:
	NodeConfiguration()
	{
		//connect the components
		adc1.connect(converter.input);
		adc2.connect(converter.input);
		input.connect(serial.input);

		//define slots for priority-scheduling
		adc1.setPriority(HIGH);
		adc2.setPriority(HIGH);
		converter.setPriority(LOW);
		serial.setPriority(LOW);
	}
};
