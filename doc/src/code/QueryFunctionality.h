template<class functionality>
class QueryFunctionality : public functionality
{
public:
	typedef typename functionality::Type Type;

	void read(	reflex::Sink1<QueryEngineData<Type> > &theAnswerQueue,
				timestamp_t value_ts = 0, 
				timestamp_t timeout = 0,
				reflex::Activity *dataGatheringEndedActivity = NULL);
};