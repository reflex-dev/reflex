class NodeConfiguration : public System {
private:
	//instantiate components for a fridge/froster
	PeriodicTimer timer1;   //a prescaling channel
	SampleControl sample1;  //sample as event channel
	ADConverter adc1;
	Converter tempConversion1;
	FridgeControl fridge1;

	//the same for the other fridges/frosters
	...

	//the mediator which controls the compressor
	Mediator mediator;

	//the output ports
	PortAB porta;
	PortAB portb;

	//output channel and interface
	OutputChannel out;
	Serial serial;

public:
	NodeConfiguration()
	{
		//do the connections here.
		...

		//set the priorities
		timer1.priority = 1;
		sample1.priority = 1;
		adc1.priority = 1;
		tempConversion.priority = 1;
		...
		serial.priority = 0; //0 = highest priority

	}
};
