template<class functionality>
class SharedArrayVariable: public functionality {
public:
	typedef typename functionality::Type Type;
	explicit SharedArrayVariable(varid_t  id, 
								 nodeid_t owningNode = tinydsm::getNodeID());
	void start();
};