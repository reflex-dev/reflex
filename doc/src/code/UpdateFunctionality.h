template<class functionality>
class UpdateFunctionality : public functionality
{
public:
	typedef typename functionality::Type Type;

	void write(	Type val, timestamp_t timeout = 0,
				reflex::Sink1<bool> *writeResultSink = NULL)
};