...
enum tinydsmCoreConstants 
{
	/* After INVALIDAFTER epochs the value will be ignored. */
	INVALIDAFTER			= 1000, // epochs

	/* Defines the maximum payload. */
	MAXSHAREDVARIABLESIZE	= sizeof(uint64),

	/* Defines the maximal number of replicas per sensor node. */
	MAXREPLICATEDVARIABLES	= 5,
	
	/* The id of this node. */
	TINYDSM_NODEID = NODEID
};
...