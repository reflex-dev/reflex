template<class functionality,
		 class ReplicationCalculator=CountTargetReplicationCalculator>
class ReplicationFunctionality : public functionality {
public:
	typedef typename functionality::Type Type;

	//state machine
	void setStateMachineParameter(Time statemachineAdvertiseIntervall,
								  uint8 advertiseRuns, 
								  Time statemachineUpdateIntervall, 
								  uint8 updateRuns)
	void startReplication();
	void stopReplication();
	ReplicationCalculator* getReplicationCalculator();

	//replica management
	void resetReplica();
	void update();
};