================================
Building REFLEX and Applications
================================

REFLEX uses the `Qt Build Suite <Qbs Documentation_>`_ as build system which is
very flexible and fast. It is possible to build REFLEX together with the
application or as a separate library.

Build receipes in Qbs are expressed in the Qt Markup Language QML, a
declarative markup language like JSON. The following vocabulary is needed
to understand the Qbs build process:

:item: A compound QML element
:property: Member variables of an item
:product: Output of a build process: an application binary or a library.
          A product defines, *what* to built *from which* sources and
          dependencies.
:dependency: A product can depend on other products or modules.
:module: A module influences, *how* a product is built. Modules are
         instanciated for each product. ``cpp`` for example is a module that
         handles the compiler and linker infrastructure.
:project: A compound Qbs item that combines products.
:profile: A Qbs build profile contains global configuration options for
          modules. This covers toolchain paths and essential compiler flags.
          Each target platform has a separate profile, but it is even possible
          to inherit from other profiles and save work.

A typical Qbs build file would look like:

.. code-block:: qml

    import qbs 1.0

    Product {                             // Define a build product
        type : "application"              // resulting in an executable binary
        name : "myApplication"            // with the name myApplication.

        Depends { name: "cpp" }           // Load the built-in module cpp so
                                          // that C/C++ files can be compiled.

        cpp.includePaths: [               // Properties of loaded modules are
            "/path/to/include"            // accessed via their identifier.
        ]

        Group {                           // Define a group
            files : [                     // of source files to be build
                "MyApplication.cc"
                "main.cc"
            ]
            prefix : "sources/"           // in a directory named 'sources'
        }

        Group {                           // Define a group
            fileTagsFilter: "application" // for all binary results
            qbs.install: true             // that will be copies
            qbs.installDir: "bin"         // in the a directory named 'bin'.
        }                                 // The qbs module is always loaded.
    }


Setup and Test Qbs
==================

1. Download and install `Qbs <Qbs Documentation_>`_
2. Create a build profile for the target platform. The qbs directory in REFLEX
   contains an example configuration file that can be imported via
   ``qbs config --import example.conf``. It contains profiles for several
   platforms. Make sure, that all paths are valid. Add the REFLEX folder to
   the *qbsSearchPaths* property so that REFLEX' internal Qbs items and modules
   can be found.
3. Check, if everything is set up correctly by building one of the example
   applications, e.g. :doc:`VirtualTimers <examples/VirtualTimers/index>`.

   .. code-block:: sh

      cd examples/VirtualTimers
      qbs install profile:posix
      ./posix-debug/install-root/bin/VirtualTimers

If this works without errors, Qbs is ready to build applications for the
posix platform.


Build an Application together with the REFLEX Library
=====================================================

In this variant, REFLEX is built as part of an application project. Everything
is then linked together to a single binary. A typical folder structure would
look like::

    MyApplication            project folder
    |
    +--MyApplication.qbs     project file
    +--sources               application source and header files

The project folder contains only the application files and a Qbs file. REFLEX
is located somewhere else in the file system. The following code snippet shows
a typical typical REFLEX project file. It uses the convenience items
:ref:`ReflexProject <reflexproject>` and
:ref:`ReflexApplication <reflexapplication>`:

.. code-block:: qml

    import qbs 1.0
    import ReflexApplication
    import ReflexProject

    ReflexProject {

        core_schedulingScheme : "PRIORITY_SCHEDULING"

        ReflexApplication {
            name : "MyApplication"

            cpp.debugInformation : true

            Group {
                prefix : "sources/"
                files : [
                    "MyApplication.cc",
                    "main.cc"
                ]
            }
        }
    }

Build and install the above project:

.. code-block:: sh

   qbs install profile:posix


After the first build, the application folder structure would look like this::

    MyApplication            project folder
    |
    +--MyApplication.qbs     project file
    +--sources               application source and header files
    |
    +--XXX.debug             build folder for profile XXX
    |  +--...                various build folders for sub-products
    |  +--install-root       installation folder for build products

Notice the new ``XXX.debug`` folder which contains all intermediate build
files and the build results. Qbs builds always out of the source tree. By
default, this is a folder ``PROFILENAME.debug`` in the project folder. All
build results are then collected in the ``install-root`` subfolder. This
can be influenced by the ``--build-directory`` and ``--install-root`` command
line flags:

.. code-block:: sh

   qbs install --build-directory /tmp/my-build --install-root /tmp/myproject profile:posix



Build REFLEX Separate from an Application
=========================================

This is not officially supported, yet.

.. _Qbs Documentation: http://doc.qt.io/qbs/index.html
