Welcome
=======
REFLEX (Real-time Event FLow EXecutive) is an application development framework
for embedded control systems and sensor networks. It provides a high level
programming model and enables the programmer to write deadlock-free, highly
concurrent applications. Following the event-flow model, REFLEX applications
are synchronized implicit.

Getting started
---------------
.. toctree::
   :maxdepth: 1

   examples
   build
   programming-tutorial
   debugging
   event-channels

Development Topics
------------------
.. toctree::
   :titlesonly:

   event-flow-model
   scheduling-framework
   structure
   interrupt-handling
   porting
   testing
   debugging
   style-guide

Packages
--------
.. toctree::
   :glob:
   :maxdepth: 1

   */index

Reference
---------
.. toctree::
   :titlesonly:
   :maxdepth: 1

   qbs-reference
