Event-flow Model
================

The event flow model is a programming model, which is based on the general
concept of dataflow models. In an event flow there are components which contain functional
entities, so called activities, inputs and output references. Communication
between components is push based and conceptually asynchronous. A component assigns
data or raises an event on its output which is connected to a subseeding input.
This input buffers the event and marks the corresponding activity for activation. Then,
the activities are scheduled according to a choosen scheduling scheme. Note, that the initial
source for events are always interrupts.

The following figure shows a graph for such an event flow system.

``![Event-flow model of REFLEX](\ref img:event-flow)``

There are three layers, one for the hardware, one for drivers and one for the
application. The layers reflect the distinction between
hardware dependent and hardware independent code.
The software layers contain components that can be connected
via input and output ports.
Components are logical entities and contain
interrupt handlers or schedulable activities.

The main difference between the event flow model and the dataflow model is, that event flow
model defines a scheduling strategy, whereas
the dataflow models usually do not make any assumption about the time of activation of activities.
Furthermore, in the event flow model the channels have different buffer semantics. In
dataflow it is always an unbounded %FIFO, which is hardly possible to realize in memory
constrainted systems.
The implicit buffer semantics of channels is also an extension to pure event driven systems,
where the events are decoupled from the data and the receiving component is
responsible for buffering. This is especially noteworthy since buffers are
synchronized. Another extension to pure event driven systems is that activities have
a state due to their object nature, which eases their handling by the system.


``[img:event-flow]: figures/BaseModel.svg "Event-flow model of REFLEX"``
