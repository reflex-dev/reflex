Debugging Hints
===============
Debugging of embedded applications is not easy, even when a debugger is available.
Therefore we implemented support for debugging of application logic in the guest
environment. This allows to test the application on any PC while the test run
can be controlled in a good manner and a common debugger can be used.

The component model of REFLEX also allows easy implementation of a
guest environment for PC systems, which allows compiling the same application
for the device and the guest environment. Due to the event driven interfaces
used in REFLEX, the drivers can be replaced easily with input and output wrappers.
In general these will read the stimuli out of a file or write the results to a
file. For input and output overloading stream operators are used. The wrappers
themselves are templates, which get the type of the data to read or write as parameter.
The instantiation of an `InputWrapper` which reads data of type integer
is shown in figure \ref{wrapper}. The corresponding stimuli file is very simple.
It is organized line wise, each line contains a tuple of a time-stamp in clock
ticks and an integer value. Note that the clock speed can be adjusted to application demands
in {\sc Reflex}.
This scheme allows extensive testing of the application, since these stimuli files
can be generated by scripts. Furthermore, the test is deterministic. Listing
\ref{stimuli} shows an example stimuli file
for a wrapper of type InputWrapper<int>, which could simulate periodic reading
of an analog temperature sensor by an analog-to-digital converter.

After testing, the same application code is compiled with the machine-specific
drivers for the specific hardware platform.
Furthermore, a {\sc Reflex} application is nothing more than an object running on a
device, this allows for example simulation of a whole sensor net in one process on a PC. Therefore,
only {\it n} instances of the application must be instantiated as threads in the
simulation process. Each data access is implicitly right, because a C++ method
knows its corresponding object. Therefore, with {\sc Reflex},
it is possible to run the same code inside a simulator and in a sensor node. This
is a big advantage for example for developing sensor network algorithms.

Listing inputlisting (Example instantiation of an input wrapper)
\include code/Wrapper.cc

Listing stimuli (Example stimuli file for a analog sensor):
\include code/stimuli.txt



