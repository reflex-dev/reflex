Virtual Timers Example Application
==================================
This example shows how to write a multi-platform application using multiple
virtual timers. The application contains 3 virtual timer objects, each running
in a different interval. Depending on the platform, each timer event results in
a LED toggle or console output.


Supported Platforms
-------------------
=============  ======
Platform       Output
=============  ======
posix          Command line output in the form ``[*] [ ] [*]``
ez430chronos   Blinking symbols in the LCD display and the current system time
=============  ======


Application Structure
---------------------
.. figure:: structure.png
   :align: center

   This figure shows the application structure. All components despite
   ``DisplayDriver`` are platform-independent.

:cpp:class:`BlinkApplication` is the root component. It contains 3
:cpp:class:`reflex::VirtualTimer` components ``t0..t2``, the application logic
and references a platform-dependend :cpp:class:`DisplayDriver` component.
The application logic consists of 3 :cpp:class:`Event` inputs ``e0..e2`` for
the virtual timer ticks, 3 activities ``blink0..blink2`` (encapsulated as
functors) which are executed on each event and one shared output
``out_state`` for the current application state.

Build and Run
-------------
To build the example for the posix platform:

 1. Make sure, that qbs is set up correctly
 2. Go into the application folder: ``cd examples/VirtualTimers``
 3. Build the example: ``qbs install profile:posix``
 4. Execute the binary: ``./posix-debug/install-root/bin/VirtualTimers``


Directory Layout
----------------
The folder structure of this example looks as following::

    VirtualTimers               # project folder
    |
    +--VirtualTimers.qbs        # qbs project file
    +--sources                  # application source and header files
    |  +--BlinkApplication.h
    |  +--BlinkApplication.cc
    |  +--main.cc
    |  |
    |  +--platform              # platform-specific source and header files
    |     +--ez430chronos
    |     |  +--sources
    |     |     +--DisplayDriver.h
    |     |     +--DisplayDriver.cc
    |     |
    |     +--posix
    |        +--sources
    |           +--DisplayDriver.h
    |           +--DisplayDriver.cc
    |
    +--XXX-debug                # build folder for profile XXX
       |                        # created by qbs
       +--...                   # various build folders for sub-products
       +--install-root          # installation folder for build products
          +--bin
          |  +--VirtualTimers   # the executable application binary
          |
          +--include            # exported REFLEX header files, not needed here

The XXX-debug folder is created during the build procedure and can be removed
afterwards. The build and install folder can be chosen by command line
parameters during the build procedure. Example::

    qbs build --build-directory /tmp/build-VirtualTimers profile:XXX
    qbs install --install-root /tmp/VirtualTimers profile:XXX


Code Structure
--------------
Let's have a look into the code. Here is the class definition for the root
component ``BlinkApplication``:

.. code-block:: cpp

    #include "DisplayDriver.h"

    #include <reflex/scheduling/ActivityFunctor.h>
    #include <reflex/sinks/Event.h>
    #include <reflex/sinks/Sink.h>
    #include <reflex/timer/VirtualTimer.h>

    namespace examples {
    namespace virtualtimers {

    //! Platform-independent central application logic
    class BlinkApplication {
    public:
        //! Initializes the application structure
        BlinkApplication();

    private:
        // Child components
        reflex::VirtualTimer t0;
        reflex::VirtualTimer t1;
        reflex::VirtualTimer t2;
        platform::DisplayDriver displayDriver;

        // Inputs
        reflex::Event e0;                       // for timer ticks
        reflex::Event e1;
        reflex::Event e2;

        // Output
        reflex::Sink1<uint8>* out_state;      // for the current blink state

        // Activity methods
        void blink0();
        void blink1();
        void blink2();

        // Activity functor objects to encapsulate the activity methods
        reflex::ActivityFunctor<BlinkApplication, &BlinkApplication::blink0> func0;
        reflex::ActivityFunctor<BlinkApplication, &BlinkApplication::blink1> func1;
        reflex::ActivityFunctor<BlinkApplication, &BlinkApplication::blink2> func2;

        // Private component member variables
        uint8 currentState;     // Current blink state encoded as bitmask
    };

    }
    }

Please notice in the above code how:

 - REFLEX classes are imported via ``#include`` statements and accessed
   in the ``reflex`` namespace,
 - this example uses its own namespace. This is not necessary, but may become
   helpful in bigger projects.
 - a sub-namespace ``platform`` is used for the platform-dependent display driver.
 - activities are encapsulated in :cpp:class:`reflex::ActivityFunctor` objects.

We now have a look into the implementation of the ``BlinkApplication`` class.
The constructor initializes and connects all member components and binds all
inputs to their correspondig activities.

.. code-block:: cpp

    using namespace examples;
    using namespace virtualtimers;
    using namespace reflex;

    BlinkApplication::BlinkApplication() :
        t0(VirtualTimer::PERIODIC),
        t1(VirtualTimer::PERIODIC),
        t2(VirtualTimer::PERIODIC),
        func0(*this), func1(*this), func2(*this)
    {
        // Connect timer outputs to events
        t0.connect_output(&e0);
        t1.connect_output(&e1);
        t2.connect_output(&e2);

        // Events are related to activities
        e0.init(&func0);
        e1.init(&func1);
        e2.init(&func2);

        // Connect to the display driver
        out_state = displayDriver.get_in_state();

        // Initialize timers with random values
        t0.set(666);
        t1.set(1347);
        t2.set(6666);

        // Initialize other members
        currentState = 0;
    }

To fill the component with live it is necessary to implement the activities:

.. code-block:: cpp

    void BlinkApplication::blink0()
    {
        currentState ^= 0x01;
        out_state->assign(currentState);
    }

    void BlinkApplication::blink1()
    {
        currentState ^= 0x02;
        out_state->assign(currentState);
    }

    void BlinkApplication::blink2()
    {
        currentState ^= 0x04;
        out_state->assign(currentState);
    }

Each time one of the activities is executed, they toggle a bit in the shared
state variable and forward it to the common output ``out_state``. The connected
display driver will then show it.

Finally, the application needs a setup and start routine. This is done in the
``main.cc`` file:

.. code-block:: cpp

    #include "BlinkApplication.h"
    #include <reflex/CoreApplication.h>

    int main()
    {
        reflex::mcu::CoreApplication core;
        examples::virtualtimers::BlinkApplication app;

        reflex::mcu::CoreApplication::instance()->scheduler()->start();
        return(0);
    }

The ``core`` object is platform-specific and references to one of the
:cpp:class:`reflex::mcu::CoreApplication` classes. It is implemented
as singleton, so only one instance can exist. The ``app`` object is constructed
on the stack and does not have any magic behind. It would even be
possible to have multiple instances. Once all objects are created,
the event handling can be started.


Platform-dependent Display Driver
---------------------------------
REFLEX makes it easy to develop multi-platform applications. In the
`VirtualTimers` example, the only platform-dependent component is the display
driver. This component shows an appropriate notice when a timer fires.

The declaration for both platforms consists of an input, one activity and
some private member variables. The following code snippet shows the posix
display driver:

.. code-block:: cpp

    #include <reflex/scheduling/ActivityFunctor.h>
    #include <reflex/sinks/SingleValue.h>
    #include <reflex/timer/VirtualTimer.h>

    namespace examples {
    namespace virtualtimers {
    namespace posix {

    /*!
    \brief Prints the timer status on the command line.

    This class represents a platform-dependent component.

    */
    class DisplayDriver {
    public:
        DisplayDriver();
        inline reflex::Sink1<uint8>* get_in_state() { return &in_state; }

    private:
        reflex::SingleValue1<uint8> in_state;

        void run_display();
        reflex::ActivityFunctor<DisplayDriver, &DisplayDriver::run_display> act_display;

        reflex::VirtualTimer timer; // dummy timer to acquire system time
    };

    }

    namespace platform = posix;
    }
    }

Notice, how ``platform`` is defined as namespace alias for ``posix`` at the end
of the above code. This makes it possible, to refer to the code for the
``posix`` and the ``ez430chronos`` display driver as
``platform::DisplayDriver`` and leave the correct header file resolution up to
the build system.

