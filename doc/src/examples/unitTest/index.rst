Unit-testing Example Application
================================

This is a test system for regression tests of the whole REFLEX framework
including all sub-modules. It is currently implemented as example application,
but might get separated in the future.

Class Reference
---------------
.. doxygengroup:: unitTest

Test System Core
^^^^^^^^^^^^^^^^
.. toctree::
   :maxdepth: 1

   create
   TestCase
   TestEnvironment
   TestSuite


Platform-independent Test Cases
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. toctree::
   :maxdepth: 1

   EdfSchedulerTest
   FifoSchedulerTest
   LinkedListTest
   PrioritySchedulerTest
   TimeTriggeredSchedulerTest


Test Cases for the Atmega Platform
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. toctree::
   :maxdepth: 1

   ExternalInterruptTest
   PinChangeInterruptTest
   PortTest
   SpiTest
