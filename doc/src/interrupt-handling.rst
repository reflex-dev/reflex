Interrupt Handling
==================
For handling of interrupts, several actions must be taken.
For example, saving and restoring of volatile (clobbered) registers,
switching to high-level language and especially in {\sc Reflex} -- adaptation to the
scheduling scheme. Because the size of this common code has a relevant size, handlers
share code in {\sc Reflex} as shown in figure \ref{interrupts}.

The specific wrapper saves the interrupt number and calls the common
wrapper, which stores and recovers the volatile (clobbered) registers and
calls a higher-level language function. There, the synchronization with
scheduling is performed and the specific handler is called. This is mandatory for
preemptive scheduling schemes like EDF and FP. For the non-preemptive schedulers,
no interaction with the scheduler is needed, since no new scheduling frames must be allocated on the stack.
The wrappers are machine dependent and usually written in assembler code.
The common handling code, however, is written in C++. In this part,
a logical interrupt vector table exists, which contains poin\-ters to objects
of type {\tt InterruptHandler}. If a device driver wants to listen to
interrupts it just has to be derived from {\tt InterruptHandler} and must
implement a {\tt handle()} method. The registration of the handler is done in
the constructor. Non-registered interrupts are mapped to a default handler, initially.

![Interrupt handling scheme][img:interrupt-handling]

[img:interrupt-handling]: figures/interrupts.png ""
