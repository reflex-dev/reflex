OMNeT++ SchedulerWrapper Class
==============================

.. doxygenclass:: reflex::omnetpp::SchedulerWrapper
   :members:


.. doxygenclass:: reflex::omnetpp::SchedulerWrapper< FifoScheduler >
   :members:
