Coding Style Guide
==================
First of all *keep clean*. So if you think you have a trade off somewhere, take the cleaner
approach, even if you have some lines more. And while your meaning of clean is maybe different to
ours, here is some help for you.

General Conventions
===================

Keep the structure
------------------
Reflex is structured as shown in \link{DirectoryStructure.pdf}. This structure is introduced for
separation of different code parts (System, Machine, Application). So do not change this structure
for any reason.

Versioning
----------
For keeping versions under control, a version control system is used. For any kind of development
these version system must be used. See \link{VersionControl.pdf} for further details.


Code Conventions
================
Naming
------
In general all names should meaningful and written out. At least there is \emph{no} kind of name mangling by use of
pre- or postfixes are allowed.

- __Source and Headerfiles__ named equal to the class in this files, in case of C or Assemblerfiles, the name expresses the provided functionality. C++ filenames begin with a capital letter. Allowed extensions are ".cc" for C++ sources, ".h" for headerfiles, ".c" for C sources and ".s" for assembler sources.
- __Variables__ names start with a small letter and every new word in name with a capital letter.
- __Classes__ names start with a capital letter and every new word in name also.
- __Function__ names start with a small letter and every new word in name with a capital letter. Furthermore a verb should be included in name.
- __Assembler Function__ names start with an underscore.
- __Constants__ names consist of capital letters where the words are separated by an underscore.


~~~{.cpp}
#include "scheduler/DeadlineActivity.h"

class DeadlineActivity : public reflex::FifoActivity
{
public:

private:
    int interruptCount;
    bool isFree() const;

    enum Bits {
        ENABLE_INTERRUPTS = 0x1
    };
};
~~~

Short Rules
-----------
1. __No mixed code__ is allowed, keep languages separated because of portability. Strictly put source code of one language only in files where it belongs.

Documentation Conventions
=========================
For source code documentation doxygen is used on header files. So write doxygen style documentation in each header
file. While your function should have meaningful names, maybe think more about assumption, pre- post- conditions and
side effects.
