Scheduling Framework
====================

The main idea of a scheduling framework is to have the choice between different
scheduling schemes for an application at compile time.
From a programmers point of view an application is made of activities, therefore a general activity abstraction is
needed which can be used in the applications. The logical structure of the different activity types
is shown in figure \ref{logicStructure}. There is a base activity type and different
subtypes for FCFS-scheduling (First Come First Serve), FP-scheduling (Fixed Priority),
EDF-scheduling (Earliest Deadline First) and TT-scheduling (Time Triggered).
Each activity type implements the methods `trigger()`, `lock()`
and `unlock()` for runtime management; this masks the interaction with the
scheduler from the application. Each activity also stores metric information
needed by the corresponding scheduler.

![Logical Structure for Activities and Schedulers](\ref img:logic-structure)

An important issue is the transparent usage of activity types, especially in libraries. This means,
it must be simple to switch between scheduling schemes, so the activity
code does not depend on a specific scheduling scheme. With the presented scheduling schemes
this is fairly simple to implement, since there is a activity abstraction, which
hides its interaction with the system from the activity programmer. Listing~\ref{taskImplementation}
exemplarily shows a converter activity. It can be used with any scheduling
scheme; it only has to implement a `run()` method. The base activity class will
provide the common methods for activation and state management. Only at
the point of instantiation of the activities the used scheduling scheme must be considered ---
the metric information has to be provided
for the scheduler. This means the priority for the FP scheduler, the response time
and period for EDF, the slots for TT
and none for FCFS based systems.

The following code snippet shows the implementation of a activity.

\include code/Converter.h

In figure \ref{senseAndSend} a simple sense and sense application is shown.
The construction code for that application is given in the following listing:

\include code/NodeConfiguration.h

The application samples 2 analog values using AD-converters, converts the values to
temperatures and sends the values over a serial interface.
The whole system is an object of type {\it NodeConfiguration}, which is derived from the
base class system that contains the scheduler. The used components are class members
and are connected among each other in the constructor. In
this example, the usage of a fixed priority scheduler is assumed and therefore
each component is set to a priority level.
Note that assigning the scheduling metrics on component level
is very natural from the programmer's point of view. Furthermore, the inner
implementation of the components is not affected.
Therefore, a component can be used in different applications with
different scheduling schemes without change. Nevertheless, it is still possible that
a component relies on a special scheduling scheme; for example blocking activities
are only allowed in preemptively scheduled fixed priority systems. This
component simply uses the specific type rather than the signature type. If it is
used with a wrong scheduling scheme the compiler will show an error message.

The presented framework allows easy replacement of the scheduling
strategy. This is reached by a strict separation of code belonging to the
the scheduling scheme and code belonging to the application.

[img:logic-structure]: figures/TaskSchedulerDiagramm.svg ""
