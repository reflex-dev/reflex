/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *  Author: Stefan Nuernberger
 **/
#include "reflex/io/SCP1000_I2C.h"

namespace reflex
{

using devices::START_WRITE;
using devices::START_READ;
using devices::CHECK_ACK;
using devices::STOP;
using devices::ACK;

using mcu::Port2;

void big_delay()
{
    for (volatile uint16 i = 0; i !=10000; ++i) {
        asm("   nop");
    }
}

/* constructor */
SCP1000_I2C::SCP1000_I2C() :
        PowerManageAble(SECONDARY)
        ,port2IV(mcu::interrupts::PORT2)
        ,measureFunc(*this)
        ,resultFunc(*this)
{
    this->setSleepMode(mcu::LPM4); //set deepest possible sleep mode of driver (in this case the port interrupt)
    (*port2IV)[Port2::Interrupt_traits::PIN6]=&ready; //register ready event at interrupt dispatcher

    // set factory programmed slave address
    setAddress(SLAVE_ADDRESS);

    // connect activity functors
    measure.init(&measureFunc); // request measurement
    ready.init(&resultFunc); // read results
}

/* i2c_configure
 * configure the SCP sensor
 * called after the master has changed.
 */
void SCP1000_I2C::i2c_configure()
{
startup:
    // STARTUP SEQUENCE:
    // ~60msec delay after powerup/reset
    // status register check for end of STARTUP sequence
    // EEPROM checksum error check
    // low noise configuration
    // 100msec delay before standby is reached

    // check status register (startup still running while STARTUP bit is set)
    // FIXME: this may deadlock if sensor is faulty
    uint16 status; // need 16 bit for error code detection, although STATUS reg is only 8 bit wide
    do {
        big_delay();
        status = readRegister(STATUS);
    } while ((status & STARTUP) || (status == SENSOR_NOT_READY));

    // LSB of DATARD8 is zero on EEPROM checksum error
    if (!(readRegister(DATARD8) & STARTUP)) {
        // some error reporting through temperature output
//        if (temperature) temperature->assign(SENSOR_EEPROM_ERROR);
        // reset ASIC and retry
        writeRegister(RSTR, RESET_ASIC);
        goto startup;
    }

    // low noise configuration
    writeRegister(ADDPTR, MODTEST2);
    writeRegister(DATAWR, REDUCED_NOISE);
    writeRegister(OPERATION, OP_WRITE_INDIRECT);

    // wait 100ms after low noise configuration
    big_delay();
    disable(); // switchOff doesn't work because device is not marked enabled, yet
}


/* init
 * connect outputs for pressure and temperature sensor values
 */
void SCP1000_I2C::init(Sink1<uint32> *pressure, Sink1<uint16> *temperature)
{
    this->pressure = pressure;
    this->temperature = temperature;
}

/* startMeasure
 * trigger measurement
 */
void SCP1000_I2C::startMeasure()
{
    switchOff(); // PM call to disable() if still running (IMPLICIT RESET AFTER OPERATION FAILURE)
    switchOn(); // PM call to enable()
}

void SCP1000_I2C::readResults()
{
    Port2()->IFG &= ~0x40; // clear interrupt flag

    uint16 tValue; // temperature value (14 bit)
    uint32 pValue; // pressure value (19 bit)

    // read temperature
    tValue = readRegister(TEMPOUT);
    // read pressure most significant byte
    pValue = ((uint32)readRegister(DATARD8)) << 16;
    // read pressure low bytes (resets DRDY interrupt)
    pValue |= readRegister(DATARD16);

    // pressure result is always 19 bits wide (pValue[18:0])
    // with last 2 bits 0 in high resolution (17 bit)?,
    // with last 4 bits 0 in low resolution (15 bits)? FIXME: please check!
    // convert: pValue / 4 = Pa

    // temperature result is 14 bits wide (tValue[13:0])
    // tValue[13] is the sign bit (marks negative values)
    // tValue[12:0] hold the integer representation of temperature.
    // convert:
    //  if sign bit is zero: tValue / 20 = °C
    //  if sign bit is one, convert based on 2's-Complement:
    //      (((~tValue) & 0x3fff) + 0x01) / 20 = -°C

    // disable sensor (STANDBY)
    switchOff(); // PM call to disable()

    if (pressure)
    {
        pressure->assign(pValue);
    }
    if (temperature)
    {
        temperature->assign(tValue);
    }
}

/* writeRegister
 * write a register
 * write a register on the device
 * @param reg, address of the register
 * @param val, new register value
 */
void SCP1000_I2C::writeRegister(uint8 reg, uint8 val)
{
    i2c_command(START_WRITE);
    if (!i2c_command(CHECK_ACK)) goto err_out;

    i2c_send(reg); // write register address
    if (!i2c_command(CHECK_ACK)) goto err_out;

    i2c_send(val); // write register value
    if (!i2c_command(CHECK_ACK)) goto err_out;
    i2c_command(STOP);

    // may fall through since no errors are reported
err_out:
    return;
}

uint16 SCP1000_I2C::readRegister(uint8 reg)
{
    uint16 res = 0; // result value

    i2c_command(START_WRITE);
    if (!i2c_command(CHECK_ACK)) goto err_out;
    i2c_send(reg);
    if (!i2c_command(CHECK_ACK)) goto err_out;
    i2c_command(START_READ);
    if (!i2c_command(CHECK_ACK)) goto err_out;
    // DATARD16 and TEMPOUT are 16 bit registers, all others 8 bit
    if ((reg == DATARD16) || (reg == TEMPOUT)) {
        res = i2c_recv(ACK) << 8; // read first byte, send ack
        res |= i2c_recv(STOP); // read second byte, don't ack
    } else {
        res = i2c_recv(STOP); // read single byte, NACK+STOP afterwards
    }

    return res;

err_out:
    return SENSOR_NOT_READY;
}

/* enable
 * configure sensor in correct operation mode, start operation
 */
void SCP1000_I2C::enable()
{
    // configure interrupt pin
    Port2()->DIR &= ~0x40; // pin 2.6 (DRDY) is input
    Port2()->IES &= ~0x40; // interrupt on rising edge
    Port2()->IFG &= ~0x40; // clear interrupt flag
    Port2()->IE |= 0x40; // interrupt enable pin 2.6 (DRDY)

    // check whether it is safe to switch to new opmode (last op finished)
    uint16 opstatus; // need 16 bit for error detection, although OPSTATUS reg is only 8 bit wide
    do {
        big_delay();
        opstatus = readRegister(OPSTATUS);
    } while ((opstatus & RUNNING) || (opstatus == SENSOR_NOT_READY));

    writeRegister(OPERATION, OP_ULTRA_LOW_POWER); // start measurement in ULP mode
}

/* disable
 * switch off sensor
 */
void SCP1000_I2C::disable()
{
    Port2()->IE &= ~0x40; //interrupt off pin 2.6 (DRDY)
    writeRegister(OPERATION, OP_STAND_BY);
}

} // ns reflex
