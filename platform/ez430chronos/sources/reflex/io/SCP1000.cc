/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 **/

#include "reflex/MachineDefinitions.h"
#include "reflex/io/SCP1000.h"
#include "reflex/io/Ports.h"

using namespace reflex;
using namespace mcu;



SCP1000::SCP1000() : PowerManageAble(PowerManageAble::PRIMARY), initFunctor(*this)
					,port2IV(mcu::interrupts::PORT2)
{
	this->setSleepMode(mcu::LPM4); //set deepest possible sleep mode of driver (in this case the port interrupt)

	(*port2IV)[Port2::Interrupt_traits::PIN6]=this; //register our notify at pin6 of the port2 interrupt dispatcher

	currentMode = 0;
	sample.init(this);

}


void SCP1000::run()
{

}


void SCP1000::init(Sink0* interruptOut, Sink1<uint16>* tempOut)
{
	this->interruptOut = interruptOut;
	this->tempOut = tempOut;
}

void SCP1000::setMode(uint8 mode,uint8 resolution)
{
	//must go this way because of the fucking protosthread
	//timer does not allow me to use parameters
	this->currentMode = mode;
	this->resolution = resolution;
	setMode_internal();
}


void SCP1000::setMode_internal()
{

	uint8 success, status, eeprom, timeout;

	PT_BEGIN;


	PS_INT_DIR &= ~PS_INT_PIN;            	// DRDY is input
	PS_INT_IES &= ~PS_INT_PIN;				// Interrupt on DRDY rising edge
	PS_TWI_OUT |= PS_SCL_PIN + PS_SDA_PIN; 	// SCL and SDA are outputs by default
	PS_TWI_DIR |= PS_SCL_PIN + PS_SDA_PIN; 	// SCL and SDA are outputs by default

	// Reset global ps_ok flag
//	ps_ok = 0;

		// 100msec delay to allow VDD stabilisation
		//Timer0_A4_Delay(CONV_MS_TO_TICKS(100));
	PT_YIELD_TIMER(100, initFunctor);

		// Reset pressure sensor -> powerdown sensor
	success = ps_write_register(0x06, 0x01);

		// 100msec delay
		//Timer0_A4_Delay(CONV_MS_TO_TICKS(100));
	PT_YIELD_TIMER(100, initFunctor);

		// Check if STATUS register BIT0 is cleared
	status = ps_read_register(0x07, PS_TWI_8BIT_ACCESS);
	if (((status & BIT0) == 0) && (status != 0))
	{
		// Check EEPROM checksum in DATARD8 register
//		eeprom = ps_read_register(0x7F, PS_TWI_8BIT_ACCESS);
//		if (eeprom == 0x01) ps_ok = 1;
//		else 				ps_ok = 0;
	} else {while(1);}

	enable();  //wir machen es einfach mal an
	PT_END;
}





uint16 SCP1000::getTemperature()
{
  //uint16 data = ps_read_register(0x81, PS_TWI_16BIT_ACCESS);
  //return data;

  uint16 temp = 0;
  uint8 is_negative = 0;
  uint16 kelvin;
  uint16 data = ps_read_register(0x81, PS_TWI_16BIT_ACCESS);
  if ((data & 0x2000) == 0x2000)
    {
      // Sign extend temperature
      data |= 0xC000;
      // Convert two's complement
      data = ~data;
      data += 1;
      is_negative = 1;
    }

  temp = data / 2;

  // Convert from °C to °K
  if (is_negative)	kelvin = 2732 - temp;
  else				kelvin = temp + 2732;

  return (kelvin);


}



void reflex::SCP1000::enable()
{
	//setMode(currentMode, resolution);
	// Start sampling data in ultra low power mode
	Port2()->IE |= 0x40;
	ps_write_register(0x03, 0x0B);

}

void reflex::SCP1000::disable()
{
	Port2()->IE &= ~0x40; //interrupt off pin 2.6
	// Put sensor to standby
	ps_write_register(0x03, 0x00);
}

void reflex::SCP1000::notify()
{


	uint16 data = ps_read_register(0x7F, PS_TWI_8BIT_ACCESS);
	data = ps_read_register(0x80, PS_TWI_16BIT_ACCESS); //dummy read
	Port2()->IFG &= ~0x40; //reset interrupt status
	if (interruptOut) interruptOut->notify(); //notify connected sink
}
/*
//dann bei run.....
void ps_start(void)
{

	ps_write_register(0x03, 0x0B);
}



//bei run
void ps_stop(void)
{
	// Put sensor to standby
	ps_write_register(0x03, 0x00);
}
*/


// *************************************************************************************************
// @fn          ps_twi_sda
// @brief       Control SDA line
// @param       u8 condition		PS_TWI_SEND_START, PS_TWI_SEND_RESTART, PS_TWI_SEND_STOP
//										PS_TWI_CHECK_ACK
// @return      u8					1=ACK, 0=NACK
// *************************************************************************************************
uint8 reflex::SCP1000::ps_twi_sda(uint8 condition)
{
	uint8 sda = 0;

	if (condition == PS_TWI_SEND_START)
	{
		PS_TWI_SDA_OUT;			// SDA is output
		PS_TWI_SCL_HI;
		twi_delay();
		PS_TWI_SDA_LO;
		twi_delay();
		PS_TWI_SCL_LO;			// SCL 1-0 transition while SDA=0
		twi_delay();
	}
	else if (condition == PS_TWI_SEND_RESTART)
	{
		PS_TWI_SDA_OUT;			// SDA is output
		PS_TWI_SCL_LO;
		PS_TWI_SDA_HI;
		twi_delay();
		PS_TWI_SCL_HI;
		twi_delay();
		PS_TWI_SDA_LO;
		twi_delay();
		PS_TWI_SCL_LO;
		twi_delay();
	}
	else if (condition == PS_TWI_SEND_STOP)
	{
		PS_TWI_SDA_OUT;			// SDA is output
		PS_TWI_SDA_LO;
		twi_delay();
		PS_TWI_SCL_LO;
		twi_delay();
		PS_TWI_SCL_HI;
		twi_delay();
		PS_TWI_SDA_HI;			// SDA 0-1 transition while SCL=1
		twi_delay();
	}
	else if (condition == PS_TWI_CHECK_ACK)
	{
		PS_TWI_SDA_IN;			// SDA is input
		PS_TWI_SCL_LO;
		twi_delay();
		PS_TWI_SCL_HI;
		twi_delay();
		sda = PS_TWI_IN & PS_SDA_PIN;
		PS_TWI_SCL_LO;
	}

	// Return value will only be evaluated when checking device ACK
	return (sda == 0);
}



// *************************************************************************************************
// @fn          twi_delay
// @brief       Delay between TWI signal edges.
// @param       none
// @return      none
// *************************************************************************************************
void reflex::SCP1000::twi_delay()
{
	asm("	nop");
}


// *************************************************************************************************
// @fn          ps_twi_write
// @brief       Clock out bits through SDA.
// @param       u8 data		Byte to send
// @return      none
// *************************************************************************************************
void reflex::SCP1000::ps_twi_write(uint8 data)
{
	uint8 i, mask;

	// Set mask byte to 10000000b
	mask = BIT0<<7;

	PS_TWI_SDA_OUT;		// SDA is output

	for (i=8; i>0; i--)
	{
		PS_TWI_SCL_LO;	// SCL=0
		if ((data & mask) == mask)
		{
			PS_TWI_SDA_HI; // SDA=1
		}
		else
		{
			PS_TWI_SDA_LO; // SDA=0
		}
		mask = mask >> 1;
		twi_delay();
		PS_TWI_SCL_HI;	// SCL=1
		twi_delay();
	}

	PS_TWI_SCL_LO;		// SCL=0
	PS_TWI_SDA_IN;		// SDA is input
}


// *************************************************************************************************
// @fn          ps_twi_read
// @brief       Read bits from SDA
// @param       u8 ack		1=Send ACK after read, 0=Send NACK after read
// @return      u8			Bits read
// *************************************************************************************************
uint8 reflex::SCP1000::ps_twi_read(uint8 ack)
{
	uint8 i;
	uint8 data = 0;

	PS_TWI_SDA_IN;		// SDA is input

	for (i=0; i<8; i++)
	{
		PS_TWI_SCL_LO;			// SCL=0
		twi_delay();
		PS_TWI_SCL_HI;			// SCL=0
		twi_delay();

		// Shift captured bits to left
		data = data << 1;

		// Capture new bit
		if ((PS_TWI_IN & PS_SDA_PIN) == PS_SDA_PIN) data |= BIT0;
	}

	PS_TWI_SDA_OUT;			// SDA is output

	// 1 aditional clock phase to generate master ACK
	PS_TWI_SCL_LO;			// SCL=0
	if (ack == 1)	PS_TWI_SDA_LO		// Send ack -> continue read
	else			PS_TWI_SDA_HI		// Send nack -> stop read
	twi_delay();
	PS_TWI_SCL_HI;			// SCL=0
	twi_delay();
	PS_TWI_SCL_LO;

	return (data);
}



// *************************************************************************************************
// @fn          as_write_register
// @brief  		Write a byte to the pressure sensor
// @param       u8 address			Register address
//				u8 data			Data to write
// @return      u8
// *************************************************************************************************
uint8 reflex::SCP1000::ps_write_register(uint8 address, uint8 data)
{
  volatile uint8 success;

  ps_twi_sda(PS_TWI_SEND_START);			// Generate start condition

  ps_twi_write((0x11<<1) | PS_TWI_WRITE); 	// Send 7bit device address 0x11 + write bit '0'
  success = ps_twi_sda(PS_TWI_CHECK_ACK);	// Check ACK from device
  if (!success) return (0);

  ps_twi_write(address);					// Send 8bit register address
  success = ps_twi_sda(PS_TWI_CHECK_ACK);	// Check ACK from device
  if (!success) return (0);

  ps_twi_write(data);						// Send 8bit data to register
  success = ps_twi_sda(PS_TWI_CHECK_ACK);	// Check ACK from device
 // Slave does not send this ACK
 // if (!success) return (0);

  ps_twi_sda(PS_TWI_SEND_STOP);				// Generate stop condition

  return (1);
}


// *************************************************************************************************
// @fn          ps_read_register
// @brief       Read a byte from the pressure sensor
// @param       u8 address		Register address
//				u8	mode		PS_TWI_8BIT_ACCESS, PS_TWI_16BIT_ACCESS
// @return      u16			Register content
// *************************************************************************************************
uint16 reflex::SCP1000::ps_read_register(uint8 address, uint8 mode)
{
  uint8 success;
  uint16 data = 0;

  ps_twi_sda(PS_TWI_SEND_START);			// Generate start condition

  ps_twi_write((0x11<<1) | PS_TWI_WRITE); 	// Send 7bit device address 0x11 + write bit '0'
  success = ps_twi_sda(PS_TWI_CHECK_ACK);	// Check ACK from device
  if (!success) return (0);

  ps_twi_write(address);					// Send 8bit register address
  success = ps_twi_sda(PS_TWI_CHECK_ACK);	// Check ACK from device
  if (!success) return (0);

  ps_twi_sda(PS_TWI_SEND_RESTART);			// Generate restart condition

  ps_twi_write((0x11<<1) | PS_TWI_READ); 	// Send 7bit device address 0x11 + read bit '1'
  success = ps_twi_sda(PS_TWI_CHECK_ACK);	// Check ACK from device
  if (!success) return (0);

  if (mode == PS_TWI_16BIT_ACCESS)
  {
	  data =  ps_twi_read(1) << 8;			// Read MSB 8bit data from register
	  data |= ps_twi_read(0);				// Read LSB 8bit data from register
  }
  else
  {
	  data = ps_twi_read(0);				// Read 8bit data from register
  }

  ps_twi_sda(PS_TWI_SEND_STOP);				// Generate stop condition

  return (data);
}


// dummy read um in zu clearen
/*
u32 ps_get_pa(void)
{
	volatile u32 data = 0;

	// Get 3 MSB from DATARD8 register
	data = ps_read_register(0x7F, PS_TWI_8BIT_ACCESS);
	data = ((data & 0x07) << 8) << 8;

	// Get 16 LSB from DATARD16 register
	data |= ps_read_register(0x80, PS_TWI_16BIT_ACCESS);

	// Convert decimal value to Pa
	data = (data >> 2);

	return (data);
}

*/


// *************************************************************************************************
// @fn          ps_get_temp
// @brief       Read out temperature.
// @param       none
// @return      u16		13-bit temperature value in xx.x°K format
// *************************************************************************************************
uint16 reflex::SCP1000::ps_get_temp()
{
	volatile uint16 data = 0;
	uint16 temp = 0;
	uint8 is_negative = 0;
	uint16 kelvin;

	// Get 13 bit from TEMPOUT register
	data = ps_read_register(0x81, PS_TWI_16BIT_ACCESS);

/*	// Convert negative temperatures
	if ((data & BIT(13)) == BIT(13))
	{
		// Sign extend temperature
		data |= 0xC000;
		// Convert two's complement
		data = ~data;
		data += 1;
		is_negative = 1;
	}

	temp = data / 2;

	// Convert from °C to °K
	if (is_negative)	kelvin = 2732 - temp;
	else				kelvin = temp + 2732;

	return (kelvin);*/
	return data;
}


//----------------------------------------------------------------------------



























