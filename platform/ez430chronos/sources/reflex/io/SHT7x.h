#ifndef SHT7X_H
#define SHT7X_H

/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *	Class(ses):	SHT7x
 *	Author:		Stefan Nuernberger
 *
 *	Description:	Driver for the temperature/humidity sensor SHT7x
 *	    Communication with this sensor requires implementation of a
 *      protocol, which is very similar but not compatible to I2C
 *      standard. Therefore tight integration in controller
 *      context is done.
 *
 *      ATTENTION!:
 *      Driver does power-on at request time. A virtual timer is used to
 *      generate a sensor ready interrupt. A port interrupt is used to
 *      signal the availability of a measurement result. The power state
 *      of the device is managed exclusively by the driver (SECONDARY).
 *      Thus the device does not need to be put in any PowerManager group.
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 */

#include "reflex/types.h"
#include "reflex/powerManagement/PowerManageAble.h"
#include "reflex/sinks/Sink.h"
#include "reflex/sinks/Event.h"
#include "reflex/scheduling/ActivityFunctor.h"
#include "reflex/timer/VirtualTimer.h"
#include "reflex/interrupts/InterruptDispatcher.h"
#include "reflex/data_types/Singleton.h"
#include "reflex/interrupts/InterruptLock.h"
#include "reflex/MachineDefinitions.h"

// we need to check conf.h for redefinition of SHT_MAX_REQUESTS
#include "conf.h"

// you may define SHT_MAX_REQUESTS in conf.h yourself
#ifndef SHT_MAX_REQUESTS
#define SHT_MAX_REQUESTS 2
#endif

namespace reflex {

template <typename TPort, uint8 POWERPIN, uint8 CLKPIN, uint8 DATAPIN>
class SHT7x:
        public Sink0, // interrupt notification
        public Sink1<uint8>, // command and control
        public PowerManageAble // PM features
{
private:

        uint8 currentRequest; // currently active request (value)
        uint8 request[SHT_MAX_REQUESTS]; // pending requests
        uint8 firstRequest; // first pending request (index)
        uint8 nextRequest; // value for next request (index)
        uint8 status; // last used status register value

        Event sensorReady; // triggered by the startup timer
        Event resultAvailable; // triggered by interrupt handler
        Event sensorTimeout; // triggered by timeout timer

        // timer to signal ready state -> measurement start
        VirtualTimer startupTimer;
        // timer to signal sensor timeout
        VirtualTimer timeoutTimer;

        data_types::Singleton<typename TPort::IVDispatcher> portIV; // where port interrupts are delegated

public:

        // outputs for temperature and humidity
        Sink1<uint16>* temperature;
        Sink1<uint16>* humidity;

        /**
         * operation of sensor
         * NOTE: TEMPERATURE is zero! Least significant bit is used to
         * distinguish between measurement types.
         * HUMIDITY sets LSB, TEMPERATURE leaves it zero.
         * BIG FAT WARNING: DO NOT USE "TEMPERATURE" TO TEST FOR A CONDITION!
         * IT WILL ALWAYS EVALUATE TO FALSE! ALWAYS USE "HUMIDITY" INSTEAD!
         * The other values might be combined as desired.
         */
        enum Operation {
                TEMPERATURE = 0x00, // measure temperature
                HUMIDITY = 0x01, // measure humidity
                HEATER = 0x02, // use built-in heater
                LOW_RES = 0x04, // use low resolution
                NO_CALIBRATE = 0x08 // do not calibrate from OTP memory
        };

        /**
         * constructor
         */
        SHT7x();

        /**
         * assign
         * implements Sink1 interface
         * used to receive measurement requests from application
         * activates the sensor and sets a timer for measurement start
         * @param request bitmask indicating desired operation
         * (see enum Operation)
         */
        virtual void assign(uint8 request);

        /**
         * init
         * connect the data outputs of the sensor
         * @param temp Temperature output
         * @param hum Humidity output
         */
        void init(Sink1<uint16>* temp, Sink1<uint16>* hum);

        /**
         * notify
         * implements Sink0 interrupt notification
         * It is responsible for resetting the hardware to allow
         * further interrupts.
         */
        virtual void notify();

        /**
         * enable
         * PowerManagement enable function.
         * enables interrupt on DATA port
         */
        virtual void enable();

        /**
         * disable
         * PowerManagement disable function.
         * disables interrupt on DATA port
         */
        virtual void disable();

private:

        /**
         * sendCommand
         * This method issues a command to the sensor and enables the
         * interrupt for the result
         */
        void sendCommand();

        /**
         * getResult
         * This method reads the result from the sensor after an interrupt
         */
        void getResult();

        /**
         * timeout
         * This method stops the current sensor reading due to sensor timeout
         */
        void timeout();

        /** activity triggered by the sensorReady event */
        ActivityFunctor<SHT7x, &SHT7x::sendCommand> sendCommandFunctor;
        /** activity triggered by resultAvailable event */
        ActivityFunctor<SHT7x, &SHT7x::getResult> getResultFunctor;
        /** activity triggered by sensorTimeout event */
        ActivityFunctor<SHT7x, &SHT7x::timeout> timeoutFunctor;

        /**
         * handleNextRequest
         * advance request pointer (ringbuffer) and
         * trigger next measurement or deactivate sensor
         * if no request is left
         */
        void handleNextRequest();

        /**
         * initTransmit
         * sends the "start transmission" sequence
         */
        void initTransmit();

        /**
         * readAck
         * tries to read an acknowledgment.
         * @return 0 if everything went well
         */
        int readAck();

        /**
         * writeAck
         * write an Acknowledgment
         */
        void writeAck();

        /**
         * readByte
         * read a single byte
         * @return the received byte
         */
        unsigned char readByte();

        /**
         * writeByte
         * write a single byte
         * @param byte the byte to write
         */
        void writeByte(char byte);

        enum BitmasksAndPositions {
                // bitmasks for pin operation
                POWER = 0x1 << POWERPIN, // POWER pin
                CLK = 0x1 << CLKPIN, // serial clock pin
                DATA = 0x1 << DATAPIN, // data pin

                // those are bitmasks for commands
                CMD_SR_READ = 0x07, // read status register
                CMD_SR_WRITE = 0x06, // write status register
                CMD_MEASURE_TEMP = 0x03, // measure temperature
                CMD_MEASURE_HUM	 = 0x05, // measure humidity
                CMD_SOFT_RESET = 0x1e, // interface and status reg. reset (>11ms wait)

                // those mark bit positions in the status register
                SR_RESOLUTION = 0, // SR resolution bit
                                   // 0 = high res (12 bit hum / 14 bit temp)
                                   // 1 = low res (8 bit hum / 12 bit temp)
                SR_OTP_RELOAD = 1, // SR calibration reload bit
                                   // 0 = reload configuration from OTP memory
                                   // 1 = do not reload from OTP memory
                SR_HEATER = 2, // SR heater bit (0 = off)
                SR_LOWBATTERY = 6 // SR low battery bit (0 = OK)
        };

        enum Timing {
                SHT_POWERUP_MSEC = 12, // milliseconds from power to ready
                SHT_MEASURE_TIMEOUT = 700 // milliseconds before timeout
        };

        enum Errors {
                SENSOR_ERROR = 0xffff, // default sensor error
                SENSOR_TIMEOUT = 0xfffe // measurement timed out
        };
};

/**
 * constructor
 */
template <typename TPort, uint8 POWERPIN, uint8 CLKPIN, uint8 DATAPIN>
SHT7x<TPort,POWERPIN,CLKPIN,DATAPIN>::SHT7x(): PowerManageAble(PowerManageAble::SECONDARY),
                                firstRequest(0), nextRequest(0), status(0),
                                startupTimer(VirtualTimer::ONESHOT),
                                timeoutTimer(VirtualTimer::ONESHOT),
                                portIV(TPort::Interrupt_traits::globalVector()),
                                sendCommandFunctor(*this),
                                getResultFunctor(*this),
                                timeoutFunctor(*this)
{
	startupTimer.connect_output(&sensorReady);
	timeoutTimer.connect_output(&sensorTimeout);
        sensorReady.init(&sendCommandFunctor); // register Activity for timer event
        resultAvailable.init(&getResultFunctor); // register Activity for interrupt
        sensorTimeout.init(&timeoutFunctor); // register Activity for sensor timeout

        // initialize Port
        TPort()->SEL &= ~(CLK | DATA | POWER); // set pins to I/O functionality
        TPort()->IES |= DATA; // select falling edge for DATA line interrupt
        TPort()->IE &= ~(CLK | DATA | POWER); // disable interrupts on all pins
        TPort()->DIR |= POWER; // make power an output pin
        TPort()->OUT &= ~POWER; // deactivate power line (switch sensor off)
        TPort()->DS |= POWER; // set high output drive strength operation on power pin

        // register interrupt handler on data pin
        (*portIV)[TPort::Interrupt_traits::localVector(DATAPIN)] = this;

        // external port interrupts still work on deepest sleep mode
        setSleepMode(mcu::LPM4);
}

/**
 * init
 * connect the data outputs of the sensor
 * @param temp Temperature output
 * @param hum Humidity output
 */
template <typename TPort, uint8 POWERPIN, uint8 CLKPIN, uint8 DATAPIN>
void SHT7x<TPort,POWERPIN,CLKPIN,DATAPIN>::init(Sink1<uint16>* temp, Sink1<uint16>* hum) {
        temperature = temp;
        humidity = hum;
}

/**
 * assign
 * implements Sink1 interface
 * used to receive measurement requests from application
 * activates the sensor and sets a timer for measurement start
 * @param request bitmask indicating desired operation
 * (see enum Operation)
 */
template <typename TPort, uint8 POWERPIN, uint8 CLKPIN, uint8 DATAPIN>
void SHT7x<TPort,POWERPIN,CLKPIN,DATAPIN>::assign(uint8 request)
{
        // critical, update ringbuffer
        InterruptLock lock;

        // enable sensor if deactivated
        if (nextRequest == firstRequest) {
                // activate sensor (power up)
                TPort()->OUT |= POWER;
                // set timer
                startupTimer.set(SHT_POWERUP_MSEC);
        }

        // copy request (will be evaluated by run())
        this->request[nextRequest++] = request;
        // update nextRequest
        if (nextRequest == SHT_MAX_REQUESTS)
                nextRequest = 0;
}

/**
 * delay
 * just a little spinning for clear signals
 */
inline void delay() {
    volatile int i = 0;
    while (i != 1000) ++i;
}

/**
 * sendCommand
 * This method issues a command to the sensor and enables the
 * interrupt for the result
 */
template <typename TPort, uint8 POWERPIN, uint8 CLKPIN, uint8 DATAPIN>
void SHT7x<TPort,POWERPIN,CLKPIN,DATAPIN>::sendCommand()
{
        // critical, get request from ringbuffer
        if (true) {
                InterruptLock lock;
                currentRequest = this->request[firstRequest];
        }

        // compute status register from request
        unsigned char newStatus =
                          (((currentRequest & HEATER) && true) << SR_HEATER)
                        | (((currentRequest & LOW_RES) && true) << SR_RESOLUTION)
                        | (((currentRequest & NO_CALIBRATE) && true) << SR_OTP_RELOAD);

        // write status (if changed)
        if (newStatus != status) {
                initTransmit();
                writeByte(CMD_SR_WRITE);
                if (readAck()) goto err_out;
                writeByte(newStatus);
                if (readAck()) goto err_out;

                status = newStatus;
        }

        initTransmit();
        // transmit measure command
        if (currentRequest & HUMIDITY) { // measure humidity
                writeByte(CMD_MEASURE_HUM);
        } else { // measure temperature
                writeByte(CMD_MEASURE_TEMP);
        }
        // read acknowledgment
        if (readAck()) goto err_out;

        // wait for result (until DATA is low) or timeout
        // NOTE: we are notified by interrupt
        TPort()->DIR &= ~DATA; // data is input
        TPort()->REN |= DATA; // data with pullup/down
        TPort()->OUT |= DATA; // pullup

        delay();

        if ((TPort()->IN & DATA) != 0) {
            // set timer for measure timeout
            timeoutTimer.set(SHT_MEASURE_TIMEOUT);
            // enable interrupt
            switchOn();

            return; // no error
        }


err_out:
        TPort()->REN &= ~DATA; // disable pullup/down on data

        // return error value
        if (currentRequest & HUMIDITY) {
                if (humidity)
                        humidity->assign(SENSOR_ERROR);
        } else {
                if (temperature)
                        temperature->assign(SENSOR_ERROR);
        }

        // handle next request or deactivate sensor
        handleNextRequest();
}

/**
 * notify
 * emulates InterruptHandler (from dispatched port interrupt)
 */
template <typename TPort, uint8 POWERPIN, uint8 CLKPIN, uint8 DATAPIN>
void SHT7x<TPort,POWERPIN,CLKPIN,DATAPIN>::notify() {
        InterruptLock lock;

        timeoutTimer.stop(); // no timeout
        // disable Interrupt
        switchOff();
        // clear InterruptFlag
        TPort()->IFG &= ~DATA;
        // announce result available
        resultAvailable.notify();
}

/**
 * getResult
 * This method reads the result from the sensor after an interrupt
 */
template <typename TPort, uint8 POWERPIN, uint8 CLKPIN, uint8 DATAPIN>
void SHT7x<TPort,POWERPIN,CLKPIN,DATAPIN>::getResult() {
        uint16 result = 0;
        result |= (readByte() << 8); // MSB
        writeAck(); // ack first byte
        result |= readByte(); // LSB
        // skip ack since we don't need CRC

        TPort()->REN &= ~DATA; // disable pullup/down on data

        // return value
        if (currentRequest & HUMIDITY) {
                if (humidity) humidity->assign(result);
        } else {
                if (temperature) temperature->assign(result);
        }

        // handle next request or deactivate sensor
        handleNextRequest();
}

/**
 * timeout
 * This method stops the current sensor reading due to sensor timeout
 */
template <typename TPort, uint8 POWERPIN, uint8 CLKPIN, uint8 DATAPIN>
        void SHT7x<TPort,POWERPIN,CLKPIN,DATAPIN>::timeout() {

    // disable Interrupt
    switchOff();

    TPort()->REN &= ~DATA; // disable pullup/down on data

    // return sensor error
    if (currentRequest & HUMIDITY) {
            if (humidity) humidity->assign(SENSOR_TIMEOUT);
    } else {
            if (temperature) temperature->assign(SENSOR_TIMEOUT);
    }

    // handle next request or deactivate sensor
    handleNextRequest();
}

/**
 * handleNextRequest
 * advance request pointer (ringbuffer) and
 * trigger next measurement or deactivate sensor
 * if no request is left
 */
template <typename TPort, uint8 POWERPIN, uint8 CLKPIN, uint8 DATAPIN>
void SHT7x<TPort,POWERPIN,CLKPIN,DATAPIN>::handleNextRequest() {
        // critical, update ringbuffer
        InterruptLock lock;

        ++firstRequest; // this request is done
        if (firstRequest == SHT_MAX_REQUESTS)
                firstRequest = 0;
        if (nextRequest == firstRequest) {
                TPort()->OUT &= ~POWER; // deactivate sensor
                status = 0; // reset status
        } else {
                // trigger sendCommandFunctor for next measurement
                sendCommandFunctor.trigger();
        }
}

/**
 * enable
 * PowerManagement enable function
 * enables interrupt on DATA port
 */
template <typename TPort, uint8 POWERPIN, uint8 CLKPIN, uint8 DATAPIN>
void SHT7x<TPort,POWERPIN,CLKPIN,DATAPIN>::enable() {
        TPort()->IFG &= ~DATA; // reset all pending interrupts
        TPort()->IE |= DATA;
}

/**
 * disable
 * PowerManagement disable function.
 * disables interrupt on DATA port
 */
template <typename TPort, uint8 POWERPIN, uint8 CLKPIN, uint8 DATAPIN>
void SHT7x<TPort,POWERPIN,CLKPIN,DATAPIN>::disable() {
        TPort()->IE &= ~DATA;
}

/**
 * initTransmit
 * sends the "start transmission" sequence
 *       ____         ____
 *  DATA     |_______|
 *          ___     ___
 *  CLK  __|   |___|   |__
 */
template <typename TPort, uint8 POWERPIN, uint8 CLKPIN, uint8 DATAPIN>
void SHT7x<TPort,POWERPIN,CLKPIN,DATAPIN>::initTransmit()
{
        TPort()->DIR |= (DATA | CLK); // set data and clock to output

        //initial state
        TPort()->OUT &= ~CLK; delay(); //CLK=0
        TPort()->OUT |= DATA; delay(); //DATA=1
        TPort()->OUT |= CLK; delay(); //CLK=1
        TPort()->OUT &= ~DATA; delay(); //DATA=0
        TPort()->OUT &= ~CLK; delay(); //CLK=0
        TPort()->OUT |= CLK; delay(); //CLK=1
        TPort()->OUT |= DATA; delay(); //DATA=1
        TPort()->OUT &= ~CLK; delay(); //CLK=0
}

/**
 * readAck
 * tries to read an acknowledgment.
 * @return 0 if everything went well
 */
template <typename TPort, uint8 POWERPIN, uint8 CLKPIN, uint8 DATAPIN>
int SHT7x<TPort,POWERPIN,CLKPIN,DATAPIN>::readAck()
{
        TPort()->DIR |= CLK; // set clock to output
        TPort()->DIR &= ~DATA; // set data to input

        delay();

        TPort()->OUT |= CLK; delay(); // CLK = 1;
        // DATA should be low now
        if(TPort()->IN & DATA) return -1;
        TPort()->OUT &= ~CLK; delay(); // CLK = 0;

        return 0;
}

/**
 * writeAck
 * write an Acknowledgment
 */
template <typename TPort, uint8 POWERPIN, uint8 CLKPIN, uint8 DATAPIN>
void SHT7x<TPort,POWERPIN,CLKPIN,DATAPIN>::writeAck()
{
        TPort()->DIR |= (CLK | DATA); // set clock and data to output

        TPort()->OUT &= ~DATA; delay(); // DATA = 0;
        TPort()->OUT |= CLK; delay(); // CLK = 1;
        TPort()->OUT &= ~CLK; delay(); // CLK = 0;
        TPort()->OUT |= DATA; delay(); // DATA = 1;
}

/**
 * readByte
 * read a single byte
 * @return the received byte
 */
template <typename TPort, uint8 POWERPIN, uint8 CLKPIN, uint8 DATAPIN>
unsigned char SHT7x<TPort,POWERPIN,CLKPIN,DATAPIN>::readByte()
{
        unsigned char bit, byte = 0;

        TPort()->DIR |= CLK;	// clock is output
        TPort()->DIR &= ~DATA; // data is input

        for (bit = 0x80; bit != 0; bit >>= 1) // shift bit for masking
        {
                TPort()->OUT |= CLK; delay(); // CLK = 1
                if (TPort()->IN & DATA) byte |= bit; // read bit
                TPort()->OUT &= ~CLK; delay(); // CLK = 0;
        }

        return byte;
}

/**
 * writeByte
 * write a single byte
 * @param byte the byte to write
 */
template <typename TPort, uint8 POWERPIN, uint8 CLKPIN, uint8 DATAPIN>
void SHT7x<TPort,POWERPIN,CLKPIN,DATAPIN>::writeByte(char byte)
{
        unsigned char bit;
        TPort()->DIR |= (DATA | CLK); // data and clock as output

        for (bit = 0x80; bit != 0; bit >>= 1) //shift bit for masking
        {
                // set data line
                if (bit & byte)
                        TPort()->OUT |= DATA;	// DATA = 1
                else
                        TPort()->OUT &= ~DATA;	// DATA = 0

                delay();
                // let the clock tick
                TPort()->OUT |= CLK; delay();	// CLK=1
                TPort()->OUT &= ~CLK;	// CLK=0;
    }
}

}


#endif // SHT7X_H
