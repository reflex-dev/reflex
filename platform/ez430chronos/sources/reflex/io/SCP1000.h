/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 **/

#ifndef REFLEX_EZ430_CHRONOS_SCP1000_H
#define REFLEX_EZ430_CHRONOS_SCP1000_H

#include "reflex/io/Ports.h"
#include "reflex/interrupts/InterruptDispatcher.h"
#include "reflex/data_types/Singleton.h"
#include "reflex/sinks/Sink.h"
#include "reflex/powerManagement/PowerManageAble.h"
#include "reflex/scheduling/Activity.h"
#include "reflex/scheduling/ProtoThread.h"
#include "reflex/scheduling/ProtoThreadTimer.h"


namespace reflex {

/**
 * Driver for the SCP1000 pressure and temperature sensor.
 * FIXME: it is quite obvious that this was copied from the SPI CMA3000 driver ;-)
 */
class SCP1000 : public reflex::ProtoThread, reflex::ProtoThreadTimer
			   ,public reflex::Sink0, public PowerManageAble, public reflex::Activity
{

	// *************************************************************************************************
	// Defines section
	#define BIT0           (0x01)
	#define BIT1           (0x02)
	#define BIT2           (0x04)
	#define BIT3           (0x08)
	#define BIT4           (0x10)
	#define BIT5           (0x20)
	#define BIT6           (0x40)
	#define BIT7           (0x80)



	// Port and pin resource for TWI interface to pressure sensor
	// SCL=PJ.3, SDA=PJ.2, DRDY=P2.6
	#define PS_TWI_IN            (PortJ()->IN)
	#define PS_TWI_OUT           (PortJ()->OUT)
	#define PS_TWI_DIR           (PortJ()->DIR)
	#define PS_TWI_REN           (PortJ()->REN)
	#define PS_SCL_PIN           (BIT3)
	#define PS_SDA_PIN           (BIT2)

	// Port, pin and interrupt resource for interrupt from acceleration sensor, DRDY=P2.6
	#define PS_INT_IN            (Port2()->IN)
	#define PS_INT_OUT           (Port2()->OUT)
	#define PS_INT_DIR           (Port2()->DIR)
	#define PS_INT_IE            (Port2()->IE)
	#define PS_INT_IES           (Port2()->IES)
	#define PS_INT_IFG           (Port2()->IFG)
	#define PS_INT_PIN           (BIT6)

	// TWI defines
	#define PS_TWI_WRITE		(0x00)
	#define PS_TWI_READ			(0x01)

	#define PS_TWI_SEND_START	(0x00)
	#define	PS_TWI_SEND_RESTART	(0x01)
	#define	PS_TWI_SEND_STOP	(0x02)
	#define	PS_TWI_CHECK_ACK	(0x03)

	#define PS_TWI_8BIT_ACCESS	(0x00)
	#define PS_TWI_16BIT_ACCESS	(0x01)

	#define PS_TWI_SCL_HI		{ PS_TWI_OUT |=  PS_SCL_PIN; }
	#define PS_TWI_SCL_LO		{ PS_TWI_OUT &= ~PS_SCL_PIN; }
	#define PS_TWI_SDA_HI		{ PS_TWI_OUT |=  PS_SDA_PIN; }
	#define PS_TWI_SDA_LO		{ PS_TWI_OUT &= ~PS_SDA_PIN; }
	#define PS_TWI_SDA_IN		{ PS_TWI_OUT |=  PS_SDA_PIN; PS_TWI_DIR &= ~PS_SDA_PIN; }
	#define PS_TWI_SDA_OUT		{ PS_TWI_DIR |=  PS_SDA_PIN; }



public:

	/*
	 * operation modes, specified with setMode
	 */
	enum MODE {
		 Measure100 = 0x02
		,Measure400 = 0x04
		,Measure40 = 0x06
		,MovementDetection = 0x08
		,FreeFallDetection100 = 0x0A
		,FreeFallDetection400 = 0x0C
	};
	/*
	 * resolution, specified with setMode
	 */
	enum RESOLUTION {
			 twoG = 0x80
			,eightG = 0x00
		};

private:
	enum Registers {
		 WHO_AM_I = 0x00
		,REVID = 0x01
		,CTRL = 0x02
		,STATUS = 0x03
		,RSTR = 0x04
		,INT_STATUS = 0x05
		,DOUTX = 0x06
		,DOUTY = 0x07
		,DOUTZ = 0x08
		,MDTHR = 0x0A
		,MDFFTMR = 0x0B
		,I2C_ADDR = 0x0C
	};
	enum BITS {
		 writeFlag = 0x02
		//ctrl
		,G_RANGE = 0x80
		,INT_LEVEL = 0x40
		,MDET_EXIT = 0x20
		,I2C_DIS = 0x10
		,MODE_PD = 0x00
		,MODE_M100 = 0x02
		,MODE_M400 = 0x04
		,MODE_M40 = 0x06
		,MODE_MD = 0x08
		,MODE_FFD100 = 0x0A
		,MODE_FFD400 = 0x0C
		,INT_DIS = 0x01
		//reset
		,RESET = 0x02
		//int_status
		,FFDET  = 0x4
		,MDET_X = 0x01
		,MDET_Y = 0x02
		,MDET_Z = 0x03
		,MEDT_NO = 0x00
		//ffthr
		,FFTHR = 0x1F
	};

public:

	SCP1000();

	~SCP1000() {}


	/**
	 * init
	 * connect the interrupt signal and data output of the sensor
	 *
	 * @param interruptOut interrupt signal
	 * @param dataOut x,y,z value output
	 */
	void init(Sink0* interruptOut, Sink1<uint16>* tempOut);

	/**
	* configuration of the sensor
	*
	* @param mode
	* @param resolution
	*/
	void setMode(uint8 mode, uint8 resolution);



	/**
	 * endpoint of the interrupt dispatcher and handle method for
	 * interrupts from the sensor
	 */
	virtual void notify();

	/**
	 * activity for readout of sensor values
	 */
	virtual void run();

	/**
	 * writes a SCP1000 register using spi
	 *
	 * @param address
	 * @param data
	 * @return status byte
	 */
	uint8 writeRegister(uint8 address, uint8 data);

	/**
	 * reads SCP1000 register using spi
	 *
	 * @param address
	 * @return
	 */
	uint8 readRegister(uint8 address);

	/**
	 *
	 *
	 * @return
	 */
	uint16 getTemperature();

	/**
	 * triggers the activity
	 */
	Event sample;


	uint8 ps_twi_sda(uint8 condition);
	void twi_delay();
	void ps_twi_write(uint8 data);
	uint8 ps_twi_read(uint8 ack);
	uint8 ps_write_register(uint8 address, uint8 data);
	uint16 ps_read_register(uint8 address, uint8 mode);
	uint16 ps_get_temp();



protected:
	/**
	 * enabled the interrupt of the sensor
	 */
	virtual void enable();
	/**
	 * disables the interrupt of the sensor
	 */
	virtual void disable();

	/**
	 * internal method for configuring the sensor, needed this way because
	 * of the f*$%ing protothrads
	 */
	void setMode_internal();

	/** This is the Activity Functor
	 *  Pointing to a class and to a member function of
	 *  this class
	 */
	ActivityFunctor<SCP1000, &SCP1000::setMode_internal> initFunctor;

	/*
	 * internal pointer to the receiver of the interrupt signal
	 */
	Sink0* interruptOut;

	/**
	 * internal pointer to the data sink
	 */
	Sink1<uint16>* tempOut;


protected:
	/**
	 * where the interrupts of port2 are delegated to
	 */
	data_types::Singleton< mcu::Port2::IVDispatcher > port2IV;


	/**
	 * current mode of sensor operation
	 */
	uint8 currentMode;

	/**
	 * current resolution (2g or 8g)
	 */
	uint8 resolution;

};


}// ns reflex


#endif // SCP1000_H
