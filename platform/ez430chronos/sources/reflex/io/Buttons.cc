/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 **/
#include "reflex/MachineDefinitions.h"
#include "reflex/io/Buttons.h"
#include "reflex/powerManagement/PowerManageAble.h"

using namespace reflex;
using namespace mcu;

reflex::Buttons::Buttons()
		: PowerManageAble(PRIMARY)
		, port2IV(mcu::interrupts::PORT2)
{
	this->setSleepMode(mcu::LPM4);
}



/*! configures the pins on port2 for all buttons and finally the portinterrupts
	where enabled. The direction will be configured as input with enabled internal
	pulldown resistors. The interrupt transition will be configured with
	"low to high".
 */
void reflex::Buttons::enable()
{
	//select io function for pins
	Port2()->SEL &= ~AllButtons;
	//configure pins as input
	Port2()->DIR &= ~( AllButtons );
	//enable internal pulldowns
	Port2()->OUT &= ~(AllButtons );
	Port2()->REN |= AllButtons;
	//set interrupts on low to high transition
	Port2()->IES &= ~( AllButtons );
	//reset interrupt flags
	Port2()->IFG &= ~(AllButtons);
	//enable the interupts for the connected pins
	Port2()->IE |= AllButtons;
}

/*! disables interrupts for all buttons
*/
void reflex::Buttons::disable()
{
	Port2()->IE &= ~AllButtons;
}






