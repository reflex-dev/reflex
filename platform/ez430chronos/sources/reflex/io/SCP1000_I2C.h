/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(es):	 SCP1000_I2C
 *
 *	Author:	     Stefan Nuernberger
 *
 *	Description: Driver for SCP1000-D11 pressure sensor (MEMS-based)
 *               using the abstract I2C bus interface
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and
 *    Operating Systems. All rights reserved.
 *
 *    Redistribution and use in source and binary forms, with or without
 *    modification, are permitted provided that the following conditions
 *    are met:
 *
 *       1. Redistributions of source code must retain the above copyright
 *          notice, this list of conditions and the following disclaimer.
 *
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the
 *          distribution.
 *
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *    The views and conclusions contained in the software and documentation
 *    are those of the authors and should not be interpreted as representing
 *    official policies, either expressed or implied, of BTU Cottbus,
 *    Department for Distributed Systems and Operating Systems.
 *
 **/
#ifndef REFLEX_SCP1000_I2C_H
#define REFLEX_SCP1000_I2C_H

#include "reflex/io/I2C.h"
#include "reflex/sinks/Sink.h"
#include "reflex/sinks/Event.h"
#include "reflex/scheduling/ActivityFunctor.h"
#include "reflex/data_types/Singleton.h"
#include "reflex/io/Ports.h"
#include "reflex/interrupts/InterruptDispatcher.h"
#include "reflex/powerManagement/PowerManageAble.h"

namespace reflex {

/* SCP1000_I2C
 * I2C slave driver for SCP1000-D11 pressure sensor (I2C version)
 * All SCP1000-D11 are factory programmed with slave address 0x11.
 * We only use the ultra low power periodic measurement with 15
 * bits resolution for pressure values. Resolution for temperature
 * measurement is 14 bits.
 * Measurement is started when the measure Event is notified.
 * The DRDY interrupt signals the availability of data, which
 * is subsequently transferred to the temperature and pressure
 * outputs.
 */
class SCP1000_I2C : public devices::I2C_Device, public PowerManageAble
{
private:
    // outputs for pressure and temperature data
    Sink1<uint32>* pressure;
    Sink1<uint16>* temperature;
    // interrupt dispatcher for pins on Port2 (DRDY)
    // FIXME: this stuff is platform dependent :-(
    data_types::Singleton< mcu::Port2::IVDispatcher > port2IV;

    Event ready; // data available (DRDY interrupt)
public:
    Event measure; // request measurement

    /* constructor */
    SCP1000_I2C();

    /* init
     * connect outputs for pressure and temperature sensor values
     */
    void init(Sink1<uint32> *pressure, Sink1<uint16> *temperature);

    /* i2c_configure
     * configure the SCP sensor
     * called after the master has changed.
     */
    void i2c_configure();

    /* startMeasure
     * start measurement
     */
    void startMeasure();

    /* readResults
     * handle the DRDY interrupt (data available), signal results
     */
    void readResults();

    /* enable
     * configure sensor in correct operation mode, start operation
     */
    void enable();

    /* disable
     * switch to standby
     */
    void disable();

private:
    ActivityFunctor<SCP1000_I2C, &SCP1000_I2C::startMeasure> measureFunc;
    ActivityFunctor<SCP1000_I2C, &SCP1000_I2C::readResults> resultFunc;

    enum Errors {
        SENSOR_NOT_READY = 0xDEAD // custom sensor error code
        ,SENSOR_EEPROM_ERROR = 0xEEEE // eeprom checksum error
    };

    /* constant values for register addresses and commands */
    enum Constants {
        SLAVE_ADDRESS = 0x11 // I2C slave address (factory programmed)
        // register addresses and values
        ,CFGREG = 0x00
        ,DATAWR = 0x01
        ,ADDPTR = 0x02
        ,OPERATION = 0x03
        ,OPSTATUS = 0x04
        ,RSTR = 0x06
        ,STATUS = 0x07
        ,DATARD8 = 0x7f
        ,DATARD16 = 0x80
        ,TEMPOUT = 0x81
        ,DRDY = 0x10
        ,RUNNING = 0x01
        ,STARTUP = 0x01
        ,MODTEST2 = 0x2d
        ,RESET_ASIC = 0x01
        ,REDUCED_NOISE = 0x03
        ,CFG_HIGH_RES = 0x05
        ,CFG_LOW_RES = 0x0d
        ,OP_STAND_BY = 0x00
        ,OP_TRIGGERED = 0x0c
        ,OP_ULTRA_LOW_POWER = 0x0b
        ,OP_HIGH_RES = 0x0a
        ,OP_HIGH_SPEED = 0x09
        ,OP_WRITE_INDIRECT = 0x02
    };

    /** subroutines for certain I2C tasks **/

    /* writeRegister
     * write a register on the device
     * @param reg, address of the register
     * @param val, new register value
     */
    void writeRegister(uint8 reg, uint8 val);

    /* readRegister
     * read a register on the device (16 bit)
     * @param reg, address of the register
     */
    uint16 readRegister(uint8 reg);
};

}

#endif // REFLEX_SCP1000_I2C_H
