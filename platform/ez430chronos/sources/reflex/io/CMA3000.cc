/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 **/

#include "reflex/MachineDefinitions.h"
#include "reflex/io/CMA3000.h"
#include "reflex/io/Ports.h"

using namespace reflex;
using namespace mcu;



CMA3000::CMA3000() :
	PowerManageAble(reflex::PowerManageAble::SECONDARY)
    , port2IV(mcu::interrupts::PORT2)
	, initFunctor(*this)
{
	this->setSleepMode(mcu::LPM4); //set deepest possible sleep mode of driver (in this case the port interrupt)

	PortJ()->DIR |= 0x01; //output
	PortJ()->OUT &= ~0x01; //power off!

	(*port2IV)[Port2::Interrupt_traits::PIN5] = this; //register our notify at pin5 of the port2 interrupt dispatcher

	currentMode = POWERDOWN;

	cfg.clear();
	cfg.setMode(AccelerationSensorConfiguration::MEASUREMENT);
	cfg.setResolution(80);
	cfg.setSamplingRate(400);
	cfg.setTime(0);//not relevant in measurement mode
	cfg.setThreshold(0);//not relevant in measurement mode
	cfg.setInterruptDriven(true);

	sample.init(this); //calls the run method
}

//called by application
void CMA3000::run()
{
	if (dataOut) {
		int16 tmp[3];
		getData(tmp);
		dataOut->assign(tmp[0], tmp[1], tmp[2]);
	}
}


void CMA3000::init(Sink0* interruptOut, Sink3<int16, int16, int16>* dataOut)
{
	this->interruptOut = interruptOut;
	this->dataOut = dataOut;
}


void CMA3000::configure()
{
	if (!this->enabled)
		return;

	confData.get(cfg);

	sRate = cfg.getSamplingRate();
	interruptDriven = 0x1 & ~(cfg.getInterrupt());

	setMode_internal();
}

//needed for protothreads
void CMA3000::setMode_internal()
{
	//prepare sensor for calibration
	PT_BEGIN;
	//wait 10ms for the sensor
	PT_YIELD_TIMER(10, initFunctor);

	//reset sensor
	writeRegister(RSTR, 0x02);
	writeRegister(RSTR, 0x0A);
	writeRegister(RSTR, 0x04);

	//wait 5ms after reset
	PT_YIELD_TIMER(5, initFunctor);

	//configure port interrupt
	Port2()->DIR &= ~0x20;
	Port2()->IES &= ~0x20;
	Port2()->IFG &= ~0x20;


	// only distinguish between  (2g) and (8g)-> resolution higher than 5g is interpreted as 8g
	// lower or equal than 5g is interpreted as 2g
	// getResoultion() returns 1/10g
	res = (cfg.getResolution() > 50) ? _8G : _2G;

	currentMode = POWERDOWN;

	switch(cfg.getMode())
	{
	case AccelerationSensorConfiguration::MEASUREMENT:
		//only accept 400, 100, (40 Hz only @ 8g)
		if(sRate > 250)
			currentMode = MEASURE_400;
		else if(sRate > 70 || res == _2G)
			currentMode = MEASURE_100;
		else
			currentMode = MEASURE_40;

		writeRegister(CTRL, (MDET_EXIT | I2C_DIS | currentMode | res | interruptDriven));
		Port2()->IE |= 0x20;//enable interrupt
		break;

	case AccelerationSensorConfiguration::FREEFALL:
		//only 100 or 400 Hz are possible
		currentMode = sRate > 250 ? FREE_FALL_DETECTION_400 : FREE_FALL_DETECTION_100;
		writeRegister(FFTHR, threshold_To_THR(cfg.getThreshold(), res, currentMode)); //set threshold value
		//no break;

	case AccelerationSensorConfiguration::MOTION:
		//movement detection parameters
		if(currentMode == POWERDOWN)
		{
			currentMode = MOTION_DETECTION; //10 Hz
			writeRegister(MDTHR, threshold_To_THR(cfg.getThreshold(), res, currentMode)); //set threshold value
		}
		// set threshold time for motion- ans free fall detection
		writeRegister(MDFFTMR, thresholdTime_To_MDFFTMR(cfg.getTime(), currentMode));

		writeRegister(CTRL, (MDET_EXIT | I2C_DIS | currentMode | res | interruptDriven));
		Port2()->IE |= 0x20; //enable interrupt
		break;

	case AccelerationSensorConfiguration::DISABLE:
		//not break !!

	default:
		//not supported by this sensor
		writeRegister(CTRL, (MDET_EXIT | I2C_DIS | currentMode | res | INT_DIS));
		disable();
	}
	PT_END;
}

void reflex::CMA3000::enable()
{
	PortJ()->OUT |= 0x01; //power on!
	//setMode(currentMode, res);
	setMode_internal();
}

void reflex::CMA3000::disable()
{
	PortJ()->OUT &= ~0x01; //power off!
	Port2()->IE &= ~0x20; //interrupt off
}

//the ISR of the acceleration sensor
void reflex::CMA3000::notify()
{
	//in case of measurement mode an interrupt is generated once, after the data must polled by application
	//thats why the interrupt is deactivated
	if ((currentMode == MEASURE_40) || (currentMode == MEASURE_100) || (currentMode == MEASURE_400))
	{
		//disable device
		//writeRegister(CTRL, (MDET_EXIT | I2C_DIS | MODE_PD));
		//Port2()->IE &= ~0x20; // deactivate port interrupt
//		if (dataOut) //readout registers and send values to the connected sink
//		{
//			uint16 tmp[3];
//			getData(tmp);
//			dataOut->assign(tmp[0], tmp[1], tmp[2]);
//		}
	} else {//motion detection or free fall detection modes
		//only the interrupt is generated, if the sink wants to have data, then
		//the sink is responsible to invoke this sensor
//		if (dataOut) //readout registers and send values to the connected sink
//		{
//			uint16 tmp[3];
//			getData(tmp);
//			dataOut->assign(tmp[0], tmp[1], tmp[2]);
//		}
		readRegister(INT_STATUS); //reset interrupt status register
		if (interruptOut)
			interruptOut->notify(); //notify connected sink
	}
	Port2()->IFG &= ~0x20; //reset interrupt status
}

//void CMA3000::setMode_internal()
//{
//	if (!this->enabled) {
//		return;
//	}

//	PT_BEGIN;
//	//wait 10ms for the sensor
//	PT_YIELD_TIMER(10, initFunctor);
//
//	//reset sensor
//	writeRegister(RSTR, 0x02);
//	writeRegister(RSTR, 0x0A);
//	writeRegister(RSTR, 0x04);
//
//	//wait 5ms after reset
//	PT_YIELD_TIMER(5, initFunctor);
//
//	//config port interrupt
//	Port2()->DIR &= ~0x20;
//	Port2()->IES &= ~0x20;
//	Port2()->IFG &= ~0x20;

//	if (currentMode == MOTION_DETECTION)
//	{
//		//movement detection parameters
//		writeRegister(MDTHR,0x10); //set threshold
//		writeRegister(MDFFTMR,0x10); //set time to minimum
//		writeRegister(CTRL, (MDET_EXIT | I2C_DIS| currentMode | res));
//		Port2()->IE |= 0x20; //enable interrupt
//	}
//	else if ((currentMode == FREE_FALL_DETECTION_100) || (currentMode == FREE_FALL_DETECTION_400))
//	{
//		//free fall detection parameters
//		writeRegister(FFTHR,0x01); //set threshold
//		writeRegister(MDFFTMR,0x11); //set time to minimum
//		writeRegister(CTRL, (MDET_EXIT | I2C_DIS| currentMode | res));
//		Port2()->IE |= 0x20; //enable interrupt
//	}
	//measurement mode parameters
//	else if ((currentMode == MEASURE_40) || (currentMode == MEASURE_100) || (currentMode == MEASURE_400))
//	{
//		//measurement mode parameters
//		//keep sensor in sleep mode, wake only for measure a value
//		writeRegister(CTRL, (MDET_EXIT | I2C_DIS| MODE_PD | res));
//	}

//	PT_END;
//}





// *************************************************************************************************
// @fn          convert_acceleration_value_to_mgrav
// @brief       Converts measured value to mgrav units
// @param       u8 value	g data from sensor
// @return      u16			Acceleration (mgrav)
// *************************************************************************************************
int16 CMA3000::convert_acceleration_value_to_mgrav(uint8 value, resolution res_val)
{
	int16 result = 0;
	bool pos = true;

	if ((value & 0x80) != 0)
	{
		// Convert 2's complement negative number to positive number
		value = ~value;
		value += 1;
		pos = false;
	}

	for (uint8 i = 0; i < 7; i++)
	{
		if (res_val == _2G)
			result += ( (value & BIT(i)) >> i) * MGRAV_PER_BIT_2G[i];
		else
			result += ( (value & BIT(i)) >> i) * MGRAV_PER_BIT_8G[i];
	}

	return pos ? result : result * (-1);
}


uint8 CMA3000::threshold_To_THR(int16 threshold, resolution res, mode workingMode)
{
	if((workingMode != MOTION_DETECTION &&
		workingMode != FREE_FALL_DETECTION_400 &&
		workingMode != FREE_FALL_DETECTION_100))
		return 0;



	uint8 value = 0;
	const int16* mgField;
	uint8 i;
	int16 tmp;

	//can only handle positive values
	if (threshold < 0)
		threshold *= -1;

	//checking resolution
	if (res == _2G)
	{
		i = sizeof(MGRAV_PER_BIT_2G) / sizeof(uint16) - 1;
		mgField = MGRAV_PER_BIT_2G;

		//pay attention to the valid value for the threshold register
		if(workingMode == MOTION_DETECTION)
			i--; // the highest value is 571
		else //free fall detection
			i = i - 3; // the highest value is 143
	} else {
		i = sizeof(MGRAV_PER_BIT_8G) / sizeof(uint16) - 1;
		mgField = MGRAV_PER_BIT_8G;

		if(workingMode != MOTION_DETECTION)
			i = i - 3; // the highest value is 571
	}

	//convert the threshold
	do
	{
		tmp = threshold - mgField[i];
		if(tmp >= 0)
		{
			value = value | (1 << i);
			threshold = tmp;
		}
	} while (i-- != 0);
	//getApplication().display.getLowerLine() = (uint16)value;
	//the value to process 2g in free fall mode must shifted by 2
	return res == _2G ? value << 2 : value;
}


uint8 CMA3000::thresholdTime_To_MDFFTMR(uint16 time, mode sample)
{
	uint8 i = 0;
	const uint16* timeField;

	//checking sample rate
	switch(sample)
	{
	case MOTION_DETECTION:
		i = sizeof(THRESHOLD_TIME_10HZ) / sizeof(uint16) - 1;
		timeField = THRESHOLD_TIME_10HZ;

		break;

	case FREE_FALL_DETECTION_100:
		i = sizeof(THRESHOLD_TIME_100HZ) / sizeof(uint16) - 1;
		timeField = THRESHOLD_TIME_100HZ;
		break;

	case FREE_FALL_DETECTION_400:
		i = sizeof(THRESHOLD_TIME_400HZ) / sizeof(uint16) - 1;
		timeField = THRESHOLD_TIME_400HZ;
		break;

	default://all other modes do not care on measurement time
		return 0;
	}

	uint8 value = 0;
	uint16 tmp;

	//convert the time
	do
	{
		if(time >= timeField[i])
		{
			tmp = time - timeField[i];
			value = value | (1 << i);
			time = tmp;
		}
	} while (i-- != 0);
	//getApplication().display.getUpperLine() = time - timeField[i];
	//for motion detction only the upper 4 bits are configure the time
	if(sample == MOTION_DETECTION)
		return value << 4;
	else
		return value;
}


void CMA3000::getData(int16* data)
{
	*data++ = convert_acceleration_value_to_mgrav(readRegister(DOUTX), res);
	*data++ = convert_acceleration_value_to_mgrav(readRegister(DOUTY), res);
	*data = convert_acceleration_value_to_mgrav(readRegister(DOUTZ), res);
}


uint8 CMA3000::writeRegister(registers address, uint8 data)
{
	uint8 addr = ((address << 2) | WRITE_FLAG); //shift by 2 and ad RW flag
	spi.enable();
	//TODO: evaluate the send commands
	addr = spi.send(addr);
	addr = spi.send(data);
	spi.disable();

	return addr;
}


uint8 CMA3000::readRegister(registers address)
{
	uint8 addr = address << 2; //shift by 2 and ad RW flag
	spi.enable();
	addr = spi.send(addr);
	addr = spi.send(0); //dummy write
	spi.disable();

	return addr;
}




