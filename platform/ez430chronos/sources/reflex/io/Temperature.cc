/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *

 *
 *    This file is part of REFLEX.
 *
 *    REFLEX is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as
 *    published by the Free Software Foundation, either version 3 of the
 *    License, or (at your option) any later version.
 *
 *    REFLEX is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with REFLEX.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include "reflex/io/Temperature.h"
//#include "conf.h"

#define OFFSET 0 // FIXME: Should be defined via the build system

using namespace reflex;

Temperature::Temperature()
	: adcOut(0)
{
	input.init(this);
}

void Temperature::run()
{
  
  //  ADC::Request request = ADC::Request(ADC::Ref_Vref_AVss,ADC::InCh_Temp,ADC::Ref15,ADC::CH3);
  ADC::Request request = ADC::Request(ADC::Ref_Vref_AVss,ADC::InCh_Temp,ADC::Ref15,ADC::CH3);
  if(adcOut) adcOut->assign(request);
}


//subclass Calculator

Temperature::Calculator::Calculator()
  : SingleValue1<uint16>(this)
	, valueOut(0)
{
  temperature_float = 0;

	for (uint8 i= 0; i<HISTORY_SIZE;i++)
	  {
	    rawHistory[i] = 0; //means 304V
	  }
	writePos = 0;
	raw_mean = 0;

}

void Temperature::Calculator::run()
{
	// Convert ADC value to "x.xx V"
	// Ideally we have A11=0->AVCC=0V ... A11=4095(2^12-1)->AVCC=4V
	// --> (A11/4095)*4V=AVCC --> AVCC=(A11*4)/4095
        uint16 value = 0;
	this->get(value);

	rawHistory[writePos] = value;
	writePos = (writePos + 1) % HISTORY_SIZE;

	

	//make mean of history
	raw_mean = 0;
	for (uint8 i = 0; i<HISTORY_SIZE;i++)
	  {
	    raw_mean = raw_mean + rawHistory[i];
	  }
	raw_mean = raw_mean / HISTORY_SIZE;

	
	//Temperature = ((Temperature - 0.9308971) / 0.00337358 * 10);
	//int IntDegC = (int) Temperature;
	//uint16 Temperature = temp;
	//int Temperature = (voltage-0,894f) / 0,00366f;
	//if(valueOut) valueOut->assign( Temperature);
	temperature_float = (raw_mean*1.45/40.96)/100;
	temperature_float = ((temperature_float-0.680) / 0.00225) + OFFSET;

}

