import qbs 1.0
import ReflexPackage

ReflexPackage {
    name: "ez430chronos"

    files: [
        "sources/reflex/io/*.cc",
        "sources/reflex/power/*.cc",
    ]

    Depends { name: "buffer" }
    Depends { name: "core" }
    Depends { name: "devices" }
    Depends { name: "platform" }
    Depends { name : "protothread" }
    Depends { name: "virtualtimer" }

    cpp.includePaths : "sources/"

    Export {
        Depends { name : "cpp" }
        Depends { name : "platform" }

        cpp.includePaths : "sources/"
    }
}




