#!/usr/bin/perl -w

#
# @author: Andre Sieber <as@informatik.tu-cottbus.de>
#
# This script converts a symbol table from an elf file into a symbol map
# suitable for linker scripts. This map can be used to link a modul's
# object file with a kernel that is aleady on a node
#
# Format of symbol table produced by "objdump -t file.elf" :
#    00004a1c g     F .text	0000001e _ZN6reflex12PowerManagerC1Ev
#    ^^^^^^^^                         ^^^^^^^^^^^^^^^^^^^^^^^^^^^^
#    address                          symbol name
#
# Format in linker script:
#    ZN6reflex12PowerManagerC1Ev = 0x00004a1c
 

use strict;
use warnings;



open( INFILE, "<$ARGV[0]" ) or die $!;    # open for input

my @data = <INFILE>;
close(INFILE);

foreach my $line (@data) {
	if ($line =~ m/^([0-9|a-f]+)\s.*\s(.+)$/g)
	{
	  print $2 . " = 0x" .  $1 . ";\n";
	}
}
