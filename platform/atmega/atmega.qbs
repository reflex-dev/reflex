import qbs 1.0
import ReflexPackage

Project {
    // Platform-specific classes and drivers for the atmega controller family.
    ReflexPackage {
        name: "platform"
        files: [
            "sources/reflex/CoreApplication.cc",
            "sources/reflex/interrupts/interruptFunctions.s",
            "sources/reflex/compiler/gcc_virtual.cc",
            "sources/reflex/io/ADChannel.cc",
            "sources/reflex/io/ADConverter.cc",
            "sources/reflex/io/ExternalInterrupt.cc",
            "sources/reflex/io/PinChangeInterrupt.cc",
            "sources/reflex/memory/Eeprom.cc",
            "sources/reflex/memory/Flash.cc",
            "sources/reflex/powerManagement/power.s",
            "sources/reflex/timer/HardwareTimerMilli.cc",
            "sources/reflex/timer/Watchdog.cc",
        ]

        Depends { name: "core" }
        Depends { name: "platform-essentials" }

        Group {
            files: "sources/reflex/debug/*.h"
            qbs.install: true
            qbs.installDir: "include/reflex/debug"
        }

        Group {
            files: "sources/reflex/interrupts/*.h"
            qbs.install: true
            qbs.installDir: "include/reflex/interrupts"
        }

        Group {
            files: "sources/reflex/io/*.h"
            qbs.install: true
            qbs.installDir: "include/reflex/io"
        }

        Group {
            files: "sources/reflex/memory/*.h"
            qbs.install: true
            qbs.installDir: "include/reflex/memory"
        }

        Group {
            files: "sources/reflex/timer/*.h"
            qbs.install: true
            qbs.installDir: "include/reflex/timer"
        }

        Group {
            fileTagsFilter: "staticlibrary"
            qbs.install: true
            qbs.installDir: "lib"
        }

        Export {
            Depends { name : "core" }
        }
    }

    // Platform dependend headers that are needed by the core in order to compile
    ReflexPackage {
        name: "platform-essentials"
        Depends { name: "cpp" }

        Group {
            files: [
                "sources/reflex/types.h",
                "sources/reflex/CoreApplication.h",
                "sources/reflex/MachineDefinitions.h"
            ]
            qbs.install: true
            qbs.installDir: "include/reflex/"
        }
    }
}


