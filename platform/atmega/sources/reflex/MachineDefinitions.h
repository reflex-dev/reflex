/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2012 BTU Cottbus, Department for Distributed Systems and
 *    Operating Systems. All rights reserved.
 *
 *    Redistribution and use in source and binary forms, with or without
 *    modification, are permitted provided that the following conditions
 *    are met:
 *
 *       1. Redistributions of source code must retain the above copyright
 *          notice, this list of conditions and the following disclaimer.
 *
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the
 *          distribution.
 *
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED
 *    SYSTEMS AND OPERATION SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND
 *    OPERATION SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *    The views and conclusions contained in the software and documentation
 *    are those of the authors and should not be interpreted as representing
 *    official policies, either expressed or implied, of BTU Cottbus,
 *    Department for Distributed Systems and Operating Systems.
 *
 * */
#ifndef MachineDefinitions_H_
#define MachineDefinitions_H_

#include "reflex/types.h"

/**
 \defgroup atmega
 \brief Drivers and components for the ATMEGA controller family.

 The ATMEGA controller family includes many different members which only
 differ in memory amount and available peripherie. Atmel tries to make things
 easy and therefore all peripheral units in all controllers have very similar
 features. ::Timer0 for example is always an 8Bit timer while ::Timer1 has
 a width of 16Bit. ::Timer2, if present, can always be driven externally as
 a real time clock and so on...

 While most newer controllers share a very similar register file layout,
 some of the older ones still differ. These controllers like Atmega32 are
 currently not supported. REFLEX should work on all family members of the
 3rd controller generation which offer a similar register layout. The minimum
 requirements are 8KiB ROM and 1KiB RAM, but below 16KiB/2KiB only mickey-mouse
 applications are possible.

 Functional platform tests cover the ::Atmegs328 (ARDUINO nano) and Atmega2560.

 */

/**
 \brief The event flow system and drivers for the ATMEGA controller family.
 */
namespace reflex
{

/**
 \brief Drivers and definitions for the Atmel ATMEGA controller family.

 */
namespace atmega
{

/**
 \ingroup atmega
 \brief USART interface enumerator for the ATMEGA controller family

 \note Not all controllers offer all interfaces.
 \see Usart
 */
enum UsartNr
{
	Usart0 = 0, Usart1 = 1, Usart2 = 2, Usart3 = 3
};

/*!
 \ingroup atmega
 \brief ATMEGA hardware timer enumerator.

 ::Timer3, ::Timer4, ::Timer5 are not available on all controllers.

 \see CompareMatch, Overflow, Pwm, Timer
 */
enum TimerNr
{
	Timer0 = 0, //!< 8 Bit counter.
	Timer1, //!< 16 Bit counter.
	Timer2, //!< 8 Bit counter with support for an external crystal and asynchronous operation.
	Timer3, //!< 16 Bit counter.
	Timer4,//!< 16 Bit counter.
	Timer5 //!< 16 Bit counter.
};

/*!
 \ingroup atmega
 \brief Specifies an output compare match unit of a hardware timer on ATMEGA.

 ::ChannelB and ::ChannelC are not available on all timers/controllers.

 \see CompareMatch, Pwm
 */
enum TimerChannel
{
	ChannelA = 0, ChannelB, ChannelC
};

//! ATMEGA platform power modes. See the datasheet for details.
enum PowerModes : uint8
{
	ACTIVE = 0,
	Idle,
	AdcNoiseReduction,
	PowerDown,
	PowerSave,
	StandBy,
	ExtendedStandBy,
	DeepestSleepMode = ExtendedStandBy,
	NrOfPowerModes = 7
};

/*!
 \brief Implemented and (partially) tested ATMEGA family members

 ::CurrentMcu contains the McuType for which reflex is compiled. Compilation
 may fail, if a controller is chosen that is not implemented or tested, yet.

 */
enum McuType
{
	Atmega32,
	Atmega164,
	Atmega168,
	Atmega169,
	Atmega324,
	Atmega328,
	Atmega640,
	Atmega644,
	Atmega1280,
	Atmega1284,
	Atmega2560,
#ifdef __AVR_ATmega32__
	CurrentMcu = Atmega32
#elif defined __AVR_ATmega164__ || __AVR_ATmega164P__
	CurrentMcu = Atmega164
#elif defined __AVR_ATmega168__ || __AVR_ATmega168P__
	CurrentMcu = Atmega168
#elif defined __AVR_ATmega169__
	CurrentMcu = Atmega169
#elif defined __AVR_ATmega324__ || __AVR_ATmega324P__
	CurrentMcu = Atmega324
#elif defined __AVR_ATmega328__ || __AVR_ATmega328P__
	CurrentMcu = Atmega328
#elif defined __AVR_ATmega640__ || __AVR_ATmega640P__
	CurrentMcu = Atmega640
#elif defined __AVR_ATmega644__ || __AVR_ATmega644P__
	CurrentMcu = Atmega644
#elif defined __AVR_ATmega1284__ || __AVR_ATmega1284P__
	CurrentMcu = Atmega1284
#elif defined __AVR_ATmega2560__
	CurrentMcu = Atmega2560
#else
	CurrentMcu = Error_MCU_not_implemented_nor_tested
#endif
};

template<McuType mcuType> struct McuCore;

}

}

/*
 Every controller is a partial specialization of the class "Controller".
 A common implementation is provided by MachineDefinitions_default.h which
 is suitable for most newer family members. For those controllers, the
 partial specialization may derive from Controller<DefaultAVR> and only
 define the interrupt vectors.
 Exotic or older atmegas may define their own register file and enums.
 */
#include "reflex/MachineDefinitions_atmega32.h"
#include "reflex/MachineDefinitions_atmega169.h"
#include "reflex/MachineDefinitions_atmega640_1280_2560.h"
#include "reflex/MachineDefinitions_atmega164_324_644_1284.h"
#include "reflex/MachineDefinitions_atmega328.h"

namespace reflex
{
namespace atmega
{
/*!
 Use reflex:atmega::Core to access registers and definitions of a controller.
 */
typedef McuCore<CurrentMcu> Core;

/*!
 \cond
 Workaround for calculations. TODO: Use a template type for that and a macro
 like QT does with Q_DECLARE_FLAGS for example which has methods for calculation.
 */
typedef int8 InterruptVector;

/*!
 Workaround for InterruptGuardian. The atmega declares all interrupt vectors inside
 a specialized McuImplementation class.
 */
namespace interrupts
{
enum
{
	INVALID_INTERRUPT = Core::INVALID_INTERRUPT, MAX_HANDLERS = Core::MAX_HANDLERS
};
}
//! \endcond
}

/**
 \namespace reflex::mcu
 \brief Namespace alias for reflex::atmega
 */
namespace mcu = atmega;

}

#endif
