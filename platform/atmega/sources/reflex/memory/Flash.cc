#include "reflex/memory/Flash.h"
#include "reflex/MachineDefinitions.h"

using namespace reflex;
using namespace mcu;

/*!
 \brief Copies data from a \a source address in flash memory into \a dest
 address in RAM and returns the size of the copied string.
 */
void Flash::memcopy(void* dest, const void* source, size_t length)
{
	size_t pos = 0;

	while (pos < length)
	{
		static_cast<uint8*> (dest)[pos] = Flash::read(static_cast<const uint8*> (source) + pos);
		pos++;
	}
}

/*!
 \brief Copies a plain C-string from a \a source address in flash memory into \a dest
 address in RAM and returns the size of the copied string.

 The returned size does not contain the terminating null character.

 */
uint8 Flash::strcopy(char* dest, const char* source)
{
	for (size_t pos = 0; true; pos++)
	{
		char c = Flash::read(source + pos);
		dest[pos] = c;
		if (c == '\0')
		{
			return pos;
		}
	}
}

