#ifndef INTERRUPTVECTOR_ATMEGA128_H_
#define INTERRUPTVECTOR_ATMEGA128_H_
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2011 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATION SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATION SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */

//! \cond
// TODO: Copy this content to MachineDefinitions_atmega128
namespace reflex
{
namespace mcu
{
namespace interrupts
{
enum InterruptVector {
	INVALID_INTERRUPT 		= -1,
	INTVEC_INT0				= 0,
	INTVEC_INT1				= 1,
	INTVEC_INT2				= 2,
	INTVEC_INT3				= 3,
	INTVEC_INT4				= 4,
	INTVEC_INT5				= 5,
	INTVEC_INT6				= 6,
	INTVEC_INT7				= 7,
	INTVEC_TIMER2COMP		= 8,
	INTVEC_TIMER2OVF		= 9,
	INTVEC_TIMER1CAPT		= 10,
	INTVEC_TIMER1COMPA		= 11,
	INTVEC_TIMER1COMPB		= 12,
	INTVEC_TIMER1OVF		= 13,
	INTVEC_TIMER0COMP		= 14,
	INTVEC_TIMER0OVF		= 15,
	INTVEC_SPI_STC			= 16,
	INTVEC_USART0_RX		= 17,
	INTVEC_USART0_UDRE		= 18,
	INTVEC_USART0_TX		= 19,
	INTVEC_ADC				= 20,
	INTVEC_EE_READY			= 21,
	INTVEC_ANALOG_COMP		= 22,
	INTVEC_TIMER1_COMPC		= 23,
	INTVEC_TIMER3CAPT		= 24,
	INTVEC_TIMER3COMPA		= 25,
	INTVEC_TIMER3COMPB		= 26,
	INTVEC_TIMER3COMPC		= 27,
	INTVEC_TIMER3OVF		= 28,
	INTVEC_USART1_RX		= 29,
	INTVEC_USART1_UDRE		= 30,
	INTVEC_USART1_TX		= 31,
	INTVEC_TWI				= 32,
	INTVEC_SPM_READY		= 33,
	VECTOR_BASE 			= 0x0,
	MAX_HANDLERS = 34
};
}
}
}

//! \endcond

#endif /* INTERRUPTVECTOR_ATMEGA128_H_ */
