;/*
; * REFLEX - Real-time Event FLow EXecutive
; *
; * A lightweight operating system for deeply embedded systems.
; *
; *
; * Class(ses):
;
; * Author:     Karsten Walther, Sören Höckner
; *
; * Description:    interrupt vector wrapper and utility functions
; */
.global _interruptsEnable
.global _interruptsDisable
.global _reset

.section .text

_interruptsEnable:
    ser r24
    brie _intEnable1
    com r24
_intEnable1:
    sei
    ret

_interruptsDisable:
    ser r24
    brie _intDisable1
    com r24
_intDisable1:
    cli
    ret

# Pseudo software reset. This should be replaced by a real hardware reset
# which can only be achieved using the watchdog timer.
_reset:
    cli; disable interrupts
    jmp __init

;let the interrupt handler reside within the first 128k of the text segment
.section .lowtext

;macro to generate interrupt handle routines
.macro vector p
.global __vector_\p
__vector_\p:
    push r24
    ldi r24, (\p-1) ; decrement the vectornumber to match the arrayindex in the Guardian
    rjmp _wrapper_body
.endm


; going to generate the __vector_X symbols starting with 1 cause the first symbol has to be __vector_1
.irp i 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56
vector \i
.endr

# Prologue and epilogue for interrupt handlers.
# Treatment of register files according to the gcc ABI:
# http://gcc.gnu.org/wiki/avr-gcc#Register_Layout
_wrapper_body:
    push r0
    push r1
                    ; r2-r17 are call-saved
    push r18
    push r19
    push r20
    push r21
    push r22
    push r23
                    ; r24 contains the handler index
    push r25
    push r26
    push r27
                    ; r28-r29 are call-saved
    push r30
    push r31

    in r26, 0x3f    ;save the status register SREG
    push r26

    clr r1          ; Since a multiplication could have been interrupted, r1 has to be zeroed manually.

    call handle

    pop r26         ; restore SREG
    out 0x3f, r26

    pop r31
    pop r30

    pop r27
    pop r26
    pop r25
    pop r23
    pop r22
    pop r21
    pop r20
    pop r19
    pop r18

    pop r1
    pop r0
    pop r24        ; r24 has been overwritten in vector macro
    reti
