#ifndef MACHINEDEFINITIONS_ATMEGA2560_H_
#define MACHINEDEFINITIONS_ATMEGA2560_H_
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2012 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATION SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATION SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */
#include "reflex/MachineDefinitions_default.h"

namespace reflex
{
namespace atmega
{

template<> struct McuCore<Atmega640> : public internal::DefaultController
{

	/*!
	 \brief Interrupt vector definitions for the ::Atmega640.
	 */
	enum InterruptVector
	{
		INVALID_INTERRUPT = -1,//!< INVALID_INTERRUPT
		INTVEC_INT0 = 0,       //!< INTVEC_INT0
		INTVEC_INT1,           //!< INTVEC_INT1
		INTVEC_INT2,           //!< INTVEC_INT2
		INTVEC_INT3,           //!< INTVEC_INT3
		INTVEC_INT4,           //!< INTVEC_INT4
		INTVEC_INT5,           //!< INTVEC_INT5
		INTVEC_INT6,           //!< INTVEC_INT6
		INTVEC_INT7,           //!< INTVEC_INT7
		INTVEC_PCINT0,         //!< INTVEC_PCINT0
		INTVEC_PCINT1,         //!< INTVEC_PCINT1
		INTVEC_PCINT2,         //!< INTVEC_PCINT2
		INTVEC_WDT,            //!< INTVEC_WDT
		INTVEC_TIMER2COMPA,    //!< INTVEC_TIMER2COMPA
		INTVEC_TIMER2COMPB,    //!< INTVEC_TIMER2COMPB
		INTVEC_TIMER2OVF,      //!< INTVEC_TIMER2OVF
		INTVEC_TIMER1CAPT,     //!< INTVEC_TIMER1CAPT
		INTVEC_TIMER1COMPA,    //!< INTVEC_TIMER1COMPA
		INTVEC_TIMER1COMPB,    //!< INTVEC_TIMER1COMPB
		INTVEC_TIMER1COMPC,    //!< INTVEC_TIMER1COMPC
		INTVEC_TIMER1OVF,      //!< INTVEC_TIMER1OVF
		INTVEC_TIMER0COMPA,    //!< INTVEC_TIMER0COMPA
		INTVEC_TIMER0COMPB,    //!< INTVEC_TIMER0COMPB
		INTVEC_TIMER0OVF,      //!< INTVEC_TIMER0OVF
		INTVEC_SPI_STC,        //!< INTVEC_SPI_STC
		INTVEC_USART0_RX,      //!< INTVEC_USART0_RX
		INTVEC_USART0_UDRE,    //!< INTVEC_USART0_UDRE
		INTVEC_USART0_TX,      //!< INTVEC_USART0_TX
		INTVEC_ANALOG_COMP,    //!< INTVEC_ANALOG_COMP
		INTVEC_ADC,            //!< INTVEC_ADC
		INTVEC_EE_READY,       //!< INTVEC_EE_READY
		INTVEC_TIMER3CAPT,     //!< INTVEC_TIMER3CAPT
		INTVEC_TIMER3COMPA,    //!< INTVEC_TIMER3COMPA
		INTVEC_TIMER3COMPB,    //!< INTVEC_TIMER3COMPB
		INTVEC_TIMER3COMPC,    //!< INTVEC_TIMER3COMPC
		INTVEC_TIMER3OVF,      //!< INTVEC_TIMER3OVF
		INTVEC_USART1_RX,      //!< INTVEC_USART1_RX
		INTVEC_USART1_UDRE,    //!< INTVEC_USART1_UDRE
		INTVEC_USART1_TX,      //!< INTVEC_USART1_TX
		INTVEC_TWI,            //!< INTVEC_TWI
		INTVEC_SPM_READY,      //!< INTVEC_SPM_READY
		INTVEC_TIMER4CAPT,     //!< INTVEC_TIMER4CAPT
		INTVEC_TIMER4COMPA,    //!< INTVEC_TIMER4COMPA
		INTVEC_TIMER4COMPB,    //!< INTVEC_TIMER4COMPB
		INTVEC_TIMER4COMPC,    //!< INTVEC_TIMER4COMPC
		INTVEC_TIMER4OVF,      //!< INTVEC_TIMER4OVF
		INTVEC_TIMER5CAPT,     //!< INTVEC_TIMER5CAPT
		INTVEC_TIMER5COMPA,    //!< INTVEC_TIMER5COMPA
		INTVEC_TIMER5COMPB,    //!< INTVEC_TIMER5COMPB
		INTVEC_TIMER5COMPC,    //!< INTVEC_TIMER5COMPC
		INTVEC_TIMER5OVF,      //!< INTVEC_TIMER5OVF
		INTVEC_USART2_RX,      //!< INTVEC_USART2_RX
		INTVEC_USART2_UDRE,    //!< INTVEC_USART2_UDRE
		INTVEC_USART2_TX,      //!< INTVEC_USART2_TX
		INTVEC_USART3_RX,      //!< INTVEC_USART3_RX
		INTVEC_USART3_UDRE,    //!< INTVEC_USART3_UDRE
		INTVEC_USART3_TX,      //!< INTVEC_USART3_TX
		MAX_HANDLERS           //!< MAX_HANDLERS
	};

	/*!
	 \brief Addional port pin definitions for special purpose pins.
	 */
	enum PortPins
	{
		SS = 1 << 0, SCK = 1 << 1, MOSI = 1 << 2, MISO = 1 << 3
	};

	template<TimerNr nr> struct TimerRegisters: public internal::TimerRegisters<Atmega640, nr,
			(nr == Timer0) ? INTVEC_TIMER0OVF :
			(nr == Timer1) ? INTVEC_TIMER1OVF :
			(nr == Timer2) ? INTVEC_TIMER2OVF :
			(nr == Timer3) ? INTVEC_TIMER3OVF :
			(nr	== Timer4) ? INTVEC_TIMER4OVF :
			(nr == Timer5) ? INTVEC_TIMER5OVF :
			INVALID_INTERRUPT,
			(nr == Timer0) ? INTVEC_TIMER0COMPA :
			(nr	== Timer1) ? INTVEC_TIMER1COMPA :
			(nr == Timer2) ? INTVEC_TIMER2COMPA :
			(nr	== Timer3) ? INTVEC_TIMER3COMPA :
			(nr == Timer4) ? INTVEC_TIMER4COMPA :
			(nr	== Timer5) ? INTVEC_TIMER5COMPA :
			INVALID_INTERRUPT,
			(nr == Timer0) ? INTVEC_TIMER0COMPB :
			(nr == Timer1) ? INTVEC_TIMER1COMPB :
			(nr == Timer2) ? INTVEC_TIMER2COMPB :
			(nr == Timer3) ? INTVEC_TIMER3COMPB :
			(nr	== Timer4) ? INTVEC_TIMER4COMPB :
			(nr == Timer5) ? INTVEC_TIMER5COMPB	:
			INVALID_INTERRUPT,
			(nr == Timer0) ? INVALID_INTERRUPT	:
			(nr == Timer1) ? INTVEC_TIMER1COMPC :
			(nr == Timer2) ? INVALID_INTERRUPT	:
			(nr == Timer3) ? INTVEC_TIMER3COMPC	:
			(nr == Timer4) ? INTVEC_TIMER4COMPC	:
			(nr == Timer5) ? INTVEC_TIMER5COMPC :
			INVALID_INTERRUPT,
			(nr == Timer1) ? INTVEC_TIMER1CAPT :
			(nr == Timer3) ? INTVEC_TIMER3CAPT :
			(nr == Timer4) ? INTVEC_TIMER4CAPT :
			(nr == Timer5) ? INTVEC_TIMER5CAPT :
			INVALID_INTERRUPT>
	{

	};

	/*!
	 \brief Maps interrupt vectors to the common USART register abstraction.
	 */
	template<UsartNr nr> struct UsartRegisters : public internal::UsartRegisters<Atmega640, nr,
		(nr == Usart0) ? INTVEC_USART0_RX :
		(nr == Usart1) ? INTVEC_USART1_RX :
		(nr == Usart2) ? INTVEC_USART2_RX :
		(nr == Usart3) ? INTVEC_USART3_RX :
		INVALID_INTERRUPT,
		(nr == Usart0) ? INTVEC_USART0_UDRE :
		(nr == Usart1) ? INTVEC_USART1_UDRE :
		(nr == Usart2) ? INTVEC_USART2_UDRE :
		(nr == Usart3) ? INTVEC_USART3_UDRE :
		INVALID_INTERRUPT,
		(nr == Usart0) ? INTVEC_USART0_TX :
		(nr == Usart1) ? INTVEC_USART1_TX :
		(nr == Usart2) ? INTVEC_USART2_TX :
		(nr == Usart3) ? INTVEC_USART3_TX :
		INVALID_INTERRUPT>
	{

	};
};

template<> struct McuCore<Atmega1280> : public McuCore<Atmega640>
{

};


template<> struct McuCore<Atmega2560> : public McuCore<Atmega640>
{

};

}

}

#endif /* MACHINEDEFINITIONS_ATMEGA2560_H_ */
