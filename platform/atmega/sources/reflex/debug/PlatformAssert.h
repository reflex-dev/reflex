/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *    This file is part of REFLEX.
 *
 *    REFLEX is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as 
 *    published by the Free Software Foundation, either version 3 of the 
 *    License, or (at your option) any later version.
 *
 *    REFLEX is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with REFLEX.  If not, see <http://www.gnu.org/licenses/>.
 * */
#ifndef PlatformAssert_h
#define PlatformAssert_h

//#include "reflex/memory/Flash.h"

extern "C" bool _interruptsDisable();
extern "C" void _powerdown();

/*
 * An assert message shall look like "filename:line".
 * For that purpose exist the preprocessor directives __FILE__ and __LINE__.
 * But unfortunately __LINE__ does not convert to string easily.
 * So some preprocessor magic is needed.
 *
 * Fount at http://www.decompile.com/cpp/faq/file_and_line_error_string.htm
 */
#define STRINGIFY(x) #x
#define TOSTRING(x) STRINGIFY(x)
#define POSITION __FILE__ ":" TOSTRING(__LINE__)

void printError(const char* string, const void* addr);

inline void assert()
{
	_interruptsDisable();
	while (true)
		; // everything else does not stop the AVR
	//_powerdown();
}

#define ASSERT(x) Assert(x)

/*
#define Assert(expr)	\
		(expr) ? (void) (0) : printError(FLASH_PTR(POSITION), this);
*/

#define Assert(expr)	\
		(expr) ? (void) (0) : assert();

#endif

