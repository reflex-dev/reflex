;/*
; * REFLEX - Real-time Event FLow EXecutive
; *
; * A lightweight operating system for deeply embedded systems.
; *
; *
; * Class(es):
; *
; * Author:     Sören Höckner
; *
; * Description:    assembler power modes stuff
; */
.global _idle
.global _adc_noise
.global _powerdown
.global _powersave
.global _standby
.global _extstandby

.section .text

; helper function
_getSMCR: ; wrapper loads SMCR and clears SM0..SM2
    in r16, 0x33; load SMCR
    andi r16,  (0x07 << 1) | (1 << 0) ; clear all bits, but SM0..SM2
    ret; return to power mode select (idle, adc...,powerdown, powersave)

_gosleep: ;assumes a valid SMCR with a valid power mode is hold in r16
    ori r16, 1 << 0 ;set SE bit
    out 0x33, r16; store SMCR value
    sei ;start enabling interrupts
    sleep; enter sleepmode and enable interrupts
    ;return from sleepmode.
    andi r16, ~(1 << 0);clear SE bit
    ;sei; enable interrupts
    pop r16 ;restore r16
    ret ;return to schedule
    nop;

;**** POWER MODES ****
_idle:
    push r16; save r16 must be done here
    call _getSMCR;
    ;ori r16,0x0<<2  ;set SM2..SM0
    rjmp _gosleep; go sleep and return to schedule
    nop;

_adc_noise:
    push r16; save r16 must be done here
    call _getSMCR;
    ori r16, 0x01 << 1  ;set SM2..SM0
    rjmp _gosleep; go sleep and return to schedule
    nop;

_powerdown:
    push r16; save r16 must be done here
    call _getSMCR;
    ori r16, 0x02 << 1  ;set SM2..SM0 Power down (all off, but TWI and Wachtdog)
    rjmp _gosleep; go sleep and return to schedule
    nop;

_powersave:
    push r16; save r16 must be done here
    call _getSMCR;
    ori r16, 0x03 << 1 ;set SM2..SM0
    rjmp _gosleep; go sleep and return to schedule
    nop;

_standby:
    push r16; save r16 must be done here
    call _getSMCR;
    ori r16, 0x06 << 1  ;set SM2..SM0
    rjmp _gosleep; go sleep and return to schedule
    nop;

_extstandby:
    push r16; save r16 must be done here
    call _getSMCR;
    ori r16, 0x07 << 1 ;set SM2..SM0
    rjmp _gosleep; go sleep and return to schedule
    nop;
