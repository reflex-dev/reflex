#ifndef MACHINEDEFINITIONS_ATMEGA32_H_
#define MACHINEDEFINITIONS_ATMEGA32_H_
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2012 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATION SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATION SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */

#include "MachineDefinitions_default.h"

//namespace reflex
//{
//namespace atmega
//{
//
//template<> struct McuImplementation<Atmega32> : public internal::DefaultController
//{
//	enum InterruptVector
//	{
//		INVALID_INTERRUPT = -1,
//		INTVEC_INT0 = 0,
//		INTVEC_INT1,
//		INTVEC_INT2,
//		INTVEC_TIMER2COMP,
//		INTVEC_TIMER2OVF,
//		INTVEC_TIMER1CAPT,
//		INTVEC_TIMER1COMPA,
//		INTVEC_TIMER1COMPB,
//		INTVEC_TIMER1OVF,
//		INTVEC_TIMER0COMP,
//		INTVEC_TIMER0OVF,
//		INTVEC_SPI_STC,
//		INTVEC_USART0_RX,
//		INTVEC_USART0_UDRE,
//		INTVEC_USART0_TX,
//		INTVEC_ADC,
//		INTVEC_EE_READY,
//		INTVEC_ANALOG_COMP,
//		INTVEC_TWI,
//		INTVEC_SPM_READY,
//		MAX_HANDLERS
//	};
//
//	enum RegisterBits
//	{
//		// Watchdog
//		PORF = 0x01,
//		EXTRF = 0x02,
//		BORF = 0x04,
//		WDRF = 0x08,
//		WDP0 = 0x01,
//		WDP1 = 0x02,
//		WDP2 = 0x04,
//		WDE = 0x08,
//		WDCE = 0x10,
//		WDP3 = 0x20,
//		WDIE = 0x40,
//		WDIF = 0x80,
//		//ADCSRA
//		ADPS0 = 0x01,
//		ADPS1 = 0x02,
//		ADPS2 = 0x04,
//		ADIE = 0x08,
//		ADIF = 0x10,
//		ADFR = 0x20,
//		ADSC = 0x40,
//		ADEN = 0x80,
//		//ADCSRB
//		ADTS0 = 0x01,
//		ADTS1 = 0x02,
//		ADTS2 = 0x04,
//		MUX5 = 0x08,
//		ACME = 0x40,
//		//ADMUX
//		MUX0 = 0x01,
//		MUX1 = 0x02,
//		MUX2 = 0x04,
//		MUX3 = 0x08,
//		MUX4 = 0x10,
//		ADLAR = 0x20,
//		REFS0 = 0x40,
//		REFS1 = 0x80,
//		//ACSR
//		ACIS0 = 0x01,
//		ACIS1 = 0x02,
//		ACIC = 0x04,
//		ACIE = 0x08,
//		ACI = 0x10,
//		ACO = 0x20,
//		ACBG = 0x40,
//		ACD = 0x80,
//		//TCCR0A TCCR0B
//		FOC0A = 0x80,
//		FOC0B = 0x80,
//		WGM00 = 0x40,
//		COM0A1 = 0x20,
//		COM0A0 = 0x10,
//		COM0B1 = 0x20,
//		COM0B0 = 0x10,
//		WGM01 = 0x08,
//		CS02 = 0x04,
//		CS01 = 0x02,
//		CS00 = 0x01,
//		//TIFR0
//		OCF0B = 0x02,
//		OCF0A = 0x02,
//		TOV0 = 0x01,
//		//TIMSK0
//		OCIE0B = 0x02,
//		OCIE0A = 0x02,
//		TOIE0 = 0x01,
//		//TCCRnB
//		ICNCn = 0x80,
//		ICESn = 0x40,
//		//
//		WGMn3 = 0x10,
//		WGMn2 = 0x08,
//		CSn2 = 0x04,
//		CSn1 = 0x02,
//		CSn0 = 0x01,
//		//TCCRnA
//		COMnA1 = 0x80,
//		COMnA0 = 0x40,
//		COMnB1 = 0x20,
//		COMnB0 = 0x10,
//		COMnC1 = 0x08,
//		COMnC0 = 0x04,
//		WGMn1 = 0x02,
//		WGMn0 = 0x01,
//		// TIMSKn
//		ICIEn = 0x20,
//		OCIEnC = 0x08,
//		OCIEnB = 0x04,
//		OCIEnA = 0x02,
//		TOIEn = 0x01,
//		//GTCCR
//		TSM = 0x80,
//		PSRASY = 0x02,
//		PSRSYNC = 0x01,
//		//UCSRA
//		MPCM = 0x01,
//		U2X = 0x02,
//		UPE = 0x04,
//		DOR = 0x08,
//		FE = 0x10,
//		UDRE = 0x20,
//		TXC = 0x40,
//		RXC = 0x80,
//		//UCSRB
//		TXB8 = 0x01,
//		RXB8 = 0x02,
//		UCSZ2 = 0x04,
//		TXEN = 0x08,
//		RXEN = 0x10,
//		UDRIE = 0x20,
//		TXCIE = 0x40,
//		RXCIE = 0x80,
//		//UCSRC
//		UCPOL = 0x01,
//		UCSZ0 = 0x02,
//		UCSZ1 = 0x04,
//		USBS = 0x08,
//		UPM0 = 0x10,
//		UPM1 = 0x20,
//		UMSEL0 = 0x40,
//		UMSEL1 = 0x80,
//		/* EECR */
//		EEPM1 = 0x20,
//		EEPM0 = 0x10,
//		EERIE = 0x08,
//		EEMPE = 0x04,
//		EEPE = 0x02,
//		EERE = 0x01,
//		//SPCR control register
//		SPIE = 0x80, ///< SPI Interrupt enable
//		SPE = 0x40, ///< SPI enable
//		DORD = 0x20, ///< DataOrder LSB(1)/ MSB(0) written first
//		MSTR = 0x10, ///< Master (1)/Slave(0) Mode
//		CPOL = 0x08, ///< Clock Polarity
//		CPHA = 0x04, ///< Clock Phase
//		SPR1 = 0x02, ///< Clock Rate Select1
//		SPR0 = 0x01, ///< Clock Rate Select0
//		//SPSR status register
//		SPIF = 0x80, ///< SPI Interrupt Flag
//		WCOL = 0x40, ///< Write collision
//		SPI2x = 0x01
//	///< double speed flag
//	};
//
//	/**
//	 * The register file.
//	 */
//	struct Registers
//	{
//		uint8 reserved0[0x20 - 0x00]; // 0x00..0x19
//		volatile uint8 TWBR; // 0x20
//		volatile uint8 TWSR; // 0x21
//		volatile uint8 TWAR; // 0x22
//		volatile uint8 TWDR; // 0x23
//		volatile uint8 ADCL; // 0x24
//		volatile uint8 ADCH; // 0x25
//		volatile uint8 ADCSRA; // 0x26
//		volatile uint8 ADMUX; // 0x27
//		volatile uint8 ACSR; // 0x28
//		uint8 reserved1[0x2D - 0x29]; // 0x29..0x2C
//		volatile uint8 SPCR; // 0x2D
//		volatile uint8 SPSR; // 0x2E
//		volatile uint8 SPDR; // 0x2F
//		uint8 reserved2[0x3C - 0x30]; // 0x30..3B
//		volatile uint8 EECR; // 0x3C
//		volatile uint8 EEDR; // 0x3D
//		volatile uint8 EEARL; // 0x3E
//		volatile uint8 EEARH; // 0x3F
//		uint8 reserved3[1]; // 0x40..0x40
//		volatile uint8 WDTCR; // 0x41
//		volatile uint8 ASSR; // 0x42
//		volatile uint8 OCR2; // 0x43
//		volatile uint8 TCNT2; // 0x44
//		volatile uint8 TCCR2; // 0x45
//		uint8 reserved4[0x50 - 0x46]; // 0x46..0x4F
//		volatile uint8 SFIOR; // 0x50
//		union
//		{
//			volatile uint8 OCDR; // 0x51
//			volatile uint8 OSCCAL; // 0x51
//		};
//		volatile uint8 TCCR0; // 0x52
//		volatile uint8 TCNT0; // 0x53
//		volatile uint8 MCUCSR; // 0x54
//		volatile uint8 MCUCR; // 0x55
//		volatile uint8 TWCR; // 0x56
//		volatile uint8 SPMCR; // 0x57
//		volatile uint8 TIFR; // 0x58
//		volatile uint8 TIMSK; // 0x59
//		volatile uint8 GIFR; // 0x5A
//		volatile uint8 GICR; // 0x5B
//		volatile uint8 OCR0; // 0x5C
//		volatile uint8 SPL; // 0x5D
//		volatile uint8 SPH; // 0x5E
//		volatile uint8 SREG; // 0x5F
//		Registers* operator->()
//		{
//			return reinterpret_cast<Registers*> (0);
//		}
//	};
//
//	/**
//	 * Some functional components of the ATMega are duplicates.
//	 * They are normally not accessed through the main Registers file.
//	 */
//	template<uint16 base>
//	struct PortRegisters
//	{
//		volatile uint8 PIN; //Pin state
//		volatile uint8 DDR; //data direction
//		volatile uint8 PORT; //data register
//
//		PortRegisters* operator->()
//		{
//			return reinterpret_cast<PortRegisters*> (base);
//		}
//	};
//
//	typedef PortRegisters<0x39> PortA;
//	typedef PortRegisters<0x36> PortB;
//	typedef PortRegisters<0x33> PortC;
//	typedef PortRegisters<0x30> PortD;
//
//	/**
//	 * Serial registers.
//	 * Each serial port follows the same register layout, so that all
//	 * serial interfaces can be addressed in one struct.
//	 */
//	template<uint8 nr>
//	struct UsartRegisters
//	{
//		volatile uint8 UBRRL;
//		volatile uint8 UCSRB;
//		volatile uint8 UCSRA;
//		volatile uint8 UDR;
//		uint8 dummy[0x40 - 0x2D];
//		union
//		{
//			volatile uint8 UBRRH;
//			volatile uint8 UCSRC;
//		};
//
//		UsartRegisters* operator->()
//		{
//			switch (nr)
//			{
//			case 0:
//				return reinterpret_cast<UsartRegisters*> (0x29);
//				break;
//			default:
//				STATIC_ASSERT(nr < 4, only_4_UARTS_available_on_atmega2560)
//				;
//				break;
//			}
//		}
//	};
//
//	/**
//	 * Timer registers are scattered around in the register file, so that
//	 * different structs are necessary.
//	 */
//	template<uint8 timerNr>
//	class TimerRegisters;
//
//	template<>
//	class TimerRegisters<0>
//	{
//	public:
//		static const mcu::InterruptVector OVF = mcu::interrupts::INTVEC_TIMER0OVF;
//		static const mcu::InterruptVector COMPA = mcu::interrupts::INTVEC_TIMER0COMP;
//		static const mcu::InterruptVector COMPB = mcu::interrupts::INTVEC_TIMER0COMP;
//
//		volatile uint8 TCNT0;
//		union
//		{
//			volatile uint8 TCCR0A;
//			volatile uint8 TCCR0B;
//		};
//		uint8 dummy0[0x58 - 0x54];
//		volatile uint8 TIFR0;
//		volatile uint8 TIMSK0;
//		uint8 dummy1[0x5C - 0x5A];
//		union
//		{
//			volatile uint8 OCR0A;
//			volatile uint8 OCR0B;
//		};
//
//		TimerRegisters* const operator->()
//		{
//			return reinterpret_cast<TimerRegisters* const > (0x52);
//		}
//
//	};
//
//	template<>
//	class TimerRegisters<1>
//	{
//	public:
//		static const mcu::InterruptVector OVF = mcu::interrupts::INTVEC_TIMER1OVF;
//		static const mcu::InterruptVector COMPA = mcu::interrupts::INTVEC_TIMER1COMPA;
//		static const mcu::InterruptVector COMPB = mcu::interrupts::INTVEC_TIMER1COMPB;
//		static const mcu::InterruptVector COMPC = mcu::interrupts::INTVEC_TIMER1COMPB;
//
//		volatile uint8 ICRnL;
//		volatile uint8 ICRnH;
//		volatile uint8 OCRnBL;
//		volatile uint8 OCRnBH;
//		volatile uint8 OCRnAL;
//		volatile uint8 OCRnAH;
//		volatile uint8 TCNTnL;
//		volatile uint8 TCNTnH;
//		union
//		{
//			volatile uint8 TCCRnC; // only for compatibility
//			volatile uint8 TCCRnB;
//		};
//		volatile uint8 TCCRnA; // 0x80
//		uint8 dummy0[0x58 - 0x50];
//		volatile uint8 TIFRn; // 0x36
//		volatile uint8 TIMSKn; // 0x6F
//
//		TimerRegisters* const operator->()
//		{
//			return reinterpret_cast<TimerRegisters* const > (0x46);
//		}
//
//};
//
//};
//
//}
//}

#endif /* MACHINEDEFINITIONS_ATMEGA32_H_ */
