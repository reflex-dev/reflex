#ifndef MACHINEDEFINITIONS_ATMEGA164_H_
#define MACHINEDEFINITIONS_ATMEGA164_H_
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2012 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATION SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATION SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */
#include "MachineDefinitions_default.h"

namespace reflex
{
namespace atmega
{

template<> struct McuCore<Atmega164> : public internal::DefaultController
{

	enum InterruptVector
	{
		INVALID_INTERRUPT = -1,
		INTVEC_INT0 = 0,
		INTVEC_INT1,
		INTVEC_INT2,
		INTVEC_PCINT0,
		INTVEC_PCINT1,
		INTVEC_PCINT2,
		INTVEC_PCINT3,
		INTVEC_WDT,
		INTVEC_TIMER2COMPA,
		INTVEC_TIMER2COMPB,
		INTVEC_TIMER2OVF,
		INTVEC_TIMER1CAPT,
		INTVEC_TIMER1COMPA,
		INTVEC_TIMER1COMPB,
		INTVEC_TIMER1OVF,
		INTVEC_TIMER0COMPA,
		INTVEC_TIMER0COMPB,
		INTVEC_TIMER0OVF,
		INTVEC_SPI_STC,
		INTVEC_USART0_RX,
		INTVEC_USART0_UDRE,
		INTVEC_USART0_TX,
		INTVEC_ANALOG_COMP,
		INTVEC_ADC,
		INTVEC_EE_READY,
		INTVEC_TWI,
		INTVEC_SPM_READY,
		INTVEC_USART1_RX,
		INTVEC_USART1_UDRE,
		INTVEC_USART1_TX,
		MAX_HANDLERS
	};

	enum PortPins
	{
		SS = 1 << 4, SCK = 1 << 7, MOSI = 1 << 5, MISO = 1 << 6
	};

	template<TimerNr nr> struct TimerRegisters: public internal::TimerRegisters<Atmega164, nr,
		(nr == Timer0) ? INTVEC_TIMER0OVF :
		(nr == Timer1) ? INTVEC_TIMER1OVF :
		(nr == Timer2) ? INTVEC_TIMER2OVF :
		INVALID_INTERRUPT,
		(nr == Timer0) ? INTVEC_TIMER0COMPA :
		(nr	== Timer1) ? INTVEC_TIMER1COMPA :
		(nr == Timer2) ? INTVEC_TIMER2COMPA :
		INVALID_INTERRUPT,
		(nr == Timer0) ? INTVEC_TIMER0COMPB :
		(nr == Timer1) ? INTVEC_TIMER1COMPB :
		(nr == Timer2) ? INTVEC_TIMER2COMPB :
		INVALID_INTERRUPT,
		INVALID_INTERRUPT,
		(nr == Timer1) ? INTVEC_TIMER1CAPT :
		INVALID_INTERRUPT
		>
	{

	};

	template<UsartNr nr> struct UsartRegisters : public internal::UsartRegisters<Atmega164, nr,
		(nr == Usart0) ? INTVEC_USART0_RX :
		(nr == Usart1) ? INTVEC_USART1_RX :
		INVALID_INTERRUPT,
		(nr == Usart0) ? INTVEC_USART0_UDRE :
		(nr == Usart1) ? INTVEC_USART1_UDRE :
		INVALID_INTERRUPT,
		(nr == Usart0) ? INTVEC_USART0_TX :
		(nr == Usart1) ? INTVEC_USART1_TX :
		INVALID_INTERRUPT>
	{

	};

};

template<> struct McuCore<Atmega324> : public McuCore<Atmega164>
{

};

template<> struct McuCore<Atmega644> : public McuCore<Atmega164>
{

};

template<> struct McuCore<Atmega1284> : public McuCore<Atmega164>
{
	enum InterruptVector
	{
		INVALID_INTERRUPT = -1,
		INTVEC_INT0 = 0,
		INTVEC_INT1,
		INTVEC_INT2,
		INTVEC_PCINT0,
		INTVEC_PCINT1,
		INTVEC_PCINT2,
		INTVEC_PCINT3,
		INTVEC_WDT,
		INTVEC_TIMER2COMPA,
		INTVEC_TIMER2COMPB,
		INTVEC_TIMER2OVF,
		INTVEC_TIMER1CAPT,
		INTVEC_TIMER1COMPA,
		INTVEC_TIMER1COMPB,
		INTVEC_TIMER1OVF,
		INTVEC_TIMER0COMPA,
		INTVEC_TIMER0COMPB,
		INTVEC_TIMER0OVF,
		INTVEC_SPI_STC,
		INTVEC_USART0_RX,
		INTVEC_USART0_UDRE,
		INTVEC_USART0_TX,
		INTVEC_ANALOG_COMP,
		INTVEC_ADC,
		INTVEC_EE_READY,
		INTVEC_TWI,
		INTVEC_SPM_READY,
		INTVEC_USART1_RX,
		INTVEC_USART1_UDRE,
		INTVEC_USART1_TX,
		INTVEC_TIMER3CAPT,
		INTVEC_TIMER3COMPA,
		INTVEC_TIMER3COMPB,
		INTVEC_TIMER3OVF,
		MAX_HANDLERS
	};

	template<TimerNr nr> struct TimerRegisters: public internal::TimerRegisters<Atmega164, nr,
		(nr == Timer0) ? INTVEC_TIMER0OVF :
		(nr == Timer1) ? INTVEC_TIMER1OVF :
		(nr == Timer2) ? INTVEC_TIMER2OVF :
		(nr == Timer3) ? INTVEC_TIMER3OVF :
		INVALID_INTERRUPT,
		(nr == Timer0) ? INTVEC_TIMER0COMPA :
		(nr	== Timer1) ? INTVEC_TIMER1COMPA :
		(nr == Timer2) ? INTVEC_TIMER2COMPA :
		(nr == Timer3) ? INTVEC_TIMER3COMPA :
		INVALID_INTERRUPT,
		(nr == Timer0) ? INTVEC_TIMER0COMPB :
		(nr == Timer1) ? INTVEC_TIMER1COMPB :
		(nr == Timer2) ? INTVEC_TIMER2COMPB :
		(nr == Timer3) ? INTVEC_TIMER3COMPB :
		INVALID_INTERRUPT,
		INVALID_INTERRUPT,
		(nr == Timer1) ? INTVEC_TIMER1CAPT :
		(nr == Timer3) ? INTVEC_TIMER3CAPT :
		INVALID_INTERRUPT
		>
	{

	};

};

}
}

#endif /* MACHINEDEFINITIONS_ATMEGA2560_H_ */
