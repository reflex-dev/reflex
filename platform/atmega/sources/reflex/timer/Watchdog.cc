#include "reflex/MachineDefinitions.h"
#include "reflex/interrupts/InterruptLock.h"
#include "reflex/timer/Watchdog.h"

using namespace reflex;
using namespace mcu;

Watchdog::Interval Watchdog::interval()
{
	const uint8 timeoutMask = Core::WDP0 | Core::WDP1 | Core::WDP2 | Core::WDP3;
	uint8 timeout = Core::Registers()->WDTCSR & timeoutMask;
	return static_cast<Interval> (timeout);
}

void Watchdog::setInterval(Watchdog::Interval time)
{
	_wdr();
	InterruptLock lock;
	uint8 registerContent = Core::Registers()->WDTCSR;
	registerContent &= ~(Core::WDP3 | Core::WDP2 | Core::WDP1 | Core::WDP0);
	registerContent |= time;
	/* Store new timeout */
	Core::Registers()->WDTCSR |= Core::WDCE | Core::WDE;
	Core::Registers()->WDTCSR = registerContent;
}

void Watchdog::start()
{
	InterruptLock lock;
	Core::Registers()->WDTCSR |= Core::WDCE | Core::WDE;
	Core::Registers()->WDTCSR |= Core::WDE;
}

void Watchdog::start(Interval time)
{
	setInterval(time);
	start();
}

void Watchdog::stop()
{
	_wdr();
	InterruptLock lock;
	uint8 disabled = Core::Registers()->WDTCSR & ~(Core::WDE | Core::WDCE);
	Core::Registers()->WDTCSR |= Core::WDCE | Core::WDE;
	Core::Registers()->WDTCSR = disabled;
}

void Watchdog::reset()
{
	_wdr();
}

void Watchdog::_wdr()
{
	asm volatile ("wdr"::);
}

__attribute__((naked)) __attribute__((section(".init1"))) void initWatchdog()
{
	Core::Registers()->MCUSR &= ~Core::WDRF;
	Watchdog watchdog;
	watchdog.stop();
}
