/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2012 BTU Cottbus, Department for Distributed Systems and
 *    Operating Systems. All rights reserved.
 *
 *    Redistribution and use in source and binary forms, with or without
 *    modification, are permitted provided that the following conditions
 *    are met:
 *
 *       1. Redistributions of source code must retain the above copyright
 *          notice, this list of conditions and the following disclaimer.
 *
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the
 *          distribution.
 *
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED
 *    SYSTEMS AND OPERATION SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND
 *    OPERATION SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *    The views and conclusions contained in the software and documentation
 *    are those of the authors and should not be interpreted as representing
 *    official policies, either expressed or implied, of BTU Cottbus,
 *    Department for Distributed Systems and Operating Systems.
 *
 * */
#ifndef TIMER_H
#define TIMER_H

#include "reflex/interrupts/InterruptLock.h"
#include "reflex/MachineDefinitions.h"
#include "reflex/types.h"

namespace reflex {

namespace atmega {

/*!
 \ingroup atmega
 \brief Driver for the basic functionality of ATMEGA hardware timers.

 Each controller has a different set of timers, but nearly all of them
 do offer ::Timer0, ::Timer1, ::Timer2. The template parameter \a timerNr
 specifies a hardware timer unit.

 \sa TimerNr
 */
template<TimerNr timerNr>
class Timer {
public:

	//! Source of the clock input. See the data sheet for details.
	enum ClockSource {
		ClockDisabled = 0, //!< timer is stopped
		ClockOsc1 = Core::CSn0, //!< system clock divided by 1
		ClockOsc8 = Core::CSn1, //!< system clock divided by 8
		ClockOsc32 = (timerNr == Timer2) ? Core::CSn1 | Core::CSn0 : 0, //!< system clock divided by 32. Only available for ::Timer2
		ClockOsc64 = (timerNr == Timer2) ? Core::CSn2 : Core::CSn0 | Core::CSn1, //!< system clock divided by 64
		ClockOsc128 = (timerNr == Timer2) ? Core::CSn2 | Core::CSn0 : 0,//!< system clock divided by 128. Only available for ::Timer2
		ClockOsc256 = (timerNr == Timer2) ? Core::CSn2 | Core::CSn1
		        : Core::CSn2, //!< system clock divided by 256
		ClockOsc1024 = (timerNr == 2) ? Core::CSn2 | Core::CSn1 | Core::CSn0
		        : Core::CSn2 | Core::CSn0, //!< system clock divided by 1024
		ExternalFallingEdge = Core::CSn2 | Core::CSn1, //!< falling edges at the Tn pin.
		ExternalRisingEdge = Core::CSn2 | Core::CSn1 | Core::CSn0
	//!< rising edges at the Tn pin.
	};

	/*!
	 \brief The mode of operation.

	 ::Timer0 and ::Timer2 only offer \a Mode0 .. \a Mode7 while all other
	 timers support all modes. Modes are different for different timers.
	 Consult the datasheet for details.
	 */
	enum Mode {
		Mode0 = 0, //!< Normal operation. Counting from 0 to MAX.
		Mode1 = (timerNr == Timer0) ? Core::WGM00
		        : (timerNr == Timer2) ? Core::WGM20 : Core::WGMn0,
		Mode2 = (timerNr == Timer0) ? Core::WGM01
		        : (timerNr == Timer2) ? Core::WGM21 : Core::WGMn1,
		Mode3 = (timerNr == Timer0) ? (Core::WGM01 | Core::WGM00) : (timerNr
		        == Timer2) ? Core::WGM21 | Core::WGM20 : Core::WGMn1
		        | Core::WGMn0,
		Mode4 = (timerNr == Timer0) ? (Core::WGMn2) : Core::WGMn2 << 8,
		Mode5 = (timerNr == Timer0) ? (Core::WGMn2 | Core::WGM00)
		        : (Core::WGMn2 << 8) | Core::WGMn0,
		Mode6 = (timerNr == Timer0) ? (Core::WGMn2 | Core::WGM01)
		        : (Core::WGMn2 << 8) | Core::WGMn1,
		Mode7 = (timerNr == Timer0) ? (Core::WGMn2 | Core::WGM01 | Core::WGM00)
		        : (Core::WGMn2 << 8) | Core::WGMn1 | Core::WGMn0,
		Mode8 = (Core::WGMn3 << 8),
		Mode9 = (Core::WGMn3 << 8) | Core::WGMn0,
		Mode10 = (Core::WGMn3 << 8) | Core::WGMn1,
		Mode11 = (Core::WGMn3 << 8) | Core::WGMn1 | Core::WGM00,
		Mode12 = (Core::WGMn3 | Core::WGMn2) << 8,
		Mode13 = ((Core::WGMn3 | Core::WGMn2) << 8) | Core::WGMn0,
		Mode14 = ((Core::WGMn3 | Core::WGMn2) << 8) | Core::WGMn1,
		Mode15 = (Core::WGMn3 | Core::WGMn2) << 8 | Core::WGMn1 | Core::WGMn0,
		ModeMask = (timerNr == Timer0) ? ((Core::WGMn2 << 8) | Core::WGM01
		        | Core::WGM00) : (Core::WGMn3 | Core::WGMn2) << 8 | Core::WGMn1
		        | Core::WGMn0
	};

	//! Asynchronous operation mode of ::Timer2.
	enum AsyncMode {
		AsyncDisabled = 0,
		ExternalClock = Core::EXCLK,
		CrystalEnabled = Core::AS2
	};

	Timer();

	//! Configures an asynchronous mode.
	void setAsyncMode(AsyncMode mode);

	//! Configures the counter behaviour.
	void setMode(Mode mode);

	//! Sets the counter register.
	void setTime(uint16 ticks);

	//! Starts the hardware timer using \a source.
	void start(ClockSource source);

	//! Stops the counter immediately.
	void stop();

	//! Returns the actual count.
	uint16 time() const;

private:
	typedef Core::TimerRegisters<timerNr> Register;
};

template<TimerNr timerNr>
Timer<timerNr>::Timer()
{
	Register()->TCCRnB = 0;
	Register()->TCCRnA = 0;
}

template<TimerNr timerNr>
void Timer<timerNr>::setMode(Timer<timerNr>::Mode mode)
{
	Register()->TCCRnB &= ~(static_cast<uint16> (ModeMask) >> 8);
	Register()->TCCRnA &= ~static_cast<uint8> (ModeMask);
	Register()->TCCRnB |= static_cast<uint16> (mode) >> 8;
	Register()->TCCRnA |= static_cast<uint8> (mode);
}

template<TimerNr timerNr>
void Timer<timerNr>::setTime(uint16 time)
{
	Register()->TCNTnH = (time >> 8);
	Register()->TCNTnL = time;
}

template<TimerNr timerNr>
void Timer<timerNr>::start(Timer<timerNr>::ClockSource source)
{
	Register()->TCCRnB |= static_cast<uint8> (source);
}

template<TimerNr timerNr>
void Timer<timerNr>::stop()
{
	Register()->TCCRnB &= ~(Core::CSn2 | Core::CSn1 | Core::CSn0);
}

template<TimerNr timerNr>
uint16 Timer<timerNr>::time() const
{
	uint16 time = 0;
	switch (timerNr) {
	case Timer1:
	case Timer3:
	case Timer4:
	case Timer5:
		time = (static_cast<uint16> (Register()->TCNTnH) << 8);
	case Timer0:
	case Timer2:
		time |= Register()->TCNTnL;
	}
	return time;
}

/**
 * \note Only ::Timer2 does support this feature.
 */
template<TimerNr timerNr>
void Timer<timerNr>::setAsyncMode(AsyncMode mode)
{
	Core::Registers()->ASSR = mode;
}
}

}

#endif /* TIMER_H */
