/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *	Class(ses):	HardwareTimerMilli

 *	Author:		Stefan Nuernberger
 * */

//#include "conf.h"
#include "reflex/timer/HardwareTimerMilli.h"
#include "reflex/interrupts/InterruptLock.h"

using namespace reflex;

void HardwareTimerMilli::Counter::notify()
{
	++value;
}

/** constructor
 */
HardwareTimerMilli::HardwareTimerMilli()
{
	compareMatch.output = this;
	overflow.output = &overflows;
	overflow.switchOn();
}

/** notify
 * implements Sink0
 * adjusts alarm timer and notifies output
 */
void HardwareTimerMilli::notify()
{
	setAlarm();
}

/** getNow
 * @return current counter value (local time)
 */
Time HardwareTimerMilli::getNow()
{
	// atomic operation
	InterruptLock lock;

	if (overflow.isOverflowPending())
	{
		overflow.clearOverflow();
		overflows.notify();
	}

	// assemble local time
	return ((overflows.get() << OverflowShift) | (timer.time() >> PrecisionShift));
}

/** getTimestamp
 * @return 64 bit counter value in hardware precision
 */
uint64 HardwareTimerMilli::getTimestamp()
{
    // atomic operation
    InterruptLock lock;

    if (overflow.isOverflowPending())
    {
        overflow.clearOverflow();
        overflows.notify();
    }

    // assemble hw precision timestamp
    return ((overflows.get64() << (OverflowShift + PrecisionShift))
            | timer.time());
}

/** start
 * set a timeout until next interrupt and start the timer
 * @param ticks number of ticks until timer event
 */
void HardwareTimerMilli::start(Time ticks)
{
	startAt(getNow(), ticks);
}

/**
 * startAt
 * set a new time interrupt to occur ticks after t0
 * @param t0 time from where to set the timer
 * @param ticks number of ticks after t0 when the timer should fire
 */
void HardwareTimerMilli::startAt(Time t0, Time ticks)
{
	this->time = t0;
	this->delta = ticks;

	setAlarm();
}

/** stop
 * stop the hardware timer alarm
 * NOTE: THIS DOES NOT STOP THE LOCAL TIME COUNTER!
 */
void HardwareTimerMilli::stop()
{
	compareMatch.switchOff();

	timer.stop();
}

/** setAlarm
 * set the alarm counter
 */
void HardwareTimerMilli::setAlarm()
{
	Time now = getNow();
	Time elapsed = now - time;

	if (elapsed >= delta)
	{
		if (output)
		{
			output->notify(); // notify output
		}
		return;
	}

	Time remaining = delta - elapsed;

	/* update alarm timer */
	uint16 nextAlarm;

	/* check maximum */
	if (remaining > MaxTimerValue)
	{
		remaining = MaxTimerValue;
	}

	/* check minimum */
	if (remaining > SafeMinimum)
	{
		nextAlarm = (uint16) ((now + remaining) << PrecisionShift);
	}
	else
	{
		nextAlarm = (uint16) ((remaining << PrecisionShift) + timer.time());
	}

	compareMatch.setMatchAt(nextAlarm);
	compareMatch.switchOn();
	timer.start(timer.ClockOsc1024); // 1ms@16Mhz
}
