/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *

 *	Author:		Karsten Walther
 *
 *  some function definitions needed by avr-g++ for compilation of c++
 */

extern "C" void __cxa_pure_virtual();
extern "C" void _pure_virtual(void);
extern "C" void __pure_virtual(void);
extern "C" int atexit( void (*func)(void));
extern "C" int __cxa_atexit();

void __cxa_pure_virtual()
{
}

void _pure_virtual()
{
}

void __pure_virtual()
{
}

int atexit( void (*func)(void)) {
    (void)func;
    return -1;
}

int __cxa_atexit() {return -1;}
