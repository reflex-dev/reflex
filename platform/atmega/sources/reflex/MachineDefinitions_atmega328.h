#ifndef MACHINEDEFINITIONS_ATMEGA328_H_
#define MACHINEDEFINITIONS_ATMEGA328_H_
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2012 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATION SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATION SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */

#include "MachineDefinitions_default.h"

namespace reflex
{
namespace atmega
{

template<> struct McuCore<Atmega328> : public internal::DefaultController
{
	enum InterruptVector
	{
		INVALID_INTERRUPT = -1,
		INTVEC_INT0 = 0,
		INTVEC_INT1,
		INTVEC_PCINT0,
		INTVEC_PCINT1,
		INTVEC_PCINT2,
		INTVEC_WDT,
		INTVEC_TIMER2COMPA,
		INTVEC_TIMER2COMPB,
		INTVEC_TIMER2OVF,
		INTVEC_TIMER1CAPT,
		INTVEC_TIMER1COMPA,
		INTVEC_TIMER1COMPB,
		INTVEC_TIMER1OVF,
		INTVEC_TIMER0COMPA,
		INTVEC_TIMER0COMPB,
		INTVEC_TIMER0OVF,
		INTVEC_SPI_STC,
		INTVEC_USART0_RX,
		INTVEC_USART0_UDRE,
		INTVEC_USART0_TX,
		INTVEC_ADC,
		INTVEC_EE_READY,
		INTVEC_ANALOG_COMP,
		INTVEC_TWI,
		INTVEC_SPM_READY,
		MAX_HANDLERS
	};

	enum PortPins
	{
		SS = 1 << 2, SCK = 1 << 5, MOSI = 1 << 3, MISO = 1 << 4
	};

	template<TimerNr nr> struct TimerRegisters: public internal::TimerRegisters<Atmega328, nr,
		(nr == Timer0) ? INTVEC_TIMER0OVF :
		(nr == Timer1) ? INTVEC_TIMER1OVF :
		(nr == Timer2) ? INTVEC_TIMER2OVF :
		INVALID_INTERRUPT,
		(nr == Timer0) ? INTVEC_TIMER0COMPA :
		(nr	== Timer1) ? INTVEC_TIMER1COMPA :
		(nr == Timer2) ? INTVEC_TIMER2COMPA :
		INVALID_INTERRUPT,
		(nr == Timer0) ? INTVEC_TIMER0COMPB :
		(nr == Timer1) ? INTVEC_TIMER1COMPB :
		INVALID_INTERRUPT,
		INVALID_INTERRUPT,
		(nr == Timer1) ? INTVEC_TIMER1CAPT :
		INVALID_INTERRUPT>
	{

	};

	template<UsartNr nr> struct UsartRegisters : public internal::UsartRegisters<Atmega328, nr,
		(nr == Usart0) ? INTVEC_USART0_RX :
		INVALID_INTERRUPT,
		(nr == Usart0) ? INTVEC_USART0_UDRE :
		INVALID_INTERRUPT,
		(nr == Usart0) ? INTVEC_USART0_TX :
		INVALID_INTERRUPT>
	{

	};

};

template<> struct McuCore<Atmega168> : public McuCore<Atmega328>
{

};
}
}

#endif /* MACHINEDEFINITIONS_ATMEGA328_H_ */
