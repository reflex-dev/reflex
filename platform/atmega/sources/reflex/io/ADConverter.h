#ifndef ADConverter_h
#define ADConverter_h
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2012 BTU Cottbus, Department for Distributed Systems and
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */

#include "reflex/data_types/Singleton.h"
#include "reflex/interrupts/InterruptHandler.h"
#include "reflex/MachineDefinitions.h"
#include "reflex/scheduling/Activity.h"
#include "reflex/sinks/Queue.h"

namespace reflex
{

namespace atmega {

class ADChannel;

/**
 \ingroup atmega
 \brief Analog digital conversion unit of the ATMEGA controller family.

 This driver contains the main functionality for AD conversion and serves
 as helper for ADChannel. You may not use it directly except to set the
 reference source. Therefore the constructor is disabled.
 Use ADChannel::converter() to access the single instance of ADConverter.

 Due to hardware restrictions, the first conversion may take up to 25
 AD clock cycles. With a default prescaler of 128 and a system clock
 of 16MHz this results in 200µs measurement time. Succeeding conversions
 take up to 15 ADC ticks (120µs). If multiple ADChannel objects have
 scheduled a conversion, the ADC unit is not switched off in between.

 PowerManagement of this driver is handled implicitly.

 \see ADChannel

 */
class ADConverter: public Activity, protected InterruptHandler
{
	friend class ADChannel;
	friend class data_types::Singleton<ADConverter>;
	typedef Core::Registers Registers;
public:
	/**
	 \brief Voltage reference source that is used for conversion.

	 */
	enum Reference
	{
		ExternalAREF = 0, //!< external reference on AREF pin
		ExternalAVCC = Core::REFS0, //!< reference connected to AVCC pin
		Internal11V = Core::REFS1, //!< internal reference 1.1V
		Internal256V = Core::REFS1 | Core::REFS0
	//!< internal reference 2.56V
	};

	/**
	 \brief AD converter input channels

	 Channel0 .. Channel15 are single ended voltage inputs.
	 */
	enum Channel
	{
		Channel0 = 0x0,
		Channel1 = 0x1,
		Channel2 = 0x2,
		Channel3 = 0x3,
		Channel4 = 0x4,
		Channel5 = 0x5,
		Channel6 = 0x6,
		Channel7 = 0x7,
		Channel8 = 0x8,
		Channel9 = 0x9,
		Channel10 = 0xA,
		Channel11 = 0xB,
		Channel12 = 0xC,
		Channel13 = 0xD,
		Channel14 = 0xE,
		Channel15 = 0xF
	};

	//!  Set the voltage reference source to \a reference.
	void setReference(Reference reference);

private:

	ADConverter();
	ADConverter(const ADConverter&);
	ADConverter& operator=(const ADConverter&);

	Queue<ADChannel*> input_request; ///< pending conversion requests

private:

	//! Reads the sampled value and propagates it to the output.
	void handle();

	//! Starts the AD-conversion.
	void run();

	//! Dummy
	void enable();

	//! Dummy
	void disable();

	ADChannel* currentChannel; ///< currenct active channel
};

}

} //ns reflex
#endif
