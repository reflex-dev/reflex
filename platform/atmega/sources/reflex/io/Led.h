#ifndef LED_h
#define LED_h
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *    This file is part of REFLEX.
 *
 *    REFLEX is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as
 *    published by the Free Software Foundation, either version 3 of the
 *    License, or (at your option) any later version.
 *
 *    REFLEX is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with REFLEX.  If not, see <http://www.gnu.org/licenses/>.
 * */

#include "reflex/sinks/Sink.h"
#include "reflex/types.h"
#include "reflex/MachineDefinitions.h"
namespace reflex
{

namespace atmega {

/**
 * \ingroup atmega
 * \brief Convenience class to access LEDs on the STK600 eval board.
 *
 * The STK600 contains 8 LEDs which are driven active low. This driver
 * assumes all leds connected to the same port.
 * The port is configurable by the template parameter \a TPort.
 *
 * The STK600 LEDs are inverted. A logical 0 means "on" and 1 "off".
 *
 * \see atmega::IOPort
 *
 */
template<typename TPort>
class Led: public Sink1<uint8>
{
public:
	enum Nr
	{
		LED0 = 0x01,
		LED1 = 0x02,
		LED2 = 0x04,
		LED3 = 0x08,
		LED4 = 0x10,
		LED5 = 0x20,
		LED6 = 0x40,
		LED7 = 0x80,
		ALL = 0xFF	/// access all LEDs
	};

	Led()
	{
		TPort()->DDR = 0xFF; //set direction of pin to output
		TPort()->PORT = 0xFF; // switch all leds off
	}

	/**
	 * Turn the light on
	 * @param col led(s) which should be shown
	 */
	static void turnOn(uint8 col);

	/**
	 * Turn the light off
	 * @param col leds(s) which should be off
	 */
	static void turnOff(uint8 col);

	/**
	 * Change the led's status
	 * @param col led(s) which should blink
	 */
	static void toggle(uint8 col);

	/**
	 * This method sets the led to the wanted state.
	 *
	 *  @param value : 0 (led off) to 7 (all on)
	 */
	virtual void assign(uint8 col);
};

template<typename TPort>
void Led<TPort>::turnOn(uint8 col)
{
	TPort()->DDR |= col; //set direction of pin to output
	TPort()->PORT &= (~col); //pull pin x low
}

template<typename TPort>
void Led<TPort>::turnOff(uint8 col)
{
	TPort()->DDR |= col; //set direction of pin to output
	TPort()->PORT |= col; //pull pin x high
}

template<typename TPort>
void Led<TPort>::toggle(uint8 col)
{
	if (TPort()->PORT & col)
	{
		turnOn(col);
	}
	else
	{
		turnOff(col);
	}
}

template<typename TPort>
void Led<TPort>::assign(uint8 col)
{
	TPort()->PORT = ~col; //pull pin x low
}

}

} //reflex
#endif
