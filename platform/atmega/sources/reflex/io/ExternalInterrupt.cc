#include "reflex/debug/Assert.h"
#include "reflex/io/ExternalInterrupt.h"
#include "reflex/sinks/Sink.h"
#include "reflex/CoreApplication.h"

using namespace reflex;
using namespace atmega;

/*!
 Do not forget to configure a valid pin before using
 ExternalInterrupt.

 \sa setPin()
 */
ExternalInterrupt::ExternalInterrupt() :
	InterruptHandler(Core::INVALID_INTERRUPT, PowerManageAble::PRIMARY)
{
	this->setSleepMode(ExtendedStandBy);
	this->_pin = InvalidPin;
	this->out_event = 0;
}

ExternalInterrupt::~ExternalInterrupt()
{
	if (this->isEnabled())
	{
		this->switchOff();
	}

	this->setPin(InvalidPin);
}

void ExternalInterrupt::handle()
{
	if (out_event)
	{
		out_event->notify();
	}
}

/*!
 If no pin has been selected, this method returns ExternalInterrupt::InvalidPin.

 \sa setPin()
 */
ExternalInterrupt::Pin ExternalInterrupt::pin() const
{
	return static_cast<Pin> (_pin);
}

/*!
 If no pin has been selected, this method returns ExternalInterrupt::InvalidEvent.

 \sa setEvent(), setPin()
 */
ExternalInterrupt::Event ExternalInterrupt::event() const
{
	if (pin() == InvalidPin)
	{
		return InvalidEvent;
	}

	uint8 edge = LowLevel;
	if (pin() < INT4)
	{
		edge = Register()->EICRA >> static_cast<uint8> (pin() << 1);
	}
	else
	{
		edge = Register()->EICRB >> static_cast<uint8> ((pin() - INT4) << 1);
	}
	return static_cast<Event> (edge & 0x03);
}

/**
 It is possible to change the pin during runtime. In this case, ExternalInterrupt
 will unregister from the old interrupt vector and register to the new one.
 A previous configured event of the new interrupt pin will remain. It should be
 set immediatly after this method, before switching the driver on again.

 It is not possible to have multiple driver instances listening to the same interrupt
 source. The last one will always override previous driver instances.

 This method will fail when the driver has not been disabled before.

 \sa pin(), setEvent(), switchOff()
 */
void ExternalInterrupt::setPin(Pin pin)
{
	Assert(!this->isEnabled())

	uint8 oldPin = _pin;
	_pin = pin;

	// Register new interrupt and determine power mode
	if (oldPin != InvalidPin)
	{
		unregisterFrom(Core::INTVEC_INT0 + oldPin);
	}
	if (_pin != InvalidPin)
	{
		registerTo(Core::INTVEC_INT0 + _pin);
		mcu::PowerModes mode = (_pin < INT4) ? mcu::ExtendedStandBy : mcu::Idle;
		setSleepMode(mode);
	}
	else
	{
		setSleepMode(mcu::DeepestSleepMode);
	}
}

/**
 It is possible to change the event type during runtime while the driver
 is enabled.

 This method will fail when no interrupt pin has been configured or
 when trying to set ExternalInterrupt::InvalidEvent.

 \sa setPin()

 */
void ExternalInterrupt::setEvent(Event event)
{
	Assert(pin() != InvalidPin)
	Assert(event != InvalidEvent)

	if (pin() < INT4)
	{
		Register()->EICRA &= ~(0x03 << (pin() << 1));
		Register()->EICRA |= event << (pin() << 1);
	}
	else
	{
		Register()->EICRB &= ~(0x03 << ((pin() - INT4) << 1));
		Register()->EICRB |= static_cast<uint8> (event << ((pin() - INT4) << 1));
	}
}

/*!
 This method will fail if no interrupt pin has been set.
 */
void ExternalInterrupt::enable()
{
	Assert(pin() != InvalidPin);
	Register()->EIMSK |= static_cast<uint8> (1 << pin());
}

/*!
 This method will fail if no interrupt pin has been set.
 */
void ExternalInterrupt::disable()
{
	Assert(pin() != InvalidPin);
	Register()->EIMSK &= ~static_cast<uint8> (1 << pin());
}
