#include "reflex/io/ADChannel.h"
#include "reflex/MachineDefinitions.h"

using namespace reflex;
using namespace atmega;

ADChannel::ADChannel()
{
	this->_channel = ADConverter::Channel0;
	this->out_value = 0;
}

void ADChannel::setChannel(uint8 channel)
{
	this->_channel = channel;
}

uint8 ADChannel::channel() const
{
	return this->_channel;
}

void ADChannel::notify()
{
	InterruptLock lock;
	if (!this->isLinked())
	{
		adc->input_request.assign(this);
	}
}

ADConverter* ADChannel::converter()
{
	return &adc.getInstance();
}
