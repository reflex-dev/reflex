/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2012 BTU Cottbus, Department for Distributed Systems and
 *    Operating Systems. All rights reserved.
 *
 *    Redistribution and use in source and binary forms, with or without
 *    modification, are permitted provided that the following conditions
 *    are met:
 *
 *       1. Redistributions of source code must retain the above copyright
 *          notice, this list of conditions and the following disclaimer.
 *
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the
 *          distribution.
 *
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *    The views and conclusions contained in the software and documentation
 *    are those of the authors and should not be interpreted as representing
 *    official policies, either expressed or implied, of BTU Cottbus,
 *    Department for Distributed Systems and Operating Systems.
 *
 * */
#ifndef Usart_h
#define Usart_h

#include "reflex/MachineDefinitions.h"
#include "reflex/interrupts/InterruptFunctor.h"
#include "reflex/memory/Buffer.h"
#include "reflex/powerManagement/PowerManageAbleFunctor.h"
#include "reflex/scheduling/Activity.h"
#include "reflex/sinks/Queue.h"
#include "reflex/sinks/Sink.h"

namespace reflex
{
namespace atmega
{
/**
 \ingroup atmega
 \brief Driver for the USART interfaces of the ATMEGA controller family.

 Usart is a bidirectional serial driver component with a configurable
 baudrate and different transfer modes (only asynchronous single-speed mode at the moment).
 The USART hardware instance is selectable via the template parameter \a usartNr
 which can be a value of ::UsartNr. The number of available USART channels
 depends on the controller model.

 Incoming characters are forwarded through Usart::out_rx.
 Depending on the baudrate, the succeeding component should
 use a Fifo input to prevent from data loss. Outgoing messages represented
 as Buffer objects are read from Usart::in_tx. Once a buffer has been
 completely transmitted, a signal on Usart::out_txFinished is created. This
 is especially helpful where the amount of buffers is limited and the
 transmission speed is low.

 Power management for the sending part is done implicitly. The hardware is
 switched on once a datagram is scheduled for transmission and switched off
 after the last one has been sent.
 Before the Usart component is able to receive data, it must be explicitly
 enabled by calling
 \code
 serial.rxHandler().switchOn();
 \endcode
 or by adding the receiver power handler to a power group
 that is activated later.

 The port pins TXD and RXD are automatically brought into a high impendance
 state. No extra configuration is needed.

 \see UsartNr
 */
template<UsartNr usartNr>
class Usart: public Activity
{
public:

	//! Creates a driver instance.
	Usart();

	inline Queue<Buffer*>* get_in_tx();
	inline void set_out_rx(Sink1<uint8>* input);
	inline void set_out_txFinished(Sink0* input);

	//! Returns the receiver power handler.
	inline PowerManageAble& rxHandler();

	//! Configures the baud rate register.
	inline void setBaudRate(uint32 bauds, uint32 systemClockHz, bool doubleSpeedEnabled = true);

	//! Sends a character immediately.
	void sendChar(char c);

private:
	void run();
	void dummy()
	{
	}
	void handleTXC();
	void handleUDRE();
	void handleRX();
	void enableRX();
	void disableRX();

private:
	//! Input for outgoing messages
	Queue<Buffer*> in_tx;

	//! Component to be notified on completed transfers
	Sink0* out_txFinished;

	//! Receiver for incoming characters
	Sink1<uint8>* out_rx;

	InterruptFunctor<Usart, &Usart::handleRX, &Usart::enableRX, &Usart::disableRX> handler_RX; ///< handles incoming bytes
	InterruptFunctor<Usart, &Usart::handleUDRE, &Usart::dummy, &Usart::dummy> handler_UDRE; ///<functor that handles IRQ for transmission
	InterruptFunctor<Usart, &Usart::handleTXC, &Usart::dummy, &Usart::dummy> handler_TXC; ///< handles end of transmission

	uint8* sendPos; ///< current sending position inside of the buffer
	uint8 remaining; ///< remaining character count to be sended
	Buffer* buffer; ///< current sended buffer

	typedef Core::UsartRegisters<usartNr> Register;

};

/**
 The USART hardware is set to asynchronous single-speed mode
 and a frame format of 8 Bit with 1 Stop bit is configured.
 No baudrate is being set.

 \see setBaudRate()
 */
template<UsartNr usartNr>
Usart<usartNr>::Usart() :
	handler_RX(Register::RxVector, *this, PowerManageAble::PRIMARY),
			handler_UDRE(Register::UdreVector, *this, PowerManageAble::SECONDARY),
			handler_TXC(Register::TxcVector, *this, PowerManageAble::SECONDARY)
{
	Register()->UCSRA = 0x00; // disable all hardware
	Register()->UCSRB = 0x00; // disable all hardware

	sendPos = 0;
	remaining = 0;
	buffer = 0;
	out_rx = 0;
	out_txFinished = 0;
	in_tx.init(this);

	handler_RX.setSleepMode(Idle);
	handler_UDRE.setSleepMode(Idle);
	handler_TXC.setSleepMode(Idle);

	// set serial mode to 8 data bits and 1 stop bit
	Register()->UCSRC = Core::UCSZ1 | Core::UCSZ0;
	// Enable U2X by default since most baudrates are more accurate
}

template<UsartNr usartNr>
Queue<Buffer*>* Usart<usartNr>::get_in_tx()
{
	return &in_tx;
}

template<UsartNr usartNr>
void Usart<usartNr>::set_out_rx(Sink1<uint8>* input)
{
	out_rx = input;
}

template<UsartNr usartNr>
void Usart<usartNr>::set_out_txFinished(Sink0* input)
{
	out_txFinished = input;
}

template<UsartNr usartNr>
void Usart<usartNr>::run()
{
	lock();
	// Although it seems unlogical, we have to disable pending interrupts here
	Register()->UCSRB &= ~Core::TXCIE;
	Register()->UCSRB &= ~Core::UDRIE;
	buffer = in_tx.get();

	remaining = buffer->getLength();
	if (remaining > 0)
	{
		sendPos = buffer->getStart();
		Register()->UCSRB |= Core::TXEN; // enable transmitter
		Register()->UCSRB |= Core::UDRIE;
		handler_UDRE.switchOn();
	}
	else
	{
		buffer->downRef();
		unlock();
		if (out_txFinished)
		{
			out_txFinished->notify();
		}
	}
}

template<UsartNr usartNr>
PowerManageAble& Usart<usartNr>::rxHandler()
{
	return handler_RX;
}

template<UsartNr usartNr>
void Usart<usartNr>::handleRX()
{
	// be sure to confirm the RXInterrupt by reading the UDR even no receiver is present
	uint8 byte = Register()->UDR;
	if (out_rx)
	{
		out_rx->assign(byte);
	}
}

template<UsartNr usartNr>
void Usart<usartNr>::handleTXC()
{
	Register()->UCSRB &= ~Core::TXCIE;
	Register()->UCSRB &= ~(Core::TXEN);
	handler_TXC.switchOff();
	if (out_txFinished)
	{
		out_txFinished->notify();
	}
	unlock();
}

template<UsartNr usartNr>
void Usart<usartNr>::handleUDRE()
{
	Register()->UDR = *sendPos;
	sendPos++;
	remaining--;

	if (remaining == 0)
	{
		/* we have to wait for transmission complete (TXC interrupt) before
		 the transmitter could be disabled */
		/* disable data-reg empty interrupt case we have nothing to send */
		Register()->UCSRB &= ~Core::UDRIE;
		// enable transmission complete interrupt
		Register()->UCSRB |= Core::TXCIE;
		handler_UDRE.switchOff();
		handler_TXC.switchOn();
		buffer->downRef();
	}
}

/*!
 Write a character via the serial interface immediately.

 Sometimes it is necessary to send a message bypassing the event flow.
 This method provides such functionality even though the application
 may have crashed. It enables the transmitter, waits for an empty
 transmit buffer, writes one byte and returns immediately. The
 transmitter stays enabled afterwards.

 This method works only, if a valid baudrate has been set.

 \see setBaudRate()

 */
template<UsartNr usartNr>
void Usart<usartNr>::sendChar(char c)
{
	Register()->UCSRB |= Core::TXEN; // enable transmitter
	/* Wait for empty transmit buffer */
	while ((Register()->UCSRA & Core::UDRE) != Core::UDRE)
		;
	Register()->UDR = c;
}

template<UsartNr usartNr>
void Usart<usartNr>::enableRX()
{
	Register()->UCSRB |= Core::RXEN | Core::RXCIE; //enable receiving
}

template<UsartNr usartNr>
void Usart<usartNr>::disableRX()
{
	Register()->UCSRB &= ~(Core::RXEN | Core::RXCIE); //clean Receiving bit
}

/*
 * Calculation is done according to the data sheet, but rounding is considered.
 * Otherwise results at high baud rates would be wrong.
 */
/**
 The parameter \a bauds is the desired communication speed in baud/s whereas
 \a systemClockHz specifies the cpu clock frequency in Hz. The latter is
 necessary to calculate the correct prescaler value for the baudrate generation
 register UBRR.
 */
template<UsartNr usartNr>
void Usart<usartNr>::setBaudRate(uint32 bauds, uint32 systemClockHz, bool doubleSpeedEnabled)
{
	if (doubleSpeedEnabled == true)
	{
		Register()->UCSRA |= Core::U2X;
	}
	else
	{
		Register()->UCSRA &= ~Core::U2X;
	}

	uint16 ubrr = 0;
	// Asynchronous double speed mode
	if (Register()->UCSRA & Core::U2X)
	{
		ubrr = (systemClockHz + (bauds << 2)) / (bauds << 3) - 1;
	}
	// Asynchronous mode (normal mode)
	else
	{
		ubrr = (systemClockHz + (bauds << 3)) / (bauds << 4) - 1;
	}

	// set baudrate
	Register()->UBRR = ubrr;

}
}
} //namespace reflex

#endif
