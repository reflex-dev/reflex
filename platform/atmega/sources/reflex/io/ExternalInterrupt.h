#ifndef ExternalInterrupt_h
#define ExternalInterrupt_h
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2012 BTU Cottbus, Department for Distributed Systems and
 *    Operating Systems. All rights reserved.
 *
 *    Redistribution and use in source and binary forms, with or without
 *    modification, are permitted provided that the following conditions
 *    are met:
 *
 *       1. Redistributions of source code must retain the above copyright
 *          notice, this list of conditions and the following disclaimer.
 *
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the
 *          distribution.
 *
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED
 *    SYSTEMS AND OPERATION SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND
 *    OPERATION SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *    The views and conclusions contained in the software and documentation
 *    are those of the authors and should not be interpreted as representing
 *    official policies, either expressed or implied, of BTU Cottbus,
 *    Department for Distributed Systems and Operating Systems.
 *
 * */

#include "reflex/data_types/BitField.h"
#include "reflex/types.h"
#include "reflex/interrupts/InterruptHandler.h"
#include "reflex/MachineDefinitions.h"

namespace reflex
{

class Sink0;
namespace atmega
{

/**
 \ingroup atmega
 \brief Interrupt Handler for the external interrupt pins INT0..INT7
 of the ATMEGA controller family

 ATMEGA controllers offer up to 8 pins to serve as interrupt sources depending
 on the controller model (ExternalInterrupt::Pin).
 These pins are able to detect different events defined in
 ExternalInterrupt::Event. Once an event has been detected,
 it is forwarded via the output ExternalInterrupt::out_event.
 When being set to detect ExternalInterrupt::LowLevel or
 ExternalInterrupt::FallingEdge, the pull-up resistor of the corresponding
 IOPort pin should also be enabled to guarantee a valid level.

 The event type ExternalInterrupt::LowLevel needs some extra care. When being
 enabled, ExternalInterrupt may create so many events, that the
 application will crash. Nevertheless it can be configured to wake up the
 controller from very deep sleep modes. See datasheet for further details.

 Power management for this driver has to be done explicitly by the user by
 calling switchOn() and switchOff() or adding ExternalInterrupt to a
 power group.

 It is absolutly necessary, to set an interrupt source pin before activation:
 Since ExternalInterrupt holds its internal data in registers only, some of
 the methods will fail to prevent from unpredictable behaviour.

 \sa IOPort, PowerManageAble

 */
class ExternalInterrupt: public InterruptHandler
{
public:

	Sink0* out_event; //!< receiver for interrupt events

	//! The interrupt source pin
	enum Pin
	{
		INT0 = 0, INT1, INT2, INT3, INT4, INT5, INT6, INT7, InvalidPin = 0xFF
	};

	//! The event type to detect
	enum Event
	{
		LowLevel = 0, AnyEdge = 1, FallingEdge = 2, RisingEdge = 3, InvalidEvent = 0xFE
	};

	//! Initializes the driver to no interrupt pin
	ExternalInterrupt();

	//! Switches the driver off and unloads itself from interrupt handling.
	~ExternalInterrupt();

	//! The current configured interrupt source pin.
	Pin pin() const;

	//! The current configured event type.
	Event event() const;

	//! Configures the driver to use \a pin as interrupt source.
	void setPin(Pin pin);

	//!  Sets ExternalInterrupt to detect events specified by \a event.
	void setEvent(Event event);

private:
	void enable();
	void disable();
	void handle();

	uint8 _pin;

	typedef Core::Registers Register;
};
}
}
#endif /* ExternalInterrupt_h */
