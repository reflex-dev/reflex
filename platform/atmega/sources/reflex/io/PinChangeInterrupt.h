#ifndef PINCHANGEINTERRUPT_H_
#define PINCHANGEINTERRUPT_H_
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2012 BTU Cottbus, Department for Distributed Systems and
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATION SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATION SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */
#include "reflex/data_types/Flags.h"
#include "reflex/io/IOPort.h"
#include "reflex/interrupts/InterruptHandler.h"
#include "reflex/MachineDefinitions.h"
#include "reflex/sinks/Sink.h"
#include "reflex/types.h"

namespace reflex
{
namespace atmega
{

/**
 \ingroup atmega
 \brief Handler for pin change interrupts of the ATMEGA controller family

 ATMEGAs offer an interrupt on pin changes at different IOPort pins.
 These pins are organized in groups PinChangeInterrupt::PinGroup, where
 each contains 8 pins and all pins of one group share the same interrupt vector.

 To enable this driver, configure a pin-group by calling setPinGroup().
 The method setPinsEnabled(uint8) then enables a set of pins.

 When a pin change event is detected, PinChangeInterrupt::out_event is being
 notified.

 This driver offers implicit power management. As long as one pin is enabled,
 interrupts are handled and sleepmodes below atmega::PowerModes::Idle are not
 entered.

 */
class PinChangeInterrupt: public InterruptHandler
{
public:

	/**
	 \brief A group of coherent pins which share a common interrupt.

	 Some controllers offer only a subset of pin groups.
	 */
	enum PinGroup : uint8
	{
		PinGroup0 = 0, //!< PCINT0..PCINT7
		PinGroup1, //!< PCINT8..PCINT15
		PinGroup2, //!< PCINT16..PCINT23
		PinGroup3, //!< PCINT24..PCINT31
		InvalidPinGroup = 0xFF
	};

	//! Initializes and enables a group of pins.
	PinChangeInterrupt();

	//! Disables all pins and unregisters itself from interrupt handling.
	~PinChangeInterrupt();

	//! Configures the driver to listen to pin-group \a group.
	void setPinGroup(PinGroup group);

	//! Returns the current configured pin-group.
	PinGroup pinGroup() const;

	//! Disables pin change functionality for a set of \a pins.
	void setPinsDisabled(PortPins pins);

	//! Enables pin change functionality for a set of \a pins.
	void setPinsEnabled(PortPins pins);

	//! Returns all enabled pins of the current configured pin group.
	uint8 enabledPins() const;

	Sink0* out_event; //!< output for pin change events

private:
	void handle();
	void enable();
	void disable();

	PinGroup _pinGroup;

	typedef Core::Registers Register;
};

}

}

#endif /* PINCHANGEINTERRUPT_H_ */
