#ifndef ADCHANNEL_H_
#define ADCHANNEL_H_
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2012 BTU Cottbus, Department for Distributed Systems and
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATION SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATION SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */

#include "reflex/data_types/ChainLink.h"
#include "reflex/data_types/Singleton.h"
#include "reflex/io/ADConverter.h"

namespace reflex
{
namespace atmega {
/**
 \ingroup atmega
 \brief Virtual analog digital converter channel for the ATMEGA controller family.

 ADChannel is a component for single shot AD conversions. It can be bound to one
 of the ADC hardware channels by calling setChannel(). When being triggered via
 notify(), ADChannel schedules itself into a queue of ADConverter which is handled
 as a Singleton instance. An application may contain as many ADChannel objects as needed.

 The result of a conversion is then forwarded to ADChannel::out_value.

 To set global configuration parameters of the AD conversion unit, use the single
 ADConverter instance by calling converter().

 \see ADConverter for configuring the AD conversion hardware.


 */
class ADChannel: public data_types::ChainLink, public Sink0
{
	friend class ADConverter;

public:
	Sink0* in_start() { return this; }
	void set_out_value(Sink1<uint16>* input) { out_value = input; }

	Sink1<uint16>* out_value;

	//! ADChannel is set to ADConverter::Channel0 by default.
	ADChannel();

	//! Sets the current active channel hardware channel to \a channel.
	void setChannel(uint8 channel);

	//! The current selected hardware channel.
	uint8 channel() const;

	//! Triggers a single shot.
	void notify();

	//! Returns the single ADConverter instance.
	ADConverter* converter();

private:
	uint8 _channel;
	data_types::Singleton<ADConverter> adc;
};

}

}

#endif /* ADCHANNEL_H_ */
