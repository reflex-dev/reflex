#include "reflex/io/IOPort.h"

using namespace reflex;

void dummy()
{
	//! [Init Port]
	using namespace reflex;
	using namespace mcu;
	IOPort<Core::PortA>()->DDR |= Pin0 | Pin1;
	//! [Init Port]

	//! [Read Port]
	uint8 state = IOPort<Core::PortA>()->PIN & (Pin6 | Pin7);
	//! [Read Port]

	//! [Write Port]
	IOPort<Core::PortA>()->PORT &= ~Pin0;
	IOPort<Core::PortA>()->PORT |= Pin1;
	//! [Write Port]
}
