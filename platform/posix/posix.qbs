import qbs 1.0

Project {
    // Platform-specific classes and drivers for the atmega controller family.
    ReflexPackage {
        type: "staticlibrary"
        name: "platform"
        files: [
            "sources/reflex/CoreApplication.cc",
            "sources/reflex/interrupts/interruptFunctions.c",
            "sources/reflex/powerManagement/power.c",
            "sources/reflex/timer/SystemTimer.cc"
        ]

        Depends { name: "core" }
        Depends { name: "platform-essentials" }

        Group {
            files: "sources/reflex/debug/*.h"
            qbs.install: true
            qbs.installDir: "include/reflex/debug"
        }

        Group {
            files: "sources/reflex/interrupts/*.h"
            qbs.install: true
            qbs.installDir: "include/reflex/interrupts"
        }

        Group {
            files: "sources/reflex/io/*.h"
            qbs.install: true
            qbs.installDir: "include/reflex/io"
        }

        Group {
            files: "sources/reflex/memory/*.h"
            qbs.install: true
            qbs.installDir: "include/reflex/memory"
        }

        Group {
            files: "sources/reflex/timer/*.h"
            qbs.install: true
            qbs.installDir: "include/reflex/timer"
        }

        Group {
            fileTagsFilter: "staticlibrary"
            qbs.install: true
            qbs.installDir: "lib"
        }
    }

    // Platform dependend headers that are needed by the core in order to compile
    ReflexPackage {
        name: "platform-essentials"

        Group {
            files: [
                "sources/reflex/types.h",
                "sources/reflex/CoreApplication.h",
                "sources/reflex/MachineDefinitions.h"
            ]
            qbs.install: true
            qbs.installDir: "include/reflex/"
        }

        Export {
            Depends { name: "cpp" }

            cpp.includePaths: [
                "sources"
            ]

            cpp.defines : [
                "LINUX"
            ]
        }
    }
}


