/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
  
 *	Author:		Reinhard Hemmerling
 */

#include "reflex/interrupts/InterruptVector.h"

#include "reflex/interrupts/InterruptGuardian.h"
#include "reflex/CoreApplication.h"

extern "C" void wrapper_0();

using namespace reflex;

InterruptVector InterruptVector::itable[ITABLE_SIZE];

InterruptVector::InterruptVector()
{
}

void InterruptVector::execute()
{
	mcu::CoreApplication::instance()->guardian()->handle(vector);
}

InterruptVector& InterruptVector::getInterruptVector(int num)
{
	return itable[num];
}


void InterruptVector::operator= (char* targetVector)
{
   	itable[(int)this].vector = (int)(targetVector - (unsigned long)wrapper_0);
}


