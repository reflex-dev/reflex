/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2012 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATION SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATION SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */
#ifndef FLASHLINUX_H_
#define FLASHLINUX_H_

#include "reflex/MachineDefinitions.h"

namespace reflex
{

namespace posix
{
/*!
 \ingroup posix
 \brief Generic interface for accessing read-only data.

 Some platforms can hold read-only data in a non-volatile memory e.g Flash
 memory. On some platforms, this memory section is accessed the dame way
 as it would be RAM. On other platforms, special instructions are needed
 for accessing it (atmega).

 This class implements a generic interface for platform-independent applications.
 Read-only data can be through Flash::read and should not produce any overhead.

 \sa IN_FLASH, FLASH_PTR, reflex::atmega::Flash

 */
class Flash
{
public:

	template<typename T, size_t size> struct Array
	{
		operator T*()
		{
			return &data[0];
		}
		T data[size];
	};

	/*!
	 \brief Reads an array of type \a T from flash memory address \a addr
	 into the RAM and returns it.
	 */
	template<typename T, size_t n>
	static Array<T, n> read(const T(*data)[n]);

	/*!
	 \brief Reads a value of type \a T from address \a addr.

	 This method should be dissolved during compile-time.
	 */
	template<typename T>
	inline static T read(T const* addr)
	{
		return *addr;
	}
};

/*!
 The reason why this method was added is unknown.
 \todo Maybe remove this method.
 */
template<typename T, size_t size>
Flash::Array<T, size> Flash::read(const T(*data)[size])
{
	Array<T, size> string;
	for (size_t i = 0; i < 30; i++)
	{
		string.data[i] = Flash::read(&((*data)[i]));
	}
	return string;
}

}

}

/*!
 Dummy macro which does not have any functionality on this platform.
 */
#ifndef IN_FLASH
#define IN_FLASH
#endif

/*!
 Dummy macro which does not have any functionality on this platform.
 */
#ifndef READ_FLASH
#define READ_FLASH(data) data
#endif

/*!
 Dummy macro which does not have any functionality on this platform.
 */
#ifndef FLASH_PTR
#define FLASH_PTR(data) data
#endif


#endif /* FLASHLINUX_H_ */
