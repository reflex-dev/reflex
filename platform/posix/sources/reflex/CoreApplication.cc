#include <reflex/CoreApplication.h>

using namespace reflex;
using namespace posix;

extern "C" void _interruptsEnable();
extern "C" void _wait();
extern "C" void _yield();

namespace {
    PowerDownFunction powerFunctions[mcu::NrOfPowerModes] = {_active, _wait, _yield};
}

// These variables are located into the bss segment and are therfore
// being initialized to zero according to C++ Standard 3.6.2 (1):
// "Objects with static storage duration (3.7.1) shall be zero-initialized
// (8.5) before any other initialization takes place."
// We have to rely on that, because CoreApplication might get initialized
// as static variable.
CoreApplication* CoreApplication::_instance;
bool CoreApplication::_isInitialized;

/*!
\brief Creates a CoreApplication object.

CoreApplication is a singleton. Calling this method twice will fail or may
result in undefined behaviour.
 */
CoreApplication::CoreApplication()
    : _powerManager(powerFunctions), _guardian(_handlerTable, interrupts::MAX_HANDLERS)
{
    Assert(_isInitialized == false);
    _instance = this;
    _isInitialized = true;
}
