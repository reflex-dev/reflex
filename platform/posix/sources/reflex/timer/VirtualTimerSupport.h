#ifndef REFLEX_POSIX_VIRTUALTIMERSUPPORT_H
#define REFLEX_POSIX_VIRTUALTIMERSUPPORT_H
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(es):	VirtualTimerSupport
 *
 *	Author:		Stefan Nuernberger
 *
 *	Description:	Everything needed to provide Virtual Timers with
 *			millisecond precision on POSIX compliant systems.
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and
 *    Operating Systems. All rights reserved.
 *
 *    Redistribution and use in source and binary forms, with or without
 *    modification, are permitted provided that the following conditions
 *    are met:
 *
 *       1. Redistributions of source code must retain the above copyright
 *          notice, this list of conditions and the following disclaimer.
 *
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the
 *          distribution.
 *
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *    The views and conclusions contained in the software and documentation
 *    are those of the authors and should not be interpreted as representing
 *    official policies, either expressed or implied, of BTU Cottbus,
 *    Department for Distributed Systems and Operating Systems.
 *
 **/

#include "reflex/timer/SystemTimer.h"
#include "reflex/timer/VirtualizedTimer.h"

namespace reflex {
/* NOTE: This class may not live in any namespace besides 'reflex'! */

/** VirtualTimerSupport
  * Everything needed to provide signals for VirtualTimer instances
  * on POSIX compatible platforms. This class provides the timers with
  * millisecond precision.
  */
class VirtualTimerSupport
{
public:
    VirtualTimerSupport() :
        hwTimer(timer)
    {
		// The System provides a SystemTimer with the name 'timer'
        // We use this as our hardware timer abstraction.
        hwTimer.init(&vTimer);
        vTimer.setHardwareTimer(&hwTimer);

        // Assigns timer to a power group.
        // Not assigning timer to a group would disable it as
        // soon as the application enables a group. And because there
        // is no knoen application that does not use GROUP1 as "DEFAULT"
        // this is likely to work.
        // Because of such side-effects, the current power management
        // implementation is not well though-out and should be
        // simplified.
        timer.setGroups(powerManagement::GROUP1);
        // Enable timer in any case, whether groups are used or not.
        timer.switchOn();
	}

private:
    friend class VirtualTimer;
    SystemTimer timer;
	VirtualizedTimer vTimer; // the multiplexer for hardware timer
    HardwareTimer& hwTimer; // the hardware timer abstraction
};

} // ns reflex

#endif // REFLEX_POSIX_VIRTUALTIMERSUPPORT_H
