#ifndef OutputWrapper_h
#define OutputWrapper_h
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(ses):	InputWrapper
 *
 *	Author:		Karsten Walther, Reinhard Hemmerling
 *
 *	Description:	Allows actuator emulation, writes an output file
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include "reflex/System.h"
#include "reflex/types.h"
#include "reflex/sinks/Sink.h"

namespace reflex {

/**
 * The template class OutputWrapper implements the trigger interface.
 * Whenever a value is written to it, this value will be stored in
 * its associated data file with the current timecode.
 */
template <typename T>
class OutputWrapper :
public Sink1<T>
{

	int file;			//!< the file handle for the data file

protected:

	/** Helper function, writes a timecode to the data file.
	 *
	 *  @param time timestamp to write
	 *  @return number of written bytes
	 */
	int writeTime(Time& time)
	{
		return write(file, &time, sizeof(Time));
	}

	/** Helper funtion, writes a data value to the file.
	 *
	 *  @param data data to write
	 *  @return number of written bytes
	 */
	int writeData(T& data)
	{
		return write(file, &data, sizeof(T));
	}

public:

	/** Create an OutputWrapper that implements the trigger interface.
	 *  This will also create an output file. Data written to the
	 *  OutputWrapper will be put into the data file with the current
	 *  timecode.
	 *
	 *	@param outputFile name of the file in which should be written
	 */
	OutputWrapper(char* outputFile)
	{
		// create a new data file
		file = creat(outputFile, S_IREAD | S_IWRITE);
	}

	/** Close the data file and make sure it contains at least one
	 *  (timecode, data) pair.
	 */
	virtual ~OutputWrapper()
	{
		// make sure that the data file contains at least on
		// (timecode, data) pair
		enum { MAX_POSSIBLE_TIME = 1000000 };
		Time time = MAX_POSSIBLE_TIME;
		T t;
		writeTime(time);
		writeData(t);

		// close the data file
		if (file >= 0)
			close(file);
	}

	/** When a value is assigned to OutputWrapper, it will be written to the
	 *  data file with the current timecode.
	 *
	 *  @param data data to write
	 */
	virtual void assign(T data)
	{
		Time currentTime = getSystem().timer.getNow();
		writeTime(currentTime);
		writeData(data);
	}
};

} //namespace reflex

#endif
