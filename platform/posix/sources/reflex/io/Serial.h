#ifndef SERIAL_H
#define SERIAL_H
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(ses):	Serial
 *
 *	Author:	Jens Neumann
 *
 *	Description: Driver for serial interface.
 *
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */

#include "reflex/sinks/Sink.h"
#include "reflex/interrupts/InterruptFunctor.h"
#include "reflex/sinks/Fifo.h"
#include "reflex/scheduling/Activity.h"

#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>
#include <termios.h>
#include <pthread.h>


namespace reflex {
/* UNFINISHED - deprecated interface, comments missing */
/**
 *
 */
template <char BufferSize>
class Serial : public Activity
{
 private:
	void open_tty(char* tty_name);

 public:
	Serial(speed_t baud = B19200);

	void init(Sink1<char>* receiver, Sink0* sender, char* tty_name = "/dev/ttyS0");
	virtual void run();

	// all data waiting for transmission is stored in this FIFO
	Fifo2<char*,unsigned char,BufferSize> fifo;

	void read_loop(void* arg);
 private:
	// sender is notified, if last send request is finished
	Sink0* sender;


	unsigned char sendLen;
	char* sendPos;

	pthread_t read_thread;

	int tty; // fd for the used tty
	// when the SCI receives data, it is written to output
	Sink1<char>* receiver;


};
}// end namespace

#include "reflex/io/Serial.cc"

#endif
