/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *

 *	Author:		Karsten Walther
 */

#include "reflex/io/Console.h"

#include <iostream>

using namespace reflex;

Console::Console()
{
	out_txFinished = 0;
}

void Console::assign(Buffer* buffer)
{
	/* NOTE: ensure initialization of std::cout etc. before use.
	 * This is a workaround for some GCC implementations that would
	 * otherwise cause a segmentation fault (SIGSEGV).
	 * See here: http://gcc.gnu.org/ml/gcc-bugs/2006-02/msg00476.html
	 * for a description of the problem.
	 */
	static std::ios_base::Init initIoStreams;

	char* pos = (char*) buffer->getStart();
	unsigned size = buffer->getLength();
	while (size--)
	{
		std::cout << (char) *pos++;
	}
	std::cout << std::flush;
	buffer->downRef();

	if (out_txFinished)
	{
		out_txFinished->notify();
	}
}
