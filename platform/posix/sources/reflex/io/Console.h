#ifndef Console_h
#define Console_h
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(ses):	Console
 *
 *	Author:		Karsten Walther
 *
 *	Description:	IO Abstraction which allows to use the Console like
 *                  a serial interface as output.
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */
#include "reflex/sinks/Sink.h"
#include "reflex/memory/Buffer.h"
#include <iostream>

namespace reflex {

/** Writes assigned buffers to stdout.
 */
class Console : public Sink1<Buffer*> {
public:
	Console();

	inline Sink1<Buffer*>* in_tx() { return this; }
	inline void set_out_txFinished(Sink0* input) { out_txFinished = input; }

	/** Writes the data bytewise and raw to the stdout interface.
	 *
	 *  @param buffer the data to write
	 */
	virtual void assign(Buffer* buffer);

private:
	Sink0* out_txFinished;
};

inline Console::Console()
{
	out_txFinished = 0;
}

inline void Console::assign(Buffer* buffer)
{
	/* NOTE: ensure initialization of std::cout etc. before use.
	 * This is a workaround for some GCC implementations that would
	 * otherwise cause a segmentation fault (SIGSEGV).
	 * See here: http://gcc.gnu.org/ml/gcc-bugs/2006-02/msg00476.html
	 * for a description of the problem.
	 */
	static std::ios_base::Init initIoStreams;

	char* pos = (char*) buffer->getStart();
	unsigned size = buffer->getLength();
	while (size--)
	{
		std::cout << (char) *pos++;
	}
	std::cout << std::flush;
	buffer->downRef();

	if (out_txFinished)
	{
		out_txFinished->notify();
	}
}

} //namespace reflex

#endif
