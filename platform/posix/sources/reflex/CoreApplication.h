#ifndef COREAPPLICATION_H
#define COREAPPLICATION_H

#include <reflex/data_types/Static.h>
#include <reflex/interrupts/InterruptGuardian.h>
#include <reflex/powerManagement/PowerManager.h>
#include <reflex/scheduling/Scheduler.h>

namespace reflex {

namespace posix {
/*!
\brief Basic Application Class for the Posix Platform.

CoreApplication provides the necessary infrastructure for a minimal REFLEX
application such as:

- Interrupt Guardian
- Power Manager
- Scheduler

They can be retrieved through the accessor methods guardian(), powerManager()
and scheduler().

CoreApplication is implemented as singleton object. For the whole life-time of
an application, only one instance exists and can be retrieved using the
static instance() method. Therefore, the copy constructor and assignment
operator are disabled.

Two possible use-cases exist:

1. For statically linked applications
2. For applications that are located on stack

<h2>Static Usage</h2>
The whole application can be linked statically for the benefit of link-time
analysis, because all addresses can be determined during link-time. In order
for this to work, it has to be ensured, that the core application is
initialized before the application components. The following code snippet show,
how this can be achieved.

\code
// -------- File ReflexApplication.h --------

// Application class for static usage
// Correct initialization order is guaranteed through inheritance
class ReflexApplication : public reflex::mcu::CoreApplication
{
    // Here goes the whole component structure
};

// -------- File main.cc --------

// This is the global application object
ReflexApplication app;

int main()
{
    reflex::mcu::CoreApplication::exec();
    return 0;
}
\endcode


## Usage on Stack
The application can be created on stack as well. This might result in easier
debugging and makes it possible, to terminate the whole application without
performing a system reset. This is necessary for system updates as well.
The following code snippet shows, how to achieve this.

\code
// -------- File ReflexApplication.h --------

// Application class for stack usage
class ReflexApplication
{
    // Here goes the whole component structure
};

// -------- File main.cc --------

#include <reflex/CoreApplication.h>

int main()
{
    reflex::mcu::CoreApplication core;
    ReflexApplication app;
    reflex::mcu::CoreApplication::exec();
    return 0;
}
\endcode

 */
class CoreApplication
{
public:
    CoreApplication();
    ~CoreApplication();

    InterruptGuardian* guardian();
    PowerManager* powerManager();
    Scheduler* scheduler();

    static void exec();
    static CoreApplication* instance();

private:
    CoreApplication(const CoreApplication&);
    CoreApplication& operator=(const CoreApplication&);

    PowerManager _powerManager;
    InterruptHandler* _handlerTable[interrupts::MAX_HANDLERS];
    InterruptGuardian _guardian;
    Scheduler _scheduler;

    static CoreApplication* _instance;
    static bool _isInitialized;
};

inline CoreApplication::~CoreApplication()
{
    Assert(_isInitialized == true);
    _instance = 0;
    _isInitialized = false;
}

/*!
\brief Accessor method to retrieve the interrupt guardian object.
 */
inline InterruptGuardian* CoreApplication::guardian() { return &_guardian; }

/*!
\brief Accessor method to retrieve the power manager object.
 */
inline PowerManager* CoreApplication::powerManager() { return &_powerManager; }

/*!
\brief Accessor method to retrieve the scheduler object.

The exact type of the schuleder depends on the scheduling scheme which
the REFLEX library is being compiled for.

 */
inline Scheduler* CoreApplication::scheduler() { return &_scheduler; }

/*!
\brief Starts event-handling via scheduler.

This method does not return.

 */
inline void CoreApplication::exec()
{
    _instance->_scheduler.start();
}

/*!
\brief Returns the single instance of CoreApplication.

It has to be ensured, that an instance of CoreApplication has been created
before this function is called.

 */
inline CoreApplication* CoreApplication::instance()
{
    return _instance;
}

}

}

#endif // COREAPPLICATION_H
