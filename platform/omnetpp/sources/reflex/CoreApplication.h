#ifndef COREAPPLICATION_H
#define COREAPPLICATION_H

#include <csimplemodule.h>

#include <reflex/data_types/Static.h>
#include <reflex/interrupts/InterruptGuardian.h>
#include <reflex/powerManagement/PowerManager.h>
#include <reflex/scheduling/Scheduler.h>
#include "reflex/scheduling/SchedulerWrapper.h"

class cMessage;

namespace reflex {

namespace omnetpp {
/*!
\brief Basic Application Class for the OMNeT++ Platform.

CoreApplication provides the necessary infrastructure for a minimal REFLEX
application such as:

- Interrupt Guardian
- Power Manager
- Scheduler

They can be accessed through the methods guardian(), powerManager() and
scheduler().

Unlike on other platforms, multiple instances of CoreApplication exist in the
same memory: one for each node, implemented as OMNeT++ cSimpleModule. Therefore
it is possible to have different applications in the same simulation.

Usage example:

\rst
.. code-block:: cpp

    class MyApplication1 : public reflex::omnetpp::CoreApplication
    {
      // Components and other stuff...
    };

    class MyApplication2 : public reflex::omnetpp::CoreApplication
    {
      // Components and other stuff...
    };

    // Do not forget to make the above classes accessible to OMNeT++
    // in a source file.
    Define_Module(MyApplication1);
    Define_Module(MyApplication2);

\endrst

A OMNeT++ simulation runs in different stages:

- build
- initialization
- event
- finish

During \em build stage, the application constructor is called. A custom
\em initialization procedure can be defined be reimplementing the
initialize() method. When the simulation is running in the
\a event stage, handleMessage() serves as entry
point to the REFLEX system. Messages are injected as interrupts and the
scheduler is invoked afterwards to dispatch each pending activity. It returns
once the scheduler's activity queue is empty. The \em finish stage would call
finish() which also can be reimplemented in a derived
class.

Note, that the OMNeT++ platform contains a modified scheduler
reflex::omnetpp::SchedulerWrapper, which supports only fifo scheduling.

*/
class CoreApplication : public cSimpleModule
{
    friend class ReflexBaseApp;
public:
    CoreApplication();
    ~CoreApplication();

    InterruptGuardian* guardian();
    PowerManager* powerManager();
    SchedulerWrapper<Scheduler>* scheduler();

    static uint32 currentId();
    static cMessage* currentMessage();
    static void exec();
    static CoreApplication* instance();

protected:
    virtual void initialize();
    virtual void handleMessage(cMessage *msg);
    virtual void finish();
    virtual void deleteModule();

private:
    CoreApplication(const CoreApplication&);
    CoreApplication& operator=(const CoreApplication&);

    uint32 _id;
    PowerManager _powerManager;
    InterruptHandler* _handlerTable[interrupts::MAX_HANDLERS];
    InterruptGuardian _guardian;
    SchedulerWrapper<Scheduler> _scheduler;

    static uint32 _count;
    static cMessage* _currentMessage;
    static CoreApplication* _instance;
};



/*!
\brief Accessor method to retrieve the interrupt guardian object.
 */
inline InterruptGuardian* CoreApplication::guardian() { return &_guardian; }

/*!
\brief Accessor method to retrieve the power manager object.
 */
inline PowerManager* CoreApplication::powerManager() { return &_powerManager; }

/*!
\brief Accessor method to retrieve the scheduler object.

The exact type of the schuleder depends on the scheduling scheme which
the REFLEX library is being compiled for.

 */
inline SchedulerWrapper<Scheduler>* CoreApplication::scheduler() { return &_scheduler; }


/*!
\brief Returns the current message injected by the simulation environment.

As messages are injected as interrupts, this method can be used from within
an interrupt handler to retrieve the current message.
 */
inline cMessage* CoreApplication::currentMessage()
{
    Assert(_currentMessage);
    return _currentMessage;
}

}

}

#endif // COREAPPLICATION_H
