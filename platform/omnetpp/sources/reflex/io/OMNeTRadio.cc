/*
 * OMNeTRadio.cpp
 *
 *  Created on: 03.08.2012
 *      Author: slohs
 */

#include "ReflexPacket_m.h"
#include "reflex/io/OMNeTRadio.h"
#include "reflex/System.h"
#include <base/utils/NetwToMacControlInfo.h>
#include <base/phyLayer/MacToPhyControlInfo.h>
#include <base/utils/NetwControlInfo.h>

//#define WITH_DEBUG_MSG
//#define DEBUG_PACKETS

OMNeTRadio::OMNeTRadio(bool withDebug)
	: withDebug(withDebug)
    , PowerManageAble(reflex::PowerManageAble::PRIMARY)
	, pathLogger( NULL )
{
    this->setSleepMode(reflex::mcu::ACTIVE);
    state = TRANSCEIVE;
}

void OMNeTRadio::initialize()
{
#ifdef WITH_PATH_LOGGER
    pathLogger = AbstractBufferPathLogger::instance();
#endif
}

void OMNeTRadio::handleUpperLayerIn_Impl()
{
	reflex::Buffer* buffer = upperLayerIn.get();

#ifdef WITH_DEBUG_MSG
    std::cerr << __PRETTY_FUNCTION__ << ": send Message." << std::endl;
#endif

	if ( pathLogger ) pathLogger->logMessage( __PRETTY_FUNCTION__, buffer, 0xFE ) ;

    if ( state == INACTIVE)
    {
		if ( pathLogger ) pathLogger->unregisterBuffer( buffer, 0 );
        buffer->downRef();

#ifdef WITH_DEBUG_MSG
    std::cerr << "\t" << __PRETTY_FUNCTION__ << ": cannot send Message, radio is off." << std::endl;
#endif

        return;
    }

	if ( pathLogger ) pathLogger->sendBuffer( buffer );

    ReflexPacket* reflexPkg = new ReflexPacket("ReflexPacket", 0);

    buffer->push(buffer->getFreeStackSpace() );

    unsigned length = buffer->getLength();
    uint8* data = buffer->getStart();

    reflexPkg->setPayLoadArraySize( length );

    // Setting up destination for this Message
    // standard broadcast destination
    NetwControlInfo* cInfo = new NetwControlInfo(LAddress::L3BROADCAST);
//  NetwToMacControlInfo* cInfo = new NetwToMacControlInfo(LAddress::L2BROADCAST);
//	MacToPhyControlInfo* cInfo = new MacToPhyControlInfo();
	reflexPkg->setControlInfo(cInfo);

    for ( unsigned i=0; i<length;++i )
    {
        reflexPkg->setPayLoad( i, *(data++) );
#ifdef DEBUG_PACKETS
        std::cerr << " " << (int) *(data - 1);
#endif
	}
#ifdef DEBUG_PACKETS
    std::cerr << std::endl;
#endif

	buffer->downRef();

	omnetGate.send(reflexPkg);
}

void OMNeTRadio::handleLowerLayerIn_Impl()
{
	 //we assume some cPkg here
    ReflexPacket* reflexPkg = check_and_cast<ReflexPacket*>( ReflexBaseApp::currentMessage() );

#ifdef WITH_DEBUG_MSG
    std::cerr << __PRETTY_FUNCTION__ << ": receive Message." << std::endl;
#endif

    if ( state < RECEIVE)
    {
        omnetGate.module.cancelAndDelete( reflexPkg );

#ifdef WITH_DEBUG_MSG
        std::cerr << "\t" << __PRETTY_FUNCTION__ << ": Receiver is OFF" << std::endl;
#endif

        return;
    }

    if (upperLayerOut)
    {
        reflex::Buffer* recBuffer = new (pool) reflex::Buffer(pool);

        if (!recBuffer)
        {
			omnetGate.module.cancelAndDelete(reflexPkg);
			return;
		}

        recBuffer->initOffsets(reflexPkg->getPayLoad( 0 ));

        for (unsigned i = 1; i < reflexPkg->getPayLoadArraySize(); ++i)
        {
			recBuffer->write(reflexPkg->getPayLoad(i));
		}

		if ( pathLogger ) pathLogger->receiveBuffer( recBuffer );

        upperLayerOut->assign(recBuffer);
	}

	omnetGate.module.cancelAndDelete(reflexPkg);

}

void OMNeTRadio::handleLowerControlIn_Impl()
{
	cPacket* controlPacket = check_and_cast<cPacket*>(ReflexBaseApp::currentMessage());

	controlGate.module.cancelAndDelete(controlPacket);
}


void OMNeTRadio::enable()
{
#ifdef WITH_DEBUG_MSG
    std::cerr << __PRETTY_FUNCTION__ << std::endl;
#endif

    state = TRANSCEIVE;
}

void OMNeTRadio::disable()
{
#ifdef WITH_DEBUG_MSG
    std::cerr << __PRETTY_FUNCTION__ << std::endl;
#endif

    state = INACTIVE;
}

