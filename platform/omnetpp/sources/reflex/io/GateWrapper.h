


#include "reflex/io/InGateWrapper.h"
#include "reflex/io/OutGateWrapper.h"

namespace reflex {


template<mcu::interrupts::InterruptVector TGateIndIn, mcu::interrupts::InterruptVector TGateIndOut = TGateIndIn>
class GateWrapper
	: public InGateWrapper<TGateIndIn>
	, public OutGateWrapper<TGateIndOut>
{
public:
	GateWrapper() {}

	void lock()
	{
		OutGateWrapper<TGateIndOut>::lock();
	}

	void unlock()
	{
		OutGateWrapper<TGateIndOut>::unlock();
	}
};


}
