/*
 * OMNeTRadioStructure.h
 *
 *  Created on: 03.08.2012
 *      Author: slohs
 */

#ifndef OMNETRADIOSTRUCTURE_H_
#define OMNETRADIOSTRUCTURE_H_

#include "reflex/types.h"
#include "reflex/memory/Buffer.h"
#include "reflex/memory/Pool.h"
//#include "reflex/timer/VirtualTimer.h"
#include "reflex/sinks/Event.h"
#include "reflex/sinks/Queue.h"
#include "reflex/scheduling/ActivityFunctor.h"

#include "reflex/interrupts/InterruptVector.h"

#include "OMNeTGateWrapper.h"

#include <omnetpp.h>
#include "BaseConnectionManager.h"

template <typename T>
class OMNeTRadio_Structure : public cSimpleModule
{
private:

public:
	reflex::Pool* pool;

	reflex::Queue<reflex::Buffer*> upperLayerIn;
	reflex::Sink1<reflex::Buffer*>* upperLayerOut;

    /*
        cModule* nic;
        BaseConnectionManager* connectionManager;
        Coord pos;
        ConnectionManagerAccess* chAccess;
    */
public:
	OMNeTRadio_Structure()	:
		handleUpperLayerInFunctor(*this)
		, omnetGate(*this)
		, controlGate(*this)
	{
		upperLayerIn.init(&handleUpperLayerInFunctor);
	}

	reflex::Sink1<reflex::Buffer*>* get_in_upperLayer()
	{
		return &upperLayerIn;
	}

	void connect_out_upperLayer(reflex::Sink1<reflex::Buffer*>* upperLayerOut)
	{
		this->upperLayerOut = upperLayerOut;
	}

	void connect_pool(reflex::Pool* pool)
	{
		this->pool = pool;
	}
private:
	void handleUpperLayerIn_Stub()
	{
		static_cast<T*>(this)->handleUpperLayerIn_Impl();
	}

	void handleLowerLayerIn_Stub()
	{
		static_cast<T*>(this)->handleLowerLayerIn_Impl();
	}

	void handleLowerControlIn_Stub()
	{
		static_cast<T*>(this)->handleLowerControlIn_Impl();
	}

	reflex::ActivityFunctor<OMNeTRadio_Structure, &OMNeTRadio_Structure::handleUpperLayerIn_Stub> handleUpperLayerInFunctor;
public:
	OMNeTGateWrapper<OMNeTRadio_Structure, &OMNeTRadio_Structure::handleLowerLayerIn_Stub
			, reflex::mcu::interrupts::IV_GATE0, reflex::mcu::interrupts::IV_GATE0> omnetGate;

	OMNeTGateWrapper<OMNeTRadio_Structure, &OMNeTRadio_Structure::handleLowerControlIn_Stub
			, reflex::mcu::interrupts::IV_GATE1, reflex::mcu::interrupts::IV_GATE1> controlGate;
};

#endif /* OMNETRADIOSTRUCTURE_H_ */
