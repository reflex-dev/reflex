/*
 * EvaluationMaster.h
 *
 *  Created on: 01.07.2014
 *      Author: slohs
 */

#ifndef ABSTRACTBUFFERPATH_H_
#define ABSTRACTBUFFERPATH_H_

#include "reflex/memory/Buffer.h"
#include <iostream>
using namespace std;

class AbstractBufferPathLogger
{
    static AbstractBufferPathLogger* _instance;
public:
    AbstractBufferPathLogger();
    inline static AbstractBufferPathLogger* instance() {return _instance;}

    virtual void logMessage( std::string currFunction, reflex::Buffer* buffer, int nextHopDestination ) {}

/*

    void registerBuffer( reflex::Buffer* buffer, addr_t nodeId, uint8 type );
    void reassignBuffer( reflex::Buffer* oldBuffer, reflex::Buffer* newBuffer);
    void reassignBuffer( reflex::Buffer* oldBuffer, reflex::Buffer* newBuffer, uint8 type);
   */
    /**
     * @brief unregisterBuffer unregisters a buffer if the logging is finished
     * @param buffer the adress of the buffer
     * @param type the type of buffer, if type is INVALID_TYPE the complete buffer_entry is removed.
     */
    void unregisterBuffer( reflex::Buffer* buffer, uint8 type ) {}
    virtual void sendBuffer( reflex::Buffer* sendBuffer ) {}
    virtual void receiveBuffer( reflex::Buffer* receivedBuffer ){}

    //virtual void handleMessage(cMessage *msg);
private:

    /*
    BufferListElem* searchElem( reflex::Buffer* buffer );
    void addElem( BufferListElem* newElement );
    void removeElem( BufferListElem* removeElement );


    void clearEntries( BufferListElem* element );
    BufferIDElem* removeEntry( BufferListElem* element, uint8 entryType );
    BufferIDElem* copyEntry( BufferIDElem* toCopy );
*/
};


#endif /* EVALUATIONMASTER_H_ */
