/*
 * OMNeTGateWrapper.h
 *
 *  Created on: 03.08.2012
 *      Author: slohs
 */

#ifndef OMNETGATEWRAPPER_H_
#define OMNETGATEWRAPPER_H_

#include "reflex/interrupts/InterruptHandler.h"
#include "reflex/interrupts/InterruptVector.h"
#include "omnetpp/ReflexBaseApp.h"

#include <cgate.h>
#include <cmessage.h>
#include <csimplemodule.h>

// Index of Gate, which needs to be wrapped = Gate Index is the same as the interrupt vector!
template<typename T, void (T::* MemFn)(), reflex::mcu::interrupts::InterruptVector TGateIndexIn, reflex::mcu::interrupts::InterruptVector TGateIndexOut>
class OMNeTGateWrapper :
	protected reflex::InterruptHandler
{
public:
	cSimpleModule& module;
	T& handleObject;
public:
	OMNeTGateWrapper(T& handleObject) :
		reflex::InterruptHandler(TGateIndexIn ,PowerManageAble::SECONDARY)
		, module(*ReflexBaseApp::currentModule())
		, handleObject(handleObject)
	{}

	void setHandlerObject(T* pointerToHandler)
	{
		this->pointerToHandler = pointerToHandler;
	}

	void send(cPacket* packet)
	{
		cGate* gate = module.gate("lowerGateOut",TGateIndexOut);
		Assert(gate);

		module.send(packet, gate);
	}

	virtual void handle()
	{
		(handleObject.*MemFn)();
	}

	virtual void enable(){}
	virtual void disable(){}
};

#endif /* OMNETGATEWRAPPER_H_ */
