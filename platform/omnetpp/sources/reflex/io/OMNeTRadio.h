/*
 * OMNeTRadio.h
 *
 *  Created on: 03.08.2012
 *      Author: slohs
 */

#ifndef OMNETRADIO_H_
#define OMNETRADIO_H_

#include "OMNeTRadio_Structure.h"
#include "AbstractBufferPathLogger.h"
#include "reflex/powerManagement/PowerManageAble.h"

class OMNeTRadio : public OMNeTRadio_Structure<OMNeTRadio>
        , public reflex::PowerManageAble
{
private:

    enum RadioStates {
        INACTIVE = 0
        , RECEIVE
        , TRANSCEIVE
    };

    RadioStates state;

    bool withDebug;

    AbstractBufferPathLogger* pathLogger;

public:
	OMNeTRadio(bool withDebug = false);

    void initialize();

	void handleUpperLayerIn_Impl();

	void handleLowerLayerIn_Impl();

	void handleLowerControlIn_Impl();

/* Functions of PowerManageables */
    void enable();
    void disable();
};

#endif /* OMNETRADIO_H_ */
