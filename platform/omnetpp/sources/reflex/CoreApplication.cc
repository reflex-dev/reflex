#include <reflex/CoreApplication.h>

#include <cmessage.h>

extern "C" void _interruptsEnable();
extern "C" void _wait();
extern "C" void _yield();

using namespace reflex;
using namespace omnetpp;

namespace {
    PowerDownFunction powerFunctions[mcu::NrOfPowerModes] = {_active, _wait, _yield};
}

cMessage* CoreApplication::_currentMessage = NULL;

CoreApplication* CoreApplication::_instance;
uint32 CoreApplication::_count = 0;

Define_Module(CoreApplication);

/*!
\brief Creates a CoreApplication object.

CoreApplication is a singleton. Calling this method twice will fail or may
result in undefined behaviour.
 */
CoreApplication::CoreApplication() : _id(_count),
    _powerManager(powerFunctions), _guardian(_handlerTable, interrupts::MAX_HANDLERS)
{
    _instance = this;
    _count++;
}

CoreApplication::~CoreApplication()
{
    _instance = 0;
}

/*!
\brief Simulation hook during the \em init stage.

When reimplementing this method, make sure to call this method before
taking any action.
*/
void CoreApplication::initialize()
{
    _instance = this;
    cSimpleModule::initialize();
}

/*!
\brief Injects a OMNeT++ message as interrupt into the REFLEX core.

This method is called by the simulation environment during the \em event stage.
it is not supposed to be reimplemented.
 */
void CoreApplication::handleMessage(cMessage *msg)
{
	_currentMessage = msg;
	reflex::mcu::interrupts::InterruptVector vectorNumber = reflex::mcu::interrupts::INVALID_INTERRUPT;

	if (msg->isSelfMessage())
	{
		vectorNumber = reflex::mcu::interrupts::InterruptVector(msg->getKind());
	}
	else
	{
		vectorNumber = reflex::mcu::interrupts::InterruptVector(msg->getArrivalGate()->getIndex());
	}

	/**
	 * the number of the interruptvector is the  vectornumber of the interrupt. check here if in bounds
	 */
	Assert((vectorNumber >= reflex::mcu::interrupts::INVALID_INTERRUPT) &&  (vectorNumber < reflex::mcu::interrupts::MAX_HANDLERS));
    ::handle(vectorNumber);
	_scheduler.exec();
}

/*!
\brief Simulation hook during the \em finish stage.

This method is called after end of simulation, if it terminated without error.
When reimplementing this method, make sure to call this method before taking
any action.
*/
void CoreApplication::finish()
{
    _instance = this;
    cSimpleModule::finish();
}

void CoreApplication::deleteModule()
{
    _instance = this;
    cSimpleModule::deleteModule();
}

/*!
\brief Returns the current application instance id.

The id starts at 0 and counts up to n-1 whereas n is the total amount of
application nodes.
 */
uint32 CoreApplication::currentId()
{
    return instance()->_id;
}

/*!
\brief Returns the current CoreApplication instance.

The current application instance handles the current message in the
simulation environment.
*/
CoreApplication* CoreApplication::instance()
{
    switch(cSimulation::getActiveSimulation()->getSimulationStage())
    {
    case CTX_BUILD:
    case CTX_FINISH:
    case CTX_CLEANUP:
        return _instance;
    default:
        return static_cast<CoreApplication*>(
            cSimulation::getActiveSimulation()->getContextModule());
    }
}


