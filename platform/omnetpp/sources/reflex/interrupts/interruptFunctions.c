/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 */

#ifndef __GNUC__
#error "currently only gcc is supported here"
#endif

#ifdef DARWIN
#include <libkern/OSAtomic.h>
#endif

//#ifdef OMNETPP
//#define TEST_AND_SET_FLAG activeNode->iflag
//#else
unsigned iflag = 0;
#define TEST_AND_SET_FLAG iflag
//#endif

unsigned _isLocked ()
{
	return TEST_AND_SET_FLAG;
}

unsigned _interruptsDisable (void)
{
#ifdef DARWIN
	// set flag, return previous value of bit
	return OSAtomicTestAndSet(0x1F,&TEST_AND_SET_FLAG);

#elif  defined(LINUX) || defined (CYGWIN)

	#if __GNUC__ == 3
		register unsigned val asm ("eax") = 1;
		asm volatile ("xchg %0, %2": "=r" (val) :"0" (val),"m" (TEST_AND_SET_FLAG) );
		return val;
	#else
		return (unsigned)__sync_lock_test_and_set (&TEST_AND_SET_FLAG, (unsigned)1);
	#endif
#else
#error "either DARWIN, LINUX or CYGWIN has to be defined, that means your environment is not supported"
#endif
}

void _interruptsEnable (void)
{
#ifdef DARWIN
	// clear flag, ignore return value (previous value of bit)
	OSAtomicTestAndClear(0x1F,&TEST_AND_SET_FLAG);

#elif defined(LINUX) || defined (CYGWIN)
	#if __GNUC__ == 3
		register unsigned val asm ("eax") = 0;
		asm volatile ("xchg %0, %2": "=r" (val) :"0" (val),"m" (TEST_AND_SET_FLAG) );
	#else
		__sync_lock_release(&TEST_AND_SET_FLAG);
	#endif
#else
#error "either DARWIN, LINUX or CYGWIN has to be defined, that means your environment is not supported"
#endif
}
