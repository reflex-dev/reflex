/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(ses):	Assert
 *
 *	Author:		Soeren Hoeckner
 *
 *	Desciption:	Assert function
 *
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */

#ifndef _ASSERT_FAIL_H
#define _ASSERT_FAIL_H

#include <iostream>
#include <csimplemodule.h>

//#include "misc/stacktrace.h"

/** takes care of Assert output.
 *	it will never return
 */
inline void Assert_fail (const char* exp,const char* file,int line,const char* func)
{
	//print_trace();

	std::cerr <<": "<< "Assertion ("<<exp<<") in "<<file<<":"<<line<<" at "<<func<<" \n";

	abort();
	//throw cTerminationException("Assertion (%s) in %s : %i at %s",exp,file,line,func);
}

/**	macro Assert(exp) similar to assert(exp).
 *	Maybe it will be implemented otherwise on other Machines  e.g without __FILE__ ... macros
 **/
#ifdef Assert
#undef Assert
#endif

#define Assert(expr)				\
  ((expr)							\
   ? (void) (0)						\
   : (Assert_fail (__STRING(expr), __FILE__, __LINE__, __PRETTY_FUNCTION__)));

#ifdef ASSERT
#undef ASSERT
#endif

#define ASSERT(expr) Assert(expr)
#endif /* _ASSERT_FAIL_H */

