/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *

 *	Author:		 Soeren Hoeckner
 */

#include "omnetpp/ReflexBaseApp.h"
#include "reflex/types.h"
#include "reflex/timer/SystemTimer.h"

#include <cmessage.h>

//#include "conf.h"
//#include "reflex/emulator/NetworkNode.h"


#include "reflex/debug/OmnetDebug.h"

using namespace reflex;


SystemTimer::SystemTimer()
    : InterruptHandler(mcu::interrupts::IV_TIMER,PowerManageAble::SECONDARY)
    , output(0)
    , tickSpeed(0)
    , reflexTimeScale ( 1000 )
    , simInterval(0)
    , active(false)
{
    OmnetDebug().prefix()<< " SystemTimer()\n";
    this->setSleepMode(mcu::LINUXIDLE);

    timeoutEvent = new cMessage("timeout"); //OmnetMsg
    timeoutEvent->setKind(mcu::interrupts::IV_TIMER); // for delivering to intvec

}

SystemTimer::~SystemTimer()
{
    ReflexBaseApp::currentModule()->cancelAndDelete( timeoutEvent );
}

void SystemTimer::init(Sink0 *output)
{
    this->output = output;
}

void SystemTimer::setTickSpeed(Time ms)
{
    OmnetDebug().prefix()<< " SystemTimer::setTickSpeed("<< (void*)this<<")\n";
    tickSpeed = ms;
    simInterval = ((double)ms)/reflexTimeScale;
}


void SystemTimer::reStart(Time ms)
{
    OmnetDebug().prefix() <<".Systemtimer restart() \n";
    setTickSpeed(ms);
    this->start();
}

void SystemTimer::start()
{
    //opp_dbg.prefix() <<".Systemtimer start():"<< (void*)this<<"\n";
    ReflexBaseApp::currentModule()->cancelEvent( timeoutEvent );
    ReflexBaseApp::currentModule()->scheduleAt(simTime()+simInterval,timeoutEvent);
    active = true;
}

const Time SystemTimer::getTime() const {
    Time time = simTime().dbl()*reflexTimeScale;
    return time;
}

const uint64 SystemTimer::getTimestamp() const {
    uint64 time = simTime().dbl() * 1000000; // usec integer
    return time;
}

void SystemTimer::stop()
{
    OmnetDebug().prefix() <<".Systemtimer stop():"<< (void*)this<<"\n";
    ReflexBaseApp::currentModule()->cancelEvent( timeoutEvent );
    active = false;
}

void SystemTimer::enable()
{
    OmnetDebug().prefix() <<".Systemtimer enable():"<< (void*)this<<"\n";
    this->start();
}

void SystemTimer::disable()
{
    OmnetDebug().prefix() <<".Systemtimer disable():"<< (void*)this<<"\n";
    this->stop();
}

void SystemTimer::handle()
{
    if (!active)
    {
        return;
    }
    //	opp_dbg.prefix() <<".Systemtimer handle event \n";
    this->stop();

    if (output)
    {
            output->notify();
    }
}




