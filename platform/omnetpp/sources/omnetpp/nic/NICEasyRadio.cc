/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
  
 *	Author:		 Soeren Hoeckner
 */

#include "omnetpp/nic/NICEasyRadio.h"
#include "Ieee80211Consts.h"  //XXX for the COLLISION and BITERROR msg kind constants
#include "reflex/interrupts/InterruptVector.h" // for enum INTVEC_SERIAL_TX_END
#include <cdispstr.h> //change nthe displaystring
#include "reflex/MachineDefinitions.h"

#define MK_TRANSMISSION_OVER  1 //defined in AbstractRadio.cc

Define_Module(NICEasyRadio);




//void NICEasyRadio::handleUpperMsg (AirFrame *airframe)
//{
	//EV<<"NichEasyRadio handleUpperMSG\n";
	//if (isPerfect)
	//{
		////notify transfer end
		//cMessage *msg_tx_end = new cMessage("ser_tx_end",reflex::INTVEC_SERIAL_TX_END);
		//send(msg_tx_end, uppergateOut);
	//}
	//AbstractRadio::handleUpperMsg(airframe);
	
//}

void NICEasyRadio::handleSelfMsg (cMessage *msg)
{
	int msgKind = msg->kind();
	AbstractRadio::handleSelfMsg (msg);
	if (msgKind == MK_TRANSMISSION_OVER)
	{
		cMessage *msg_tx_end = new cMessage("ser_tx_end",reflex::mcu::interrupts::INTVEC_SERIAL_TX_END);
		send(msg_tx_end, uppergateOut);
	}
}

void NICEasyRadio::sendUp(AirFrame *msg)
{
	cMessage *frame = msg->decapsulate();
	delete msg;
	
	if (!(frame->kind() == COLLISION || frame->kind() == BITERROR) )
	{
		EV << "sending up frame " << frame->name() << endl;
		send(frame, this->uppergateOut);
	}else
	{
		delete (frame);
	}
}
