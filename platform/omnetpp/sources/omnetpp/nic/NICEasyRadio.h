/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(ses):	NicEasyRadio
 *
 *	Author:		Soeren Hoeckner
 *	Desciption:	implementation for the builtin omnet simulation
 *
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */
#ifndef NICEASYRADIO_H
#define NICEASYRADIO_H


#include "AbstractRadio.h"
#include "coutvect.h"
#include "IScriptable.h"


/**
 * Generic radio module, employing PathLossReceptionModel and GenericRadioModel.
 */
class INET_API NICEasyRadio : public AbstractRadio
{
public:
	NICEasyRadio(): AbstractRadio()
	{}
	virtual ~NICEasyRadio()
	{}

protected:

	/**	wrap due to able to enable/disable the radio.
	 *
	 *	@param airframe	message from the air
	 */
	//virtual void handleUpperMsg (AirFrame *airframe);

	/**	propagetes transmission-end msg(INTVEC_SERIAL_TX_END) as simulated
	 *	interrupt for reflex::Serial. acts on MK_TRANSMISSION_OVER == msg.kind()
	 *
	 *	@param msg	selfmessage
	 */
	virtual void handleSelfMsg (cMessage *msg);

	virtual void sendUp(AirFrame *msg);


	virtual IReceptionModel *createReceptionModel()
	{
		return (IReceptionModel *)createOne("PathLossReceptionModel");
	}

	virtual IRadioModel *createRadioModel()
	{
		return (IRadioModel *)createOne("GenericRadioModel");
	}
};

#endif

