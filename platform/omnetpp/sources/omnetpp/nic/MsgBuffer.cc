
#include <iostream>
#include "reflex/interrupts/InterruptVector.h"
#include "omnetpp/nic/MsgBuffer.h"


Define_Module(MsgBuffer);


/** @brief (one liner)
  *
  * (documentation goes here)
  */
void MsgBuffer::initialize()
{
//	std::cerr<<"initialize";
	uppergateIn = findGate("uppergateIn");
	lowergateIn = findGate("lowergateIn");
	isfree = true;
 	queue.setName("queue");
	WATCH(isfree);
	WATCH (queue);
}


/** @brief (one liner)
  *
  * (documentation goes here)
  */
void MsgBuffer::handleSelfMsg(cMessage *msg)
{

}

/** @brief (one liner)
  *
  * (documentation goes here)
  */
void MsgBuffer::handleUpperMsg(cMessage *msg)
{
//	std::cerr<<"handleUpperMsg\n";
	if (isfree)
	{
//		std::cerr<<"send lowergateOut\n";
		isfree=false;
		send(msg,"lowergateOut");
	}else
	{
		std::cerr<<"enque Message\n";
		queue.insert(msg);
	}
}

/** @brief (one liner)
  *
  * (documentation goes here)
  */
void MsgBuffer::handleLowerMsg(cMessage *msg)
{
//	std::cerr<<"handleLowerMsg\n";
	if (msg->kind()== reflex::INTVEC_SERIAL_TX_END )
	{
//		std::cerr<<"handleSerialMsg\n";

		if (!queue.empty())
		{
			cMessage *tmpmsg = check_and_cast<cMessage *>(queue.pop());
			if (tmpmsg)
			{
//				std::cerr<<"send lowergateOut\n";
				isfree=false;
				send(tmpmsg,"lowergateOut");
			}else
			{
				std::cerr<<"MSGBUFFER:handlelwerMsg tmpmsg == 0 this shouldnt happen\n";
			}
		}else
		{
			//queue is empty
			isfree=true;
		}
		cancelAndDelete(msg);
	}else
	{
//		std::cerr<<"send uppergateOut\n";
		send(msg,"uppergateOut");
	}

}

/** @brief (one liner)
  *
  * (documentation goes here)
  */
void MsgBuffer::handleMessage(cMessage *msg)
{
    if (msg->arrivalGateId()==uppergateIn)
    {
		handleUpperMsg(msg);
    }else if (msg->arrivalGateId()==lowergateIn)
    {
		handleLowerMsg(msg);
	}

}


/** @brief (one liner)
  *
  * (documentation goes here)
  */
 MsgBuffer::~MsgBuffer()
 {
 }

 /** @brief (one liner)
  *
  * (documentation goes here)
  */
 MsgBuffer::MsgBuffer()
 {

 }
