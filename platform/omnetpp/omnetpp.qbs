import qbs 1.0
import ReflexPackage

Project {
    // Platform-specific classes and drivers for the omnetpp platform
    ReflexPackage {
        name: "platform"

        files: [
            "sources/reflex/CoreApplication.cc",
            "sources/reflex/interrupts/interruptFunctions.c",
            "sources/reflex/io/OmnetConsole.cc",
            "sources/reflex/io/AbstractBufferPathLogger.cc",
            "sources/reflex/powerManagement/power.cc",
            "sources/reflex/scheduling/SchedulerWrapper.cc",
            "sources/reflex/timer/HardwareTimerMilli.cc",
            "sources/reflex/timer/SystemTimer.cc",
            "sources/omnetpp/msg/ReflexPacket.msg",
        ]

        Depends { name : "cpp" }
        Depends { name : "reflex.omnetpp" }

        Depends { name : "platform-essentials" }
        Depends { name : "core" }
        Depends { name : "buffer" }

        Group {
            files : "sources/reflex/io/OMNeTRadio.cc"

            cpp.includePaths : [
                reflex.omnetpp.miximpath + "/src/base/modules",
                reflex.omnetpp.miximpath + "/src/base/messages",
                reflex.omnetpp.miximpath + "/src/base/connectionManager",
                reflex.omnetpp.miximpath + "/src/base/utils",
                reflex.omnetpp.miximpath + "/src/modules/messages",
                reflex.omnetpp.miximpath + "/src/modules/phy",
                reflex.omnetpp.miximpath + "/src/modules/mac",
                reflex.omnetpp.miximpath + "/src/modules/analgueModel",
                reflex.omnetpp.miximpath + "/src/modules/connectionManager",
                reflex.omnetpp.miximpath + "/src/inet_stub/base",
                reflex.omnetpp.miximpath + "/src/inet_stub/linklayer",
                reflex.omnetpp.miximpath + "/src/inet_stub/mobility",
                reflex.omnetpp.miximpath + "/src/inet_stub/networklayer",
                reflex.omnetpp.miximpath + "/src/inet_stub/util",
            ]
        }

        Group {
            files: "sources/reflex/debug/*.h"
            qbs.install: true
            qbs.installDir: "include/reflex/debug"
        }

        Group {
            files: "sources/reflex/interrupts/*.h"
            qbs.install: true
            qbs.installDir: "include/reflex/interrupts"
        }

        Group {
            files: "sources/reflex/io/*.h"
            qbs.install: true
            qbs.installDir: "include/reflex/io"
        }

        Group {
            files: "sources/reflex/memory/*.h"
            qbs.install: true
            qbs.installDir: "include/reflex/memory"
        }

        Group {
            files: "sources/reflex/timer/*.h"
            qbs.install: true
            qbs.installDir: "include/reflex/timer"
        }

        Group {
            fileTagsFilter: "staticlibrary"
            qbs.install: true
            qbs.installDir: "lib"
        }

        Group {
            files : [
                "ned/*.ned"
            ]
            qbs.install : true
            qbs.installDir : "ned/reflex/platform/omnetpp"
        }

        Export {
            Depends { name : "cpp" }
            Depends { name: "reflex.omnetpp" }

            Depends { name: "platform-essentials" }
            Depends { name: "core" }
        }
    }

    // Platform dependend headers that are needed by the core in order to compile
    ReflexPackage {
        name: "platform-essentials"

        Depends { name: "cpp" }
        Depends { name: "reflex.omnetpp" }

        Group {
            files: [
                "sources/reflex/types.h",
                "sources/reflex/CoreApplication.h",
                "sources/reflex/MachineDefinitions.h"
            ]
            qbs.install: true
            qbs.installDir: "include/reflex/"
        }

        Export {
            Depends { name: "cpp" }
            Depends { name: "reflex.omnetpp" }

            cpp.includePaths: [
                "sources"
            ]
        }
    }
}


