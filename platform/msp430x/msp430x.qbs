import qbs 1.0
import ReflexPackage

Project {
    // Platform-specific classes and drivers for the atmega controller family.

    // todo: TIMER_PRECISION define in VirtualTImerSupport of mSP

    ReflexPackage {
        name: "platform"

        Depends { name: "buffer" }
        Depends { name: "core" }
        Depends { name: "platform-essentials" }
        Depends { name : "reflex.msp430x" } // this shouldn't be necessary!

        property bool update_enabled : reflex.msp430x.update_enabled
        property int nodeinfoaddress : reflex.msp430x.nodeinfoaddress

        files: [
            "sources/reflex/adc/*.cc",
            "sources/reflex/compiler/*.cc",
            "sources/reflex/flash/FLASH.cc",
            "sources/reflex/init/*.cc",
            "sources/reflex/interrupts/*.cc",
            "sources/reflex/interrupts/interruptFunctions.S",
            "sources/reflex/lcd/*.cc",
            "sources/reflex/mcu/power.S",
            "sources/reflex/memory/*.cc",
            "sources/reflex/pmm/*.cc",
            "sources/reflex/powerManagement/*.cc",
            "sources/reflex/rf/RF1A.cc",
            "sources/reflex/rtc/*.cc",
            "sources/reflex/timer/*.cc",
            "sources/reflex/ucs/*.cc",
            "sources/reflex/SystemStatusBlock.cc"
        ]

        Group {
            prefix : "sources/reflex/"
            files : "CoreApplication.cc"

            cpp.defines : [
                    "NODEINFOADDRESS=0x" + nodeinfoaddress.toString(16)
                ]
        }

        Group {
            prefix : "sources/reflex/"
            files :  "rf/RADIO.cc"

            Properties {
                condition : update_enabled == true
                cpp.defines : [
                    "UPDATE_ENABLED"
                ]
            }
        }

        Export {
            Depends { name : "cpp" }
            Depends { name : "core" }
            Depends { name : "buffer" }

            cpp.includePaths: [
                "sources/"
            ]
        }

        // Todo: remove this dependency
        cpp.includePaths : [
            "../ez430chronos/sources/"
        ]
    }

    // Platform dependend headers that are needed by the core in order to compile
    ReflexPackage {
        name: "platform-essentials"

        Depends { name : "cpp" }
        Depends { name : "reflex.msp430x" }

        Group {
            files: [
                "sources/reflex/CoreApplication.h",
                "sources/reflex/MachineDefinitions.h",
                "sources/reflex/types.h"
            ]
            qbs.install: true
            qbs.installDir: "include/reflex/"
        }

        Export {
            Depends { name : "reflex.msp430x" } // this does not always work in qbs 1.3.0
            Depends { name : "cpp" }
            cpp.includePaths: [
                "sources/"
            ]
        }
    }
}


