#include "RadioConfig.h"
#include "radio.h"

#define MAX_RXFIFO_SIZE 64

#define BV(n) (1 << (n))
#define RF_STATE 0x70
#define STATE_SHIFT 4

#define RF_STATE_RX (1 << STATE_SHIFT)
#define RF_STATE_TX (2 << STATE_SHIFT)
#define RF_STATE_RX_OVFL (6 << STATE_SHIFT)

/**
 * send a commando to the radio
 * @param strobe sends a commando to the radio
 * @return status byte (@see radio driver) to indicate if operation was successful. 0 => operation definitly fails
 */
uint8 radio_strobe(uint8 strobe);

uint8 radio_strobe(uint8 strobe)
{
    volatile uint8 statusByte;

    // Check for valid strobe command
     if((strobe == 0xBD) || ((strobe >= 0x30) && (strobe <= 0x3D)))
     {
        RF1AIFCTL1 &= ~(RFSTATIFG);             // Clear the status read flag

        while( !(RF1AIFCTL1 & RFINSTRIFG)  );   // Wait for INSTRIFG
        RF1AINSTRW_H = strobe;                    // Write the strobe command RF1AINSTRB

        if(strobe != 0x30) while( !(RF1AIFCTL1 & RFSTATIFG) );
        statusByte = RF1ASTAT0W_H; //read status from RF1ASTATB
     }
     else
       return 0;                               // Invalid strobe was sent

     return statusByte;
}


/**
 * this file contains the declaration of the update specific driver for the radio
 * after returning from this function the radio is in IDLE state
 */
void radio_reset()
{  
    volatile uint16 i;
    uint8 x;

    // Reset radio core
    radio_strobe(RF_SRES);

    // Wait before checking IDLE
    for (i=0; i<1000; i++);
    do {
        x = radio_strobe(RF_SIDLE);
    } while (x & 0xF0);

    // Clear radio error register
    RF1AIFERR = 0;
}

void radio_setReceiveEnabled()
{
    // get radio into idle state
    uint8 status = radio_strobe(RF_SIDLE);
    while (status & 0xF0)
    {
        status = radio_strobe(RF_SNOP);
    }

    // clear rx fifo
    status = radio_strobe(RF_SFRX | RF_RXSTAT);
    while (status & 0x0F)
    {
        status = radio_strobe(RF_SNOP | RF_RXSTAT);
    }

    // get into rx mode
    status = radio_strobe(RF_SRX);
    while ((status & 0xF0) != 0x10)
    {
        status = radio_strobe(RF_SNOP);
    }
}

/**
 *
 */
inline uint8 radio_readSingleReg(uint8 addr)
{
	volatile uint8 x;

	while (!(RF1AIFCTL1 & RFINSTRIFG));

	if( (addr <= 0x2E) || (addr == 0x3E) )
	  RF1AINSTR1B = (addr | RF_SNGLREGRD);
	else
	  RF1AINSTR1B = (addr | RF_REGRD);

	x = RF1ADOUTB;

	return x;
}

/**
 * @todo: -> inline is a first workaround
 */
inline void radio_writeSingleReg(unsigned char addr, unsigned char value)
{
	volatile uint16 i;

    while (!(RF1AIFCTL1 & RFINSTRIFG));     // Wait for the Radio to be ready for the next instruction

    RF1AINSTRW = ((addr | RF_REGWR) << 8 ) + value; // Send address + Instruction
	while (!(RFDINIFG & RF1AIFCTL1));

	i = RF1ADOUTB;                            // Reset RFDOUTIFG flag which contains status byte

}

// *************************************************************************************************
// @fn          ReceiveOff
// @brief       Put Radio in Idle Mode
// @param       none
// @return      none
// *************************************************************************************************
void radio_setReceiveDisabled(void)
{
   uint8 x;

   // Update Mode Flag
   //wbslMode_flag = WBSL_IDLE_MODE;

   do {
      x = radio_strobe(RF_SIDLE);
   } while (x & 0x70);
   /* Wait for XOSC to be stable and radio in IDLE state */

   radio_strobe( RF_SFRX);      /* Flush the receive FIFO of any residual data */

   RF1AIFG =0;                         // Clear pending IFG
}


// *************************************************************************************************
// @fn          WriteBurstReg
// @brief       Write sequence of bytes to register.
// @param       none
// @return      none
// *************************************************************************************************
inline void radio_writeBurstReg(unsigned char addr, unsigned char *buffer, unsigned char count)
{
	volatile uint8 i;

	while (!(RF1AIFCTL1 & RFINSTRIFG));       // Wait for the Radio to be ready for next instruction
	RF1AINSTRW = ((addr | RF_REGWR)<<8 ) + buffer[0]; // Send address + Instruction

	// Write Burst works wordwise not bytewise - bug known already
	for (uint8 i = 1; i < count; i++)
	{
		RF1ADINB = buffer[i];                   // Send data
		while (!(RFDINIFG & RF1AIFCTL1));       // Wait for TX to finish
	}

	i = RF1ADOUTB;                            // Reset RFDOUTIFG flag which contains status byte
}


uint8 radio_config(const uint8 radioCfg[][2], uint8 pairs, uint8 channel)
{
	/* Wait for XOSC to be stable and radio in IDLE state */
	while (radio_strobe( RF_SNOP ) & 0xF0) ;

	radio_strobe( RF_SFRX ); //flush fifo

	/*
	*  Select Interrupt edge for PA_PD and SYNC signal:
	* Interrupt Edge select register: 1 == Interrupt on High to Low transition.
	*/
	RF1AIES = 0x0201;

	volatile unsigned char valueRead = 0;

	while(valueRead != 0x51)
	{

		/* Write the power output to the PA_TABLE and verify the write operation.  */
		unsigned char i = 0;

		/* wait for radio to be ready for next instruction */
		while( !(RF1AIFCTL1 & RFINSTRIFG));
		RF1AINSTRW = 0x7E00 + 0x51; // PA Table write (burst) //and write first value see errata

		/* wait for radio to be ready for next instruction */
		while( !(RF1AIFCTL1 & RFINSTRIFG));
		RF1AINSTR1B = RF_PATABRD;                 // -miguel read & write RF1AINSTR1B

		// Traverse PATABLE pointers to read
		for (i = 0; i < 7; i++)
		{
			while( !(RF1AIFCTL1 & RFDOUTIFG));
			valueRead  = RF1ADOUT1B; //RF1ADOUT1B;
		}

		while( !(RF1AIFCTL1 & RFDOUTIFG));
		valueRead  = RF1ADOUT0B; //RF1ADOUTB; ????????
	}


	//writes the given settings into radio configuration registers
	for (uint8 i = 0; i < pairs; i++)
	  radio_writeSingleReg(radioCfg[i][0], radioCfg[i][1]);


	for (uint8 i = 0; i < pairs; i++)
	{
		if( radioCfg[i][1] != radio_readSingleReg(radioCfg[i][0]) )
			return 0;

	}

	/**
	 * channel:
	 * 	0, => 0
		50, => 1
		80, => 2
		110 => 3
	 */
	radio_writeSingleReg(0x0A, channel);

	/**
	 * setting power level
	 0x03	//-30dBm -> 11,9mA
	,0x0D	//-20dBm -> 12,4mA
	,0x1C	//-15dBm -> 13,0mA
	,0x34	//-10dBm -> 14,5mA
	,0x57	//-5dBm -> 15,1mA
	,0x8E	//0dBm -> 16,9mA
	,0x85	//5dBm -> 20,0mA
	,0xCC	//7dBm -> 25,8mA
	,0xC3	//10dBm -> 31,1mA
	 */
	radio_writeSingleReg(PATABLE, 0x8E);//0 dBm

	return 1;
}


bool radio_hasData()
{
    return RF1AIFG & BV(9);
}

void radio_powerDown()
{
    uint8 status = 0;
    // Powerdown radio
    status = radio_strobe(RF_SIDLE);
    while (status & 0xF0)
    {
        status = radio_strobe(RF_SNOP);
    }
    radio_strobe(RF_SXOFF);
}

/**
 * @return length of the received data
 */
uint8 radio_readData(uint8* buffer, uint8 len)
{
	uint8 state = radio_strobe(RF_SNOP) & RF_STATE;

	uint8 rxBytes;
	uint8 tL;

	uint8* tmpRxBuffer = buffer;

	//checking radio state
	switch(state)
	{
	case RF_STATE_RX:
		break;

	case RF_STATE_TX:

	case RF_STATE_RX_OVFL:

	default:
		//ReceiveOff(); //goto idle + flush
		radio_setReceiveEnabled();
		//return 0;
	}

   // Check the SYNC Word detected flag
	while(!(RF1AIFG & BV(9)))
		state = radio_strobe(RF_SNOP);

	// Clear RX Interrupt Flag (SyncWord)
	RF1AIFG &= ~BV(9);

	// Clean Buffer to help protect against spurios frames
	for(uint8 i = 0; i < len; i++)
		tmpRxBuffer[i] = 0;

	// Read the number of bytes ready in the FIFO
	rxBytes = radio_readSingleReg( RXBYTES );

	do {
		tL = rxBytes;
		rxBytes = radio_readSingleReg( RXBYTES );
	} while(tL != rxBytes);   // Due to a chip bug, the RXBYTES has to read the same value twice for it to be correct

	if(rxBytes == 0)   // Check if the RX FIFO is empty, this may happen if address check is enabled and the FIFO is flushed when address doesn't match
		return 0;

	radio_readSingleReg( RXFIFO );//read length byte
	rxBytes--; // length was read



	//checking expected packet size + 2 status bytes || overflow
	if(rxBytes > len + 2 || (rxBytes & 0x80))
	{
		//handle overflow
		if(rxBytes & 0x80)
			radio_strobe( RF_SFRX );//flushes FIFO only when overflow!!

		radio_setReceiveDisabled();// on!

		if( !(rxBytes & 0x80) )//no overflow, then flush now
			radio_strobe( RF_SFRX );//flushes FIFO only when overflow!!

		radio_setReceiveEnabled(); // off!
		return 0;
	}


	rxBytes -= 2; //reading only the data part into buffer, ignore status bytes
	tL = rxBytes;
	while(rxBytes)
	{
		*tmpRxBuffer++ = radio_readSingleReg( RXFIFO );
		rxBytes--;
	}

	// the two status bytes have to be handled
	rxBytes = radio_readSingleReg( RXFIFO ); // dummy read (first status byte isn't need)
	uint8 crc = radio_readSingleReg( RXFIFO );
	//checking CRC
	if(crc & 0x80)
		return tL;
	else
		return 0;
}

