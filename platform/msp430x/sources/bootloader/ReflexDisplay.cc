/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 **/

#include "ReflexDisplay.h"
#include <reflex/MachineDefinitions.h>
#include <reflex/io/Ports.h>

using namespace reflex;
using namespace msp430x;
using namespace display;

/** initialized lcd
 */
Display::Display()
{
	//set lcd functionality for SEG
	uint16 pins[4]={0xFFFF,0x00FF,0x00,0x00};
	lcdC.enableSegments(pins);

	//set pins 6,7,8 to output and functionality com1,com2,com3
	//Note: pin 5(com0) has always this funtionality
	Port5()->SEL |= BIT_5|BIT_6|BIT_7;
	Port5()->DIR |= BIT_5|BIT_6|BIT_7;

	lcdC.setACLK();

	// LCD_FREQ = ACLK/16/8 = 256Hz
	// Frame frequency = 256Hz/4 = 64Hz
	lcdC.setLCDFreq(lcd::LCDDIV_16, lcd::LCDPRE_8);
	// LCB_BLK_FREQ = ACLK/8/4096 = 1Hz
	lcdC.setBLKFreq(lcd::BLKDiv_8, lcd::BLKPre_4096);

	lcdC.setMux(lcd::MUX_4);

	//this->setSleepMode( LPM3 ); //aclk is active since lpm3
}

void Display::enable()
{
	lcdC.enable();
}

void Display::disable()
{
	lcdC.disable();
}


void LCDMemWriter::write(const uint8 &memposOffset, const uint8 &valOffset , const bool &swap){
	//assumed the position of the obj is above the LCD_Memory or LCD_Blink_Memory
	uint8&	segment = *(reinterpret_cast<uint8*const>(this) + Lookup::offsets[memposOffset]);
	//check array bounds
	uint8 bits;
	if (valOffset < Lookup::FONT_SIZE ) {
		//get bitmask from lookup table
		bits = Lookup::font[valOffset];
	} else {
		bits = 0;
	}
	// in the lower line we have to swap the bits, cause the com signals are different to the other line
	if (swap) {
		bits= ((bits << 4) & 0xF0) | ((bits >> 4) & 0x0F);
		// and we have also to swap the clear mask
		segment &=  ~ ((( SEG_MASK << 4) & 0xF0) | ((SEG_MASK >> 4) & 0x0F ));
	} else {
		//clear the old value, wipe the SEG bits
		segment &= ~ SEG_MASK;
	}
	// set new value (taken from a lookuptable) and write it into the lcd memory
	segment |= bits;
}
/*!
*/
void display::LCDMemWriter::setSymbol(const Symbol& symb)
{
	const uint8 memposOffset = symb>>8;
	const uint8 val = symb&0xff;
	uint8&	mem = *(reinterpret_cast<uint8*const>(this) + Lookup::offsets[memposOffset]);
	mem |= val;
}

void display::LCDMemWriter::clearSymbol(const Symbol& symb)
{
	const uint8 memposOffset = symb>>8;
	const uint8 val = symb&0xff;
	uint8&	mem = *(reinterpret_cast<uint8*const>(this) + Lookup::offsets[memposOffset]);
	mem &= ~val;
}

void display::LCDMemWriter::toggleSymbol(const Symbol& symb)
{
	const uint8 memposOffset = symb>>8;
	const uint8 val = symb&0xff;
	uint8&	mem = *(reinterpret_cast<uint8*const>(this) + Lookup::offsets[memposOffset]);
	mem ^= val;
}

UpperLine& display::UpperLine::operator =(const char(&string)[DigitCount + 1])
{
	for(int i = 0; i != DigitCount; ++i) {
		if (string[i] <= 90) { /* presumably numbers and capital letters */
			this->write(i,string[i]-48);
		} else { /* presumably small caps */
			this->write(i,string[i]-80);
		}
	}
	return *this;
}

UpperLine& display::UpperLine::operator=(const uint16& val) {
	uint16 tmp = val;
	for (int i=3;i>=0; --i) {
		this->write(i,tmp&0xF);
		tmp>>=4;
	}
	return *this;
}

LowerLine& display::LowerLine::operator =(const char(&string)[DigitCount + 1])
{
	for(int i = 0; i != DigitCount; ++i) {
		if (string[i] <= 90) { /* presumably numbers and capital letters */
			this->write(i+UpperLine::DigitCount,string[i]-48,true);
		} else { /* presumably small caps */
			this->write(i+UpperLine::DigitCount,string[i]-80,true);
		}
	}
	return *this;
}

LowerLine& display::LowerLine::operator=(const uint16& val) {
	uint16 tmp = val;
	for (int i=4;i>=0; --i) {
		this->write(i+UpperLine::DigitCount,tmp&0xF,true);
		tmp>>=4;
	}
	return *this;
}

LowerLine& display::LowerLine::operator=(const uint32& val) {
	uint32 tmp = val;
	for (int i=4;i>=0; --i) {
		this->write(i+UpperLine::DigitCount,tmp&0xF,true);
		tmp>>=4;
	}
	return *this;
}

using namespace display;

// Table with memory bit assignment for digits "0" to "9" and characters "A" to "Z"
const uint8 Lookup::font[FONT_SIZE] =
{
  SEG_A+SEG_B+SEG_C+SEG_D+SEG_E+SEG_F,           // Displays "0"
	SEG_B+SEG_C,                             // Displays "1"
  SEG_A+SEG_B+      SEG_D+SEG_E+      SEG_G,     // Displays "2"
  SEG_A+SEG_B+SEG_C+SEG_D+            SEG_G,     // Displays "3"
	SEG_B+SEG_C+            SEG_F+SEG_G,     // Displays "4"
  SEG_A+      SEG_C+SEG_D+      SEG_F+SEG_G,     // Displays "5"
  SEG_A+      SEG_C+SEG_D+SEG_E+SEG_F+SEG_G,     // Displays "6"
  SEG_A+SEG_B+SEG_C,                             // Displays "7"
  SEG_A+SEG_B+SEG_C+SEG_D+SEG_E+SEG_F+SEG_G,     // Displays "8"
  SEG_A+SEG_B+SEG_C+SEG_D+      SEG_F+SEG_G,     // Displays "9"
  SEG_A+SEG_B+SEG_C+      SEG_E+SEG_F+SEG_G,     // Displays "A"
	      SEG_C+SEG_D+SEG_E+SEG_F+SEG_G,     // Displays "b"
  SEG_A+            SEG_D+SEG_E+SEG_F      ,     // Displays "C"
  SEG_B+SEG_C+SEG_D+SEG_E+      SEG_G	   ,     // Displays "d"
  SEG_A+           +SEG_D+SEG_E+SEG_F+SEG_G,     // Displays "E"
  SEG_A+                  SEG_E+SEG_F+SEG_G,     // Displays "F"
  0                                        ,     // Displays " "
  SEG_A+SEG_B+SEG_C+      SEG_E+SEG_F+SEG_G,     // Displays "A"
	      SEG_C+SEG_D+SEG_E+SEG_F+SEG_G,     // Displays "b"
  SEG_A+            SEG_D+SEG_E+SEG_F      ,     // Displays "C"
//                  SEG_D+SEG_E+      SEG_G,     // Displays "c"
	SEG_B+SEG_C+SEG_D+SEG_E+      SEG_G,     // Displays "d"
  SEG_A+           +SEG_D+SEG_E+SEG_F+SEG_G,     // Displays "E"
  SEG_A+                  SEG_E+SEG_F+SEG_G,     // Displays "F"
//SEG_A+      SEG_C+SEG_D+SEG_E+SEG_F+SEG_G,     // Displays "G"
  SEG_A+SEG_B+SEG_C+SEG_D+      SEG_F+SEG_G,     // Displays "g"
	      SEG_C+      SEG_E+SEG_F+SEG_G,     // Displays "h"
			  SEG_E+SEG_F      ,     // Displays "I"
  SEG_A+SEG_B+SEG_C+SEG_D                  ,     // Displays "J"
//      SEG_B+SEG_C+      SEG_E+SEG_F+SEG_G,     // Displays "k"
		    SEG_D+SEG_E+SEG_F+SEG_G,     // Displays "k"
		    SEG_D+SEG_E+SEG_F      ,     // Displays "L"
  SEG_A+SEG_B+SEG_C+      SEG_E+SEG_F      ,     // Displays "M"
	      SEG_C+      SEG_E+      SEG_G,     // Displays "n"
	      SEG_C+SEG_D+SEG_E+      SEG_G,     // Displays "o"
  SEG_A+SEG_B+            SEG_E+SEG_F+SEG_G,     // Displays "P"
  SEG_A+SEG_B+SEG_C+SEG_D+SEG_E+SEG_F      ,     // Displays "Q"
			  SEG_E+      SEG_G,     // Displays "r"
  SEG_A+      SEG_C+SEG_D+      SEG_F+SEG_G,     // Displays "S"
		    SEG_D+SEG_E+SEG_F+SEG_G,     // Displays "t"
	      SEG_C+SEG_D+SEG_E            ,     // Displays "u"
	      SEG_C+SEG_D+SEG_E            ,     // Displays "u"
				      SEG_G,     // Displays "-"
	SEG_B+SEG_C+     +SEG_E+SEG_F+SEG_G,     // Displays "X"
	SEG_B+SEG_C+SEG_D+      SEG_F+SEG_G,     // Displays "Y"
  SEG_A+SEG_B+      SEG_D+SEG_E+      SEG_G,     // Displays "Z"
};

const uint8 Lookup::offsets[OFFSETS_SIZE] =
{
	//upper line
	0x1,//LCDMEM2
	0x2,//LCDMEM3
	0x3,//LCDMEM4
	0x5, //LCDMEM6
	//lower line
	0xb,//(LCD_MEM_12)
	0xa,//(LCD_MEM_11)
	0x9,//(LCD_MEM_10)
	0x8,//(LCD_MEM_9)
	0x7,//(LCD_MEM_8)
	//additional symbols
	0x0, //MEM_1
	0x4, //MEM_5
	0x6, //MEM_7
};
