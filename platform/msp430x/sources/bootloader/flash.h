#ifndef FLASH_H
#define FLASH_H

/**
 * erases flash memory
 */
void flash_erase(void* startAddr, unsigned size);

/**
 * writes len bytes into the flash
 */
void flash_write(void* src, void* dst, unsigned len);

#endif // FLASH_H
