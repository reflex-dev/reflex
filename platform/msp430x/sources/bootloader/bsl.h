#ifndef BSL_UPDATE_H_
#define BSL_UPDATE_H_
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Author:		 Hannes Menzel
 *
 *	Description: This header provides the basic function to realize the
 *	the wireless update feature. BSL_Update.h represents only the
 *	platform independent part.
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and
 *    Operating Systems. All rights reserved.
 *
 *    Redistribution and use in source and binary forms, with or without
 *    modification, are permitted provided that the following conditions
 *    are met:
 *
 *       1. Redistributions of source code must retain the above copyright
 *          notice, this list of conditions and the following disclaimer.
 *
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the
 *          distribution.
 *
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *    The views and conclusions contained in the software and documentation
 *    are those of the authors and should not be interpreted as representing
 *    official policies, either expressed or implied, of BTU Cottbus,
 *    Department for Distributed Systems and Operating Systems.
 *
 *
 */
#include <reflex/types.h>
#include <reflex/CoreApplication.h>

#include <reflex/UpdateTypes.h>
#include <bootloader/RadioConfig.h>

extern const uint8 mrfiRadioCfg[][2];

enum State
{
    WaitForBeacon = 1,
    ReceiveUpdate = 2,
    CheckCrc = 3,
    StartApplication = 4
};

void bsl_checkCrc();
void bsl_receiveUpdate();
void bsl_startApplication() __attribute__ ((noreturn));
void bsl_waitForBeacon();


void timer_setTimeout(uint16 ticks);
void timer_reset();
void timer_stop();
bool timer_timeout();

#endif /* BSL_UPDATE_H_ */
