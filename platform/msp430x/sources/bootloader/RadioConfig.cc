#include "bsl.h"

const uint8 mrfiRadioCfg[][2] =
{
    /* internal radio configuration */
    {  IOCFG0,    MRFI_SETTING_IOCFG0       }, /* Configure GDO_0 to output PA_PD signal (low during TX, high otherwise). */
    {  IOCFG1,    MRFI_SETTING_IOCFG1       }, /* Configure GDO_1 to output RSSI_VALID signal (high when RSSI is valid, low otherwise). */
    {  MCSM1,     MRFI_SETTING_MCSM1        }, /* CCA mode, RX_OFF_MODE and TX_OFF_MODE */
    {  MCSM0,     MRFI_SETTING_MCSM0        }, /* AUTO_CAL and XOSC state in sleep */
    {  PKTLEN,    MRFI_SETTING_PKTLEN       },
    {  PKTCTRL0,  MRFI_SETTING_PKTCTRL0     },
    {  PKTCTRL1,  MRFI_SETTING_PKTCTRL1	  },/* added by menzehan*/
    {  FIFOTHR,   MRFI_SETTING_FIFOTHR      },
    {  SYNC1,     0xD3                      },/* added by menzehan*/
    {  SYNC0,     0x91                      },/* added by menzehan*/
    /* imported SmartRF radio configuration */
    {  FSCTRL1,   SMARTRF_SETTING_FSCTRL1   },
    {  FSCTRL0,   SMARTRF_SETTING_FSCTRL0   },
    {  FREQ2,     SMARTRF_SETTING_FREQ2     },
    {  FREQ1,     SMARTRF_SETTING_FREQ1     },
    {  FREQ0,     SMARTRF_SETTING_FREQ0     },
    {  MDMCFG4,   SMARTRF_SETTING_MDMCFG4   },
    {  MDMCFG3,   SMARTRF_SETTING_MDMCFG3   },
    {  MDMCFG2,   SMARTRF_SETTING_MDMCFG2   },
    {  MDMCFG1,   SMARTRF_SETTING_MDMCFG1   },
    {  MDMCFG0,   SMARTRF_SETTING_MDMCFG0   },
    {  DEVIATN,   SMARTRF_SETTING_DEVIATN   },
    {  FOCCFG,    SMARTRF_SETTING_FOCCFG    },
    {  BSCFG,     SMARTRF_SETTING_BSCFG     },
    {  AGCCTRL2,  SMARTRF_SETTING_AGCCTRL2  },
    {  AGCCTRL1,  SMARTRF_SETTING_AGCCTRL1  },
    {  AGCCTRL0,  SMARTRF_SETTING_AGCCTRL0  },
    {  FREND1,    SMARTRF_SETTING_FREND1    },
    {  FREND0,    SMARTRF_SETTING_FREND0    },
    {  FSCAL3,    SMARTRF_SETTING_FSCAL3    },
    {  FSCAL2,    SMARTRF_SETTING_FSCAL2    },
    {  FSCAL1,    SMARTRF_SETTING_FSCAL1    },
    {  FSCAL0,    SMARTRF_SETTING_FSCAL0    },
    {  TEST2,     SMARTRF_SETTING_TEST2     },
    {  TEST1,     SMARTRF_SETTING_TEST1     },
    {  TEST0,     SMARTRF_SETTING_TEST0     },
};
