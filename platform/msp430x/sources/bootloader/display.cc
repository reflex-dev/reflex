#include "display.h"
#include "ReflexDisplay.h"

// We use the display driver of REFLEX here, even seems to cause code-bloat
Display bsl_display;

void display_init()
{
    bsl_display.enable();
}

void display_writeLower(uint16 value)
{
    bsl_display.getLowerLine() = value;
}

void display_writeUpper(uint16 value)
{
    bsl_display.getUpperLine() = value;
}

