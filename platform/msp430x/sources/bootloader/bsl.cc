/*
 * BSL_Update.c
 *
 *  Created on: Apr 14, 2011
 *      Author: hannes
 */

#include "bsl.h"
#include "cpu.h"
#include "display.h"
#include "flash.h"
#include "radio.h"
#include <reflex/UpdateTypes.h>

#ifndef FIRMWAREADDRESS
    #error(FIRMWAREADDRESS is not defined)
#endif
#ifndef NODEINFOADDRESS
    #error(NODEINFOADDRESS is not defined)
#endif

#define HIGHGESTADDRESS 0x10000
#define FIRMWAREPOINTER reinterpret_cast<void*>(FIRMWAREADDRESS)
#define FIRMWARESIZE    (HIGHGESTADDRESS - FIRMWAREADDRESS) // firmware image has always the same size
#define NODEID          *reinterpret_cast<uint8*>(NODEINFOADDRESS)
#define RESET_VECTOR    0xFFFE

reflex::msp430x::FirmwareInfo firmware = *reinterpret_cast<reflex::msp430x::FirmwareInfo*>(FIRMWAREADDRESS);
uint16 bsl_packetCount;
uint8  bsl_remainder;
State  bsl_nextState;
State  bsl_state;
UpdatePacket bsl_buffer;


uint16 crc16(void* data, uint16 length)
{
    uint8* data_p = reinterpret_cast<uint8*>(data);
    uint16 crc = 0xFFFF;
    for (uint16 i = 0; i < length; i++){
        uint8 x = crc >> 8 ^ *data_p++;
        x ^= x>>4;
        crc = (crc << 8) ^ ((uint16)(x << 12)) ^ ((uint16)(x <<5)) ^ ((uint16)x);
    }
    return crc;
}

uint16 calcChecksum()
{
    void* address = (void*)(FIRMWAREADDRESS + sizeof(reflex::msp430x::FirmwareInfo));
    // volatile prevents a compiler bug which makes the optimizer throwing away
    // the call to crc16. For whatever reason.
    volatile uint32 size = FIRMWARESIZE - sizeof(reflex::msp430x::FirmwareInfo);
    return crc16(address, size);
}

int main()
{
    cpu_init();
    display_init();

    bsl_nextState = WaitForBeacon;
    while (true)
    {
        bsl_state = bsl_nextState;
        display_writeUpper(bsl_state);
        switch (bsl_state)
        {
        case WaitForBeacon:
            bsl_waitForBeacon();
            break;
        case ReceiveUpdate:
            bsl_receiveUpdate();
            break;
        case CheckCrc:
            bsl_checkCrc();
            break;
        case StartApplication:
            bsl_startApplication();
        }
    }
}

void bsl_checkCrc()
{
    if (calcChecksum() == firmware.checksum)
        bsl_nextState = StartApplication;
    else
        bsl_nextState = WaitForBeacon;
}


void bsl_startApplication()
{
    timer_stop();
    radio_setReceiveDisabled();
    radio_powerDown();

    void* address = (void*)FIRMWAREADDRESS;
    __asm__ __volatile__("br %[addr]" : /*"=r"*/ : [addr] "r" (address) );
     __asm__ __volatile__("nop");
     __asm__ __volatile__("nop");
     __asm__ __volatile__("nop");
    while(true);
}

void bsl_waitForBeacon()
{
    display_writeLower(0);
    firmware = *reinterpret_cast<reflex::msp430x::FirmwareInfo*>(FIRMWAREADDRESS);
    bool firmwareIsValid = (calcChecksum() == firmware.checksum) ? true : false;

    radio_reset();
    while(!(radio_config((mrfiRadioCfg), 35, CHANNEL)));
    radio_setReceiveEnabled();
    timer_setTimeout(32000);
    while(!timer_timeout())
    {
        if (!radio_hasData())
            continue;

        UpdateMetaPacket& metaPacket = bsl_buffer.meta;
        uint8 packetLen = radio_readData( (uint8*)((&bsl_buffer)), sizeof(UpdatePacket) );

        if (packetLen == sizeof(UpdateMetaPacket)
                && (metaPacket.header.type == MetaPacket)
                && ((metaPacket.header.version != firmware.version)
                    || metaPacket.header.getOverride()
                    || (firmwareIsValid == false))
          )
        {
            bsl_packetCount = metaPacket.packetCount;
            bsl_remainder = metaPacket.remainder;
            firmware.version = metaPacket.header.version;
            bsl_nextState = ReceiveUpdate;
            return;
        }
    }

    bsl_nextState = CheckCrc;
}

void bsl_receiveUpdate()
{
    // Perform mass-erase and restore reset vector
    void* resetVector = *((void**)RESET_VECTOR);
    flash_erase(FIRMWAREPOINTER, FIRMWARESIZE);
    flash_write(&resetVector, (void*)RESET_VECTOR, sizeof(void*));

    uint8 bitArray[FIRMWARESIZE / UpdateDataPacket::PayloadSize / 8 + 1];
    for (uint16 i = 0; i < sizeof(bitArray); i++)
        bitArray[i] = 0xff;
    // padd the last byte in the bit array with zeroes
    bitArray[bsl_packetCount >> 3] = 0xff >> (7 - (bsl_packetCount % 8));

    timer_setTimeout(32000);
    for (uint16 remainingPackets = bsl_packetCount; remainingPackets > 0; )
    {
        while (!radio_hasData())
        {
            if (timer_timeout())
            {
                bsl_nextState = WaitForBeacon;
                return;
            }
        }

        UpdateDataPacket& packet = bsl_buffer.data;
        uint8 packetLen = radio_readData( (uint8*)(&bsl_buffer), sizeof(UpdateDataPacket) );

        if ((packetLen == sizeof(UpdateDataPacket))
                && (packet.header.type == DataPacket)
                && (packet.header.version == firmware.version))
        {
            // checking, if the data have to be written into flash
            // => if the coresponding bit == 0, then writing was already successful
            // and writing the received data into flash isn't needed anymore
            if( bitArray[packet.seqNo >> 3] & (1 << (packet.seqNo % 8)) )
            {
                uint8 size = UpdateDataPacket::PayloadSize;
                if(packet.seqNo == bsl_packetCount)
                {
                    if(bsl_remainder)
                        size = bsl_remainder;

                    if(size)
                        size -= 2;// avoid overwritting reset_vector
                }
                void* dstAddress =
                        (void*)(FIRMWAREADDRESS
                                + (packet.seqNo * UpdateDataPacket::PayloadSize));
                flash_write(packet.data, dstAddress, size);

                // mark data as successfully written
                bitArray[packet.seqNo >> 3] &= ~(1 << ((packet.seqNo % 8)));
                remainingPackets--;
            }
            timer_reset();
        }
        else if ((packetLen == sizeof(UpdateLeapPacket))
                    && (packet.header.type == LeapPacket)
                    && (packet.header.version == firmware.version))
        {
            uint16 skipCount = bsl_buffer.leap.repeatCount;
            for (uint16 i = packet.seqNo; i < (packet.seqNo + skipCount); i++)
            {
                // mark data as successfully written
                if( bitArray[i >> 3] & (1 << (i % 8)) )
                {
                    bitArray[i >> 3] &= ~(1 << ((i % 8)));
                    remainingPackets--;
                }
            }
            timer_reset();
        }
        else if (timer_timeout())
        {
            bsl_nextState = WaitForBeacon;
            return;
        }
        display_writeLower(remainingPackets);
    }

    // calculate checksum and update meta data
    firmware.checksum = calcChecksum();
    flash_write(&firmware, (void*)FIRMWAREADDRESS, sizeof(reflex::msp430x::FirmwareInfo));

    bsl_nextState = CheckCrc;
}
