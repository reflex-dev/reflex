#include "flash.h"

#define BLOCKSIZE  128
#define INFOMEM_SEGMENTSIZE 128
#define MAINMEM_SEGMENTSIZE 512
#define INFOMEM_START 0x1800
#define MAINMEM_START 0x8000

#define sfrb_(x,x_) \
    extern "C" volatile unsigned char x asm(#x_)

#define sfrw_(x,x_) \
    extern "C" volatile unsigned int x asm(#x_)

#define sfrb(x,x_) sfrb_(x,x_)
#define sfrw(x,x_) sfrw_(x,x_)

#define __MSP430_FLASH_BASE__ 0x0140
#define FCTL1_ __MSP430_FLASH_BASE__ + 0x00  /* FLASH Control 1 */
sfrw(FCTL1, FCTL1_);
#define FCTL1_L_ __MSP430_FLASH_BASE__ + 0x00
sfrb(FCTL1_L, FCTL1_L_);
#define FCTL1_H_ __MSP430_FLASH_BASE__ + 0x01
sfrb(FCTL1_H, FCTL1_H_);
//sfrbw    FCTL2               (0x0142)  /* FLASH Control 2 */
#define FCTL3_ __MSP430_FLASH_BASE__ + 0x04  /* FLASH Control 3 */
sfrw(FCTL3, FCTL3_);
#define FCTL3_L_ __MSP430_FLASH_BASE__ + 0x04
sfrb(FCTL3_L, FCTL3_L_);
#define FCTL3_H_ __MSP430_FLASH_BASE__ + 0x05
sfrb(FCTL3_H, FCTL3_H_);
#define FCTL4_ __MSP430_FLASH_BASE__ + 0x06  /* FLASH Control 4 */
sfrw(FCTL4, FCTL4_);
#define FCTL4_L_ __MSP430_FLASH_BASE__ + 0x06
sfrb(FCTL4_L, FCTL4_L_);
#define FCTL4_H_ __MSP430_FLASH_BASE__ + 0x07
sfrb(FCTL4_H, FCTL4_H_);

#define FRKEY                  (0x9600)       /* Flash key returned by read */
#define FWKEY                  (0xA500)       /* Flash key for write */
#define FXKEY                  (0x3300)       /* for use with XOR instruction */

/* FCTL1 Control Bits */
#define ERASE                  (0x0002)       /* Enable bit for Flash segment erase */
#define MERAS                  (0x0004)       /* Enable bit for Flash mass erase */
#define SWRT                   (0x0020)       /* Smart Write enable */
#define WRT                    (0x0040)       /* Enable bit for Flash write */
#define BLKWRT                 (0x0080)       /* Enable bit for Flash segment write */

/* FCTL3 Control Bits */
#define BUSY                   (0x0001)       /* Flash busy: 1 */
#define KEYV                   (0x0002)       /* Flash Key violation flag */
#define ACCVIFG                (0x0004)       /* Flash Access violation flag */
#define WAIT                   (0x0008)       /* Wait flag for segment write */
#define LOCK                   (0x0010)       /* Lock bit: 1 - Flash is locked (read only) */
#define EMEX                   (0x0020)       /* Flash Emergency Exit */
#define LOCKA                  (0x0040)       /* Segment A Lock bit: read = 1 - Segment is locked (read only) */

/* FCTL4 Control Bits */
#define VPE                    (0x0001)       /* Voltage Changed during Program Error Flag */
#define MGR0                   (0x0010)       /* Marginal read 0 mode. */
#define MGR1                   (0x0020)       /* Marginal read 1 mode. */
#define LOCKINFO               (0x0080)       /* Lock INFO Memory bit: read = 1 - Segment is locked (read only) */

unsigned u_segment_size(void* addr)
{
    if (addr >= (void*)(MAINMEM_START))
    {
        return MAINMEM_SEGMENTSIZE;
    }
    else if (addr >= (void*)(INFOMEM_START))
    {
        return INFOMEM_SEGMENTSIZE;
    }
}

void flash_erase(void* startAddr, unsigned size)
{
	//write once on each segment to erase
	for(unsigned i = 0; i < size; i += u_segment_size((startAddr)))
	{
		//waiting until flash is ready
		while(FCTL3 & BUSY);
		FCTL3 = FWKEY; // writing PW

		FCTL1 = ERASE | FWKEY; //set to segment erase
		((unsigned char*)startAddr)[i] = 11;		//dummy write

		FCTL1 = FWKEY | 0x00; //clr erase bit

		while(FCTL3 & BUSY);

		FCTL3 = FWKEY | LOCK; // lock flash
	}
}


void flash_write(void* src, void* dst, unsigned len)
{
	unsigned char* src8 = reinterpret_cast<unsigned char*>(src) + len - 1;
	unsigned char* dst8 = reinterpret_cast<unsigned char*>(dst) + len - 1;

	for(; len != 0; len--)
	{
		//is impossible to optimize
		while(FCTL3 & BUSY);
		FCTL3 = FWKEY; // clear lock
		FCTL1 = WRT | FWKEY; // set write bit
		(*dst8--) = *src8--; // writing data into flash

		while(FCTL3 & BUSY);
		FCTL1 = 0x00 | FWKEY;  //clr write bit
		FCTL3 = LOCK | FWKEY; // Lock again
	}
}
