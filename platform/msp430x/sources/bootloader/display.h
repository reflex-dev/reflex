#ifndef DISPLAY_H
#define DISPLAY_H

#include <reflex/types.h>

void display_clear();
void display_init();
void display_writeUpper(uint16 value);
void display_writeLower(uint16 value);

#endif // DISPLAY_H
