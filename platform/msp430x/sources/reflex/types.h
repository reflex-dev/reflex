#ifndef types_h
#define types_h
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(ses):
 *
 *	Author:		Karsten Walther
 *
 *	Description:	Some common type definitions
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */

typedef unsigned int size_t;
typedef char* caddr_t;

typedef signed char int8;
typedef unsigned char uint8;
typedef signed int int16;
typedef unsigned int uint16;
typedef signed long int32;
typedef unsigned long uint32;

typedef unsigned long long uint64;
typedef signed long long int64;

typedef int ptrdiff_t;
typedef unsigned long flash_addr_t;

typedef unsigned long Time;
typedef unsigned word;

enum BITS {
	BIT_0 =  0x1<<0
	,BIT_1 = 0x1<<1
	,BIT_2 = 0x1<<2
	,BIT_3 = 0x1<<3
	,BIT_4 = 0x1<<4
	,BIT_5 = 0x1<<5
	,BIT_6 = 0x1<<6
	,BIT_7 = 0x1<<7
	,BIT_8 = 0x1<<8
	,BIT_9 = 0x1<<9
	,BIT_10 = 0x1<<10
	,BIT_11 = 0x1<<11
	,BIT_12 = 0x1<<12
	,BIT_13 = 0x1<<13
	,BIT_14 = 0x1<<14
	,BIT_15 = 0x1<<15
};

#ifndef NULL
	#define NULL 0
#endif

enum {
	MAX_UINT8 = 255,
	MAX_INT8 = MAX_UINT8/2,
	MAX_UINT16 = 65535,
	MAX_INT16 = MAX_UINT16/2,
    MAX_INT_DIGITS = 5,
    MAX_LONG_DIGITS = 10,
    MAX_LONG_LONG_DIGITS = 20,
    MAX_FLOAT_DIGITS = 15
};


#endif
