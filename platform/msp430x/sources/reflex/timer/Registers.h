/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 **/

#ifndef REFLEX_TIMER0_A5_REGISTERS_H
#define REFLEX_TIMER0_A5_REGISTERS_H

#include "reflex/mcu/msp430x.h"
#include "reflex/types.h"
#include "reflex/data_types/ReadOnly.h"
#include "reflex/data_types/Register.h"
#include "reflex/interrupts/InterruptDispatcher.h"

namespace reflex {
namespace msp430x {
namespace timer_a {


enum RegisterBITS {
	//TAxCTL
	TASSELx		= 0x0300 ///clock source select
	,TASSEL00	= 0x0000 ///TACLK as source
	,TASSEL01	= 0x0100 ///ACLK as source
	,TASSEL10	= 0x0200 ///SMCLK as source
	,TASSEL11	= 0x0300 ///INCLK as source
	,IDx		= 0x00c0 ///Divider of clock
	,ID00		= 0x0000 ///Factor 1
	,ID01		= 0x0040 ///Factor 2
	,ID10		= 0x0080 ///Factor 4
	,ID11		= 0x00c0 ///Factor 8
	,MCx		= 0x0030 ///Count mode
	,MC00		= 0x0000 ///stop
	,MC01		= 0x0010 ///Up mode: the timer counts up to TACCR0
	,MC10		= 0x0020 ///continuous mode
	,MC11		= 0x0030 ///Up&Down mode
	,TACLR		= 0x0004 ///resets divider and TAR
	,TAIE		= 0x0002 ///Timer A interrupt enable (overflow)
	,TAIFG		= 0x0001 ///Timer A interrupt flag (overflow)
	//TAIV
	,TAIVx		= 0x000e
	,TACCR1_CCIFG	= 0x0002
	,TACCR2_CCIFG	= 0x0004
	,TACCR3_CCIFG	= 0x0006
	,TACCR4_CCIFG	= 0x0008
	,TACCR5_CCIFG	= 0x000A
	,TAIFG_TACTL	= 0x000E //?????????????????????
	//TAxCCTLx
	,CMx		= 0xc000
	,CM01		= 0x4000
	,CM10		= 0x8000
	,CM11		= 0xc000
	,CCISx		= 0x3000
	,CCIS01		= 0x1000
	,CCIS10		= 0x2000
	,CCIS11		= 0x3000
	,SCS		= 0x0800
	,SCCI		= 0x0400
	,CAP		= 0x0100 /// capture mode for TACCTLx
	,OUTMODx	= 0x00e0
	,OUTMOD001	= 0x0020
	,OUTMOD010	= 0x0040
	,OUTMOD011	= 0x0060
	,OUTMOD100	= 0x0080
	,OUTMOD101	= 0x00a0
	,OUTMOD110	= 0x00c0
	,OUTMOD111	= 0x00e0
	,CCIE		= 0x0010
	,CCI		= 0x0008
	,OUT		= 0x0004
	,COV		= 0x0002
	,CCIFG		= 0x0001
	//TAXEX0
	,IDEXx		= 0x0007
	,IDEX111	= 0x0007
	,IDEX110	= 0x0006
	,IDEX101	= 0x0005
	,IDEX100	= 0x0004
	,IDEX011	= 0x0003
	,IDEX010	= 0x0002
	,IDEX001	= 0x0001
	,IDEX000	= 0x0000
};


/** Registerfile for the Timer0_A5 module
 */
class Registers_A5
{
	typedef data_types::Register<uint16> Register;
	typedef data_types::ReadOnly<Register> RORegister;
public:
	struct Interrupt_traits {
		enum Vector {
			//alias
			//CCR0 has a separate direct interrupt, not through TAIV
			 CC1=interrupts::IV0	/// capture/compare 1 and alarm
			,CC2=interrupts::IV1	/// capture/compare 2
			,CC3=interrupts::IV2	/// capture/compare 3
			,CC4=interrupts::IV3	/// capture/compare 4
			,CC5=interrupts::IV4	/// capture/compare 5 NOT AVAILABLE
			,CC6=interrupts::IV5	/// capture/compare 6 NOT AVAILABLE
			,TO=interrupts::IV6	/// timer overflow
		};
		enum { COUNT=8 };

		//! reference to the interrupt vector register
		typedef interrupts::IVRef<Registers_A5> VectorRef;

		inline static InterruptVector globalVector() {return interrupts::TIMER0_A5_0;}
		inline static InterruptVector globalVectorCCR0() {return interrupts::TIMER0_A5_1;}
	};

	typedef reflex::InterruptDispatcher< Interrupt_traits > IVDispatcher;

public:
	Register TACTL;       // TACTL at address 0x0340,
	Register TACCTL0;     ///TACCTL0 at address 0x0342,
	Register TACCTL1;     ///TACCTL1 at address 0x0344,
	Register TACCTL2;     ///TACCTL2 at address 0x0346,
	Register TACCTL3;     ///TACCTL3 at address 0x0348,
	Register TACCTL4;     ///TACCTL4 at address 0x034A,
	uint32 :32; ///< CCTL5/6 not available on Timer0_A5
	//Register TACCTL5;	///TACCTL5 at address 0x034C,
	//Register TACCTL6;	///TACCTL6 at address 0x034E,

	Register TAR;		///TAR at address 0x0350,
	Register TACCR0;	///TACCR0 at address 0x0352,
	Register TACCR1;	///TACCR1 at address 0x0354,
	Register TACCR2;	///TACCR2 at address 0x0356
	Register TACCR3;	///TACCR3 at address 0x0358,
	Register TACCR4;	///TACCR4 at address 0x035A
	uint32 :32; ///< CCR5/6 not available on Timer0_A5
	//Register TACCR5;	///TACCR5 at address 0x035C,
	//Register TACCR6;	///TACCR6 at address 0x035E

	Register TAEX0;		///TA Expansion at address 0x0360
	uint16 :16;
	uint16 :16;
	uint16 :16;
	uint16 :16;
	uint16 :16;
	uint16 :16;
	RORegister IV;		///TAIV at address 0x036E
public:
       Registers_A5(){}
       operator Registers_A5*() {return operator->();}
       Registers_A5* operator-> () {return reinterpret_cast<Registers_A5*>(bases::TIMER0_A5+offsets::TIMER0_A5);}
};


/** Registerfile for the Timer1_A3 module
 */
class Registers_A3
{
	typedef data_types::Register<uint16> Register;
	typedef data_types::ReadOnly<Register> RORegister;
public:
	struct Interrupt_traits {
		enum Vector {
			//alias
			//CCR0 has a separate direct interrupt, not through TAIV
			 CC1=interrupts::IV0	/// capture/compare 1 and alarm
			,CC2=interrupts::IV1	/// capture/compare 2
			,CC3=interrupts::IV2	/// capture/compare 3 NOT AVAILABLE
			,CC4=interrupts::IV3	/// capture/compare 4 NOT AVAILABLE
			,CC5=interrupts::IV4	/// capture/compare 5 NOT AVAILABLE
			,CC6=interrupts::IV5	/// capture/compare 6 NOT AVAILABLE
			,TO=interrupts::IV6	/// timer overflow
		};
		enum { COUNT=8 };

		//! reference to the interrupt vector register
		typedef interrupts::IVRef<Registers_A3> VectorRef;

		inline static InterruptVector globalVector() {return interrupts::TIMER1_A3_0;}
		inline static InterruptVector globalVectorCCR0() {return interrupts::TIMER1_A3_1;}
	};

	typedef reflex::InterruptDispatcher< Interrupt_traits > IVDispatcher;

public:
	Register TACTL;       // TACTL at address 0x0340,
	Register TACCTL0;     ///TACCTL0 at address 0x0342,
	Register TACCTL1;     ///TACCTL1 at address 0x0344,
	Register TACCTL2;     ///TACCTL2 at address 0x0346,
	uint32 :32; ///< CCTL3/4 not available on Timer1_A3
//	Register TACCTL3;     ///TACCTL3 at address 0x0348,
//	Register TACCTL4;     ///TACCTL4 at address 0x034A,
	uint32 :32; ///< CCTL5/6 not available on Timer1_A3
	//Register TACCTL5;	///TACCTL5 at address 0x034C,
	//Register TACCTL6;	///TACCTL6 at address 0x034E,

	Register TAR;		///TAR at address 0x0350,
	Register TACCR0;	///TACCR0 at address 0x0352,
	Register TACCR1;	///TACCR1 at address 0x0354,
	Register TACCR2;	///TACCR2 at address 0x0356
	uint32 :32; ///< CCR3/4 not available on Timer1_A3
	//Register TACCR3;	///TACCR3 at address 0x0358,
	//Register TACCR4;	///TACCR4 at address 0x035A
	uint32 :32; ///< CCR5/6 not available on Timer1_A3
	//Register TACCR5;	///TACCR5 at address 0x035C,
	//Register TACCR6;	///TACCR6 at address 0x035E

	Register TAEX0;		///TA Expansion at address 0x0360
	uint16 :16;
	uint16 :16;
	uint16 :16;
	uint16 :16;
	uint16 :16;
	uint16 :16;
	RORegister IV;		///TAIV at address 0x036E
public:
	Registers_A3(){}
	operator Registers_A3*() {return operator->();}
	Registers_A3* operator-> () {return reinterpret_cast<Registers_A3*>(bases::TIMER1_A3+offsets::TIMER1_A3);}
};

}//ns timer_a

}}//ns msp430,reflex


#endif // REGISTERS_H
