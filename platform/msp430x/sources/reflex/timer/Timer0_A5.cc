/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *	Author:	Stefan Nuernberger
 */
#include "reflex/timer/Timer0_A5.h"
#include "reflex/interrupts/InterruptLock.h"

#include "reflex/mcu/msp430x.h"
#include "reflex/sys/SYS.h"

using namespace reflex;
using namespace msp430x;
using namespace timer_a;

/** constructor
 */
Timer0_A5::Timer0_A5()
	: PowerManageAble(PowerManageAble::PRIMARY)
	, ivDispatcher(Registers::Interrupt_traits::globalVector())
{
	/* select ACLK/8, stopped */
	Registers()->TACTL = TASSEL01 | ID11 | MC00;
    Registers()->TAEX0 = IDEX011; // divide by 4 (extension)
	Registers()->TAR = 0; // reset count register
	Registers()->TACCTL0 = 0; // reset CCR0 alarm (unused)
	Registers()->TACCTL1 = 0; // reset alarm control
	Registers()->TACCTL2 = 0; // reset CCR2 alarm (unused)

	/* deepest sleep mode for ACLK is LPM3 */
	this->setSleepMode(LPM3);
}

/** init
 * @param alarm output to notify on alarm
 * @param overflow output to notify on timer overflow
 */
void Timer0_A5::init(Sink0& alarm, Sink0& overflow)
{
	ivDispatcher[Registers::Interrupt_traits::CC1]= &alarm;
	ivDispatcher[Registers::Interrupt_traits::TO]= &overflow;
}

/** setPrecision
 * configure clock source / hardware divider
 * @param prec the desired hardware precision
 */
void Timer0_A5::setPrecision(Precision prec)
{
    switch (prec)
    {
    case ACLK32kHz:
        /* select ACLK/1, stopped */
        Registers()->TACTL = TASSEL01 | ID00 | MC00;
        Registers()->TAEX0 = IDEX000; // divide by 1 (extension)
        break;
    case ACLK1kHz: // this is the default
    default:
        /* select ACLK/8, stopped */
        Registers()->TACTL = TASSEL01 | ID11 | MC00;
        Registers()->TAEX0 = IDEX011; // divide by 4 (extension)
        break;
    }
}

/** start
 * start the counter. Alarm and overflow interrupts may
 * occur after that.
 */
void Timer0_A5::start()
{
	Registers()->TACTL |= MC10;
}

/** stop
 * stop the counter. No interrupts will happen after this
 * was called.
 */
void Timer0_A5::stop() {
	Registers()->TACTL &= ~MCx; // pause timer
}

/** getTime
 * get the current counter value
 * @return uint16 current counter register value
 */
uint16 Timer0_A5::getTime() {
	/* NOTE: The timer source is not in sync with system clock,
	 * so we have to take a majority vote (or stop the timer).
	 */
	uint16 time; // read count register
	do {
		time = Registers()->TAR;
	} while (time != Registers()->TAR);

	return time;
}

/** isOverflowPending
 * @return bool true if an overflow interrupt is pending
 */
bool Timer0_A5::isOverflowPending() {
	return ((Registers()->TACTL & TAIE) && (Registers()->TACTL & TAIFG));
}

/** clearOverflow
 * clear the overflow interrupt flag
 */
void Timer0_A5::clearOverflow() {
	Registers()->TACTL &= ~TAIFG; // clear pending interrupt
}

/** enableOverflow
 * enable overflow interrupt notification
 */
void Timer0_A5::enableOverflow() {
	clearOverflow();
	Registers()->TACTL |= TAIE; // enable overflow interrupt
}

/** disableOverflow
 * disable overflow interrupt notification
 */
void Timer0_A5::disableOverflow() {
	Registers()->TACTL &= ~TAIE; // disable overflow interrupt
}

/** startAlarm
 * start a oneshot alarm.
 * @param time when the alarm should occur.
 */
void Timer0_A5::startAlarm(uint16 time) {

	Registers()->TACCTL1 &= ~CCIFG; // reset pending interrupt
	Registers()->TACCR1 = time; // set compare register
	Registers()->TACCTL1 |= CCIE; // enable alarm interrupt

    if(this->isEnabled()) { Registers()->TACTL |= MC10; }// resume timer in continuous mode
}

/** stopAlarm
 * disable alarm notification.
 */
void Timer0_A5::stopAlarm() {
	/* deactivate interrupt */
	Registers()->TACCTL1 &= ~CCIE;
}

/** enable
 * PowerManager function
 * enables the Timer0_A5
 */
void Timer0_A5::enable()
{
	start();
}

/** disable
 * PowerManager function
 * disables the Timer0_A5
 */
void Timer0_A5::disable()
{
	stop();
}
