#ifndef REFLEX_MSP430X_IO_SD_CARD_H
#define REFLEX_MSP430X_IO_SD_CARD_H
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *	Class(es):	SD_Card
 *
 *	Author:		Stefan Nuernberger
 *
 *	Description: SD/MMC Card access through SPI. This wraps the
 *          extern "C" methods required by Petit FatFs (FileOutput.h)
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and
 *    Operating Systems. All rights reserved.
 *
 *    Redistribution and use in source and binary forms, with or without
 *    modification, are permitted provided that the following conditions
 *    are met:
 *
 *       1. Redistributions of source code must retain the above copyright
 *          notice, this list of conditions and the following disclaimer.
 *
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the
 *          distribution.
 *
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *    The views and conclusions contained in the software and documentation
 *    are those of the authors and should not be interpreted as representing
 *    official policies, either expressed or implied, of BTU Cottbus,
 *    Department for Distributed Systems and Operating Systems.
 *
 **/

#include "reflex/types.h"
#include "reflex/powerManagement/PowerManageAble.h"
#include "reflex/io/GenericDiskIO.h"


namespace reflex {
namespace msp430x {

template <typename TPort, uint8 POWERPIN, uint8 CSPIN, uint8 CLKPIN, uint8 DIPIN, uint8 DOPIN>
class SD_Card : public PowerManageAble, public GenericDiskIO
{
public:
    enum Pins {
         CS = 0x1 << CSPIN
        ,CLK = 0x1 << CLKPIN
        ,DI = 0x1 << DIPIN
        ,DOUT = 0x1 << DOPIN // 'DO' already used as a macro in PFF
        ,POWER = 0x1 << POWERPIN
    };

    SD_Card();

    /* init disk */
    virtual DSTATUS disk_initialize();
    /* write partial sector */
    virtual DRESULT disk_writep(const BYTE*, DWORD);
    /* read partial sector */
    virtual DRESULT disk_readp(BYTE*, DWORD, WORD, WORD);

    /* PM functionality, switch SD_Card On/Off */
    void enable();
    void disable();

private:
    /* Definitions for MMC/SDC command */
    enum Commands {
         CMD0	= (0x40+0)	/* GO_IDLE_STATE */
        ,CMD1	= (0x40+1)	/* SEND_OP_COND (MMC) */
        ,ACMD41	= (0xC0+41)	/* SEND_OP_COND (SDC) */
        ,CMD8	= (0x40+8)	/* SEND_IF_COND */
        ,CMD16	= (0x40+16)	/* SET_BLOCKLEN */
        ,CMD17	= (0x40+17)	/* READ_SINGLE_BLOCK */
        ,CMD24	= (0x40+24)	/* WRITE_BLOCK */
        ,CMD55	= (0x40+55)	/* APP_CMD */
        ,CMD58	= (0x40+58)	/* READ_OCR */
    };
    /* Card type flags (CardType) */
    enum CardTypes {
         CT_MMC		= 0x01	/* MMC ver 3 */
        ,CT_SD1		= 0x02	/* SD ver 1 */
        ,CT_SD2		= 0x04	/* SD ver 2 */
        ,CT_SDC		= (CT_SD1|CT_SD2)	/* SD */
        ,CT_BLOCK	= 0x08	/* Block addressing */
    };

    BYTE CardType;			/* b0:MMC, b1:SDv1, b2:SDv2, b3:Block addressing */

    /* Initialize MMC control port (CS/CLK/DI:output, DO:input) */
    inline void init_port() {
        // initialize Port
        TPort()->DIR |= (CS | CLK | DI); // make CS, CLK, DI output pins
        TPort()->IE &= ~(CS | CLK | DI | DOUT); // disable interrupts on all pins
        TPort()->DIR &= ~(DOUT); // make DO input pin
    }
    /* Delay n microseconds */
    inline void dly_us(unsigned int us) {
        us <<= 3; // approx. 8 "nop"s per us
        while (us--) {
            asm(" nop");
        }
    }
    inline void bset(uint8 pins) {
        TPort()->OUT |= pins;
        dly_us(20);
    }
    inline void bclr(uint8 pins) {
        TPort()->OUT &= ~pins;
        dly_us(20);
    }
    inline uint8 btest(uint8 pins) {
        return (TPort()->IN & pins);
        dly_us(20);
    }

    /* bit banging operations */
    void release_spi();
    BYTE send_cmd(BYTE, DWORD);
    void skip_mmc(WORD);
    BYTE rcvr_mmc();
    void xmit_mmc(BYTE);
    void forward(BYTE) {}; // not using in-time processing...
};

/*-------------------------------------------------------------------------*/
/* Platform dependent macros and functions needed to be modified           */
/*-------------------------------------------------------------------------*/

#define	INIT_PORT()	init_port()	/* Initialize MMC control port (CS/CLK/DI:output, DO:input) */
#define DLY_US(n)	dly_us(n)	/* Delay n microseconds */
#define	FORWARD(d)	forward(d)	/* Data in-time processing function (depends on the project) */

#define	CS_H()		bset(CS)	/* Set MMC CS "high" */
#define CS_L()		bclr(CS)	/* Set MMC CS "low" */
#define CK_H()		bset(CLK)	/* Set MMC SCLK "high" */
#define	CK_L()		bclr(CLK)	/* Set MMC SCLK "low" */
#define DI_H()		bset(DI)	/* Set MMC DI "high" */
#define DI_L()		bclr(DI)	/* Set MMC DI "low" */
#define DO			btest(DOUT)	/* Get MMC DO value (high:true, low:false) */


template <typename TPort, uint8 POWERPIN, uint8 CSPIN, uint8 CLKPIN, uint8 DIPIN, uint8 DOPIN>
SD_Card<TPort,POWERPIN,CSPIN,CLKPIN,DIPIN,DOPIN>::SD_Card() {
    DISK_IO = this; // set object pointer for wrappers
    setSleepMode(LPM4); // set deepest possible sleep mode
}


/* init disk */
template <typename TPort, uint8 POWERPIN, uint8 CSPIN, uint8 CLKPIN, uint8 DIPIN, uint8 DOPIN>
DSTATUS SD_Card<TPort,POWERPIN,CSPIN,CLKPIN,DIPIN,DOPIN>::disk_initialize()
{
    BYTE n, cmd, ty, buf[4];
    UINT tmr;


    INIT_PORT();

    CS_H();
    skip_mmc(10);			/* Dummy clocks */

    ty = 0;
    if (send_cmd(CMD0, 0) == 1) {			/* Enter Idle state */
        if (send_cmd(CMD8, 0x1AA) == 1) {	/* SDv2 */
            for (n = 0; n < 4; n++) buf[n] = rcvr_mmc();	/* Get trailing return value of R7 resp */
            if (buf[2] == 0x01 && buf[3] == 0xAA) {			/* The card can work at vdd range of 2.7-3.6V */
                for (tmr = 1000; tmr; tmr--) {				/* Wait for leaving idle state (ACMD41 with HCS bit) */
                    if (send_cmd(ACMD41, 1UL << 30) == 0) break;
                    DLY_US(1000);
                }
                if (tmr && send_cmd(CMD58, 0) == 0) {		/* Check CCS bit in the OCR */
                    for (n = 0; n < 4; n++) buf[n] = rcvr_mmc();
                    ty = (buf[0] & 0x40) ? CT_SD2 | CT_BLOCK : CT_SD2;	/* SDv2 (HC or SC) */
                }
            }
        } else {							/* SDv1 or MMCv3 */
            if (send_cmd(ACMD41, 0) <= 1) 	{
                ty = CT_SD1; cmd = ACMD41;	/* SDv1 */
            } else {
                ty = CT_MMC; cmd = CMD1;	/* MMCv3 */
            }
            for (tmr = 1000; tmr; tmr--) {			/* Wait for leaving idle state */
                if (send_cmd(ACMD41, 0) == 0) break;
                DLY_US(1000);
            }
            if (!tmr || send_cmd(CMD16, 512) != 0)			/* Set R/W block length to 512 */
                ty = 0;
        }
    }
    CardType = ty;
    release_spi();

    return ty ? 0 : STA_NOINIT;
}

/* read partial sector */
template <typename TPort, uint8 POWERPIN, uint8 CSPIN, uint8 CLKPIN, uint8 DIPIN, uint8 DOPIN>
DRESULT SD_Card<TPort,POWERPIN,CSPIN,CLKPIN,DIPIN,DOPIN>::disk_readp (
    BYTE *buff,		/* Pointer to the read buffer (NULL:Read bytes are forwarded to the stream) */
    DWORD lba,		/* Sector number (LBA) */
    WORD ofs,		/* Byte offset to read from (0..511) */
    WORD cnt		/* Number of bytes to read (ofs + cnt mus be <= 512) */
)
{
    DRESULT res;
    BYTE d;
    WORD bc, tmr;

    if (!(CardType & CT_BLOCK)) lba *= 512;		/* Convert to byte address if needed */

    res = RES_ERROR;
    if (send_cmd(CMD17, lba) == 0) {		/* READ_SINGLE_BLOCK */

        tmr = 1000;
        do {							/* Wait for data packet in timeout of 100ms */
            DLY_US(100);
            d = rcvr_mmc();
        } while (d == 0xFF && --tmr);

        if (d == 0xFE) {				/* A data packet arrived */
            bc = 514 - ofs - cnt;

            /* Skip leading bytes */
            if (ofs) skip_mmc(ofs);

            /* Receive a part of the sector */
            if (buff) {	/* Store data to the memory */
                do
                    *buff++ = rcvr_mmc();
                while (--cnt);
            } else {	/* Forward data to the outgoing stream */
                do {
                    d = rcvr_mmc();
                    FORWARD(d);
                } while (--cnt);
            }

            /* Skip trailing bytes and CRC */
            skip_mmc(bc);

            res = RES_OK;
        }
    }

    release_spi();

    return res;
}

/* write partial sector */
template <typename TPort, uint8 POWERPIN, uint8 CSPIN, uint8 CLKPIN, uint8 DIPIN, uint8 DOPIN>
DRESULT SD_Card<TPort,POWERPIN,CSPIN,CLKPIN,DIPIN,DOPIN>::disk_writep(const BYTE* buff, DWORD sa)
{
    DRESULT res;
    WORD bc, tmr;
    static WORD wc;


    res = RES_ERROR;

    if (buff) {		/* Send data bytes */
        bc = (WORD)sa;
        while (bc && wc) {		/* Send data bytes to the card */
            xmit_mmc(*buff++);
            wc--; bc--;
        }
        res = RES_OK;
    } else {
        if (sa) {	/* Initiate sector write process */
            if (!(CardType & CT_BLOCK)) sa *= 512;	/* Convert to byte address if needed */
            if (send_cmd(CMD24, sa) == 0) {			/* WRITE_SINGLE_BLOCK */
                xmit_mmc(0xFF); xmit_mmc(0xFE);		/* Data block header */
                wc = 512;							/* Set byte counter */
                res = RES_OK;
            }
        } else {	/* Finalize sector write process */
            bc = wc + 2;
            while (bc--) xmit_mmc(0);	/* Fill left bytes and CRC with zeros */
            if ((rcvr_mmc() & 0x1F) == 0x05) {	/* Receive data resp and wait for end of write process in timeout of 300ms */
                for (tmr = 10000; rcvr_mmc() != 0xFF && tmr; tmr--)	/* Wait for ready (max 1000ms) */
                    DLY_US(100);
                if (tmr) res = RES_OK;
            }
            release_spi();
        }
    }

    return res;
}

template <typename TPort, uint8 POWERPIN, uint8 CSPIN, uint8 CLKPIN, uint8 DIPIN, uint8 DOPIN>
void SD_Card<TPort,POWERPIN,CSPIN,CLKPIN,DIPIN,DOPIN>::enable()
{
    TPort()->SEL &= ~(CS | CLK | DI | DOUT | POWER); // set pins to I/O functionality
    TPort()->IE &= ~(CS | CLK | DI | DOUT | POWER); // disable interrupts on all pins
    TPort()->DIR |= (CS | CLK | DI | POWER); // make CS, CLK, DI, POWER output pins
    TPort()->DIR &= ~(DOUT); // make DO input pin
    TPort()->DS |= CS | CLK | POWER; // set high output drive strength
    TPort()->OUT |= POWER; // switch on POWER.
    dly_us(2000); // wait at least 1 ms
    TPort()->OUT |= (DI | CS); // set DI and CS high
    // apply at least 74 clock pulses (10 bytes)
    skip_mmc(10);

    // should be ready to use card now...
}

template <typename TPort, uint8 POWERPIN, uint8 CSPIN, uint8 CLKPIN, uint8 DIPIN, uint8 DOPIN>
void SD_Card<TPort,POWERPIN,CSPIN,CLKPIN,DIPIN,DOPIN>::disable()
{
    // switch off power
    TPort()->OUT &= ~POWER;
}

// bit banging operations ...

/*-----------------------------------------------------------------------*/
/* Transmit a byte to the MMC (bitbanging)                               */
/*-----------------------------------------------------------------------*/

template <typename TPort, uint8 POWERPIN, uint8 CSPIN, uint8 CLKPIN, uint8 DIPIN, uint8 DOPIN>
void SD_Card<TPort,POWERPIN,CSPIN,CLKPIN,DIPIN,DOPIN>::xmit_mmc (
    BYTE d			/* Data to be sent */
)
{
    if (d & 0x80) DI_H(); else DI_L();	/* bit7 */
    CK_H(); CK_L();
    if (d & 0x40) DI_H(); else DI_L();	/* bit6 */
    CK_H(); CK_L();
    if (d & 0x20) DI_H(); else DI_L();	/* bit5 */
    CK_H(); CK_L();
    if (d & 0x10) DI_H(); else DI_L();	/* bit4 */
    CK_H(); CK_L();
    if (d & 0x08) DI_H(); else DI_L();	/* bit3 */
    CK_H(); CK_L();
    if (d & 0x04) DI_H(); else DI_L();	/* bit2 */
    CK_H(); CK_L();
    if (d & 0x02) DI_H(); else DI_L();	/* bit1 */
    CK_H(); CK_L();
    if (d & 0x01) DI_H(); else DI_L();	/* bit0 */
    CK_H(); CK_L();
}



/*-----------------------------------------------------------------------*/
/* Receive a byte from the MMC (bitbanging)                              */
/*-----------------------------------------------------------------------*/

template <typename TPort, uint8 POWERPIN, uint8 CSPIN, uint8 CLKPIN, uint8 DIPIN, uint8 DOPIN>
BYTE SD_Card<TPort,POWERPIN,CSPIN,CLKPIN,DIPIN,DOPIN>::rcvr_mmc (void)
{
    BYTE r;


    DI_H();	/* Send 0xFF */

    r = 0;   if (DO) r++;	/* bit7 */
    CK_H(); CK_L();
    r <<= 1; if (DO) r++;	/* bit6 */
    CK_H(); CK_L();
    r <<= 1; if (DO) r++;	/* bit5 */
    CK_H(); CK_L();
    r <<= 1; if (DO) r++;	/* bit4 */
    CK_H(); CK_L();
    r <<= 1; if (DO) r++;	/* bit3 */
    CK_H(); CK_L();
    r <<= 1; if (DO) r++;	/* bit2 */
    CK_H(); CK_L();
    r <<= 1; if (DO) r++;	/* bit1 */
    CK_H(); CK_L();
    r <<= 1; if (DO) r++;	/* bit0 */
    CK_H(); CK_L();

    return r;
}



/*-----------------------------------------------------------------------*/
/* Skip bytes on the MMC (bitbanging)                                    */
/*-----------------------------------------------------------------------*/

template <typename TPort, uint8 POWERPIN, uint8 CSPIN, uint8 CLKPIN, uint8 DIPIN, uint8 DOPIN>
void SD_Card<TPort,POWERPIN,CSPIN,CLKPIN,DIPIN,DOPIN>::skip_mmc (
    WORD n		/* Number of bytes to skip */
)
{
    DI_H();	/* Send 0xFF */

    do {
        CK_H(); CK_L();
        CK_H(); CK_L();
        CK_H(); CK_L();
        CK_H(); CK_L();
        CK_H(); CK_L();
        CK_H(); CK_L();
        CK_H(); CK_L();
        CK_H(); CK_L();
    } while (--n);
}



/*-----------------------------------------------------------------------*/
/* Deselect the card and release SPI bus                                 */
/*-----------------------------------------------------------------------*/

template <typename TPort, uint8 POWERPIN, uint8 CSPIN, uint8 CLKPIN, uint8 DIPIN, uint8 DOPIN>
void SD_Card<TPort,POWERPIN,CSPIN,CLKPIN,DIPIN,DOPIN>::release_spi()
{
    CS_H();
    rcvr_mmc();
}


/*-----------------------------------------------------------------------*/
/* Send a command packet to MMC                                          */
/*-----------------------------------------------------------------------*/

template <typename TPort, uint8 POWERPIN, uint8 CSPIN, uint8 CLKPIN, uint8 DIPIN, uint8 DOPIN>
BYTE SD_Card<TPort,POWERPIN,CSPIN,CLKPIN,DIPIN,DOPIN>::send_cmd (
    BYTE cmd,		/* Command byte */
    DWORD arg		/* Argument */
)
{
    BYTE n, res;


    if (cmd & 0x80) {	/* ACMD<n> is the command sequense of CMD55-CMD<n> */
        cmd &= 0x7F;
        res = send_cmd(CMD55, 0);
        if (res > 1) return res;
    }

    /* Select the card */
    CS_H(); rcvr_mmc();
    CS_L(); rcvr_mmc();

    /* Send a command packet */
    xmit_mmc(cmd);					/* Start + Command index */
    xmit_mmc((BYTE)(arg >> 24));	/* Argument[31..24] */
    xmit_mmc((BYTE)(arg >> 16));	/* Argument[23..16] */
    xmit_mmc((BYTE)(arg >> 8));		/* Argument[15..8] */
    xmit_mmc((BYTE)arg);			/* Argument[7..0] */
    n = 0x01;						/* Dummy CRC + Stop */
    if (cmd == CMD0) n = 0x95;		/* Valid CRC for CMD0(0) */
    if (cmd == CMD8) n = 0x87;		/* Valid CRC for CMD8(0x1AA) */
    xmit_mmc(n);

    /* Receive a command response */
    n = 10;								/* Wait for a valid response in timeout of 10 attempts */
    do {
        res = rcvr_mmc();
    } while ((res & 0x80) && --n);

    return res;			/* Return with the response value */
}


} // ns msp430x
} // ns reflex

#endif // REFLEX_MSP430X_IO_SD_CARD_H
