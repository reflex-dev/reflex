/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 **/

#ifndef REFLEX_MSP430X_IO_PORTS_H
#define REFLEX_MSP430X_IO_PORTS_H

#include "reflex/mcu/msp430x.h"
#include "reflex/io/Registers.h"
#include "reflex/interrupts/InterruptDispatcher.h"
#include "reflex/pmc/Registers.h"

namespace reflex{
namespace msp430x{

/*! base class for ports with port mapping controller registers
 */
template<uint8 port_nr>
class MappablePort
{
public:
	void map(uint8 pin, uint8 function) {
		/* configure port mapping */
		pmc::Registers()->PMAPPWD = pmc::PASSWD; // PMC write-access
		pmc::Port<port_nr>()->MAP[pin] = function; // set function
		pmc::Registers()->PMAPPWD = 0; // Lock port mapping registers
	}
};

class Port1 :
	public io::PortRegistersEven,
	public MappablePort<1>
{
	typedef data_types::ReadOnly<data_types::Register<uint16> > RO_Register;
public:
	struct Interrupt_traits {
		enum Vector {
			PIN0 = interrupts::IV0 /// interrupt vector for pin 0
			,PIN1 = interrupts::IV1 /// interrupt vector for pin 1
			,PIN2 = interrupts::IV2 /// interrupt vector for pin 2
			,PIN3 = interrupts::IV3 /// interrupt vector for pin 3
			,PIN4 = interrupts::IV4 /// interrupt vector for pin 4
			,PIN5 = interrupts::IV5 /// interrupt vector for pin 5
			,PIN6 = interrupts::IV6 /// interrupt vector for pin 6
			,PIN7 = interrupts::IV7 /// interrupt vector for pin 7
		};
		enum {COUNT=9};

		typedef interrupts::IVRef<Port1> VectorRef;

		inline static InterruptVector globalVector() {return interrupts::PORT1;}
		inline static Vector localVector(const unsigned& i) { return static_cast<Vector>(i+1); }
	};

	typedef reflex::InterruptDispatcher< Interrupt_traits > IVDispatcher;

public:
	uint16 :16;
	RO_Register	IV;
	uint16: 16;
	uint16: 16;
	uint16: 16;
	uint16: 16;
	Register	IES;
	uint8 :8;
	Register	IE;
	uint8 :8;
	Register	IFG;
public:
	Port1(){}
	operator Port1*() {return operator->();}
	Port1* operator-> () {return reinterpret_cast<Port1*>(bases::PORT1+offsets::PORT1);}
};


class Port2 :
	public io::PortRegistersOdd, //cause those starts on an odd address
	public MappablePort<2>
{
	typedef data_types::ReadOnly<data_types::Register<uint16> > RO_Register;
public:
	struct Interrupt_traits {
		enum Vector {
			PIN0 = interrupts::IV0 /// interrupt vector for pin 0
			,PIN1 = interrupts::IV1 /// interrupt vector for pin 1
			,PIN2 = interrupts::IV2 /// interrupt vector for pin 2
			,PIN3 = interrupts::IV3 /// interrupt vector for pin 3
			,PIN4 = interrupts::IV4 /// interrupt vector for pin 4
			,PIN5 = interrupts::IV5 /// interrupt vector for pin 5
			,PIN6 = interrupts::IV6 /// interrupt vector for pin 6
			,PIN7 = interrupts::IV7 /// interrupt vector for pin 7
		};
		enum {	COUNT=9 };//size of the vector includes the zero for performance issues

		typedef interrupts::IVRef<Port2> VectorRef;

		inline static InterruptVector globalVector() {return interrupts::PORT2;}
		inline static Vector localVector(const unsigned& i) { return static_cast<Vector>(i+1); }
	};

	typedef reflex::InterruptDispatcher<Interrupt_traits > IVDispatcher;

public:
	uint16	:8;
	uint16	:16;
	uint16	:16;
	uint16	:16;
	uint16	:16;
	uint16	:16;
	uint16	:16;
	Register	IES;
	uint8 :8;
	Register	IE;
	uint8 :8;
	Register	IFG;
	RO_Register	IV;
public:
	Port2(){}
	operator Port2*() {return operator->();}
	Port2* operator-> () {return reinterpret_cast<Port2*>(bases::PORT2+offsets::PORT2);}
};


class Port3 :
	public io::PortRegistersEven,
	public MappablePort<3>
{
public:
	Port3(){}
	operator Port3*() {return operator->();}
	Port3* operator-> () {return reinterpret_cast<Port3*>(bases::PORT3+offsets::PORT3);}
};


class Port4
	: public io::PortRegistersOdd
{
public:
	Port4(){}
	operator Port4*() {return operator->();}
	Port4* operator-> () {return reinterpret_cast<Port4*>(bases::PORT4+offsets::PORT4);}
};


class Port5
	: public io::PortRegistersEven
{
public:
	Port5(){}
	operator Port5*() {return operator->();}
	Port5* operator-> () {return reinterpret_cast<Port5*>(bases::PORT5+offsets::PORT5);}
};


class PortJ
{
protected:
	typedef data_types::Register<uint16> Register;
	typedef data_types::ReadOnly<Register> RO_Register;
public:
	PortJ(){}
	operator PortJ*() {return operator->();}
	PortJ* operator-> () {return reinterpret_cast<PortJ*>(bases::PORTJ+offsets::PORTJ);}
public:
	RO_Register IN;
	Register OUT;
	Register DIR;
	Register REN;
	Register DS;
};

} // msp430x
} // reflex

#endif // PORTS_H
