/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 **/

#ifndef REFLEX_MCU_CC430X613X
#define REFLEX_MCU_CC430X613X
#include "reflex/sinks/Sink.h"

namespace reflex{
namespace msp430x {
namespace cc430x613x {
namespace bases{

enum Address {
	 SPECIALFUNCTION=	0x100 ///< Special Functions
	,PMM=			0x120 ///< Power Management Module and Supply Voltage supervisior
	,FLASHCTL=		0x140 ///< Flash Controller
	,CRC16=			0x150 ///< CRC Module
	,RAMCTL=		0x158 ///
	,WDT_A=			0x15C ///< Watchdog Timer A
	,UCS=			0x160 ///< Unified Clock System
	,SYS=			0x180 ///< System Module
	,REF=			0x1B0 ///< Shared Reference
	,PMC=			0x1C0 ///< Port Mapping Controller
	,PMCMAP=		0x1C8 ///< Map of first Port, consecutive portmaps are used
	,PORTMAP1=		0x1C8 ///< Port Mapping Port 1
	,PORTMAP2=		0x1D0 ///< Port Mapping Port 2
	,PORTMAP3=		0x1D8 ///< Port Mapping Port 3
	,PORT1=			0x200 ///< Port 1
	/// due to the creation of wrong expensive code we will start at even addresses with other types
	,PORT2=			0x200 ///< Port 2
	,PORT3=			0x220 ///< Port 3
	,PORT4=			0x220 ///< Port 4
	,PORT5=			0x240 ///< Port 5
	,PORTJ=			0x320 ///< Port J
	,TIMER0_A5=		0x340 ///< Timer0_A5
	,TIMER1_A3=		0x380 ///< Timer1_A3
	,RTC_A=			0x4A0 ///< Real Time Clock
	,DMACTL=		0x500 ///< DMA general controller
	,DMA_CHANNEL_0=		0x500 ///< DMA Channel 0
	,DMA_CHANNEL_1=		0x500 ///< DMA Channel 1
	,DMA_CHANNEL_2=		0x500 ///< DMA Channel 2
	,USCI0=			0x5c0 ///< Universal Serial Communication Interface (A and B)
	,ADC12_A=		0x700 ///< 12Bit Analog digital converter
	,LCD_B=			0xA00 ///< LCD_B Controller
	,RF1A=			0xF00 ///< CC1101 Radio Interface
};
}//ns bases
typedef bases::Address BaseAddress; ///< type of baseaddresses

namespace offsets {
/** the offset where the first register starts behind the baseaddress.
*/
enum Offset {
	SFR = 0x0
	,WDT_A =0x0 //
	,SYS = 0x0
	,REF= 0x0
	,USCIA0=0x0
	,USCIB0=0x20
	,PMM=0x0
	,PMC= 0x0
	,LCD_B=0x0
	,LCD_B_MEM=0x20
	,LCD_B_BLINK_MEM=0x40
	,UCS=0x0
	,ADC12_A = 0x0
	,PORT1=0x0
	,PORT2=0x0
	,PORT3=0x0
	,PORT4=0x0
	,PORT5=0x0
	,PORTJ=0x0
	,RTC_A=0x0
	,TIMER0_A5=0x0
	,TIMER1_A3=0x0
	,FLASHCTL=0x0
	,RF1A=0x0
};
}//ns offsets
typedef offsets::Offset BaseOffset;

namespace interrupts {
/**
 * It is just a nummeration of the possible Interrupts,
 * each Interrupt get its own number, so InterruptGuardian::handle
 * can identify, which handler is called.
 */
enum Vector {
	INTERRUPT_TABLE	= 0xFFDA  ///< lowest adr of interrupt table
	,INVALID	= -1 ///< no interrupt, only a useful dummy
	,AES		= 0 ///
	,RTC_A		= 1 ///
	,LCD_B		= 2 ///
	,PORT2		= 3 ///< Port1 IV (indirect)
	,PORT1		= 4 ///< Port2 IV (indirect)
	,TIMER1_A3_0	= 5 ///< TIMER1_A3 CCR1, CCR2, Overflow (indirect)
	,TIMER1_A3_1	= 6 ///< TIMER1_A3 CCR0
	,DMA		= 7 ///
	,RF1A		= 8 ///
	,TIMER0_A5_0	= 9 ///< TIMER0_A5 CCR1-CCR4, Overflow (indirect)
	,TIMER0_A5_1	= 10 ///< TIMER0_A5 CCR0
	,ADC12_A	= 11 ///
	,USCIB0		= 12 /// USCI_B0 (indirect)
	,USCIA0		= 13 /// USCI_A0 (indirect)
	,WDTM		= 14 ///< Watchdog timer mode
	,COMP_B		= 15 /// Voltage comparator
	,USER_NMI	= 16 ///
	,SYS_NMI	= 17 ///
	,RESET		= 18 ///< highest, RESET (Watchdog,power up...)
	,MAX_HANDLERS	= 19 ///< No interrupt, only number of interrupts
};

enum LogVector {
	IV0=1
	,IV1=2
	,IV2=3
	,IV3=4
	,IV4=5
	,IV5=6
	,IV6=7
	,IV7=8
	,IV8=9
	,IV9=10
	,IV10=11
	,IV11=12
	,IV12=13
	,IV13=14
	,IV14=15
	,IV15=16
	,IV16=17
	,IV17=18
	,IV18=19
	,IV19=20
};

//! iterator to a interrupt vector
/*!	dereferencing this type gives the value of the interrupt vector register
	owing to the fact, that the most hardware modules of the msp430x cpu have got
	2-way indirection  for their interrupts. The 1st way goes to the hardware module
	and the 2nd to a hwmodule specific vector. e.g. TXEND for the UART module
*/
template<typename T>
struct IVRef {
	word operator*() const { return (T()->IV)/*&(T::IV_MASK)*/;}
};

//! VecIterator
/*!
 */
template<typename TIVRegRef>
class VecIterator {
	typedef Sink0* value_type;
	value_type* vec;
public:
	/*!	creates iterator that knows how to efficiently walk through the logical interrupt vector
	*/
	explicit VecIterator(value_type* vec): vec(vec){}

	value_type operator*() {
		return *reinterpret_cast<value_type*>( reinterpret_cast<word>(vec) + *TIVRegRef());
	}
};

}//ns interrupts
}//ns cc430x613x

namespace cpu=cc430x613x;

namespace interrupts=cpu::interrupts;
namespace bases=cpu::bases;
namespace offsets=cpu::offsets;

//for compatibility issues
typedef interrupts::Vector InterruptVector;

} } //ns msp430, reflex

#endif
