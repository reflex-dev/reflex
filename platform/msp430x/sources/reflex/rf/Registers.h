/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 **/

#ifndef REFLEX_RF1A_REGISTERS_H
#define REFLEX_RF1A_REGISTERS_H

#include "reflex/mcu/msp430x.h"
#include "reflex/types.h"
#include "reflex/data_types/Register.h"

namespace reflex {
namespace msp430x {
namespace rf1A {

 enum RegisterBITS {
	 // RF1AIFCTL0 Control Bits
	 RFFIFOEN                = 0x0001       // CC1101 Direct FIFO access enable
	 ,RFENDIAN               = 0x0002       // CC1101 Disable endianness conversion
	 // RF1AIFCTL0 Control Bits
	 ,RFFIFOEN_L             = 0x0001       // CC1101 Direct FIFO access enable
	 ,RFENDIAN_L             = 0x0002       // CC1101 Disable endianness conversion
	 // RF1AIFCTL0 Control Bits
	 // RF1AIFCTL1 Control Bits
	 ,RFRXIFG                = 0x0001       // Radio interface direct FIFO access receive interrupt flag
	 ,RFTXIFG                = 0x0002       // Radio interface direct FIFO access transmit interrupt flag
	 ,RFERRIFG               = 0x0004       // Radio interface error interrupt flag
	 ,RFINSTRIFG             = 0x0010       // Radio interface instruction interrupt flag
	 ,RFDINIFG               = 0x0020       // Radio interface data in interrupt flag
	 ,RFSTATIFG              = 0x0040       // Radio interface status interrupt flag
	 ,RFDOUTIFG              = 0x0080       // Radio interface data out interrupt flag
	 ,RFRXIE                 = 0x0100       // Radio interface direct FIFO access receive interrupt enable
	 ,RFTXIE                 = 0x0200       // Radio interface direct FIFO access transmit interrupt enable
	 ,RFERRIE                = 0x0400       // Radio interface error interrupt enable
	 ,RFINSTRIE              = 0x1000       // Radio interface instruction interrupt enable
	 ,RFDINIE                = 0x2000       // Radio interface data in interrupt enable
	 ,RFSTATIE               = 0x4000       // Radio interface status interrupt enable
	 ,RFDOUTIE               = 0x8000       // Radio interface data out interrupt enable
	 // RF1AIFCTL1 Control Bits
	 ,RFRXIFG_L              = 0x0001       // Radio interface direct FIFO access receive interrupt flag
	 ,RFTXIFG_L              = 0x0002       // Radio interface direct FIFO access transmit interrupt flag
	 ,RFERRIFG_L             = 0x0004       // Radio interface error interrupt flag
	 ,RFINSTRIFG_L           = 0x0010       // Radio interface instruction interrupt flag
	 ,RFDINIFG_L             = 0x0020       // Radio interface data in interrupt flag
	 ,RFSTATIFG_L            = 0x0040       // Radio interface status interrupt flag
	 ,RFDOUTIFG_L            = 0x0080       // Radio interface data out interrupt flag
	 // RF1AIFCTL1 Control Bits
	 ,RFRXIE_H               = 0x0001       // Radio interface direct FIFO access receive interrupt enable
	 ,RFTXIE_H               = 0x0002       // Radio interface direct FIFO access transmit interrupt enable
	 ,RFERRIE_H              = 0x0004       // Radio interface error interrupt enable
	 ,RFINSTRIE_H            = 0x0010       // Radio interface instruction interrupt enable
	 ,RFDINIE_H              = 0x0020       // Radio interface data in interrupt enable
	 ,RFSTATIE_H             = 0x0040       // Radio interface status interrupt enable
	 ,RFDOUTIE_H             = 0x0080       // Radio interface data out interrupt enable
	 // RF1AIFERR Control Bits
	 ,LVERR                  = 0x0001       // Low Core Voltage Error Flag
	 ,OPERR                  = 0x0002       // Operand Error Flag
	 ,OUTERR                 = 0x0004       // Output data not available Error Flag
	 ,OPOVERR                = 0x0008       // Operand Overwrite Error Flag
	 // RF1AIFERR Control Bits
	 ,LVERR_L                = 0x0001       // Low Core Voltage Error Flag
	 ,OPERR_L                = 0x0002       // Operand Error Flag
	 ,OUTERR_L               = 0x0004       // Output data not available Error Flag
	 ,OPOVERR_L              = 0x0008       // Operand Overwrite Error Flag
	 // RF1AIFERR Control Bits
	 // RF1AIFERRV Definitions
	 ,RF1AIFERRV_NONE        = 0x0000       // No Error pending
	 ,RF1AIFERRV_LVERR       = 0x0002       // Low core voltage error
	 ,RF1AIFERRV_OPERR       = 0x0004       // Operand Error
	 ,RF1AIFERRV_OUTERR      = 0x0006       // Output data not available Error
	 ,RF1AIFERRV_OPOVERR     = 0x0008       // Operand Overwrite Error
	 // RF1AIFIV Definitions
	 ,RF1AIFIV_NONE          = 0x0000       // No Interrupt pending
	 ,RF1AIFIV_RFERRIFG      = 0x0002       // Radio interface error
	 ,RF1AIFIV_RFDOUTIFG     = 0x0004       // Radio i/f data out
	 ,RF1AIFIV_RFSTATIFG     = 0x0006       // Radio i/f status out
	 ,RF1AIFIV_RFDINIFG      = 0x0008       // Radio i/f data in
	 ,RF1AIFIV_RFINSTRIFG    = 0x000A       // Radio i/f instruction in
	 ,RF1AIFIV_RFRXIFG       = 0x000C       // Radio direct FIFO RX
	 ,RF1AIFIV_RFTXIFG       = 0x000E       // Radio direct FIFO TX
	 // RF1AIV Definitions
	 ,RF1AIV_NONE            = 0x0000       // No Interrupt pending
	 ,RF1AIV_RFIFG0          = 0x0002       // RFIFG0
	 ,RF1AIV_RFIFG1          = 0x0004       // RFIFG1
	 ,RF1AIV_RFIFG2          = 0x0006       // RFIFG2
	 ,RF1AIV_RFIFG3          = 0x0008       // RFIFG3
	 ,RF1AIV_RFIFG4          = 0x000A       // RFIFG4
	 ,RF1AIV_RFIFG5          = 0x000C       // RFIFG5
	 ,RF1AIV_RFIFG6          = 0x000E       // RFIFG6
	 ,RF1AIV_RFIFG7          = 0x0010       // RFIFG7
	 ,RF1AIV_RFIFG8          = 0x0012       // RFIFG8
	 ,RF1AIV_RFIFG9          = 0x0014       // RFIFG9
	 ,RF1AIV_RFIFG10         = 0x0016       // RFIFG10
	 ,RF1AIV_RFIFG11         = 0x0018       // RFIFG11
	 ,RF1AIV_RFIFG12         = 0x001A       // RFIFG12
	 ,RF1AIV_RFIFG13         = 0x001C       // RFIFG13
	 ,RF1AIV_RFIFG14         = 0x001E       // RFIFG14
	 ,RF1AIV_RFIFG15         = 0x0020       // RFIFG15
 };

 enum CoreRegisters {
	 // configuration registers */
	 IOCFG2       = 0x00      /*  IOCFG2   - GDO2 output pin configuration  */
	 ,IOCFG1      = 0x01      /*  IOCFG1   - GDO1 output pin configuration  */
	 ,IOCFG0      = 0x02      /*  IOCFG1   - GDO0 output pin configuration  */
	 ,FIFOTHR     = 0x03      /*  FIFOTHR  - RX FIFO and TX FIFO thresholds */
	 ,SYNC1       = 0x04      /*  SYNC1    - Sync word, high byte */
	 ,SYNC0       = 0x05      /*  SYNC0    - Sync word, low byte */
	 ,PKTLEN      = 0x06      /*  PKTLEN   - Packet length */
	 ,PKTCTRL1    = 0x07      /*  PKTCTRL1 - Packet automation control */
	 ,PKTCTRL0    = 0x08      /*  PKTCTRL0 - Packet automation control */
	 ,ADDR        = 0x09      /*  ADDR     - Device address */
	 ,CHANNR      = 0x0A      /*  CHANNR   - Channel number */
	 ,FSCTRL1     = 0x0B      /*  FSCTRL1  - Frequency synthesizer control */
	 ,FSCTRL0     = 0x0C      /*  FSCTRL0  - Frequency synthesizer control */
	 ,FREQ2       = 0x0D      /*  FREQ2    - Frequency control word, high byte */
	 ,FREQ1       = 0x0E      /*  FREQ1    - Frequency control word, middle byte */
	 ,FREQ0       = 0x0F      /*  FREQ0    - Frequency control word, low byte */
	 ,MDMCFG4     = 0x10      /*  MDMCFG4  - Modem configuration */
	 ,MDMCFG3     = 0x11      /*  MDMCFG3  - Modem configuration */
	 ,MDMCFG2     = 0x12      /*  MDMCFG2  - Modem configuration */
	 ,MDMCFG1     = 0x13      /*  MDMCFG1  - Modem configuration */
	 ,MDMCFG0     = 0x14      /*  MDMCFG0  - Modem configuration */
	 ,DEVIATN     = 0x15      /*  DEVIATN  - Modem deviation setting */
	 ,MCSM2       = 0x16      /*  MCSM2    - Main Radio Control State Machine configuration */
	 ,MCSM1       = 0x17      /*  MCSM1    - Main Radio Control State Machine configuration */
	 ,MCSM0       = 0x18      /*  MCSM0    - Main Radio Control State Machine configuration */
	 ,FOCCFG      = 0x19      /*  FOCCFG   - Frequency Offset Compensation configuration */
	 ,BSCFG       = 0x1A      /*  BSCFG    - Bit Synchronization configuration */
	 ,AGCCTRL2    = 0x1B      /*  AGCCTRL2 - AGC control */
	 ,AGCCTRL1    = 0x1C      /*  AGCCTRL1 - AGC control */
	 ,AGCCTRL0    = 0x1D      /*  AGCCTRL0 - AGC control */
	 ,WOREVT1     = 0x1E      /*  WOREVT1  - High byte Event0 timeout */
	 ,WOREVT0     = 0x1F      /*  WOREVT0  - Low byte Event0 timeout */
	 ,WORCTRL     = 0x20      /*  WORCTRL  - Wake On Radio control */
	 ,FREND1      = 0x21      /*  FREND1   - Front end RX configuration */
	 ,FREND0      = 0x22      /*  FREDN0   - Front end TX configuration */
	 ,FSCAL3      = 0x23      /*  FSCAL3   - Frequency synthesizer calibration */
	 ,FSCAL2      = 0x24      /*  FSCAL2   - Frequency synthesizer calibration */
	 ,FSCAL1      = 0x25      /*  FSCAL1   - Frequency synthesizer calibration */
	 ,FSCAL0      = 0x26      /*  FSCAL0   - Frequency synthesizer calibration */
	 ,RCCTRL1     = 0x27      /*  RCCTRL1  - RC oscillator configuration */
	 ,RCCTRL0     = 0x28      /*  RCCTRL0  - RC oscillator configuration */
	 ,FSTEST      = 0x29      /*  FSTEST   - Frequency synthesizer calibration control */
	 ,PTEST       = 0x2A      /*  PTEST    - Production test */
	 ,AGCTEST     = 0x2B      /*  AGCTEST  - AGC test */
	 ,TEST2       = 0x2C      /*  TEST2    - Various test settings */
	 ,TEST1       = 0x2D      /*  TEST1    - Various test settings */
	 ,TEST0       = 0x2E      /*  TEST0    - Various test settings */
	 /* status registers */
	 ,PARTNUM     = 0x30      /*  PARTNUM    - Chip ID */
	 ,VERSION     = 0x31      /*  VERSION    - Chip ID */
	 ,FREQEST     = 0x32      /*  FREQEST    � Frequency Offset Estimate from demodulator */
	 ,LQI         = 0x33      /*  LQI        � Demodulator estimate for Link Quality */
	 ,RSSI        = 0x34      /*  RSSI       � Received signal strength indication */
	 ,MARCSTATE   = 0x35      /*  MARCSTATE  � Main Radio Control State Machine state */
	 ,WORTIME1    = 0x36      /*  WORTIME1   � High byte of WOR time */
	 ,WORTIME0    = 0x37      /*  WORTIME0   � Low byte of WOR time */
	 ,PKTSTATUS   = 0x38      /*  PKTSTATUS  � Current GDOx status and packet status */
	 ,VCO_VC_DAC  = 0x39      /*  VCO_VC_DAC � Current setting from PLL calibration module */
	 ,TXBYTES     = 0x3A      /*  TXBYTES    � Underflow and number of bytes */
	 ,RXBYTES     = 0x3B      /*  RXBYTES    � Overflow and number of bytes */
	 /*PA tabel */
	 ,PATABLE     = 0x3E      //  PATABLE - PA control settings table    hm, ich packs mal hier rein
 };


//@author menzehan:
	/** Registerfile for the Unified Clock System module
	 */
 struct Registers
  {
     data_types::Register<uint16> RF1AIFCTL0; // Radio interface control register 0
     //volatile unsigned int RF1AIFCTL1; // Radio interface control register 1
     data_types::Register<uint16> RF1AIFCTL1;
     //#define  RF1AIFIFG              RF1AIFCTL1_L   // Radio interface interrupt flag register
     //#define  RF1AIFIE               RF1AIFCTL1_H   // Radio interface interrupt enable register

     uint16 :16;                        //dummy
     data_types::Register<uint16> RF1AIFERR; // Radio interface error flag register
     uint16 :16;
     uint16 :16;
     data_types::Register<uint16> RF1AIFERRV; // Radio interface error vector word register
     data_types::Register<uint16> RF1AIFIV; // Radio interface interrupt vector word register
     //volatile unsigned int RF1AINSTRW; // Radio instruction word register
     data_types::Register<uint16> RF1AINSTRW;
     //#define  RF1ADINB               RF1AINSTRW_L   // Radio byte data in register
     //#define  RF1AINSTRB             RF1AINSTRW_H   // Radio instruction byte register
     //volatile unsigned int RF1AINSTR1W; // Radio instruction 1-byte register with autoread
     data_types::Register<uint16> RF1AINSTR1W;
     //#define  RF1AINSTR1B           RF1AINSTR1W_H  // Radio instruction 1-byte register with autoread
     //volatile unsigned int RF1AINSTR2W; // Radio instruction 2-byte register with autoread
     data_types::Register<uint16> RF1AINSTR2W;
     //#define  RF1AINSTR2B           RF1AINSTR1W_H  // Radio instruction 2-byte register with autoread
     //volatile unsigned int RF1ADINW; // Radio word data in register
     data_types::Register<uint16> RF1ADINW;
     //volatile unsigned int RF1ASTAT0W; // Radio status word register without auto-read
     uint16 :16;
     uint16 :16;
     uint16 :16;
     uint16 :16;
     data_types::Register<uint16> RF1ASTAT0W;
     //#define  RF1ADOUT0B            RF1ASTAT0W_L   // Radio byte data out register without auto-read
     //#define  RF1ASTAT0B            RF1ASTAT0W_H   // Radio status byte register without auto-read
     //#define  RF1ASTATW             RF1ASTAT0W     // Radio status word register without auto-read
     //#define  RF1ADOUTB             RF1ASTAT0W_L   // Radio byte data out register without auto-read
     //#define  RF1ASTATB             RF1ASTAT0W_H   // Radio status byte register without auto-read
     //volatile unsigned int RF1ASTAT1W; // Radio status word register with 1-byte auto-read
     data_types::Register<uint16> RF1ASTAT1W;
     //#define  RF1ADOUT1B            RF1ASTAT1W_L   // Radio byte data out register with 1-byte auto-read
     //#define  RF1ASTAT1B            RF1ASTAT1W_H   // Radio status byte register with 1-byte auto-read
     //volatile unsigned int RF1ASTAT2W; // Radio status word register with 2-byte auto-read
     data_types::Register<uint16> RF1ASTAT2W;
     //#define  RF1ADOUT2B            RF1ASTAT2W_L   // Radio byte data out register with 2-byte auto-read
     //#define  RF1ASTAT2B            RF1ASTAT2W_H   // Radio status byte register with 2-byte auto-read

     uint16 :16;
     data_types::Register<uint16> RF1ADOUT0W; // Radio core word data out register without auto-read
     //#define  RF1ADOUTW             RF1ADOUT0W     // Radio core word data out register without auto-read
     //#define  RF1ADOUTW_L           RF1ADOUT0W_L   // Radio core word data out register without auto-read
     //#define  RF1ADOUTW_H           RF1ADOUT0W_H   // Radio core word data out register without auto-read
     data_types::Register<uint16> RF1ADOUT1W;// Radio core word data out register with 1-byte auto-read
     data_types::Register<uint16> RF1ADOUT2W; // Radio core word data out register with 2-byte auto-read
     uint16 :16;
     data_types::Register<uint16> RF1AIN; // Radio core signal input register
     data_types::Register<uint16> RF1AIFG; // Radio core interrupt flag register
     data_types::Register<uint16> RF1AIES; // Radio core interrupt edge select register
     data_types::Register<uint16> RF1AIE; // Radio core interrupt enable register
     data_types::Register<uint16> RF1AIV; // Radio core interrupt vector word register
     uint16 :16;
     uint16 :16;
     uint16 :16;
//     volatile unsigned int RF1ARXFIFO; // Direct receive FIFO access register
//     volatile unsigned int RF1ATXFIFO; // Direct transmit FIFO access register



  public:
      Registers(){}
      operator Registers*() {return operator->();}
      Registers* operator-> () {return reinterpret_cast<Registers*>(bases::RF1A+offsets::RF1A);}
   };
//	struct Registers
//	{
//	   volatile unsigned int RF1AIFCTL0; // Radio interface control register 0
//	   //volatile unsigned int RF1AIFCTL1; // Radio interface control register 1
//	   data_types::Register<uint16> RF1AIFCTL1;
//	   //#define  RF1AIFIFG              RF1AIFCTL1_L   // Radio interface interrupt flag register
//	   //#define  RF1AIFIE               RF1AIFCTL1_H   // Radio interface interrupt enable register
//
//	   volatile unsigned int RF1AIFCTL2; // (Radio interface control register 2)
//	   volatile unsigned int RF1AIFERR; // Radio interface error flag register
//	   uint16 :16;
//	   uint16 :16;
//	   volatile unsigned int RF1AIFERRV; // Radio interface error vector word register
//	   volatile unsigned int RF1AIFIV; // Radio interface interrupt vector word register
//	   //volatile unsigned int RF1AINSTRW; // Radio instruction word register
//	   data_types::Register<uint16> RF1AINSTRW;
//	   //#define  RF1ADINB               RF1AINSTRW_L   // Radio byte data in register
//	   //#define  RF1AINSTRB             RF1AINSTRW_H   // Radio instruction byte register
//	   //volatile unsigned int RF1AINSTR1W; // Radio instruction 1-byte register with autoread
//	   data_types::Register<uint16> RF1AINSTR1W;
//	   //#define  RF1AINSTR1B           RF1AINSTR1W_H  // Radio instruction 1-byte register with autoread
//	   //volatile unsigned int RF1AINSTR2W; // Radio instruction 2-byte register with autoread
//	   data_types::Register<uint16> RF1AINSTR2W;
//	   //#define  RF1AINSTR2B           RF1AINSTR1W_H  // Radio instruction 2-byte register with autoread
//	   //volatile unsigned int RF1ADINW; // Radio word data in register
//	   data_types::Register<uint16> RF1ADINW;
//	   //volatile unsigned int RF1ASTAT0W; // Radio status word register without auto-read
//	   uint16 :16;
//	   uint16 :16;
//	   uint16 :16;
//	   uint16 :16;
//	   data_types::Register<uint16> RF1ASTAT0W;
//	   //#define  RF1ADOUT0B            RF1ASTAT0W_L   // Radio byte data out register without auto-read
//	   //#define  RF1ASTAT0B            RF1ASTAT0W_H   // Radio status byte register without auto-read
//	   //#define  RF1ASTATW             RF1ASTAT0W     // Radio status word register without auto-read
//	   //#define  RF1ADOUTB             RF1ASTAT0W_L   // Radio byte data out register without auto-read
//	   //#define  RF1ASTATB             RF1ASTAT0W_H   // Radio status byte register without auto-read
//	   //volatile unsigned int RF1ASTAT1W; // Radio status word register with 1-byte auto-read
//	   data_types::Register<uint16> RF1ASTAT1W;
//	   //#define  RF1ADOUT1B            RF1ASTAT1W_L   // Radio byte data out register with 1-byte auto-read
//	   //#define  RF1ASTAT1B            RF1ASTAT1W_H   // Radio status byte register with 1-byte auto-read
//	   //volatile unsigned int RF1ASTAT2W; // Radio status word register with 2-byte auto-read
//	   data_types::Register<uint16> RF1ASTAT2W;
//	   //#define  RF1ADOUT2B            RF1ASTAT2W_L   // Radio byte data out register with 2-byte auto-read
//	   //#define  RF1ASTAT2B            RF1ASTAT2W_H   // Radio status byte register with 2-byte auto-read
//
//	   uint16 :16;
//	   volatile unsigned int RF1ADOUT0W; // Radio core word data out register without auto-read
//	   //#define  RF1ADOUTW             RF1ADOUT0W     // Radio core word data out register without auto-read
//	   //#define  RF1ADOUTW_L           RF1ADOUT0W_L   // Radio core word data out register without auto-read
//	   //#define  RF1ADOUTW_H           RF1ADOUT0W_H   // Radio core word data out register without auto-read
//	   volatile unsigned int RF1ADOUT1W;// Radio core word data out register with 1-byte auto-read
//	   volatile unsigned int RF1ADOUT2W; // Radio core word data out register with 2-byte auto-read
//	   uint16 :16;
//	   volatile unsigned int RF1AIN; // Radio core signal input register
//	   volatile unsigned int RF1AIFG; // Radio core interrupt flag register
//	   volatile unsigned int RF1AIES; // Radio core interrupt edge select register
//	   volatile unsigned int RF1AIE; // Radio core interrupt enable register
//	   volatile unsigned int RF1AIV; // Radio core interrupt vector word register
//	   uint16 :16;
//	   volatile unsigned int RF1ARXFIFO; // Direct receive FIFO access register
//	   volatile unsigned int RF1ATXFIFO; // Direct transmit FIFO access register
//
//
//
//	public:
//		Registers(){}
//		operator Registers*() {return operator->();}
//		Registers* operator-> () {return reinterpret_cast<Registers*>(bases::RF1A+offsets::RF1A);}
//	 };


	enum strobes {
		// Radio Core Instructions
		// command strobes
		RF_SRES                 = 0x30           //  SRES    - Reset chip.
		,RF_SFSTXON             = 0x31           //  SFSTXON - Enable and calibrate frequency synthesizer.
		,RF_SXOFF               = 0x32           //  SXOFF   - Turn off crystal oscillator. bring radio into sleep
		,RF_SCAL                = 0x33           //  SCAL    - Calibrate frequency synthesizer and turn it off.
		,RF_SRX                 = 0x34           //  SRX     - Enable RX. Perform calibration if enabled.
		,RF_STX                 = 0x35           //  STX     - Enable TX. If in RX state, only enable TX if CCA passes.
		,RF_SIDLE               = 0x36           //  SIDLE   - Exit RX / TX, turn off frequency synthesizer.
		//,RF_SRSVD            = 0x37     		 //  SRVSD   - Reserved.  Do not use.
		,RF_SWOR                = 0x38           //  SWOR    - Start automatic RX polling sequence (Wake-on-Radio)
		,RF_SPWD                = 0x39           //  SPWD    - Enter power down mode when CSn goes high.
		,RF_SFRX                = 0x3A           //  SFRX    - Flush the RX FIFO buffer.
		,RF_SFTX                = 0x3B           //  SFTX    - Flush the TX FIFO buffer.
		,RF_SWORRST             = 0x3C           //  SWORRST - Reset real time clock.
		,RF_SNOP                = 0x3D           //  SNOP    - No operation. Returns status byte.
		,RF_RXSTAT              = 0x80           // Used in combination with strobe commands delivers number of availabe bytes in RX FIFO with return status
		,RF_TXSTAT              = 0x00           // Used in combination with strobe commands delivers number of availabe bytes in TX FIFO with return status
		// other radio instr
		,RF_SNGLREGRD           = 0x80
		,RF_SNGLREGWR           = 0x00
		,RF_REGRD               = 0xC0
		,RF_REGWR               = 0x40
		,RF_STATREGRD           = 0xC0           // Read single radio core status register
		,RF_SNGLPATABRD         = (RF_SNGLREGRD+PATABLE)
		,RF_SNGLPATABWR         = (RF_SNGLREGWR+PATABLE)
		,RF_PATABRD             = (RF_REGRD+PATABLE)
		,RF_PATABWR             = (RF_REGWR+PATABLE)
		,RF_SNGLRXRD            = 0xBF
		,RF_SNGLTXWR            = 0x3F
        ,RF_TXFIFOWR            = 0x7F
		,RF_RXFIFORD            = 0xFF

	};

	enum rf1a_state {
//off = 0x0
		sleep = 0x1
		,idle = 0x2
		,rx = 0x3
		,tx = 0x5
	};


}//ns rf1a

}}//ns msp430,reflex


#endif // REGISTERS_H
