/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 **/

#include "reflex/rf/RADIO.h"
#include "reflex/memory/Buffer.h"
#include "reflex/memory/Pool.h"
#include "reflex/mcu/msp430x.h"
#include "reflex/interrupts/InterruptLock.h"
#include "reflex/debug/Assert.h"
#include "reflex/rf/Registers.h"
#include "reflex/System.h"
#include "reflex/pmm/PMM.h"

#ifdef UPDATE_ENABLED
    #include <reflex/UpdateTypes.h>
#endif

using namespace reflex;
using namespace msp430x;
using namespace rf1A;

#ifdef UPDATE_ENABLED
	void u_write_flashBytes(uint8* src, uint8* dst, uint16 len);
#endif

RADIO::RADIO(Pool& pool, bool usingTOS)
		: InterruptHandler(interrupts::RF1A, PowerManageAble::PRIMARY )
		//, ConfigurableDriver()
		, pool(pool)
		, receiveFunctor(*this)
{
	getSystem().pmm.SetVCore(reflex::msp430x::pmm::LEVEL3);
	this->setSleepMode(mcu::LPM1);
	input.init(this);

	cca_delay = 0;
	out_sendingSuccessful = 0;
	out_data = 0;
	current = 0;
	props = CCA;
	channel = 0;
	pwrLvl = 5; //0dBm
	transmissionTime = 0;
	rssi = 0;
	lqiCRC = 0;

	if (usingTOS == true)
		props = props | USING_TOS;


	this->rf1a.resetCore();
	InitRadio();


#ifdef DEBUG_RADIO
	counter_crc = 0;
	counter_len = 0;
#endif
}



void RADIO::init(Sink1<Buffer*>* out_data, Sink1<bool>* out_sendingSuccessful)
{
	this->out_data = out_data;
	this->out_sendingSuccessful = out_sendingSuccessful;
}



void RADIO::enable()
{
	this->rf1a.startRX();
	this->rf1a.setRadioState(rf1A::rx);
}

void RADIO::disable()
{
	this->rf1a.sleep();
	this->rf1a.setRadioState(rf1A::sleep);
}

void RADIO::configure()
{
	//process changes -> application ensures correctness
	//Buffer* cfgBuf = confData.get();

	RadioConfiguration cfg;
	confData.get(cfg);


//	cfgBuf->read(cfg);

	uint8 tmpState = rf1a.getRadioState();

	//no interrupts from radio!!
	this->rf1a.stopRX();



	//handle signal attenuation
	int8 att = cfg.getSignalAttenuation();
	//stupid long if-else
	if(att < -25)
		setRFPwr(0);
	else if(att < -17)
		setRFPwr(1);
	else if(att < -12)
		setRFPwr(2);
	else if(att < -7)
		setRFPwr(3);
	else if(att < -2)
		setRFPwr(4);
	else if(att < 3)
		setRFPwr(5);
	else if(att < 7)
		setRFPwr(6);
	else
		setRFPwr(7);

	//handle channel switch
	this->channel = cfg.getLogicalChannel();
	//this->setLogicalChannel(cfg.getLogicalChannel());

	//handle misc working modes
	uint8 pkt_ctrl = this->rf1a.readReg(PKTCTRL0);
	pkt_ctrl = cfg.isWhitening() ? pkt_ctrl & 0x40 : pkt_ctrl & (~0x40);
	rf1a.writeReg(PKTCTRL0, pkt_ctrl);

	// cca is always active via hardware, but it doesnt matter -> nevertheless we can send data immediatly
	props = (props & ~CCA) | (cfg.isCCA() << CCA_SHIFT);
	props = (props & ~USING_TOS) | (cfg.isSendingTOS() << TOS_SHIFT);
	props = (props & ~CONT_TRANSMISSION) | (cfg.isContinousTransmission() << CONT_TRANSMISSION_SHIFT);
	transmissionTime = cfg.getTransmissionTime();

	//cfgBuf->downRef();

	InitRadio();


	if(tmpState == rf1A::rx)
	{
		  this->rf1a.startRX();
	}
}


void RADIO::run()
{
	Buffer* current = input.get(); //get buffer to send

	if ( current == 0) {  //no data? return
		return;
	}
	uint8 len = current->getLength();
	if (len)  // buffer not big enough
	{
		//check if transmittting was successful
		bool val = transmit(((uint8*) current->getStart()), len, current->getFreeStackSpace());
		if(out_sendingSuccessful)
				out_sendingSuccessful->assign(val);

	}
	current->downRef();
}


uint8 RADIO::transmit(uint8 *buffer, uint8 count, uint8 tos)
{

	 //adding the TOS if needed
	if(props & USING_TOS)
		count++;

	//for the current implementation inclusive CRC and Length the maximum buffer size is 61 bytes
	if(count + MRFI_LENGTH_FIELD_SIZE + MRFI_RX_METRICS_SIZE > TXFIFO_SIZE )
	  return false;

	//indicates if transmit was successful or not
	uint8 returnValue = true;

	//saves the former state of radio
	uint8 formerState = 0;

	//look at state machine
	switch(rf1a.getRadioState())
	{
	case rf1A::sleep:
		  return false;
	  break;

	case rf1A::idle:
	  formerState = rf1A::idle;
	  break;

	case rf1A::rx:
	  /* Turn off receiver. We can ignore/drop incoming packets during transmit. */
	  this->rf1a.stopRX();
	  formerState = rf1A::rx;
	  break;

	case rf1A::tx:
	  formerState = rf1A::tx;
	  break;
	}


	//writes number of bytes to send in fifo
	this->rf1a.writeReg(RF_SNGLTXWR, count);

	//writes the TOS into the buffer
	if(props & USING_TOS)
		this->rf1a.writeReg(RF_SNGLTXWR, tos);

	//write data into fifo
	rf1a.writeTxFifo(buffer, count);


    /* ------------------------------------------------------------------
     *    CCA transmit
     *   ---------------
     */
	//in case of a continuous transmission no cca is used
	if((props & CCA) && !(props & CONT_TRANSMISSION))
	{
		/* set number of CCA retries */
		 uint8 ccaRetries = MRFI_CCA_RETRIES * 4;

		 // initial wait condition depending on nodeID % 16
         uint16 nodeId = (CoreApplication::nodeId() & 0x0F) + 1;
         uint16 wait = getBaseTimeForCCA() * (nodeId & 0x0F) + 1;
		 while(wait--)// Radio receives data, it will be sent to the receiver
			 asm("nop");

		/* ===============================================================================
		 *    Main Loop
		 *  =============
		 */
		for (;;)
		{

		  /* Radio must be in RX mode for CCA to happen.
		   * Otherwise it will transmit without CCA happening.
		   */

		  /* Can not use the Mrfi_RxModeOn() function here since it turns on the
		   * Rx interrupt, which we don't want in this case.
		   */
		  rf1a.strobe(RF_SRX);

		  /* wait for the rssi to be valid. */
		  //MRFI_RSSI_VALID_WAIT();
		  while(!(Registers()->RF1AIN & BV(1)));//RF1AIN is connected to signal of GDO_1, which is connected (IOCFG1) to RSSI


		  /*
		   *  Clear the PA_PD pin interrupt flag.  This flag, not the interrupt itself,
		   *  is used to capture the transition that indicates a transmit was started.
		   *  The pin level cannot be used to indicate transmit success as timing may
		   *  prevent the transition from being detected.  The interrupt latch captures
		   *  the event regardless of timing.
		   */
		  //MRFI_CLEAR_PAPD_PIN_INT_FLAG();
		  Registers()->RF1AIFG &= ~BV(0);

		  /* send strobe to initiate transmit */
		  //MRFI_STROBE( STX );
		  rf1a.strobe(RF_STX);

		  /* Delay long enough for the PA_PD signal to indicate a
		   * successful transmit. This is the 250 XOSC periods
		   * (9.6 us for a 26 MHz crystal).
		   * Found out that we need a delay of at least 25 us on CC1100 to see
		   * the PA_PD signal change. Hence keeping the same for CC430
		   */
		  //Mrfi_DelayUsec(25);

		  wait = getBaseTimeForCCA();
			//getApplication().display.writeUpper(wait);
		  while(wait--)
			  asm("nop");

		  /* PA_PD signal goes from HIGH to LOW when going from RX to TX state.
		   * This transition is trapped as a falling edge interrupt flag
		   * to indicate that CCA passed and the transmit has started.
		   */
		  if(Registers()->RF1AIFG & BV(0))
		  {
			/* ------------------------------------------------------------------
			*    Clear Channel Assessment passed.
			*   ----------------------------------
			*/

			/* Clear the PA_PD int flag */
			//MRFI_CLEAR_PAPD_PIN_INT_FLAG();
			Registers()->RF1AIFG &= ~BV(0);


			/* PA_PD signal stays LOW while in TX state and goes back to HIGH when
			 * the radio transitions to RX state.
			 */
			/* wait for transmit to complete */
			//while (!MRFI_PAPD_PIN_IS_HIGH());
			while (!(Registers()->RF1AIN & BV(0)));

			/* transmit done, break */
			break;
		  }
		  else
		  {
			/* ------------------------------------------------------------------
			 *    Clear Channel Assessment failed.
			 *   ----------------------------------
			 */

			/* Turn off radio and save some power during backoff */

			/* NOTE: Can't use Mrfi_RxModeOff() - since it tries to update the
			 * sync signal status which we are not using during the TX operation.
			 */
			//MRFI_STROBE_IDLE_AND_WAIT();
			rf1a.strobe(RF_SIDLE);
			/* Wait for XOSC to be stable and radio in IDLE state */ \
			while ( rf1a.strobe( RF_SNOP ) & 0xF0);

			/* flush the receive FIFO of any residual data */
			//MRFI_STROBE( SFRX );
			rf1a.strobe(RF_SFRX);

			/* Retry ? */
			if (ccaRetries != 0)
			{
			  /* delay for a random number of backoffs */
			  //Mrfi_RandomBackoffDelay();
			  uint8 backoffs;

			  /* calculate random value for backoffs - 1 to 16 */
			  randomSeed = (randomSeed << 1) | (rf1a.readReg(RSSI) & 0x01);
			  backoffs = (randomSeed & 0x0F) + 1;//maximum waiting 16
			  /* delay for randomly computed number of backoff periods */
			  wait = getBaseTimeForCCA() * backoffs;
			  while(wait--)// Radio receives data, it will be sent to the receiver
	  			  asm("nop");

			  /* decrement CCA retries before loop continues */
			  ccaRetries--;
			}
			else /* No CCA retries are left, abort */
			{
			 	/* set return value for failed transmit and break */
				//returnValue = MRFI_TX_RESULT_FAILED;
				returnValue = false;

				break;
			}
		  } /* CCA Failed */
		} /* CCA loop */
	} else { // no CCA
		/* ------------------------------------------------------------------
		*    Immediate transmit
		*   ---------------------
		*/
		if(props & CONT_TRANSMISSION)
		{
			uint16 freq = getSystem().ucs.getCurrentFrequency();
			uint8 time = transmissionTime;

			//workaround -> the accurate time for sending cannot determined, since
			//there is register polling in rf1a.strobe
			//thats why we use this crazy approximation (tested for 2,4MHz)
			if (time > 3)
				time = time - 2;

			do {
				do {
					this->rf1a.strobe( RF_STX );
				} while(freq--);

			} while(time--);
		} else {
			this->rf1a.strobe( RF_STX );
		}

		//sendCount = 10;
		 //getApplication().showLowerLCD(sendCount);
		//sendCount = 300000;
		//continuous transmission


		// Wait for transmit to complete  is signaled (RF1AIFG & BV(9))
		while(!(Registers()->RF1AIFG & 0x0200));

		//Clear the interrupt flag
		Registers()->RF1AIFG &= ~0x0200;
	}


	//   Flush the transmit FIFO.  It must be flushed so that
	//    the next transmit can start with a clean slate.
	this->rf1a.strobe( RF_SFTX );

	// If the radio was in RX state when transmit was attempted,
	// put it back to Rx On state.
	if(formerState ==  rf1A::rx)
		this->rf1a.startRX();


	return( returnValue );
}



/**************************************************************************************************
 * @fn          Mrfi_SyncPinRxIsr
 *
 * @brief       This interrupt is called when the SYNC signal transition from high to low.
 *              The sync signal is routed to the sync pin which is a GPIO pin.  This high-to-low
 *              transition signifies a receive has completed.  The SYNC signal also goes from
 *              high to low when a transmit completes. This is protected against within the
 *              transmit function by disabling sync pin interrupts until transmit completes.
 *
 * @param       none
 *
 * @return      none
 **************************************************************************************************
 */
//void RADIO::RF1A_sync_interrupt_handler()
void RADIO::receive()
{
	uint8 frameLen = 0x00;
	uint8 rxBytes;


	/* We should receive this interrupt only in RX state
	* Should never receive it if RX was turned On only for
	* some internal mrfi processing like - during CCA.
	* Otherwise something is terribly wrong.
	*/
	Assert(this->rf1a.getRadioState() == rf1A::rx);

	/* ------------------------------------------------------------------
	*    Get RXBYTES
	*   -------------
	*/

	/*
	*  Read the RXBYTES register from the radio.
	*  Bit description of RXBYTES register:
	*    bit 7     - RXFIFO_OVERFLOW, set if receive overflow occurred
	*    bits 6:0  - NUM_BYTES, number of bytes in receive FIFO
	*
	*  Due a chip bug, the RXBYTES register must read the same value twice
	*  in a row to guarantee an accurate value.
	*/
	{
		uint8 rxBytesVerify;

		rxBytesVerify = this->rf1a.readReg( RXBYTES );

		do
		{
			rxBytes = rxBytesVerify;
			rxBytesVerify = this->rf1a.readReg( RXBYTES );
		}while (rxBytes != rxBytesVerify);
	}
#ifdef DEBUG_RADIO
	getApplication().showUpperLCD(rxBytes);
#endif
	//getApplication().showLowerLCD(rxBytes);
	/* ------------------------------------------------------------------
	*    FIFO empty?
	*   -------------
	*/

	/*
	*  See if the receive FIFIO is empty before attempting to read from it.
	*  It is possible nothing the FIFO is empty even though the interrupt fired.
	*  This can happen if address check is enabled and a non-matching packet is
	*  received.  In that case, the radio automatically removes the packet from
	*  the FIFO.
	*/
	//menzehan checking for underflow
	if (rxBytes == 0)
	{
		/* receive FIFO is empty - do nothing, skip to end */
		//getApplication().display.getSymbols().toggle(display::SYMB_MAX);
		return;
	} else {

		/* ------------------------------------------------------------------
		*    Process frame length
		*   ----------------------
		*/

		/* read the first byte from FIFO - the packet length */
		//frameLen = this->rf1a.readReg(RF_RXFIFORD); //MRFI_LENGTH_FIELD_SIZE = 1
		this->rf1a.readRxFifo(&frameLen, 1);
		//getApplication().display.writeUpper(frameLen);

		/*
		*  Make sure that the frame length just read corresponds to number of bytes in the buffer.
		*  If these do not match up something is wrong.
		*
		*  This can happen for several reasons:
		*   1) Incoming packet has an incorrect format or is corrupted.
		*   2) The receive FIFO overflowed.  Overflow is indicated by the high
		*      bit of rxBytes.  This guarantees the value of rxBytes value will not
		*      match the number of bytes in the FIFO for overflow condition.
		*   3) Interrupts were blocked for an abnormally long time which
		*      allowed a following packet to at least start filling the
		*      receive FIFO.  In this case, all received and partially received
		*      packets will be lost - the packet in the FIFO and the packet coming in.
		*      This is the price the user pays if they implement a giant
		*      critical section.
		*   4) A failed transmit forced radio to IDLE state to flush the transmit FIFO.
		*      This could cause an active receive to be cut short.
		*
		*  Also check the sanity of the length to guard against rogue frames.
		*/

		//menzehan:
		//check size of RXFIFO is correct or if an overflow occured
		if (rxBytes != frameLen + MRFI_LENGTH_FIELD_SIZE + MRFI_RX_METRICS_SIZE  || (rxBytes & 0x80))
		{
			//error
			/*
			*  Flush receive FIFO to reset receive.  Must go to IDLE state to do this.
			*  The critical section guarantees a transmit does not occur while cleaning up.
			*/
			InterruptLock lock; //ATTENTION -> needed???

			if (rxBytes & 0x80)
				this->rf1a.strobe( RF_SFRX );//flushes FIFO only when overflow!!
			else
				this->rf1a.strobe( RF_SIDLE );


			/* Wait for XOSC to be stable and radio in IDLE state */
			while (this->rf1a.strobe( RF_SNOP ) & 0xF0) ;

			if ((rxBytes & 0x80) == 0)
				this->rf1a.strobe( RF_SFRX );//if there is no overflow, but framesize is obviously wrong -> flush here right now

			this->rf1a.strobe( RF_SRX );
			//getApplication().display.getSymbols().toggle(display::SYMB_MAX);
	    } else {
			/* bytes-in-FIFO and frame length match up - continue processing */
			/* ------------------------------------------------------------------
			*    Get packet
			*   ------------
			*/
			//------------------

	    	// -1: len was read, -1: status byte 1, -1: status byte 2
	        uint8 paketBuffer[RXFIFO_SIZE - 3];

	        this->rf1a.readRxFifo(paketBuffer, frameLen);

	        //uint8 rxStatus2 = MRFI_RX_METRICS_CRC_OK_MASK;

	        //if append mode is enabled
	        if(this->rf1a.readReg(PKTCTRL1) & 0x04)
	        {
	        	//uint8 rxStatus1 = MRFI_RX_METRICS_CRC_OK_MASK;
				this->rf1a.readRxFifo((uint8*)&rssi, 1); //RSSI
				this->rf1a.readRxFifo((uint8*)&lqiCRC, 1); //LQI + CRC
			//getApplication().display.getSymbols().toggle(display::SYMB_AVERAGE);
	        }


	      /*
	       *  Note!  Automatic CRC check is not, and must not, be enabled.  This feature
	       *  flushes the *entire* receive FIFO when CRC fails.  If this feature is
	       *  enabled it is possible to be reading from the FIFO and have a second
	       *  receive occur that fails CRC and automatically flushes the receive FIFO.
	       *  This could cause reads from an empty receive FIFO which puts the radio
	       *  into an undefined state.
	       */

	      /* determine if CRC failed */
	        if (!(lqiCRC & MRFI_RX_METRICS_CRC_OK_MASK) || !out_data)
			{
	        	/* CRC failed - do nothing, skip to end */
	        	//getApplication().display.getSymbols().toggle(display::SYMB_MAX);
	        	return;
			} else {

#ifdef UPDATE_ENABLED
				if(isUpdateRequired(paketBuffer, frameLen))
				{
					//TODO: maybe disable interrupts .. dont know
					// init update process -> rien ne va plus

					// flooding received packet
					props = CCA;// only cca should be active
					//TODO: for testing purposes deactivated
					//transmit(paketBuffer, frameLen, 0);

					// writing data into persistentUpdateMem -> the low level update driver ensures, that this part of memory
					// is writable
					// after reset, the update driver starts the update process
                    CoreApplication::setFirmwareInvalid();

					//forcing reset!!
					//@TODO: maybe correct this dirty hack
					*((uint16*)0x015C) = 0;
				}
#endif
				/* CRC passed - continue processing */
				current = new(&pool)  Buffer(&pool); // get a new buffer

				if(current)
				{
					uint8* tmpBuf = paketBuffer;
					if(props & USING_TOS)
					{
						frameLen--;
						current->initOffsets(paketBuffer[0]);
						tmpBuf++;
					}
					//TOS + data to write into buffer <= free data part in buffer
					if(frameLen <= current->getFreeFIFOSpace())
					{
						current->write(tmpBuf, frameLen);
						out_data->assign(current);
						return;
					}

					// not enough space in buffer -> release buffer
					current->downRef();
				}
			}
	    }
	}
}


//got an interface error, but, what do we do with it?
void RADIO::RF1A_interface_interrupt_handler()
{

}

void reflex::msp430x::RADIO::handle()
{
	switch ((uint16)(Registers()->RF1AIV))
	 {
	    case  0:                                // No RF core interrupt pending
	      RF1A_interface_interrupt_handler();             // means RF1A interface interrupt pending
	      break;
	    case  2: break;                         // RFIFG0
	    case  4: break;                         // RFIFG1
	    case  6: break;                         // RFIFG2
	    case  8: break;                         // RFIFG3
	    case 10:                                // RFIFG4
	      //packetReceived
	      break;
	    case 12: break;                         // RFIFG5
	    case 14: break;                         // RFIFG6
	    case 16: break;                         // RFIFG7
	    case 18: break;                         // RFIFG8
	    case 20:                                // RFIFG9
	      //packetTransmit
	    	receiveFunctor.trigger();
	    	//RF1A_sync_interrupt_handler();
	      break;
	    case 22: break;                         // RFIFG10
	    case 24: break;                         // RFIFG11
	    case 26: break;                         // RFIFG12
	    case 28: break;                         // RFIFG13
	    case 30: break;                         // RFIFG14
	    case 32: break;                         // RFIFG15
	  }


}


/**************************************************************************************************
 * @fn          MRFI_SetLogicalChannel
 *
 * @brief       Set logical channel.
 *
 * @param       chan - logical channel number
 *
 * @return      none
 **************************************************************************************************
 */
void RADIO::setLogicalChannel(uint8 chan)
{
	  /* make sure radio is off before changing channels */
	  this->rf1a.stopRX();

	/* logical channel is not valid? */
	if (chan < MRFI_NUM_LOGICAL_CHANS )
	{
		this->rf1a.writeReg( CHANNR, LogicalChanTable[chan] );
		channel = chan;
	}

	  /* turn radio back on if it was on before channel change */
	  if(this->rf1a.getRadioState() == rf1A::rx)
	  {
		  this->rf1a.startRX();
	  }
}

/**************************************************************************************************
 * @fn          MRFI_SetRFPwr
 *
 * @brief       Set output RF power level.
 *
 * @param       level - power level to be set
 *
 * @return      none
 **************************************************************************************************
 */
void RADIO::setRFPwr(uint8 level)
{
	/* Power level is not valid? */
	if (level < NUM_OF_POWER_SETTINGS )
	{
		this->rf1a.writeReg(PATABLE, RFPowerTable[level]);
		pwrLvl = level;
	}
}


/**************************************************************************************************
 * @fn          MRFI_Init
 *
 * @brief       Initialize MRFI.
 *
 * @param       none
 *
 * @return      none
 **************************************************************************************************
 */
void RADIO::InitRadio()
{
	/**
	 * setting delay for 25us for CCA
	 */
	uint16 count = (getSystem().ucs.getCurrentFrequency() >> 10) + 1;//MHz + 1
	for(uint8 i = 0; i < 25; i++)
		cca_delay += count; //needed cycles for 25us delay => base is 1 MHz


	/* ------------------------------------------------------------------
	*    Radio power-up reset
	*   ----------------------
	*/

	//@author menzehan:
	/* Strobe Reset: Resets the radio and puts it in SLEEP state. */
	//this->rf1a.strobe( RF_SRES );

	//@author menzehan:
	rf1a.resetCore();


	// Put radio in Idle state
	this->rf1a.strobe( RF_SIDLE );

	/* Wait for XOSC to be stable and radio in IDLE state */
	while (this->rf1a.strobe( RF_SNOP ) & 0xF0) ;
	this->rf1a.strobe( RF_SFRX ); //flush fifo



	/* ------------------------------------------------------------------
	*    Configure radio
	*   -----------------
	*/

	/* Configure Radio interrupts:
	*
	* RF1AIN_0 => Programmed to PA_PD signal.
	*             Configure it to interrupt on falling edge.
	*
	* RF1AIN_1 => Programmed to RSSI Valid signal.
	*             No need to configure for interrupt. This value will be read
	*             through polling.
	*
	* RF1AIN_9 => Rising edge indicates SYNC sent/received and
	*             Falling edge indicates end of packet.
	*             Configure it to interrupt on falling edge.
	*/

	/* Select Interrupt edge for PA_PD and SYNC signal:
	* Interrupt Edge select register: 1 == Interrupt on High to Low transition.
	*/
	Registers()->RF1AIES = 0x0201;
	//rf1a.Registers()->RF1AIES = BV(0) | BV(9);

	/* Write the power output to the PA_TABLE and verify the write operation.  */
	{
		InterruptLock lock;
		this->rf1a.WritePATable();
	}

	/* initialize radio registers */
	this->rf1a.WriteSmartRFReg(mrfiRadioCfg, (sizeof(mrfiRadioCfg)/sizeof(mrfiRadioCfg[0])));

	/* Confirm that the values were written correctly.
	*/
	for (uint8 i = 0; i < (sizeof(mrfiRadioCfg)/sizeof(mrfiRadioCfg[0])); i++)
	{
		//Assert( mrfiRadioCfg[i][1] == this->rf1a.readReg(mrfiRadioCfg[i][0]) );
		if( mrfiRadioCfg[i][1] != this->rf1a.readReg(mrfiRadioCfg[i][0]) )
		{
			return;
		}
	}



	//setting whitening
	uint8 pkt_ctrl = rf1a.readReg(PKTCTRL0);
	pkt_ctrl = (props & WHITENING) ? pkt_ctrl & 0x40 : pkt_ctrl & (~0x40);
//	pkt_ctrl = (props & CRC) ? pkt_ctrl & 0x04 : pkt_ctrl & (~0x04);

	rf1a.writeReg(PKTCTRL0, pkt_ctrl);
	//check if data written correctly
	if(pkt_ctrl != rf1a.readReg(PKTCTRL0))
		return;


	/* set default channel */
	setLogicalChannel(channel);

	/* Set default power level */
	setRFPwr(pwrLvl);


	/* Generate Random seed:
	* We will use the RSSI value to generate our random seed.
	*/
	{
		/* Put the radio in RX state */
		this->rf1a.strobe( RF_SRX );

		/* delay for the rssi to be valid */
		while(!(Registers()->RF1AIN & BV(1))); //MRFI_RSSI_VALID_WAIT();
		{
			uint8 i;
			for(i=0; i<16; i++)
			{
				/* use most random bit of rssi to populate the random seed */
				randomSeed = (randomSeed << 1) | (this->rf1a.readReg(RSSI) & 0x01);
			}
		}
		/* Force the seed to be non-zero by setting one bit, just in case... */
		randomSeed |= 0x0080;
		/* Turn off RF. */
		this->rf1a.stopRX();
	}
	/* Strobe Power Down (SPWD): puts the radio in SLEEP state. */
	/* Chip bug: Radio does not come out of this SLEEP when put to sleep
	* using the SPWD cmd. However, it does wakes up if SXOFF was used to
	* put it to sleep.
	*/
	//@author menzehan:
	rf1a.off();
	//this->rf1a.strobe( RF_SXOFF );


  /* Initial radio state is IDLE state */
  //mrfiRadioState = MRFI_RADIO_STATE_OFF;
  //this->rf1a.setRadioState(rf1A::sleep);



  /*****************************************************************************************
   *                            Compute reply delay scalar
   *
   * Formula from data sheet for all the narrow band radios is:
   *
   *                (256 + DATAR_Mantissa) * 2^(DATAR_Exponent)
   * DATA_RATE =    ------------------------------------------ * f(xosc)
   *                                    2^28
   *
   * To try and keep some accuracy we change the exponent of the denominator
   * to (28 - (exponent from the configuration register)) so we do a division
   * by a smaller number. We find the power of 2 by shifting.
   *
   * The maximum delay needed depends on the MAX_APP_PAYLOAD parameter. Figure
   * out how many bits that will be when overhead is included. Bits/bits-per-second
   * is seconds to transmit (or receive) the maximum frame. We multiply this number
   * by 1000 to find the time in milliseconds. We then additionally multiply by
   * 10 so we can add 5 and divide by 10 later, thus rounding up to the number of
   * milliseconds. This last won't matter for slow transmissions but for faster ones
   * we want to err on the side of being conservative and making sure the radio is on
   * to receive the reply. The semaphore monitor will shut it down. The delay adds in
   * a fudge factor that includes processing time on peer plus lags in Rx and processing
   * time on receiver's side.
   *
   * Note that we assume a 26 MHz clock for the radio...
   * ***************************************************************************************
   */
#define   MRFI_RADIO_OSC_FREQ         26000000
#define   PHY_PREAMBLE_SYNC_BYTES     8

	{
		uint32 dataRate, bits;
		uint16 exponent, mantissa;

		/* mantissa is in MDMCFG3 */
		mantissa = 256 + SMARTRF_SETTING_MDMCFG3;

		/* exponent is lower nibble of MDMCFG4. */
		exponent = 28 - (SMARTRF_SETTING_MDMCFG4 & 0x0F);

		/* we can now get data rate */
		dataRate = mantissa * (MRFI_RADIO_OSC_FREQ >> exponent);

		//TODO: this calculation only matches for the Simplicity-Protocol => currently a workaround is used
		//bits = ((uint32)((PHY_PREAMBLE_SYNC_BYTES + MRFI_MAX_FRAME_SIZE)*8))*10000;
		bits = ((uint32)((PHY_PREAMBLE_SYNC_BYTES + 64)*8))*10000;
		/* processing on the peer + the Tx/Rx time plus more */
		sReplyDelayScalar = PLATFORM_FACTOR_CONSTANT + (((bits/dataRate)+5)/10);

		/* This helper value is used to scale the backoffs during CCA. At very
		 * low data rates we need to backoff longer to prevent continual sampling
		 * of valid frames which take longer to send at lower rates. Use the scalar
		 * we just calculated divided by 32. With the backoff algorithm backing
		 * off up to 16 periods this will result in waiting up to about 1/2 the total
		 * scalar value. For high data rates this does not contribute at all. Value
		 * is in microseconds.
		 */
		sBackoffHelper = MRFI_BACKOFF_PERIOD_USECS + (sReplyDelayScalar >> 5)*1000;
	}

  /* enable global interrupts */
 // BSP_ENABLE_INTERRUPTS();
}


void RADIO::errorInReceive()
{
	/*
	*  Flush receive FIFO to reset receive.  Must go to IDLE state to do this.
	*  The critical section guarantees a transmit does not occur while cleaning up.
	*/

}

#ifdef UPDATE_ENABLED
bool RADIO::isUpdateRequired(uint8* buf, uint8 len)
{
	//checking length
	if (len == sizeof(UpdateMetaPacket))
	{
		UpdateMetaPacket* tmp = (UpdateMetaPacket*) buf;

        if ((tmp->header.type == MetaPacket)
                && ((tmp->header.version != CoreApplication::softwareVersion())
                        || tmp->header.getOverride()
                   )
            )
            return true;
	}
	return false;
}
#endif
