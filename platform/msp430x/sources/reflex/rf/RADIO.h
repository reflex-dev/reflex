/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 **/

#ifndef REFLEX_MSP430X_RADIO_H
#define REFLEX_MSP430X_RADIO_H

#include "reflex/rf/RF1A.h"
#include "reflex/ucs/UCS.h"

#include "reflex/scheduling/Activity.h"
#include "reflex/scheduling/ActivityFunctor.h"
#include "reflex/interrupts/InterruptHandler.h"

#include "reflex/sinks/Queue.h"
#include "reflex/sinks/Event.h"

#include "reflex/rf/config.h"

#include "reflex/driverConfiguration/ConfigurableDriver.h"
#include "reflex/driverConfiguration/RadioConfiguration.h"

namespace reflex {

class Pool;

namespace msp430x {

//#define DEBUG_RADIO

/**
 * High level radio driver of the cc430 series.
 */
class RADIO
	: public InterruptHandler,
	  public Activity,
	  public ConfigurableDriver<RadioConfiguration>
{

public:

	/**
	 * constructor:
	 *
	 * @param pool refrence to a pool, where the data will store into
	 * @param usingTOS true -> the radio driver additionally transmits the TOS of the buffer "input" and configures
	 * the buffer in case of receiving data with the help of TOS. TOS is inserted after length byte. false -> the
	 * radio driver doesn't insert the the TOS and saves a byte
	 */
	RADIO (Pool& pool, bool usingTOS = true);

	/**
	* Initialize the sender and the reciever, which use the serial
	*
	* @param receiver the object, which gets all receiving characters
	* @param sender gets notified if something was successfully sent
	*/
	void init(Sink1<Buffer*>* receiver, Sink1<bool>* sender) __attribute__((deprecated));

	/**
	 * connects the data output port of radio with a sink
	 * @param out_data
	 */
	inline void connect_out_data(Sink1<Buffer*>* out_data)
	{
		this->out_data = out_data;
	}

	/**
	 * connects the out going signal for a successful transmitted data with a sink
	 * @param out_data
	 */
	inline void connect_out_sendingSuccessful(Sink1<bool>* out_sendingSuccessful)
	{
		this->out_sendingSuccessful = out_sendingSuccessful;
	}


	/**
	 * @return the input for data which has to be sent
	 */
	inline Sink1<Buffer*>* get_in_input()
	{
		return &input;
	}


	/**
	 * @return the current state of the radio
	 */
	uint8 getRadioState()
	{
		return this->rf1a.getRadioState();
	}

	/**
	 * @return the number of cycles needed for PA_PD signal change. This timespane is base time for cca algorithm
	 */
	uint16 getBaseTimeForCCA()
	{
		return cca_delay;
	}


	/**
	 * returns a random seed based on the current RSSI value
	 */
	inline uint16 getRandomSeed()
	{
		return rssi;
	}


	/**
	 * TODO correct this hack
	 * first byte = RSSI
	 * second byte = LQI
	 */
	uint8 rssi;
	uint8 lqiCRC;

private:



	void RF1A_interface_interrupt_handler();
	void RF1A_sync_interrupt_handler();

	/**
	 * These method is called from the scheduler.
	 * It starts sending the next buffer from the queue.
	 */
	void run();

	/**
	 * derived from interruptHandler -> enables the device
	 */
	void enable();


	/**
	 * derived from interruptHandler -> disables the device
	 */
	void disable();


	/**
	 * initialize the radio with default / given parameters
	 */
	void InitRadio();

	/**
	 * configure the radio to listen and transmit in a special frequency range, coded into
	 * a logical channel
	 * @param chan the logical channel which describes a certain frequency range
	 */
	void setLogicalChannel(uint8 chan);

	/**
	 * is called, when an error in receive is detected
	 * => flush the RX queue, go into receive mode
	 */
	void errorInReceive();


	/**
	 * method for handling receive data
	 */
	void receive();

	/**
	 * @param buffer pointer to a buffer, where the data are present
	 * @param count amount of bytes in buffer to send (currently max. 64 bytes, depending on fill level of TX/RX buffer in radio)
	 * @param tos the
	 * @return true if sending was successful
	 *
	 * @note: if length should inserted into FIFO count buffersize is max 63byte, if additionally CRC is enabled the buffer size shrinks up to
	 * 2 bytes => so the maximum buffersize (lenght + CRC) is 61 Bytes
	 */
	uint8 transmit(uint8 *buffer, uint8 count, uint8 tos = 0);

	/**
	 * set the logical power level of the radio module
	 * @ param level the logical power level
	 */
	void setRFPwr(uint8 level);

	/**
	 * derived from configurable driver
	 */
	void configure();

	/**
	 * derived from interrupt handler
	 */
	void handle();

	/**
	 * check the incomming packet whether it is an updateInitPacket
	 *
	 * @param buf pointer to the packet
	 * @param len the length of the packet
	 *
	 * @return true if the packet is an updateInitPacket
	 */
	bool isUpdateRequired(uint8* buf, uint8 len);

	/**
	 * needed buffer pool to store and process data packets
	 */
	Pool& pool;

	/**
	 * All data waiting for transmission is stored in this queue
	 * to get scheduled
	 */
	Queue<Buffer*> input;

	/**
	 * This is the Activity Functor
	 * pointing to a class and to a member function of
	 * this class
	 */
	reflex::ActivityFunctor<RADIO,&RADIO::receive> receiveFunctor;


	/**
	 * When the Radio receives data, it will be sent to the receiver
	 */
	Sink1<Buffer*>* out_data;

	/**
	 * The sender is notified, if last send request is finished
	 */
	Sink1<bool>* out_sendingSuccessful;

	/**
	 * holds the buffer, which is currently sending
	 */
	Buffer* current;

	/**
	 * instance of the radio core driver
	 */
	RF1A rf1a;

	/*
	 * the current active channel
	 */
	//uint8 channel;
	//RadioConfiguration cfg;



	//make it uint16 since it makes more sense
	uint16 randomSeed;
	uint16 sReplyDelayScalar;
	uint16 sBackoffHelper;

	/**
	 * the current active channel
	 */
	uint8 channel;

	/**
	 * current active power level
	 */
	uint8 pwrLvl;

	/**
	 * duration for continuous transmission
	 */
	uint8 transmissionTime;

	/**
	 * holds a pre-calculated delay which is used for for cca. This value
	 * describes the minimum time needed to re-calibrate the radio depending on
	 * the DCO.
	 */
	uint16 cca_delay;

	/**
	 * to save memory, here we code some properties of the radio
	 */
	enum radioProperties
	{
		CCA = 0x1
		,USING_TOS = 0x2
		//,CRC = 0x4
		,WHITENING = 0x4
		,CONT_TRANSMISSION = 0x8
	};

	enum radioShifts
	{
		 CCA_SHIFT = 0
		,TOS_SHIFT = 1
		//,CRC_SHIFT = 3
		,WHITENING_SHIFT = 2
		,CONT_TRANSMISSION_SHIFT = 3
	};

	/**
	 * save the properties of the radio
	 */
	uint8 props;



};

}} //ns msp430x


#endif // RADIO_H
