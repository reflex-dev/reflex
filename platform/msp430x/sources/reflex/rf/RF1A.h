/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 **/

#ifndef REFLEX_MSP430X_RF1A_H
#define REFLEX_MSP430X_RF1A_H

#include "reflex/mcu/msp430x.h"

#include "reflex/rf/Registers.h"

namespace reflex {
namespace msp430x {

/**	CC1101 Radio Core low level driver (RF1A) of the cc430 series.
 */
class RF1A
{

public:

	RF1A ();

	void resetCore();
	void startRX();
	void stopRX();
	void sleep(void);

	void setRadioState(reflex::msp430x::rf1A::rf1a_state state)
	{
		radioState = state;
	}

	uint8 getRadioState()
	{
		return radioState;
	}

	 //@author menzehan:
	/**
	 * switches the radio off via SXOFF and
	 * change state into sleep
	 */
	void off();


	void init();


	uint8 strobe(uint8 addr);

	/**
	 * writes a value into the specified register, but doesnt care about invariants
	 * @param addr address of register
	 * @param value to write
	 */
	void writeReg(uint8 addr, uint8 value);
	uint8 readReg(uint8 addr);
	void writeBurstReg(uint8 addr, uint8 *buffer, uint8 count);
	void readBurstReg(uint8 addr, uint8 *buffer, uint8 count);

	/**
	 * working version of writeTXFifo
	 * @TODO: optimize implementation
	 * @param buffer, pointer of a buffer
	 * @param count, number of bytes to read
	 */
	void writeTxFifo(uint8 *buffer, uint8 count);
	void readRxFifo(uint8 *buffer, uint8 count);

	void WriteSmartRFReg(const unsigned char SmartRFSetting[][2], uint8 size);

	void WritePATable();
protected:

	reflex::msp430x::rf1A::rf1a_state radioState;

};

}} //ns msp430x



#endif // RF1A_H
