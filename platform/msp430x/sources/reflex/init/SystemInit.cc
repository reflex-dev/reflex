#include "reflex/init/SystemInit.h"
#include "reflex/init/PlatformInit.h"
#include "reflex/init/WatchdogInit.h"

using namespace reflex;
using namespace msp430x;

SystemInit::SystemInit()
{
    WatchdogInit wdt; // set Watchdog on HOLD
    PlatformInit platform; // platform dependent stuff
}
