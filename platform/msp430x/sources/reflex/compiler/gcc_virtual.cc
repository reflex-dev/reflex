/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(ses):	 none

 *	Author:		 Soeren Hoeckner
 *
 *	Description: pseudo operators for gcc
 */

#include "reflex/debug/Assert.h"
#include "reflex/types.h"

extern "C" void _reset();

/**
 * pseudo implementation of the __cxa_pure_virtual function
 * which is used by the gcc
 * in that case, it does nothing
 */
extern "C" void __cxa_pure_virtual()
{
	Assert(false);
//	_reset();
	while(true);
}
