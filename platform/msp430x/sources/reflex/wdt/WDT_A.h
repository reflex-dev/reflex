/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 **/

#ifndef REFLEX_MSP430X_WDT_A_H
#define REFLEX_MSP430X_WDT_A_H

#include "reflex/mcu/msp430x.h"

#include "reflex/wdt/Registers.h"

#include "reflex/interrupts/InterruptHandler.h"

#include "reflex/sinks/Event.h"

namespace reflex {
namespace msp430x {

/**	Watchdog Timer (WDT_A) of the cc430 series.
 * FIXME: Watchdog timer should change deepest sleepmodes if other clocks
 * are selected
 * TODO: change watchdog into a configurable driver to allow changing of its settings during
 * runtime. Hiding watchdog configuration behind configuration-objects allows an abstraction of its methods.
 */
class WDT_A: public InterruptHandler,
			//sink0 is needed to ensure no scheduling for clear counter
	  	  	 public Sink0
{
	typedef wdt_a::Registers Registers;



public:

	/**
	 * disables watchdog module. Setting the WDTHOLD bit.
	  */
	WDT_A();

	/**
	 * @param sink the sink, which have to be triggered in case of an interrupt
	 *
	 */
	void init(Sink0* const sink) __attribute__((deprecated)) {output=sink;};


	/**
	 * @param output the sink, which has to be triggered in case of an interrupt generated after timeout
	 */
	void connect_out_output(Sink0* const output)
	{
		this->output = output;
	}

	/**
	 * @param clksrc the source for counting in timerMode
	 */
	void setClockSource(wdt_a::ClockSource clksrc);

	/**
	 * setting the watchdog into timer mode: no PUC is generated, only an interrupt occurs
	 * after a certain time (set by setInterval)
	 */
	void setTimerMode();

	/**
	 * settign watchdog into watchdog mode: generates a PUC if time set by setInterval is reached
	 */
	void setWatchdogMode();

	/**
	 * setting the timer interval for watchdog oder timer mode
	 */
	void setInterval(wdt_a::Interval interval);


private:

	/**
	 * derived from InterruptHandler -> implements the
	 * action in case of an interrupt
	 */
	void handle();

	/**
	 * derived from InterruptHandler -> implements the
	 * action in case of enabling the device
	 */
	void enable();

	/**
	 * derived from InterruptHandler -> implements the
	 * action in case of disabling the device
	 */
	void disable();

	/**
	 * the sink, which have to be triggered in case of an interrupt
	 */
	Sink0* output;

	/**
	 * clear the counter to avoid PUC or interrupt
	 */
	void notify();
};

}
} //ns msp430x





#endif // WDT_A_H
