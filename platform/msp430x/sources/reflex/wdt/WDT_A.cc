/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 **/

#include "reflex/wdt/WDT_A.h"
#include "reflex/mcu/msp430x.h"
#include "reflex/sys/SYS.h"

namespace reflex {
namespace msp430x {

WDT_A::WDT_A() 
	: InterruptHandler(interrupts::WDTM,PowerManageAble::PRIMARY )
{
	wdt_a::Registers()->WDTCTL = wdt_a::HOLD|wdt_a::ACLK;
	this->setSleepMode(mcu::LPM3);
}

void WDT_A::setClockSource(wdt_a::ClockSource clksrc)
{
	uint8 lowByte = Registers()->WDTCTL.low & (~wdt_a::ClkSrcMASK);
	//change clocksource bits and reassign
	Registers()->WDTCTL= (lowByte | clksrc) ;
}

void WDT_A::enable()
{
	uint8 lowByte = Registers()->WDTCTL.low;
	//clear HOLD bit and clear counter register bit CNTCL
	Registers()->WDTCTL= (lowByte & (~wdt_a::HOLD ))|wdt_a::CNTCL;
}

void WDT_A::disable()
{
	//set HOLD bit
	Registers()->WDTCTL |= wdt_a::HOLD;
}

void WDT_A::setTimerMode()
{
	sys::SFRegisters()->SFRIE1.low |= sys::WDTIE; //enable timer mode
	Registers()->WDTCTL |= wdt_a::TMSEL|wdt_a::CNTCL ;
}

void WDT_A::setWatchdogMode()
{
	uint8 lowByte = Registers()->WDTCTL.low;
	Registers()->WDTCTL= (lowByte & ( ~wdt_a::TMSEL)) ;
}

void WDT_A::setInterval(wdt_a::Interval interval)
{
	uint8 lowByte = Registers()->WDTCTL.low & ~(wdt_a::IntervalMASK);
	Registers()->WDTCTL= (lowByte | interval) ;
}

void reflex::msp430x::WDT_A::handle()
{
	if (output)
		output->notify();
}

void reflex::msp430x::WDT_A::notify()
{
	Registers()->WDTCTL |=  wdt_a::CNTCL;
}

} // msp430x
} // reflex
