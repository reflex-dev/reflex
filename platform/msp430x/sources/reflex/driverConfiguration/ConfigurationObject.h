/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *
 *	Author:		 ${user}
 *	Date:		 ${date}
 *
 *	Description: Implements the System
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2011 BTU Cottbus, Department for Distributed Systems and
 *    Operating Systems. All rights reserved.
 *
 *    Redistribution and use in source and binary forms, with or without
 *    modification, are permitted provided that the following conditions
 *    are met:
 *
 *       1. Redistributions of source code must retain the above copyright
 *          notice, this list of conditions and the following disclaimer.
 *
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the
 *          distribution.
 *
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *    The views and conclusions contained in the software and documentation
 *    are those of the authors and should not be interpreted as representing
 *    official policies, either expressed or implied, of BTU Cottbus,
 *    Department for Distributed Systems and Operating Systems.
 *
 * */
#ifndef CONFIGURATIONOBJECT_H_
#define CONFIGURATIONOBJECT_H_

#include "reflex/sinks/SingleValue.h"


/**
 * Represents the base class of all configuration objects. Such a object is used to
 * confrom with the event-flow of reflex. Instead of calling methods / functions of components
 * to change their behavior a ConfigurationObject will be passed through an event channel. The
 * The receiver of the ConfigurationObject decodes information of ConfigurationObject and changes
 * its parameter.
 *
 * @param DerivedConfObject is needed to implement the curiously recurring template pattern. It is
 * needed to ensure correct parameter in performConfiguration(...). DerivedConfObject has to be the type
 * of the derived class of ConfigurationObject.
 */
template<class DerivedConfObject>
class ConfigurationObject
{

public:

	/**
	 * @param confType a specific configuration type
	 */
	ConfigurationObject()
	{};

	/**
	 * notifies a driver to change its setup
	 */
	void performConfiguration(reflex::Sink1<DerivedConfObject> *driverSignal)
	{
		driverSignal->assign(*(static_cast<DerivedConfObject*>(this)));
	}

	/**
	 * clear the content of the configuration object to zero
	 */
	virtual void clear() = 0;

};

#endif /* CONFIGURATIONOBJECT_H_ */
