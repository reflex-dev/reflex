/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *

 *	Author:		 Andre Sieber
 */
#include "reflex/adc/deMux.h"
#include "reflex/interrupts/InterruptLock.h"

using namespace reflex;


deMux::deMux() :  input(this)
{
  //  input.init(this);
}


void deMux::run()
{
  this->lock();
  mcu::ADC12_A::Request request = input.get();
  adcOut->assign(request);
}

void deMux::assign(uint8 channel,uint16 value)
{
  if(sinks[channel]) sinks[channel]->assign(value);//read value from conversion memory register
  this->unlock();
}

void deMux::registerSink(Sink1<uint16>* sink, uint8 channel)
{
	InterruptLock lock;
   	if(channel <= 15){
   		sinks[channel] = sink;
   	}
}



void deMux::removeSink(uint8 channel)
{
	InterruptLock lock;
	sinks[channel] = NULL;
}
