/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *

 *
 *    This file is part of REFLEX.
 *
 *    REFLEX is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as
 *    published by the Free Software Foundation, either version 3 of the
 *    License, or (at your option) any later version.
 *
 *    REFLEX is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with REFLEX.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include "reflex/adc/ADC12_A.h"
#include "reflex/ref/REF.h"

using namespace reflex;
using namespace msp430x;
using namespace adc;

//extern "C" adc::Registers adc asm("0x01a0");
//::adc.CTL0 = 0x1; //move 0x1, &0x01a0
//means the obj adc from Type adc::Registers is located on address 0x01a0


//	REFCTL0 |= REFMSTR + ref + REFON;    		// Enable internal reference (1.5V or 2.5V)
//
//	// Initialize ADC12_A
//	ADC12CTL0 = sht + ADC12ON;					// Set sample time
//	ADC12CTL1 = ADC12SHP;                     	// Enable sample timer
//	ADC12MCTL0 = ADC12SREF_1 + channel;  		// ADC input channel
//	ADC12IE = 0x001;               // ADC_IFG upon conv result-ADCMEMO



ADC12_A::ADC12_A()
	: PowerManageAble(PowerManageAble::SECONDARY)
	, input(*this)
	, ivDispatcher(interrupts::ADC12_A) //bind ivDispatcher to vector of ADC12. dont handle powermanagement
	, collector(*this)
{
// 	this->setSleepMode(LPM3); //since we going to use ACKL wich is enabled at least at LPM3

// 	Registers()->CTL0 = adc::SHT1_10+adc::SHT0_10; //set 512 clkcycles sample and holdtime for all channels
// 	Registers()->CTL1 = adc::SSEL_ACLK | adc::CONSEQ_0; //select aclk, single conversion
// 	//using 2V shared reference
// 	ref::Registers()->CTL0 |= ref::MSTR | ref::VSEL0;

  //trying th ADC12OSC which is mosc and automaticly used, ticks at around 5MHz
	this->setSleepMode(LPM4); 

	Registers()->CTL0 = adc::SHT1_10+adc::SHT0_10; //set 512 clkcycles sample and holdtime for all channels
	Registers()->CTL1 = adc::SSEL_ADC12OSC | adc::CONSEQ_0; //select aclk, single conversion
	//using 2V shared reference
	ref::Registers()->CTL0 |= ref::MSTR | ref::VSEL0;
	
}


ADC12_A::~ADC12_A()
{
	this->disable();
	Registers()->IE = 0x0; //disable all interrupts
}

void ADC12_A::enable() {
	ref::Registers()->CTL0 |= ref::ON;
	Registers()->CTL0 |= adc::ON;
	Registers()->CTL1 |= adc::SHP;
	
}

void ADC12_A::disable() {
	Registers()->CTL0 &= ~adc::ON;
	Registers()->CTL1 &= ~adc::SHP;

       	ref::Registers()->CTL0 &= ~ref::ON;
}



reflex::msp430x::ADC12_A::Measure::Measure(ADC12_A& _adc)
	: Activity()
	, SingleValue1<Request>(this)
    , adc(_adc)
{}

void reflex::msp430x::ADC12_A::Measure::run()
{
	// since we can run one conversion a time only we have to prevent against
	// restarting this activity during a conversion
	this->lock();
	adc.switchOn();

	Request& request = adc.collector.request;
	request = this->get();
	
	uint16 refV = request.getinternalRef();
	ref::Registers()->CTL0 &= ~(ref::VSEL0 | ref::VSEL1); //remove old value
	//ref::Registers()->CTL0 |= refV<<4;
	if (refV == Ref20)
	  ref::Registers()->CTL0 |= ref::VSEL0; //set new value
	else if (refV == Ref25)
	  ref::Registers()->CTL0 |= ref::VSEL1; //set new value

	// select REF as Vref=Vr+ and Vr-=Vss, also select given inputchannel
	uint8 channel = request.getCH();
	uint8* memctl= Registers()->beginMemCTL();
	memctl += channel;// point to the ctl-register of the right channel
	*memctl = request.getCTL(); //set controll bits
	Registers()->CTL1.high.high = channel; //select memory start addr the result will be written to
	adc.ivDispatcher[Registers::Interrupt_traits::adcVector(channel)]=&adc.collector; //connect to interrupt of channel

	Registers()->IE |= 0x1<<(channel); //enable interrupt for that channel

	//engage AD-conversion
	Registers()->CTL0 |= ENC|SC;
//	Registers()->CTL0 |= SC;

}
/*! */
void reflex::msp430x::ADC12_A::Collector::notify()
{
	Registers()->IE &= ~(0x1<<request.getCH()); //disable interrupt
	Registers()->CTL0 &= ~(ENC| SC);
	adc.switchOff();
	this->trigger(); // initiate epilog
}

void reflex::msp430x::ADC12_A::Collector::run()
{
	uint8 channel= request.getCH();
	const uint16* mem = Registers()->beginMem();
	mem += channel;
	if(output) output->assign(channel,*mem);//read value from conversion memory register
	adc.ivDispatcher.clear(channel);
	adc.input.unlock();
}

