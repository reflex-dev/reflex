#ifndef COREAPPLICATION_H
#define COREAPPLICATION_H

#include <reflex/SystemStatusBlock.h>
#include <reflex/init/SystemInit.h>
#include <reflex/interrupts/InterruptGuardian.h>
#include <reflex/pmm/PMM.h>
#include <reflex/powerManagement/PowerManager.h>
#include <reflex/scheduling/Scheduler.h>
#include <reflex/timer/Timer0_A5.h>
#include <reflex/ucs/UCS.h>


namespace reflex {

namespace msp430x {
/*!
\brief Basic Application Class for the MSP430X Platform.

CoreApplication provides the necessary infrastructure for a minimal REFLEX
application such as:

- Interrupt Guardian
- Power Manager
- Scheduler

They can be retrieved through the accessor methods guardian(), powerManager()
and scheduler().

CoreApplication is implemented as singleton object. For the whole life-time of
an application, only one instance exists and can be retrieved using the
static instance() method. Therefore, the copy constructor and assignment
operator are disabled.

Two possible use-cases exist:

1. For statically linked applications
2. For applications that are located on stack

<h2>Static Usage</h2>
The whole application can be linked statically for the benefit of link-time
analysis, because all addresses can be determined during link-time. In order
for this to work, it has to be ensured, that the core application is
initialized before the application components. The following code snippet show,
how this can be achieved.

\code
// -------- File ReflexApplication.h --------

// Application class for static usage
// Correct initialization order is guaranteed through inheritance
class ReflexApplication : public reflex::mcu::CoreApplication
{
    // Here goes the whole component structure
};

// -------- File main.cc --------

// This is the global application object
ReflexApplication app;

int main()
{
    reflex::mcu::CoreApplication::exec();
    return 0;
}
\endcode


## Usage on Stack
The application can be created on stack as well. This might result in easier
debugging and makes it possible, to terminate the whole application without
performing a system reset. This is necessary for system updates as well.
The following code snippet shows, how to achieve this.

\code
// -------- File ReflexApplication.h --------

// Application class for stack usage
class ReflexApplication
{
    // Here goes the whole component structure
};

// -------- File main.cc --------

#include <reflex/CoreApplication.h>

int main()
{
    reflex::mcu::CoreApplication core;
    ReflexApplication app;
    reflex::mcu::CoreApplication::exec();
    return 0;
}
\endcode

 */
class CoreApplication
{
public:
    CoreApplication();
    ~CoreApplication();

    InterruptGuardian* guardian();
    PowerManager* powerManager();
    Scheduler* scheduler();

    static void exec();
    static CoreApplication* instance();

    static uint8 nodeId();
    static void setFirmwareInvalid();
    static uint8 softwareVersion();

private:
    CoreApplication(const CoreApplication&);
    CoreApplication& operator=(const CoreApplication&);

    class Initializer
    {
    public:
        Initializer(CoreApplication* app) { _instance = app; }
    };

    Initializer _initializer;
    static CoreApplication* _instance;
    static bool _isInitialized;

public:
    mcu::SystemInit init;
    SystemStatusBlock status;

private:
    PowerManager _powerManager;
    InterruptHandler* _handlerTable[interrupts::MAX_HANDLERS];
    InterruptGuardian _guardian;
    Scheduler _scheduler;

public:
    mcu::PMM pmm;
    mcu::UCS ucs;
    mcu::Timer0_A5 timer;
};

inline CoreApplication::~CoreApplication()
{
    Assert(_isInitialized == true);
    _instance = 0;
    _isInitialized = false;
}

/*!
\brief Accessor method to retrieve the interrupt guardian object.
 */
inline InterruptGuardian* CoreApplication::guardian() { return &_guardian; }

/*!
\brief Accessor method to retrieve the power manager object.
 */
inline PowerManager* CoreApplication::powerManager() { return &_powerManager; }

/*!
\brief Accessor method to retrieve the scheduler object.

The exact type of the schuleder depends on the scheduling scheme which
the REFLEX library is being compiled for.

 */
inline Scheduler* CoreApplication::scheduler() { return &_scheduler; }

/*!
\brief Starts event-handling via scheduler.

This method does not return.

 */
inline void CoreApplication::exec()
{
    _instance->_scheduler.start();
}

/*!
\brief Returns the single instance of CoreApplication.

It has to be ensured, that an instance of CoreApplication has been created
before this function is called.

 */
inline CoreApplication* CoreApplication::instance()
{
    return _instance;
}

//! \cond
struct FirmwareInfo
{
    enum {
        Undefined = 0xFFFF,
        Valid = 0x023c
    };

    uint16 header;
    uint8 version;
    uint8 reserved;
    uint16 checksum;
};
//! \endcond

/*!
\brief Defines firmware information to be linked into the flash image.

Use this macro in an application source file to define firmware version
information. The software version is an integer value between 0 and 255.

Firmware meta information including software version is added automatically
when using the qbs files provided along with REFLEX. The software version is
then taken from the ``version`` property defined in the ``ReflexApplication``
item.

\relates reflex::msp430x::CoreApplication
 */
#define DEFINE_SOFTWARE_VERSION(softwareVersion) \
    const reflex::msp430x::FirmwareInfo firmwareInfo __attribute__ ((section (".init0"))) = \
    { \
        0x3c02, \
        softwareVersion, \
        0xff, \
        0xffff \
    } \

}

}

extern const reflex::msp430x::FirmwareInfo firmwareInfo;


#endif // COREAPPLICATION_H
