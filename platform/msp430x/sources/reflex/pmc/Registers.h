/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 **/

#ifndef REFLEX_MSP430X_PMC_REGISTERS_H
#define REFLEX_MSP430X_PMC_REGISTERS_H

namespace reflex {
namespace msp430x {
namespace pmc {

struct Registers {
	volatile unsigned PMAPPWD;
	volatile unsigned PMAPCTL;

public:
	Registers(){}
	operator Registers*() {return operator->();}
	Registers* operator-> () {return reinterpret_cast<Registers*>(bases::PMC+offsets::PMC);}
};

struct MapRegisters {
	volatile uint8 MAP[8]; ///< map registers for pins 0 to 7
};

template<size_t TPortNR >
struct Port
	: public MapRegisters
{

public:
	Port(){}
	operator Port*() {return operator->();}
	Port* operator-> () {return reinterpret_cast<Port*>
			(bases::PMCMAP+((TPortNR-1)*sizeof(MapRegisters)));}
};

/* FIXME: mnemonics are mcu-variant dependent -> should be moved to respective Mnemonics.h files */
enum MNEMONICS {
	NONE		= 0
	,PASSWD		= 0x02D52
	,UCA0RXD	= 17
	,UCA0TXD	= 18
	,UCA0SOMI	= 17
	,UCA0SIMO	= 18
	,UCA0CLK	= 19
	,UCA0STE	= 22
	,UCB0SCL	= 20
	,UCB0SDA	= 21
	,UCB0SOMI	= 20
	,UCB0SIMO	= 21
	,UCB0CLK	= 22
	,UCB0STE	= 19
	,TA0CCR0A	= 9
	,TA0CCR1A	= 10
	,TA0CCR2A	= 11
	,TA0CCR3A	= 12
	,TA0CCR4A	= 13
	,TA1CCR0A	= 14
	,TA1CCR1A	= 15
	,TA1CCR2A	= 16
	,ANALOG		= 31
};

} // pmc
} // msp430x
} // reflex

#endif // REGISTERS_H
