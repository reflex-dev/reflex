/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 **/

#include "reflex/ucs/UCS.h"
#include "reflex/mcu/msp430x.h"
#include "reflex/sys/SYS.h"
#include "reflex/sys/Registers.h"
#include "reflex/pmm/PMM.h"
#include "reflex/System.h"
#include "reflex/io/Ports.h"
using namespace reflex;
using namespace msp430x;
using namespace ucs;


#define __bis_SR_register(x)    __asm__ __volatile__("bis	%0, r2" : : "ir" ((uint16) x))
#define __bic_SR_register(x)    __asm__ __volatile__("bic	%0, r2" : : "ir" ((uint16) x))
#define SCG0                   (0x0040)


#include "reflex/ucs/Registers.h"



UCS::UCS()
{
	frequency_kHz = 0;
	initXT1(); //initialize XT1 oscillator with 32.768 KHz

	setClockSource(ucs::ACLK, ucs::XT1CLK); //set ACLK to xt1
	setDCO(ucs::Mhz8); //
	setClockSource(SMCLK,DCOCLKDIV);
	setClockSource(MCLK,DCOCLKDIV);

	// Worst-case settling time for the DCO when the DCO range bits have been
	// changed is n x 32 x 32 x f_MCLK / f_FLL_reference. See UCS chapter in 5xx
	// UG for optimization.
	// 32 x 32 x 12 MHz / 32,768 Hz = 375000 = MCLK cycles for DCO to settle
	// NOTE: here has been a forced delay, but it seems to work good without it.

	waitForStabilisation(); //what do you expect to read in that comment. Obviously, we have to wait for a stable clock here.
}


void UCS::initXT1()
{
	Port5()->SEL |= BIT_1+BIT_0; //set P5.1,2 to xin and xout functionality
	Port5()->DIR &= ~(BIT_1+BIT_0); //set P5.1,2 to direction
	Registers()->UCSCTL6 &= ~(XT1DRIVEx|XT1OFF); // Xtal is now stable, reduce drive strength
	Registers()->UCSCTL6 |= XCAP3;  // select the capacitor for
}

bool UCS::setDCO(ucs::frequencyMultiplicator N)
{
	//set the needed power level
	if (N <= Mhz12) {
		getSystem().pmm.SetVCore(pmm::LEVEL0);
	} else if (N <= Mhz16) {
		getSystem().pmm.SetVCore(pmm::LEVEL1);
	} else if (N <= Mhz20) {
		getSystem().pmm.SetVCore(pmm::LEVEL2);
	} else if (N <= Mhz25) {
		getSystem().pmm.SetVCore(pmm::LEVEL3);
	} else return false;


	if (N < Mhz8) {
	  Registers()->UCSCTL1 = DCORSEL2;          // Select DCO range 0.5-7MHz operation
	} else {
	  Registers()->UCSCTL1 = DCORSEL5;          // Select DCO range 24MHz operation
	}


	Registers()->UCSCTL3 |= SELREF0;          // Set DCO FLL reference = XT1
	__bis_SR_register(SCG0);                  // Disable the FLL control loop
	Registers()->UCSCTL0 = 0x0000;            // Set lowest possible DCOx, MODx
	Registers()->UCSCTL2 = FLLD2 + N;         // Set DCO Multiplier for target Frequency
		                                      // (N + 1) * FLLRef = Fdco
		                                      // Set FLL Div = fDCOCLK/2
	__bic_SR_register(SCG0);                  // Enable the FLL control loop

	//very rough estimation
	frequency_kHz = (N-1) * 32;//*32768 => assumption: XT1 is source of DCO and clocked with 32678 kHz
	return true;
}

void UCS::setClockSource(ucs::Clock clk,ucs::ClockSource source)
{
	uint16 byte = Registers()->UCSCTL4 & ~(ClkSrcMASK<<clk);
	//change clocksource bits
	Registers()->UCSCTL4 = (byte | (source<<clk));
}


//Loop until XT1,XT2 & DCO fault flag is cleared
void UCS::waitForStabilisation() {
	 do
	 {
		// Clear XT2,XT1,DCO fault flags
		Registers()->UCSCTL7 &= ~(XT2OFFG | XT1LFOFFG | XT1HFOFFG | DCOFFG);
		sys::SFRegisters()->SFRIFG1 &= ~(sys::OFIFG);
	} while ( sys::SFRegisters()->SFRIFG1 & sys::OFIFG ); // Test oscillator fault flag
}


uint16 UCS::getCurrentFrequency()
{
	return frequency_kHz;
}
