#include <reflex/CoreApplication.h>
#include <reflex/flash/FLASH.h>

using namespace reflex;
using namespace msp430x;

// These variables are located into the bss segment and are therfore
// being initialized to zero according to C++ Standard 3.6.2 (1):
// "Objects with static storage duration (3.7.1) shall be zero-initialized
// (8.5) before any other initialization takes place."
// We have to rely on that, because CoreApplication might get initialized
// as static variable.
CoreApplication* CoreApplication::_instance;
bool CoreApplication::_isInitialized;

extern "C" {
void _lpm0();
void _lpm1();
void _lpm2();
void _lpm3();
void _lpm4();
void _lpm5();
}

namespace {
    PowerDownFunction powerFunctions[mcu::NrOfPowerModes] = {_active, _lpm0, _lpm1, _lpm2, _lpm3, _lpm4, _lpm5};
}

/*!
\brief Creates a CoreApplication object.

CoreApplication is a singleton. Calling this method twice will fail or may
result in undefined behaviour.
 */
CoreApplication::CoreApplication() : _initializer(this),
    _powerManager(powerFunctions), _guardian(_handlerTable, interrupts::MAX_HANDLERS)
{
    Assert(_isInitialized == false);
    _instance = this;
    _isInitialized = true;
}

/*!
\brief Returns the node identifier from flash memory.

This method retrieves the node identifier from flash memory which is not part
of the firmware, but has to be programmed separatly. If not set, 0xff is being
returned.

 */
uint8 CoreApplication::nodeId()
{
    return *reinterpret_cast<uint8*>(NODEINFOADDRESS);
}

/*!
\brief Marks the firmware image as invalid.

This method alters the firmware image and marks it as invalid. This action
can not be undone and must be followed by a firmware update. Use this method
to trigger a firmware update by the bootloader.

 */
void CoreApplication::setFirmwareInvalid()
{
    FLASH::writeWord(const_cast<uint16*>(&firmwareInfo.checksum), 0);
}

/*!
\brief Returns the applications's software version.

The software version is set by defining the #DEFINE_SOFTWARE_VERSION macro in one
of the application source files.
 */
uint8 CoreApplication::softwareVersion()
{
    return firmwareInfo.version;
}
