/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 **/

#ifndef REFLEX_MSP430_LCD_B_H
#define REFLEX_MSP430_LCD_B_H

#include "reflex/lcd/Registers.h"

namespace reflex {
namespace msp430x {/*! \ingroup msp430x @{ */
	//! hardware abstraction for the LCD_B hardware module
	/*!
	  */
	class LCD_B {
		typedef lcd::B_Registers Registers; //! registerfile type
		typedef lcd::LCD_B_Memory Memory; //! memory region
		typedef lcd::LCD_B_BlinkMemory BlinkMemory; //!	blink memory region
	public:
		LCD_B();
		//! clear the entire memory, including the blinkmemory.
		void clearMem() {Registers()->MEMCTL|=lcd::LCDCLRM | lcd::LCDCLRBM;}

		void enableSegments(const uint16 (&pins)[4]) { setSegments(pins,0xFFFF);}
		void disableSegments(const uint16 (&pins)[4]){ setSegments(pins,0x0);}

		void setLCDFreq(const lcd::FrequDiv&,const lcd::FrequPre&);
		void setBLKFreq(const lcd::BLKDiv &, const lcd::BLKPre &);

		void setACLK();
		void setVLOCLK();

		void setMux(const lcd::LCDMX &);

		void enable();
		void disable();
	protected:
		void setSegments(const uint16 (&pins)[4],const uint16&);
	};



}/*! @} */ //ns msp430x
} //ns reflex


inline
reflex::msp430x::LCD_B::LCD_B() {
	Registers()->CTL0 = 0x0;
	clearMem();
}

inline
void reflex::msp430x::LCD_B::enable()
{
	Registers()->CTL0 |= lcd::LCDON|lcd::LCDSON;
}

inline
void reflex::msp430x::LCD_B::disable()
{
	Registers()->CTL0 &= ~lcd::LCDON;
}

inline
void reflex::msp430x::LCD_B::setACLK()
{
	Registers()->CTL0 &= ~BIT_7;
}

inline
void reflex::msp430x::LCD_B::setVLOCLK()
{
	Registers()->CTL0 |= BIT_7;
}



#endif // LCD_B_H
