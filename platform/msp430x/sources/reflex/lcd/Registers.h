/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 **/

#ifndef REFLEX_MSP430_LCD_B_REGISTERS_H
#define REFLEX_MSP430_LCD_B_REGISTERS_H

#include "reflex/data_types/ReadOnly.h"
#include "reflex/data_types/Register.h"
#include "reflex/MachineDefinitions.h"


namespace reflex {
namespace msp430x {
namespace lcd {
namespace impl{
	class Memory {
	protected:
		typedef data_types::Register<uint8> Register;
		typedef data_types::ReadOnly<Register> RORegister;
	public:
		Register LCDM1;
		Register LCDM2;
		Register LCDM3;
		Register LCDM4;
		Register LCDM5;
		Register LCDM6;
		Register LCDM7;
		Register LCDM8;
		Register LCDM9;
		Register LCDM10;
		Register LCDM11;
		Register LCDM12;
		Register LCDM13;
		Register LCDM14;
		Register LCDM15;
		Register LCDM16;
		Register LCDM17;
		Register LCDM18;
		Register LCDM19;
		Register LCDM20;
		Register LCDM21;
		Register LCDM22;
		Register LCDM23;
		Register LCDM24;
		Register LCDM25;
		Register LCDM26;
		uint8 :8;
		uint8 :8;
		uint8 :8;
		uint8 :8;
		uint8 :8;
		uint8 :8;

	protected:
		Memory(){}
	public:
		uint8& operator[](const uint8& i) {return *(reinterpret_cast<uint8*>(this)+i);}
	};
} //ns impl

	enum {
		LCDON=		BIT_0
		,LCDSON=	BIT_2

		,LCDDISP=	BIT_0
		,LCDCLRM=	BIT_1
		,LCDCLRBM=	BIT_2
	};

	enum FrequDiv {
		LCDDIV_1=	0x0
		,LCDDIV_2=	1<<11
		,LCDDIV_4=	3<<11
		,LCDDIV_8=	7<<11
		,LCDDIV_10= 9<<11
		,LCDDIV_16= 0xF<<11
		,LCDDIV_32= 31<<11

		,LCDDIV_MASK= BIT_11|BIT_12|BIT_13|BIT_14|BIT_15
	};

	enum FrequPre {
		LCDPRE_1=	0x0
		,LCDPRE_2=	BIT_8
		,LCDPRE_4=	BIT_9
		,LCDPRE_8=	BIT_8|BIT_9
		,LCDPRE_16=	BIT_10
		,LCDPRE_32=	BIT_10|BIT_8
		,LCDPRE_MASK=BIT_8|BIT_9|BIT_10
	};
	enum BLKDiv {
		 BLKDiv_1 = 0x0 <<5
		,BLKDiv_2 = 0x1 <<5
		,BLKDiv_3 = 0x2 <<5
		,BLKDiv_4 = 0x3 <<5
		,BLKDiv_5 = 0x4 <<5
		,BLKDiv_6 = 0x5 <<5
		,BLKDiv_7 = 0x6 <<5
		,BLKDiv_8 = 0x7 <<5
		,BLKDiv_MASK=BIT_5|BIT_6|BIT_7
	};
	enum BLKPre {
		 BLKPre_512 = 0x0<<2
		,BLKPre_1024 = 0x1<<2
		,BLKPre_2048 = 0x2<<2
		,BLKPre_4096 = 0x3<<2
		,BLKPre_8162 = 0x4<<2
		,BLKPre_16384 = 0x5<<2
		,BLKPre_32767 = 0x6<<2
		,BLKPre_65536 = 0x7<<2
		,BLKPre_MASK = BIT_2|BIT_3|BIT_4
	};
	enum BLKMOD {
		BLKDIS =		0x0
		,BLKSelSegs =	0x1
		,BLKALLSegs =	0x2
		,BLKMemSW =	0x3
		,BLKMOD_MASK = 0x3
	};
	enum LCDMX {
		 STATIC = 0x0 <<3
		,MUX_2	= 0x1<<3
		,MUX_3  = 0x2<<3
		,MUX_4  = 0x3<<3
		,MUX_MASK = 0x3<<3
	};

	//! Registerfile for the lcd_b hardware module
	class B_Registers
	{
		typedef data_types::Register<uint16> Register;
		typedef data_types::ReadOnly<Register> RORegister;
	public:
		struct Interrupt_traits {
			enum Vector {
				//FIXME: may there fail some vertordefinitions
				NOCAPIFG = interrupts::IV0
				,BLKSEGON = interrupts::IV1
				,BLKSEGOFF = interrupts::IV2
				,FRAMINT = interrupts::IV3

			};
			enum {COUNT=5};

			typedef interrupts::IVRef<B_Registers> VectorRef;

			inline static InterruptVector getGlobal() {return interrupts::LCD_B;}
		};

	public:
		Register	CTL0; /*LCD_B control register 0 */
		Register	CTL1; /*LCD_B control register 1  */
		Register	BLKCTL;/*LCD_B blinking control register*/
		Register	MEMCTL;/*LCD_B memory control register*/
		Register	VCTL;/*LCD_B voltage control register*/
		Register	PCTL0;/*LCD_B port control 0*/
		Register	PCTL1;/*LCD_B port control 1*/
		Register	PCTL2;/*LCD_B port control 2 (≥128 segments)*/
		Register	PCTL3;/*LCD_B port control 3 (192 segments)*/
		Register	CPCTL;/*LCD_B charge pump control*/
		uint16 :16;
		uint16 :16;
		uint16 :16;
		uint16 :16;
		uint16 :16;
		RORegister	IV; /*LCD_B port*/

	public:
		B_Registers(){}
		operator B_Registers*() {return operator->();}
		B_Registers* operator-> () {return reinterpret_cast<B_Registers*> (bases::LCD_B+offsets::LCD_B);}
	};

//	typedef impl::Memory LCDB_Memory;
	class LCD_B_Memory
		: public impl::Memory
	{
	public:
		LCD_B_Memory(){}
		operator LCD_B_Memory*() {return operator->();}
		LCD_B_Memory* operator &() {return operator->();}
		volatile uint8* begin() {return reinterpret_cast<uint8*>(operator->());}
		operator volatile uint8*() {return begin();}

//		volatile uint8& operator[](const word& i) {return *(begin()+i);}

		LCD_B_Memory* operator-> () {return reinterpret_cast<LCD_B_Memory*>(bases::LCD_B+offsets::LCD_B_MEM);}
	};

	class LCD_B_BlinkMemory
		: public impl::Memory
	{
	public:
		LCD_B_BlinkMemory(){}
		operator LCD_B_BlinkMemory*() {return operator->();}
		LCD_B_BlinkMemory* operator &() {return operator->();}
		operator uint8*() {return memPtr();}
		uint8* memPtr() {return reinterpret_cast<uint8*>(operator->());}

//		uint8& operator[](const uint8& i) {return *(memPtr()+i);}

		LCD_B_BlinkMemory* operator-> () {return reinterpret_cast<LCD_B_BlinkMemory*>(bases::LCD_B+offsets::LCD_B_BLINK_MEM);}
	};



}
}} //msp430, reflex

#endif // REGISTERS_H
