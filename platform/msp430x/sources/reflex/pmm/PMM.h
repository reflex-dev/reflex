/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 **/

#ifndef REFLEX_MSP430X_PMM_H
#define REFLEX_MSP430X_PMM_H

#include "reflex/pmm/Registers.h"

namespace reflex {
namespace msp430x {


/*!	power management module (PMM) of the cc430 series.
 *  code partial taken from cc430x613x_PMM.c example by ti
 */
class PMM
{

public:

	/** keep initial level.
	 */
	PMM();

	/**
	 * Set the VCore to a new level
	 *
	 * @param level  PMM level ID
	 */
	void SetVCore(pmm::CoreVlevel level);

	//	/**
	//	  * returns maximal supported MCLK frequency for
	//	  *
	//	  * @return frequency
	//	  */
	//	uint8 supportedMCLK();
	//
	//
	//	uint8 getOptimalVoltage(uint16 kHz);
	//	void setOptimalVoltage(uint16 kHz);


private:

	/**
	 * Set the VCore to a new higher level
	 *
	 * @param level       PMM level ID
	 */
	void SetVCoreUp(unsigned char level);

	/**
	 * Set the VCore to a new Lower level
	 *
	 * @param level       PMM level ID
	 */
	void SetVCoreDown(unsigned char level);

	pmm::CoreVlevel current;

}; //class pmm

}} //ns msp430x, reflex
#endif // PMM_H
