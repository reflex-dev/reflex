/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 **/

#ifndef REFLEX_MSP430X_SYS_REGISTERS_H
#define REFLEX_MSP430X_SYS_REGISTERS_H

#include "reflex/data_types/ReadOnly.h"
#include "reflex/data_types/Register.h"

#include "reflex/mcu/msp430x.h"
#include "reflex/interrupts/InterruptDispatcher.h"

namespace reflex {
namespace msp430x {
	namespace sys {

		enum SFRBits {
			 WDTIE = BIT_0
			,NMIIFG = BIT_4
			,OFIFG = BIT_1
			,WDTIFG= BIT_0
		};
		namespace bits {
			/** bits for register @name
			*/
			struct SFRIE1 {
				bool WDTIE	:1 ;
				bool OFIE	:1 ;
				bool :1;
				bool VMAIE	:1 ;
				bool NMI	:1 ;
				bool ACCVIE	:1 ;
				bool JMBINIE	:1 ;
				bool JMBOUTIE	:1 ;
				uint8 :8 ;
			};

			/** bits for register @name
			*/
			struct SFRIFG1 {
				bool WDTIFG	:1 ;
				bool OFIFG	:1 ;
				bool :1;
				bool VMAIFG	:1 ;
				bool NMIIFG	:1 ;
				bool :1;
				bool JMBINIFG	:1 ;
				bool JMOUTIFG	:1 ;
				uint8 :8;
			};

			/** bits for register @name
			*/
			struct SFRRPCR {
				bool SYSNMI	:1 ;
				bool SYSNMIIES	:1 ;
				bool SYSRSTUP	:1 ;
				bool SYSRSTRE	:1 ;
				uint8 :4;
				uint8 :8;
			};
		}

		/** special function register
		 */
		class SFRegisters
		{
			//curry like bind of the first template parameter
			template<typename T2nd=data_types::Ommitted>
			struct Register : data_types::Register<uint16,T2nd> {};

		public:
			Register<bits::SFRIE1>	SFRIE1;
			Register<bits::SFRIFG1>	SFRIFG1;
			Register<bits::SFRRPCR>	SFRRPCR;

		public:
			SFRegisters(){}
			operator SFRegisters*() {return operator->();}
			SFRegisters* operator-> () {
				return reinterpret_cast<SFRegisters*>
					(cpu::bases::SPECIALFUNCTION+offsets::SFR);
			}
		};

		class Registers
		{
			typedef data_types::Register<uint16> Register;
			typedef data_types::ReadOnly<data_types::Register<uint16> > RO_Register;
		public:
			Register	CTL; ///<
			Register	BSLC; ///<
			Register	JMBC; ///<
			uint16	:16; //1 word padding
			Register	JMBI0; ///<
			Register	JMBI1; ///<
			Register	JMBO0; ///<
			Register	JMBO1; ///<
			uint16	:16;//8 word padding
			uint16	:16;
			uint16	:16;
			uint16	:16;
			uint16	:16;
			uint16	:16;
			uint16	:16;
			uint16	:16;
			RO_Register	BERRIV; ///< Bus Error Vector Generator
			RO_Register	UNIV; ///< User NMI Vector Generator
			RO_Register	SNIV; ///< System NMI Vector Generator
			RO_Register	RSTIV; ///< Reset Vector Generator

		public:
			Registers(){}
			operator Registers*() {return operator->();}
			Registers* operator-> () {return reinterpret_cast<Registers*>(bases::SYS+offsets::SYS);}
		};


		//! properties for the logical vector of the USER_NMI vector
		struct UserNMInterrupt_traits {
		private:
			struct IVRef {	word operator* () const { return Registers()->UNIV;}	};
		public:
			//! the logical interrupt Vector for the BERR Vector
			enum Vector {
				 NMIFG = interrupts::IV0
				,OFIFG = interrupts::IV1
				,ACCVIFG = interrupts::IV2
				,RESERVED = interrupts::IV3 //for future extentions
			};
			enum {COUNT=5};

			typedef IVRef VectorRef;

			inline static InterruptVector globalVector() {return interrupts::USER_NMI;}
		};

		//! properties for the logical vector of the SYS_NMI vector
		struct SysNMInterrupt_traits {
		private:
			struct IVRef { word operator* () const { return Registers()->SNIV;}	};

		public:
			//! the logical interrupt Vector for the BERR Vector
			enum Vector {
				 SVMLIFG = interrupts::IV0
				,SVMHIFG = interrupts::IV1
				,SVSMLDLYIFG = interrupts::IV2
				,SVSMHDLYIFG=interrupts::IV3
				,VMAIFG=interrupts::IV4
				,JMBINIFG=interrupts::IV5
				,JMBOUTIFG=interrupts::IV6
				,SVMLVLRIFG=interrupts::IV7
				,SVMHVLRIFG=interrupts::IV8
				,RESERVED=interrupts::IV9
			};
			enum {COUNT=11};

			typedef IVRef VectorRef;

			inline static InterruptVector globalVector() {return interrupts::SYS_NMI;}
		};

		typedef reflex::InterruptDispatcher< UserNMInterrupt_traits > UserIVDispatcher;
		typedef reflex::InterruptDispatcher< SysNMInterrupt_traits >  SystemIVDispatcher;
//		typedef reflex::InterruptDispatcher<sys::BERRIV_SIZE, BerrIVRef >  BerrIVDispatcher;
////		typedef reflex::InterruptDispatcher<sys::RSTIV_SIZE, sys::RstIVRef >  BerrIVDispatcher;

	}//ns sys
}} //msp430x, reflex
#endif // REGISTERS_H
