/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *

 *
 *    This file is part of REFLEX.
 *
 *    REFLEX is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as
 *    published by the Free Software Foundation, either version 3 of the
 *    License, or (at your option) any later version.
 *
 *    REFLEX is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with REFLEX.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include "reflex/rtc/RTC_A.h"
#include "reflex/MachineDefinitions.h"
#include "reflex/io/Ports.h"


using namespace reflex;
using namespace msp430x;

/*!
  resets the hardware to the initial value documented in the datasheet.
  Moreover the BCD-Mode, ACLK as source are set.
*/
reflex::msp430x::RTC_A::RTC_A()
	: PowerManageAble(PowerManageAble::PRIMARY)
	, inDate(*this)
	, timerEvent(*this)
	, ivDispatcher( interrupts::RTC_A )
{
	//reset both controlregisters
	//set HOLD and BCD bit and CalMode
	Registers()->CTL01 = (rtc::HOLD|rtc::BCD|rtc::MODE|rtc::SSEL_ACLK|rtc::TEVminute)<<8;
	Registers()->CTL23 = 0x0;
	timerEvent.refreshModeMinutes();


//	Registers()->PS0CTL = rtc::RT0PSHOLD;
//	Registers()->PS1CTL |= rtc::RT1PSDIV128; //select 1 Hz prescaler event interrupt
//set frequency adjustment
///*DEBUG*/Registers()->CTL2 |= 0x20; //measured
///*DEBUG*/timerEvent.refreshModeSeconds();
///*DEBUG*/Registers()->CTL0 |= rtc::RDYIE;
////set RTCCKL pin to 512 Hz
///*DEBUG*/Registers()->CTL3=0x1;
///*DEBUG*/Port2()->SEL |= BIT_4;
///*DEBUG*/Port2()->DIR |= BIT_4;

	this->setSleepMode( LPM3 ); // RTC is sourced from the ACLK which works until lpm3

	//register interrupt handler
	ivDispatcher[Registers::Interrupt_traits::RDY]=&inDate; // INT_RDY interrupt source
	//remaining will be set when needed
}

void reflex::msp430x::RTC_A::disable()
{
	// stops the hardware
	Registers()->CTL1 |= rtc::HOLD;
//	timerEvent.disable();
}

void reflex::msp430x::RTC_A::enable()
{
	//resumes the hardware
	Registers()->CTL1 &= ~rtc::HOLD;
	timerEvent.notify();
}

/*!
  \param rm		the new interval for emitting the refresh interval
*/
void reflex::msp430x::RTC_A::setRefreshInterval(const RTC::RefreshInterval& rm)
{
	switch (rm) {
		case RTC::SECONDS :		timerEvent.refreshModeSeconds(); break;
		case RTC::MINUTES :		timerEvent.refreshModeMinutes(); break;
		case RTC::HOURS :		timerEvent.refreshModeHour(); break;
		case RTC::MIDNIGHT :	timerEvent.refreshModeMidnight(); break;
		case RTC::NOON :		timerEvent.refreshModeNoon(); break;
	}
}


/*! \class RTC_A::SetDateSink
	Owing to synchronisation issues between asynchroneous counter and cpu we have
	prevent against the hardware, when writing the registers. The date is temporarily
	be saved and will be written when the  RDY-interrupt appears. So a window of one second
	is available for writing the date.
*/

/*! saves date and enables interrupt*/
void reflex::msp430x::RTC_A::SetDateSink::assign(const Date &date)
{
	//save date
	this->date=date;
	//due to synchronisation issues with the hardware we have a window 1 second
	//to safetly write the count Registers we have to wait for the start of that window
	//signaled through the RDY interrupt.
	//enable the interrupt
	Registers()->CTL0 |= rtc::RDYIE;
}

/*! writes the register when hardware is ready*/
//FIXME: that occours within the interrupt handling routine
void reflex::msp430x::RTC_A::SetDateSink::notify()
{
	//disable the interrupt
	Registers()->CTL0 &= ~rtc::RDYIE;
	//write Date value to hardware
	Registers().getDate() = date;
	//notify for refresh


	//rtc.timerEvent.notify();
}

/*!	\class RTC::TimerEvent
	propagates periodly the new date value;
*/

/*! */
void reflex::msp430x::RTC_A::TimerEvent::notify()
{
	// fires event only if counter is zero
	if(counter-- == 0)
	{
		//save timer register values
		this->date = Registers().getDate();
		this->trigger(); //trigger propagation
		counter = counterRef;
	}
}

void reflex::msp430x::RTC_A::TimerEvent::run()
{
	if(output)
		output->assign(this->date);
	if(event)
		event->notify();
}

void reflex::msp430x::RTC_A::TimerEvent::refreshModeSeconds()
{
	Registers()->PS1CTL &= ~rtc::RT1PSHOLD;
	Registers()->PS1CTL |= rtc::RT1PSIE;
	Registers()->CTL0	&= ~rtc::TEVIE;
	//connect to the ready interrupt, which fires each second
	rtc.ivDispatcher[Registers::Interrupt_traits::RDY]=this;
	//enable the ready interrupt
	Registers()->CTL0 |= rtc::RDYIE;
}

/*! timer event minute*/
void reflex::msp430x::RTC_A::TimerEvent::refreshModeMinutes()
{
	Registers()->CTL0 |= rtc::TEVIE;
	Registers()->CTL1 = ( Registers()->CTL1 & ~rtc::TEVx) | rtc::TEVminute;
	rtc.ivDispatcher[Registers::Interrupt_traits::TEV]=this;
}

/*! timer event hour*/
void reflex::msp430x::RTC_A::TimerEvent::refreshModeHour()
{
	Registers()->CTL0 |= rtc::TEVIE;
	Registers()->CTL1 = ( Registers()->CTL1 & ~rtc::TEVx) | rtc::TEVhour;
	rtc.ivDispatcher[Registers::Interrupt_traits::TEV]=this;
}

/*! timer event every day at 00:00*/
void reflex::msp430x::RTC_A::TimerEvent::refreshModeMidnight()
{
	Registers()->CTL0 |= rtc::TEVIE;
	Registers()->CTL1 = ( Registers()->CTL1 & ~rtc::TEVx) | rtc::TEVMidnight;
	rtc.ivDispatcher[Registers::Interrupt_traits::TEV]=this;
}

/*! timer event every day at 12:00*/
void reflex::msp430x::RTC_A::TimerEvent::refreshModeNoon()
{
	Registers()->CTL0 |= rtc::TEVIE;
	Registers()->CTL1 = ( Registers()->CTL1 & ~rtc::TEVx) | rtc::TEVNoon;
	rtc.ivDispatcher[Registers::Interrupt_traits::TEV]=this;
}


void reflex::msp430x::RTC_A::Alarm::run()
{
	rtc::BCDADate& aDate = Registers().getAlarmDate();
	aDate = this->get();
	if(  aDate.isSet() ) {
		//enable the alarm interrupt
		Registers()->CTL0 |= rtc::AIE;
	}else {
		//disable the alarm interrupt
		Registers()->CTL0 &= ~rtc::AIE;
	}
}


void reflex::msp430x::RTC_A::configure()
{
	RTCConfiguration cfg;
	confData.get(cfg);

	setRefreshInterval(cfg.getPrescaling());

	timerEvent.counterRef = cfg.getNumberOfIntervals();

	if(timerEvent.counterRef != 0)
		timerEvent.counterRef -= 1;

	timerEvent.counter = timerEvent.counterRef;
}

