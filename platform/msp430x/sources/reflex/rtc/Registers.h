/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 **/

#ifndef REFLEX_MSP430X_RTC_REGISTERS_H
#define REFLEX_MSP430X_RTC_REGISTERS_H

//TODO: these file does not only contain registers -> rename it

#include "reflex/types.h"
#include "reflex/MachineDefinitions.h"
#include "reflex/interrupts/InterruptDispatcher.h"
#include "reflex/data_types/Partial.h"
#include "reflex/data_types/BCD.h"
#include "reflex/data_types/ReadOnly.h"
#include "reflex/data_types/Register.h"

#include "reflex/memory/memcpy.h"
namespace reflex {
namespace msp430x {
namespace rtc {
	enum Bits{
	  //CTL0
	  RDYIFG  = BIT_0, ///clock source select
	  AIFG  = BIT_1,
	  TEVIFG = BIT_2,
	  RDYIE = BIT_4,
	  AIE = BIT_5,
	  TEVIE = BIT_6,
	  //CTL1
	  TEVx = BIT_0|BIT_1,
	  TEVov8 = 0x00, //counter mode overflow events
	  TEVov16 = BIT_0,
	  TEVov24 = BIT_1,
	  TEVov32 = BIT_0|BIT_1,
	  TEVminute = 0x00, //calendar mode overflow events
	  TEVhour = BIT_0,
	  TEVMidnight = BIT_1,
	  TEVNoon = BIT_0 + BIT_1,
	  TEV0 = BIT_1,
	  TEV12 = BIT_0|BIT_1,
	  SSELx = BIT_2|BIT_3,
	  SSEL_ACLK = 0x00,
	  SSEL_SMCLK = BIT_0,
	  SSEL_RT1PS = BIT_2|BIT_3,
	  RDY = BIT_4,
	  MODE = BIT_5,
	  HOLD = BIT_6,
	  BCD = BIT_7,
	  //CTL2
	  CAL = 0x3F, //callibration
	  CALS = BIT_7,
	  //CTL3
	  CALFx = BIT_0|BIT_1, // callibration output frequency
	  CALFno = 0x00,
	  CALF512 = BIT_0,
	  CALF256 = BIT_1,
	  CALF1 = BIT_0|BIT_1,
	  //PS0CTL
	  RT0PSIFG = BIT_0,
	  RT0PSIE = 0x0002,
	  RT0IPx = 0x001C,
	  RT0IP2 = 0x0000,
	  RT0IP4 = 0x0004,
	  RT0IP8 = 0x0008,
	  RT0IP16 = 0x000C,
	  RT0IP32 = 0x0010,
	  RT0IP64 = 0x0014,
	  RT0IP128 = 0x0018,
	  RT0IP256 = 0x001C,
	  RT0PSHOLD = 0x0100,
	  RT0PSDIVx = 0x3800,
	  RT0PSDIV2 = 0x0000,
	  RT0PSDIV4 = 0x0800,
	  RT0PSDIV8 = 0x1000,
	  RT0PSDIV16 = 0x1800,
	  RT0PSDIV32 = 0x2000,
	  RT0PSDIV64 = 0x2800,
	  RT0PSDIV128 = 0x3000,
	  RT0PSDIV256 = 0x3800,
	  RT0SSEL = 0x4000,
	  //PS1CTL
	  RT1PSIFG = BIT_0,
	  RT1PSIE = 0x0002,
	  RT1IPx = 0x001C,
	  RT1IP2 = 0x0000,
	  RT1IP4 = 0x0004,
	  RT1IP8 = 0x0008,
	  RT1IP16 = 0x000C,
	  RT1IP32 = 0x0010,
	  RT1IP64 = 0x0014,
	  RT1IP128 = 0x0018,
	  RT1IP256 = 0x001C,
	  RT1PSHOLD = 0x0100,
	  RT1PSDIVx = 0x3800,
	  RT1PSDIV2 = 0x0000,
	  RT1PSDIV4 = 0x0800,
	  RT1PSDIV8 = 0x1000,
	  RT1PSDIV16 = 0x1800,
	  RT1PSDIV32 = 0x2000,
	  RT1PSDIV64 = 0x2800,
	  RT1PSDIV128 = 0x3000,
	  RT1PSDIV256 = 0x3800,
	  RT1SSEL = 0x4000
	};
	//! the date of the rtc encoded in the BCD format
	/*! optimized for copying from hardware */
	class BCDDate
	{
		typedef data_types::Partial<uint16> Partial;
		typedef Partial::LowType	LowPart;
		typedef Partial::HighType	HighPart;
	public:
		BCDDate(){secmin=0;hourdow=0;daymon=0x0101;year=0;}
		union {
			Partial		secmin;
			LowPart		sec;
			HighPart	min;
		};
		union {
			Partial		hourdow;
			LowPart		hour;
			HighPart	dow;
		};
		union {
			Partial		daymon;
			LowPart		day;
			HighPart	mon;
		};
		Partial		year;
	};

	//! the alarm date of the rtc encoded in the BCD format
	/*! optimized for copying from hardware */
	class BCDADate
	{
		enum { AE_BIT = BIT_7 };
		typedef data_types::Partial<uint16> Partial;
		typedef Partial::LowType	LowPart;
		typedef Partial::HighType	HighPart;
	public:
		BCDADate(){minhour=0x0; dowday=0x0101;}
		BCDADate& operator= (const BCDDate& date) {memcpy(this,&date.min,sizeof(BCDADate)); return *this;}
		bool isSet() { return ((minhour==0)||(dowday==0) );}
		void setMin(const data_types::BCD<2>& val) {this->min=val|AE_BIT;}
		void setHour(const data_types::BCD<2>& val) {this->hour=val|AE_BIT;}
		void setDay(const data_types::BCD<2>& val) {this->day=val|AE_BIT;}
		void setDoW(const data_types::BCD<2>& val) {this->dow=val|AE_BIT;}
	protected:
		union {
			Partial		minhour;
			LowPart		min;
			HighPart	hour;
		};
		union {
			Partial		dowday;
			LowPart		dow;
			HighPart	day;
		};
	};


	//! Registerfile for the real time clock module
	class Registers_A
	{
		enum {	 BASE_ADDR = bases::RTC_A+offsets::RTC_A
				,DATE_OFFSET = 8*2 // BaseAddr + 8 16Bit registers
				,ADATE_OFFSET = DATE_OFFSET + 4 *2
		};
		typedef Registers_A RegisterFile;
	public:
		struct Interrupt_traits {
			enum Vector {
				 RDY = interrupts::IV0
				,TEV = interrupts::IV1
				,ALARM = interrupts::IV2
				,RT0PS = interrupts::IV3
				,RT1PS = interrupts::IV4
			};
			enum {COUNT=9};

			typedef interrupts::IVRef<Registers_A> VectorRef;

			inline static InterruptVector globalVector() {return interrupts::RTC_A;}
		};


	public:
		typedef reflex::InterruptDispatcher<Interrupt_traits> IVDispatcher; //! interrupt dispatcher for interruptvector of rtc_a

		typedef data_types::Register<uint16> Register;
		typedef data_types::ReadOnly<Register> RORegister;


	public:
		union {
			Register CTL01;
			Register::LowType CTL0;
			Register::HighType CTL1;
		};
		union {
			Register CTL23;
			Register::LowType CTL2;
			Register::HighType CTL3;
		};
		uint16: 16;
		uint16: 16;
		Register PS0CTL;
		Register PS1CTL;
		Register PS;
		RORegister IV;
		union {
			Register TIM0;
			Register CNT12;
			Register::LowType SEC;
			Register::HighType MIN;
			Register::LowType CNT1;
			Register::HighType CNT2;
		};
		union {
			Register TIM1;
			Register CNT34;
			Register::LowType HOUR;
			Register::HighType DoW;
			Register::LowType CNT3;
			Register::HighType CNT4;
		};
		union {
			Register DATE;
			Register::LowType DAY;
			Register::HighType MON;
		};
		Register YEAR;
		union {
			Register AMINHR;
			Register::LowType AMIN;
			Register::HighType AHOUR;
		};
		union {
			Register ADOWDAY;
			Register::LowType ADOW;
			Register::HighType ADAY;
		};

	public:
		Registers_A(){}
		operator RegisterFile*() {return operator->();}
		RegisterFile* operator-> () {return getPtr();}
		const RegisterFile* operator-> () const {return getPtr();}

		BCDDate& getDate() { return *reinterpret_cast<BCDDate*> (BASE_ADDR+DATE_OFFSET);}
		BCDADate& getAlarmDate() {return *reinterpret_cast<BCDADate*> (BASE_ADDR+ADATE_OFFSET);}
	private:
		RegisterFile* getPtr() { return reinterpret_cast<RegisterFile*>(BASE_ADDR); }
		const RegisterFile* getPtr() const { return reinterpret_cast<const RegisterFile*>(BASE_ADDR); }
	};

}//ns rtc

}} //ns msp430x, reflex


#endif // REGISTERS_H
