/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 **/

#ifndef REFLEX_MSP430X_RTC_H
#define REFLEX_MSP430X_RTC_H

#include "reflex/rtc/Registers.h"
#include "reflex/powerManagement/PowerManageAble.h"
#include "reflex/scheduling/ActivityFunctor.h"
#include "reflex/sinks/SingleValue.h"
#include "reflex/sinks/Sink.h"
#include "reflex/sinks/Event.h"

#include "RTC_types.h"

#include "reflex/driverConfiguration/ConfigurableDriver.h"
#include "reflex/driverConfiguration/RTCConfiguration.h"


namespace reflex {
namespace msp430x {



/*! \ingroup MSP430x @{ */

	/* FIXME: when setting the date we have to wait for the hardware. The RDY interrupt source
	   signals the readyness of the hardware and we can write the date within the next second.
	   Otherwise the behaviour is inpredictable. Changing the refreshtime to secondly causes to use the same
	   interruptvector for date change and
	*/
	//! real time clock presentation
	/*!
		\note there is still a problem then refreshInterval Seconds is set and the date will be changed.
	*/
	class RTC_A : public PowerManageAble, public ConfigurableDriver<RTCConfiguration>
	{
		typedef rtc::Registers_A Registers; //! the registerfile
	public:
		typedef rtc::BCDDate Date;	//!	represents the date
		typedef rtc::BCDADate ADate; //! represents the alarm date



	protected:
		//! change Date of the hardware
		class SetDateSink: public Sink0, public Sink1<const Date&> {
		public:
			SetDateSink(RTC_A& rtc): rtc(rtc) , date()	{}

			//! initiate a date change on the hardware
			virtual void assign(const Date&);
		protected:
			//! notifyed when hardware is ready to be written
			/*! this function updates the date, priviously chached into ::date,
				into the hardware register. */
			virtual void notify();
//			//! initiate a date change on the hardware
//			virtual void assign(const Date&);
		protected:
			RTC_A& rtc;
			Date date;	//! cached date which will be written to the hardware registers
		};

		//! extract date value from hardware.
		class TimerEvent: public Sink0, public Activity {
		public:
			TimerEvent(RTC_A& rtc):rtc(rtc),output(0),date(), counter(0), counterRef(0){}
			~TimerEvent() {};
			//! reads date and triggers activity
			virtual void notify();
			//! propagates date to the outport
			virtual void run();

			//! @{ changes the intervall for the extraction
			void refreshModeSeconds();
			void refreshModeMinutes();
			void refreshModeHour();
			void refreshModeMidnight();
			void refreshModeNoon();
			//! @}

//			void enable() {Registers()->CTL0 |=rtc::TEVIE;}
//			void disable() {Registers()->CTL0 &=~rtc::TEVIE;}
			//! operators cause this type is acting like a outport @{
			operator Sink1<const Date&>*() {return output;}
			TimerEvent& operator=(Sink1<const Date&>*const val) { output=val; return (*this); }
			//! @}
		//protected:
			RTC_A& rtc;
			Sink1<const Date&>* output; //! sink the date value is assigned to
			Date date;	//! the date is written to the output port

			/**
			 *  external event is only triggered when counter reaches 0
			 */
			uint16 counter;

			/**
			 * the counter refrence
			 */
			uint16 counterRef;

			/**
			 * event that notifies sth. if RTC timer expired
			 */
			reflex::Event* event;
		};


		//! sink for setting the alarm values
		class Alarm : public SingleValue1<ADate>, protected Activity {
		public:
			Alarm() : SingleValue1<ADate>(this)
			{}
		protected:
			virtual void run();
		protected:
		};

	public:
		//! initialize the hardware module to calendar-mode
		RTC_A();
		//! deconstruction, do nothing
		~RTC_A(){}

		//! registers a sink that consumes the date in the specified interval
		void init(Sink1<const Date&>* refresh, Sink0* alarm) __attribute__((deprecated))
		{
            connect_out_timerEvent(refresh);
			setAlarmEvent(alarm);
		}


		void setRefreshEvent(Sink1<const Date&>* output) __attribute__((deprecated))
		{
            this->timerEvent = output;
		}

		/**
		 * connects the output with a sink. This output is a simple event
		 * with no further information
		 *
		 * @param sink the data sink where the output has to connect to
		 */
		inline void connect_out_event(Event* sink)
		{
			timerEvent.event = sink;
		}

		/**
		 * connects the output with a sink. This output additionally contains
		 * information about the current time.
		 *
		 * @param sink the data where the output has to connect to
		 */
		inline void connect_out_timerEvent(Sink1<const Date&>* sink)
		{
			timerEvent.output = sink;
		}



		void setAlarmEvent(Sink0* alarm)
		{
			if(alarm)
				ivDispatcher[Registers::Interrupt_traits::ALARM] = alarm; // register handler
			else
				ivDispatcher.clear(Registers::Interrupt_traits::ALARM);	// remove any handler
		}



//		//! change mode of module @{
//		void changetoCalMode() { Registers()->CTL1 |= rtc::MODE;}
//		void changetoCountMode() { Registers()->CTL1 &= ~rtc::MODE;}
//		//! @}
	protected:
		//! turn on the hardware
		virtual void enable();
		//! turn off the hardware, so no power will be consumed
		virtual void disable();

		/**
		 * implements the method from ConfigurableDriver
		 */
		void configure();

	public:
		SetDateSink inDate; //! set the date
		Alarm	inAlarm;	//! set an alarm
	protected:

		TimerEvent timerEvent;

		Registers::IVDispatcher ivDispatcher; //! interruptVector for the RTC_A module

	private:


        /**
         * instead of using this method directly configuration objects are recommended
         */
        void setRefreshInterval(const RTC::RefreshInterval& rm);
	};

//	//! representation of BCDDate for easy usage
//	class RTC_A::Date
//		: public rtc::BCDDate
//	{
//	public:
//		Date(){}
//		Date(const rtc::BCDDate& date):BCDDate(date){}
//		Date& operator=(const rtc::BCDDate& val ) { this->BCDDate::operator=(val); return (*this);}
//	};

/*! @} */
}
}
#endif // RTC_H
