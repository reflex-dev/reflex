/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
  
 *	Author:		 Andre Sieber
 */

#include "reflex/SystemStatusBlock.h"
#include "reflex/flash/FLASH.h"
#include "reflex/MachineDefinitions.h"

using namespace reflex;

extern "C" SystemStatusBlock __systemStatusMem;


void SystemStatusBlock::save()
{
  this->not_wrote=true;
  mcu::FLASH::copy((caddr_t)this,(caddr_t)&__systemStatusMem, sizeof(SystemStatusBlock));
}
void SystemStatusBlock::load()
{
  if (__systemStatusMem.not_wrote)
    memcpy((caddr_t)this,(caddr_t)&__systemStatusMem, sizeof(SystemStatusBlock));
}
