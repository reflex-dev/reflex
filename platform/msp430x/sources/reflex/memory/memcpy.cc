/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *

 *	Author:		 Carsten Schulze
 */

#include "reflex/memory/memcpy.h"

namespace reflex {

void* memcpy(void *dest, const void *src, size_t size)
{

	for ( size_t i = 0; i < size; i++) {
		((char*)dest)[i] = ((char*)src)[i];
	}

	return dest;
}

}//reflex
