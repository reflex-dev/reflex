/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 
 *
 *    This file is part of REFLEX.
 *
 *    REFLEX is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as
 *    published by the Free Software Foundation, either version 3 of the
 *    License, or (at your option) any later version.
 *
 *    REFLEX is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with REFLEX.  If not, see <http://www.gnu.org/licenses/>.
 **/
#include "reflex/memory/memset.h"

namespace reflex {

//! fill memory region with a constant byte
void* memset(void* dst, const int v, size_t c) {
	uint8 *mem = reinterpret_cast<uint8*>(dst);
	for (word i=0;i<c;i++) {
		*mem++ = static_cast<const uint8>(v);
	}
	return dst;
}

//! fill memory with a constant word, word by word
void* memsetWord(void* dest, const word w, const size_t count) {
	register word* mem = static_cast<word*> (dest);
	for(word i=0; i<count; i++) {
		(*mem++)=w;
	}
	return dest;
}
}

