/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(es):	 USCI_I2C_Master
 *
 *	Author:	     Stefan Nuernberger
 *
 *	Description: Bus master driver for i2c using hardware USCI module
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and
 *    Operating Systems. All rights reserved.
 *
 *    Redistribution and use in source and binary forms, with or without
 *    modification, are permitted provided that the following conditions
 *    are met:
 *
 *       1. Redistributions of source code must retain the above copyright
 *          notice, this list of conditions and the following disclaimer.
 *
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the
 *          distribution.
 *
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *    The views and conclusions contained in the software and documentation
 *    are those of the authors and should not be interpreted as representing
 *    official policies, either expressed or implied, of BTU Cottbus,
 *    Department for Distributed Systems and Operating Systems.
 *
 **/

#ifndef REFLEX_USCI_I2C_MASTER_H
#define REFLEX_USCI_I2C_MASTER_H

#include "reflex/usci/Registers.h"
#include "reflex/io/I2C.h"
#include "reflex/pmc/Registers.h"
#include "reflex/io/Ports.h"
#include "reflex/types.h"

/** USCI_I2C_Master
  *
  * This is an implementation for the master bus driver of an i2c bus.
  * Drivers for i2c devices (connected to the bus) hook up to this
  * class to communicate with the devices.
  *
  * You should not use this class directly but rather derive your slave driver
  * from devices::I2C_Device, which can be connected to any class implementing
  * the devices::I2C_Master interface. Connection with the correct master will
  * be done in the NodeConfiguration of the application.
  *
  * The driver does not use interrupts but polls on the interrupt flags.
  * This way we maximize throughput. It is not effective to signal interrupts
  * on every received/sent byte. We rather offer a synchronous bytewise interface
  * with fine grained bus control.
  *
  * Currently only 7-bit addressing is supported.
  * Multi-Master mode is currently NOT supported.
  */
namespace reflex{
namespace msp430x {
namespace usci {

enum Constants {
    I2C_MASTER_DEFAULT_ADDRESS = 0x002c // default "own" address
};

/* USCI_I2C_Master
 * The i2c master bus driver
 */
template<typename Registers>
class USCI_I2C_Master : public devices::I2C_Master
{
private:
    uint16 masterAddress; // I2C Address of USCI module

public:

    /* constructor
     * @param masterAddress I2C address of master (7 bit only)
     */
    USCI_I2C_Master(uint16 masterAddress = I2C_MASTER_DEFAULT_ADDRESS);

    /* i2c_command
     * issue bus control command
     * @param addr, slave address
     * @param cmd, bus command
     * @return bool, true if ack received (only READ_ACK), false else
     */
    virtual bool i2c_command(uint8 addr, devices::i2c_control_t cmd);

    /* i2c_send
     * send a byte to the device
     * @param byte, the byte to send
     */
    virtual void i2c_send(uint8 byte);

    /* i2c_recv
     * receive a byte from the device
     * @param addr, slave address for RESTART
     * @param finish, send ACK, (NACK+)STOP, (NACK+RE)[START_WRITE|START_READ]
     * @return uint8, received byte
     */
    virtual uint8 i2c_recv(uint8 addr, devices::i2c_control_t finish);

    /* features of PowerManageAble */
    void enable();
    void disable();

private:

    /* start
     * start or restart reading or writing
     * @param addr, slave address
     * @param cmd, bus command - should be START_READ or START_WRITE
     */
    void start(uint8 addr, devices::i2c_control_t cmd);

    /* stop
     * transmit a STOP signal
     */
    void stop();
};

} // ns usci
} // ns msp430x
} // ns reflex

namespace reflex {
namespace msp430x {
namespace usci {

using devices::START_WRITE;
using devices::START_READ;
using devices::STOP;
using devices::ACK;
using devices::CHECK_ACK;


/* constructor
 * @param masterAddress I2C address of master (7 bit only)
 */
template<typename Registers>
USCI_I2C_Master<Registers>::USCI_I2C_Master(uint16 masterAddress)
{
    this->masterAddress = masterAddress;

    // set sleepmode for power manager
    this->setSleepMode(LPM4);
}

/* i2c_command
 * issue bus control command
 * @param addr, slave address
 * @param cmd, bus command
 * @return bool, true if ack received (only CHECK_ACK), false else
 */
template<typename Registers>
bool USCI_I2C_Master<Registers>::i2c_command(uint8 addr, devices::i2c_control_t cmd)
{
    bool nack = false;

    switch (cmd)
    {
    case START_READ:
    case START_WRITE:
        start(addr, cmd);
        break;
    case STOP:
        stop();
        break;
    case CHECK_ACK:
        // check for sane state. either nack received or txbuf is free
        // FIXME: this still seems to break sometimes .. maybe force a delay
        while(!(Registers()->UCIFG & (UCNACKIFG + UCTXIFG)));
        // assume ACK if UCNACKIFG is not set
        nack = Registers()->UCIFG & UCNACKIFG;
        Registers()->UCIFG &= ~((uint8)UCNACKIFG); // reset flag
        return !nack;
    default: // should not happen
        break;
    }

    return false;
}

/* i2c_send
 * send a byte to the device
 * @param byte, the byte to send
 */
template<typename Registers>
void USCI_I2C_Master<Registers>::i2c_send(uint8 byte)
{
    // check for "bus busy". If it is not, the START_WRITE
    // was not successful.
    if (!(Registers()->UCSTAT & UCBBUSY))
        return;
    // transmit byte
    // wait for UCTXIFG to be set (then UCTXBUF is empty)
    // FIXME: this may deadlock if bus is not ready to send
    // (you have to issue a "START_WRITE" command before)
    while (!(Registers()->UCIFG & UCTXIFG));
    // the byte will be copied from buffer register to shift
    // register and UCTXIFG flag is set when the buffer
    // register is ready to receive the next byte.
    Registers()->UCTXBUF = byte;
}

/* i2c_recv
 * receive a byte from the device
 * @param addr, slave address for RESTART
 * @param finish, send ACK, (NACK+)STOP, (NACK+RE)[START_WRITE|START_READ]
 * @return uint8, received byte
 */
template<typename Registers>
uint8 USCI_I2C_Master<Registers>::i2c_recv(uint8 addr, devices::i2c_control_t finish)
{
    // wait for RX interrupt (data arrived)
    // FIXME: this may deadlock if slave did not send
    while(!(Registers()->UCIFG & UCRXIFG));

    uint8 byte;
    // INFO: it is not possible to only send a NACK. The
    // hardware will either issue STOP or START+Addr+Direction
    // after reception of the last byte.
    switch (finish)
    {
    case STOP:
        // transmit STOP condition
        Registers()->UCCTL1 |= UCTXSTP;
        byte = Registers()->UCRXBUF;
        // wait until stop was sent (UCTXSTP is cleared)
        while (Registers()->UCCTL1 & UCTXSTP);
        return byte; // we return early...
    case START_WRITE:
        // set UCTR for transmitter mode + Start condition
        Registers()->UCCTL1 |= UCTR + UCTXSTT;
        break;
    case START_READ:
        // clear UCTR for receiver mode
        Registers()->UCCTL1 &= ~((uint8) UCTR);
        // set UCTXSTT to generate START cond.
        Registers()->UCCTL1 |= UCTXSTT;
        break;
    case ACK: // nothing to do for ack (fall through to default)
    default: // illegal combinations and others are ignored (effectively sends an ACK)
        break;
    }
    // read register and return
    return Registers()->UCRXBUF;
}

/* start
 * start or restart reading or writing
 * @param addr, slave address
 * @param cmd, bus command - should be START_READ or START_WRITE
 */
template<typename Registers>
void USCI_I2C_Master<Registers>::start(uint8 addr, devices::i2c_control_t cmd)
{
    // wait for start and stop bits to be cleared
    while (Registers()->UCCTL1 & (UCTXSTT + UCTXSTP));
    // load slave address
    Registers()->UCI2CSA = addr & I2CSAx;
    // dummy register read to reset RX interrupt
    uint8 dummy __attribute__((unused)) = Registers()->UCRXBUF;
    // set START flag
    if (cmd & START_READ) {
        // clear UCTR for receiver mode
        Registers()->UCCTL1 &= ~((uint8) UCTR);
        // set UCTXSTT to generate START cond.
        Registers()->UCCTL1 |= UCTXSTT;
    } else {
        // set UCTR for transmitter mode + Start condition
        Registers()->UCCTL1 |= UCTR + UCTXSTT;
    }
    // FIXME: there are issues with NACK recognition. We force a little delay
    volatile int i;
    for (i = 0; i != 20; ++i);
}

/* stop
 * transmit a STOP signal
 */
template<typename Registers>
void USCI_I2C_Master<Registers>::stop()
{
    // transmit STOP condition
    Registers()->UCCTL1 |= UCTXSTP;
    // wait until stop was sent (UCTXSTP is cleared)
    while (Registers()->UCCTL1 & UCTXSTP);
}

/* enable
 * configure USCI for I2C mode. Set up port mapping
 * enable interrupts (if used)
 */
template<typename Registers>
void USCI_I2C_Master<Registers>::enable()
{
    Registers()->UCCTL1 |= UCSWRST; // **Put state machine in reset**

    /* FIXME: this should probably use some sort of USCI configuration object */
    /* configure USCI for I2C Master Mode */
    Registers()->UCCTL0 = UCMODE3 + UCSYNC + UCMST; // select I2C Master Operation
    Registers()->UCCTL0 &= (uint8) ~(UCA10 + UCSLA10); // use 7 bit addresses
    Registers()->UCCTL1 |= UCSSELsmclk; // use SMCLK as clock input
    Registers()->UCBR0 = 0x11; // set clock divider (low byte)
    Registers()->UCBR1 = 0x00; // set clock divider (high byte)
    Registers()->UCI2COA &= (uint8) ~UCGCEN; // disable general call
    Registers()->UCI2COA |= (masterAddress & I2COAx); // set own address

//    /* FIXME: This should use some sort of port mapping configuration object
//     * It is currently aimed at SCL = Button::M1, SDA = Button::M2 on EZChronos */
//    /* set up port mapping here... */
//    InterruptLock lock; // pmc reconfigure may not be interrupted
//    pmc::Registers()->PMAPPWD = pmc::PASSWD;  // Get write-access to port mapping regs
//    pmc::Port<2>()->MAP1 = pmc::UCB0SDA;  // USCI B0 I2C Data
//    pmc::Port<2>()->MAP2 = pmc::UCB0SCL;  // USCI B0 I2C Clock
//    pmc::Registers()->PMAPPWD = 0;        // Lock port mapping registers
//    // set pins to mapped (USCI-I2C) functionality
//    Port2()->SEL |= (0x04 /* Button::M1 */ + 0x02 /* Button::M2 */);

    Registers()->UCCTL1 &= (uint8) ~UCSWRST; // **Initialize USCI state machine**

    // currently not using interrupts ...
//    Registers()->UCIE |= UCRXIE + UCTXIE; // enable receive and transmit interrupt
}

/* disable
 * disable interrupts (if used)
 * FIXME: what else needs to be done for smooth operation?
 */
template<typename Registers>
void USCI_I2C_Master<Registers>::disable()
{
    // currently not using interrupts ...
//    Registers()->UCIE &= ~(UCRXIE + UCTXIE); // disable RX and TX interrupts

    Registers()->UCCTL1 |= UCSWRST; // **Put state machine in reset**
}

} // ns usci
} // ns msp430x
} // ns reflex

#endif // REFLEX_USCI_I2C_MASTER_H
