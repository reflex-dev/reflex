/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *	Class(es):	 RegistersAx, RegistersBx, RegistersA0, RegistersB0
 *	Author:	     Andre Sieber, Stefan Nuernberger
 *  Description: classes for generic USCI module register access
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 **/

#ifndef REFLEX_MSP430X_USCI_REGISTERS_H
#define REFLEX_MSP430X_USCI_REGISTERS_H

#include "reflex/mcu/msp430x.h"
#include "reflex/data_types/ReadOnly.h"
#include "reflex/data_types/Register.h"
#include "reflex/interrupts/InterruptDispatcher.h"

namespace reflex { namespace msp430x {
namespace usci {

class RegistersAx {
	typedef data_types::Register<uint8> Register;
	typedef data_types::ReadOnly<Register> RORegister;
public:
	// control word 0
	Register UCCTL1;    // offset 00h
	Register UCCTL0;    // offset 01h
	uint16 :16;
	uint16 :16;
	// bit rate control word
	Register UCBR0;     // offset 06h
	Register UCBR1;     // offset 07h
	// modulation control
	Register UCMCTL;    // offset 08h
	uint8 :8;
	// status register
	Register UCSTAT;    // offset 0Ah
	uint8 :8;
	// reveive buffer
	RORegister UCRXBUF;   // offset 0Ch
	uint8 :8;
	// transmit buffer
	Register UCTXBUF;   // offset 0Eh
	uint8 :8;
	// auto baudrate control
	Register UCABCTL;   // offset 10h
	uint8 :8;
	// IrDA control
	Register UCIRTCTL;  // offset 12h
	Register UCIRRCTL;  // offset 13h
	uint16 :16;
	uint16 :16;
	uint16 :16;
	uint16 :16;
	// interrupt control
	Register UCIE;      // offset 1Ch
	Register UCIFG;     // offset 1Dh
	// interrupt vector
	RORegister IV;      // offset 1Eh
};


class RegistersBx {
	typedef data_types::Register<uint8> Register;
	typedef data_types::ReadOnly<Register> RORegister;
public:
	// control word 0
	Register UCCTL1;    // offset 00h
	Register UCCTL0;    // offset 01h
	uint16 :16;
	uint16 :16;
	// bit rate control word
	Register UCBR0;     // offset 06h
	Register UCBR1;     // offset 07h
	// don't know where this is coming from (non existing in user guide!)
	Register UCI2CIE;   // offset 08h
	uint8 :8;
	// status
	Register UCSTAT;    // offset 0Ah
	uint8 :8;
	// receive buffer
	RORegister UCRXBUF;   // offset 0Ch
	uint8 :8;
	// transmit buffer
	Register UCTXBUF;   // offset 0Eh
	uint8 :8;
	// I2C own address
	Register UCI2COA;   // offset 10h
	uint8 :8; // UCI2COA is 16 bit wide
	// I2C slave adddress
	Register UCI2CSA;   // offset 12h
	uint8  :8; // UCI2CSA is 16 bit wide ...
	uint16 :16;
	uint16 :16;
	uint16 :16;
	uint16 :16;
	// interrupt control
	Register UCIE;      // offset 1Ch
	Register UCIFG;     // offset 1Dh
	// interrupt vector
	RORegister IV;      // offset 1Eh
};


struct RegistersA0 : public RegistersAx
{
public:
	struct Interrupt_traits {
		enum Vector {
			// interrupt aliases
			RX_BR=interrupts::IV0, /// RX buffer received
			TX_BE=interrupts::IV1 /// TX buffer empty
		};
		enum { COUNT=3 }; // number of interrupts + 1

		//! reference to the interrupt vector register (RegistersAx->IV)
		typedef interrupts::IVRef<RegistersA0> VectorRef;

		inline static InterruptVector globalVector() {return interrupts::USCIA0;}
        };

	typedef reflex::InterruptDispatcher<Interrupt_traits> IVDispatcher;

	RegistersA0(){}
	RegistersA0* operator-> () {return reinterpret_cast<RegistersA0*>(bases::USCI0+offsets::USCIA0);}
};


struct RegistersB0
		: public RegistersBx
{
public:
        struct Interrupt_traits {
		enum Vector {
			// interrupt aliases
			RX_BR=interrupts::IV0, /// RX buffer received
			TX_BE=interrupts::IV1, /// TX buffer empty
			AL=interrupts::IV2, /// arbitration lost (I2C)
			NACK=interrupts::IV3, /// Nack received (I2C)
			STT=interrupts::IV4, /// Start received (I2C)
			STP=interrupts::IV5, /// Stop received (I2C)
			RX_I2C=interrupts::IV6, /// RX buffer received (I2C)
			TX_I2C=interrupts::IV7 /// TX buffer empty (I2C)
		};
		enum { COUNT=9 }; // nr of interrupts + 1

		//! reference to the interrupt vector register (RegistersBx->IV)
		typedef interrupts::IVRef<RegistersB0> VectorRef;

		inline static InterruptVector globalVector() {return interrupts::USCIB0;}
        };

	typedef reflex::InterruptDispatcher<Interrupt_traits> IVDispatcher;
	RegistersB0(){}
	RegistersB0* operator-> () {return reinterpret_cast<RegistersB0*>(bases::USCI0+offsets::USCIB0);}
};

enum BITS {
	//UCCTL0
	UCSYNC = 0x01, // synchronous mode enable (UART, SPI, I2C)
	UCMODEx = 0x06, // select async(UCSYNC=0)/sync(UCSYNC=1) mode
	UCMODE0 = 0x00, // UART mode (async) 3-pin SPI (sync)
	UCMODE1 = 0x02, // Idle-line multiproc (async) 4-pin SPI (sync)
	UCMODE2 = 0x04, // Address-bit multiproc (async) 4-pin SPI (sync)
	UCMODE3 = 0x06, // UART mode w/ auto baud-rate (async) I2C (sync)
	UCSPB = 0x08, // stop bit select (UART)
	UCMST = 0x08, // master mode (SPI, I2C)
	UC7BIT = 0x10, // character length (UART, SPI)
	UCMSB = 0x20, // MSB first (UART, SPI)
	UCMM = 0x20, // multi master mode (I2C)
	UCPAR = 0x40, // Parity select (UART)
	UCCKPL = 0x40, // clock polarity select (SPI)
	UCSLA10 = 0x40, // slave address is 10bit (I2C)
	UCPEN = 0x80, // parity enable (UART)
	UCCKPH = 0x80, // clock phase select (SPI)
	UCA10 = 0x80, // own address is 10 bit (I2C)
	//UCCTL1
	UCSWRST = 0x01, // software reset (UART, SPI, I2C)
	UCTXBRK = 0x02, // transmit break (UART)
	UCTXSTT = 0x02, // transmit START condition (I2C)
	UCTXADDR = 0x04, // transmit address (UART)
	UCTXSTP = 0x04, // transmit STOP condition (I2C)
	UCDORM = 0x08, // dormant (sleep mode) (UART)
	UCTXNACK = 0x08, // transmit NACK (I2C)
	UCBRKIE= 0x10, // receive break irq (UART)
	UCTR = 0x10, // Transmitter/Receiver (I2C)
	UCRXEIE= 0x20, // receive erroneous char irq (UART)
	UCSSELx = 0xC0, // clock source select (UART, SPI, I2C)
	UCSSELuclk = 0x00, // external clock
	UCSSELaclk = 0x40, // ACLK (32kHz)
	UCSSELsmclk = 0x80, // SMCLK
	//UCMCTL
	UCOS16 = 0x01, // oversampling mode (UART)
	UCBRSx = 0x0E, // second modulation stage (UART)
	UCBRFx = 0xF0, // first modulation stage (UART)
	//UCSTAT
	UCBUSY = 0x01, // USCI busy (UART, SPI)
	UCIDLE = 0x02, // idle line detected (in idle-line mode) (UART)
	UCADDR = 0x02, // address received (in address mode) (UART)
	UCRXERR = 0x04, // receive error flag (UART)
	UCBRK = 0x08, // break detect flag (UART)
	UCPE = 0x10, // parity error flag (UART)
	UCBBUSY = 0x10, // bus busy (I2C)
	UCOE = 0x20, // overrun error flag (UART, SPI)
	UCGC = 0x20, // general call address received (I2C)
	UCFE = 0x40, // framing error flag (UART, SPI)
	UCSCLLOW = 0x40, // SCL low (I2C)
	UCLISTEN = 0x80, // loopback mode (UART, SPI)
	//UCIRTCTL
	UCIREN = 0x01, // IrDA encoder/decoder enable (UART)
	UCIRTXCLK = 0x02, // IrDA transmit pulse clock (UART)
	UCIRTXPLx = 0xFC, // transmit pulse length (UART)
	//UCIRRCTL
	UCIRRXFE = 0x01, // IrDA receive filter enable (UART)
	UCIRRXPL = 0x02, // IrDA receive input polarity (UART)
	UCIRRXFLx = 0xFC, //receive filter length (UART)
	//UCABCTL
	UCABDEN = 0x01, // auto baud-rate detect enable (UART)
	UCBTOE = 0x04, // break time out error (UART)
	UCSTOE = 0x08, // synch field time out error (UART)
	UCDELIMx = 0x30, // break/synch delimiter length (UART)
	UCDELIM1bit = 0x00,
	UCDELIM2bit = 0x10,
	UCDELIM3bit = 0x20,
	UCDELIM4bit = 0x30,
	//UCIE
	UCRXIE = 0x01, // receive interrupt enable (UART, SPI, I2C)
	UCTXIE = 0x02, // transmit interrupt enable (UART, SPI, I2C)
	UCSTTIE = 0x04, // received start interrupt enable (I2C)
	UCSTPIE = 0x08, // received stop interrupt enable (I2C)
	UCALIE = 0x10, // arbitration lost interrupt enable (I2C)
	UCNACKIE = 0x20, // not acknowledged interrupt enable (I2C)
	//UCIFG
	UCRXIFG = 0x01, // receive interrupt flag (UART, SPI, I2C)
	UCTXIFG = 0x02, // transmit interrupt flag (UART, SPI, I2C)
	UCSTTIFG = 0x04, // start received interrupt flag (I2C)
	UCSTPIFG = 0x08, // stop received interrupt flag (I2C)
	UCALIFG = 0x10, // arbitration lost interrupt flag (I2C)
	UCNACKIFG = 0x20, // not acknowledged interrupt flag (I2C)
	//UCI2COA
	UCGCEN = 0x8000, // enable general call (I2C)
	I2COAx = 0x03ff, // I2C own address (10bit) (I2C)
	//UCI2CSA
	I2CSAx = 0x03ff, // I2C slave address (10bit) (I2C)

	// FIXME: why do we need a dummy? why this value?
	dummy = 0x0004
};

}//ns usci
}} //ns msp430x, reflex

#endif // REGISTERS_H
