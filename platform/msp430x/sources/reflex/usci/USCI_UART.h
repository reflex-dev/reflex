/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *	Class(es):	USCI_UART
 *
 *	Author:		Stefan Nuernberger
 *
 *	Description:	UART driver. NOTE: UART only available on USCI_Ax
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and
 *    Operating Systems. All rights reserved.
 *
 *    Redistribution and use in source and binary forms, with or without
 *    modification, are permitted provided that the following conditions
 *    are met:
 *
 *       1. Redistributions of source code must retain the above copyright
 *          notice, this list of conditions and the following disclaimer.
 *
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the
 *          distribution.
 *
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *    The views and conclusions contained in the software and documentation
 *    are those of the authors and should not be interpreted as representing
 *    official policies, either expressed or implied, of BTU Cottbus,
 *    Department for Distributed Systems and Operating Systems.
 *
 **/

#ifndef REFLEX_MSP430X_USCI_UART_H
#define REFLEX_MSP430X_USCI_UART_H

#include "reflex/io/Ports.h"
#include "reflex/usci/Registers.h"
#include "reflex/sinks/Sink.h"
#include "reflex/sinks/SingleValue.h"
#include "reflex/interrupts/InterruptFunctor.h"
#include "reflex/pmc/Registers.h"
#include "reflex/interrupts/InterruptLock.h"

namespace reflex{
namespace msp430x {
namespace usci {

/** UART using USCI module,
 *  9600 baud full duplex through ACLK
 *  separate rx and tx interrupts
 *  single byte event interface
 *  Enable class in PowerManager to activate receive mode.
 */
template<typename Registers>
class USCI_UART : public PowerManageAble
{
private:
	typename Registers::IVDispatcher usci_iv; ///< Interrupt vector dispatcher for TX and RX

public:
	/** Configure USCI module for UART
	  */
	USCI_UART();

	//! set output for received characters
	inline void set_out_rx(Sink1<uint8> *rx) {
		this->rx.output = rx;
	}

	//! set output for successful asynchronous transmit notification
	inline void set_out_tx_ready(Sink0 *tx_ready) {
		this->tx.ready = tx_ready;
	}

	//! acquire input for asynchronous character transmit
	inline Sink1<uint8> *get_in_tx() {
		return &tx;
	}

	//! PowerManager enable (Receive mode)
	void enable();

	//! PowerManager disable (Receive mode)
	void disable();

private:
	/** Transmitter class for asynchronous transmit
	 */
	class TX : public Sink0, public Sink1<uint8>, public PowerManageAble
	{
	private:
		PowerManageAble *rx; ///< to check whether RX is active on PM change
	public:
		Sink0 *ready; ///< notified when next character may follow

		TX(PowerManageAble *rx);

		/** notify after successful transmit (interrupt routine)
		 */
		void notify();

		/** assign character to send
		 * This method loads the character into the
		 * transmit buffer and returns immediately.
		 * Successful transmit will be signaled through
		 * the tx_ready event, after which more characters
		 * may follow.
		 * @param ch, character to send
		 */
		void assign(uint8 ch);

		//! PowerManager enable
		void enable();

		//! PowerManager disable
		void disable();
	} tx; ///< transmit channel

	/** Receiver class for receiving characters
	 */
	class RX : public Sink0
	{
	public:
		Sink1<uint8> *output; ///< received characters go here

		RX();

		/** read received character from UART
		 */
		void notify();
	} rx; ///< receive channel
};

} // ns usci
} // ns msp430x
} // ns reflex

namespace reflex {
namespace msp430x {
namespace usci {

template<typename Registers>
USCI_UART<Registers>::TX::TX(PowerManageAble *rx)
	: PowerManageAble(SECONDARY)
{
	this->setSleepMode(LPM4); // for transmit
	this->rx = rx;
}

template<typename Registers>
USCI_UART<Registers>::RX::RX()
{
}

/** Configure USCI module for UART
  */
template<typename Registers>
USCI_UART<Registers>::USCI_UART() :
	PowerManageAble(PRIMARY),
	usci_iv( Registers::Interrupt_traits::globalVector() ),
	tx(this)
{
	this->setSleepMode(LPM4); // for receive

	// register ISRs
	usci_iv[Registers::Interrupt_traits::RX_BR] = &rx;
	usci_iv[Registers::Interrupt_traits::TX_BE] = &tx;

	/* set up USCI module in UART mode, 2400 baud aclk */
//	Registers()->UCCTL1 |= UCSWRST;		// **Put state machine in reset**
//	Registers()->UCCTL1 |= UCSSELaclk;	// CLK = ACLK
//	Registers()->UCCTL0 = 0x00;		// UART mode
//	Registers()->UCBR0 = 0x0d;		// 32kHz/2400=13.xx (see User's Guide)
//	Registers()->UCBR1 = 0x00;		//
//	Registers()->UCMCTL = UCBRSx & 0x0c;	// modulation for 2400 baud with ACLK (see User's Guide)

	/* set up USCI module in UART mode, 9600 baud aclk */
	Registers()->UCCTL1 |= UCSWRST;		// **Put state machine in reset**
	Registers()->UCCTL1 |= UCSSELaclk;	// CLK = ACLK
	Registers()->UCCTL0 = 0x00;		// UART mode
	Registers()->UCBR0 = 0x03;		// 32kHz/9600=3.xx (see User's Guide)
	Registers()->UCBR1 = 0x00;		//
	Registers()->UCMCTL = UCBRSx & 0x06;	// modulation for 9600 baud with ACLK (see User's Guide)

	/* set up USCI module in UART mode, 9600 baud smclk */
//	Registers()->UCCTL1 |= UCSWRST;		// **Put state machine in reset**
//	Registers()->UCCTL1 |= UCSSELsmclk;	// CLK = SMCLK (8 MHz)
//	Registers()->UCCTL0 = 0x00;		// UART mode
//	Registers()->UCBR0 = 0x41;		// 8 MHz/9600=833.xx (see User's Guide)
//	Registers()->UCBR1 = 0x03;		// ditto
//	Registers()->UCMCTL = UCBRSx & 0x04;	// modulation for 9600 baud with SMCLK (see User's Guide)

	/* set up USCI module in UART mode, 19200 baud smclk */
//	Registers()->UCCTL1 |= UCSWRST;		// **Put state machine in reset**
//	Registers()->UCCTL1 |= UCSSELsmclk;	// CLK = SMCLK (8 MHz)
//	Registers()->UCCTL0 = 0x00;		// UART mode
//	Registers()->UCBR0 = 0xa0;		// 8 MHz/19200=416.xx (see User's Guide)
//	Registers()->UCBR1 = 0x01;		// ditto
//	Registers()->UCMCTL = UCBRSx & 0x0c;	// modulation for 19200 baud with SMCLK (see User's Guide)

	/* set up USCI module in UART mode, 115200 baud smclk */
//	Registers()->UCCTL1 |= UCSWRST;		// **Put state machine in reset**
//	Registers()->UCCTL1 |= UCSSELsmclk;	// CLK = SMCLK (8 MHz)
//	Registers()->UCCTL0 = 0x00;		// UART mode
//	Registers()->UCBR0 = 0x45;		// 8 MHz/115200=69.xx (see User's Guide)
//	Registers()->UCBR1 = 0x00;		//
//	Registers()->UCMCTL = UCBRSx & 0x08;	// modulation for 115200 baud with SMCLK (see User's Guide)

	/* FIXME: This should use some sort of port mapping configuration object
	 * It is currently aimed at TX = Button::M1, RX = Button::M2 on EZChronos */
	/* set up port mapping here... */
	Port2()->map(1, pmc::UCA0TXD);
	Port2()->map(2, pmc::UCA0RXD);
	// set pins to mapped (USCI-UART) functionality
	Port2()->DIR |= 0x04; /* Button::M1 as output pin */
	Port2()->SEL |= (0x04 /* Button::M1 */ + 0x02 /* Button::M2 */);
}

/** ISR for received characters
 */
template<typename Registers>
void USCI_UART<Registers>::RX::notify()
{
	// send received character through event channel
	if (output) output->assign(Registers()->UCRXBUF);
	// RXIFG cleared by IV access in InterruptDispatcher
}

/** ISR for successful transmit
 */
template<typename Registers>
void USCI_UART<Registers>::TX::notify()
{
	// TXIFG cleared by IV access in InterruptDispatcher
	this->switchOff();
	if (ready) ready->notify();
}

/** asynchronous transmit of a single byte
 */
template<typename Registers>
void USCI_UART<Registers>::TX::assign(uint8 ch)
{
	// wait till previous transfer finished
	while (this->isEnabled());
	this->switchOn(); // enable USCI for transmit
	// set next character to transmit
	Registers()->UCTXBUF = ch;
}

//! PowerManager enable TX interrupt
template<typename Registers>
void USCI_UART<Registers>::TX::enable() {
	if (!rx->isEnabled()) {
		// **Initialize USCI state machine**
		Registers()->UCCTL1 &= (uint8) ~UCSWRST;
	}
	// enable TX interrupt
	Registers()->UCIE |= UCTXIE;
}

//! PowerManager disable TX interrupt
template<typename Registers>
void USCI_UART<Registers>::TX::disable() {
	if (!rx->isEnabled()) {
		// **Put state machine in reset**
		Registers()->UCCTL1 |= UCSWRST;
	}
	// disable TX interrupt
	Registers()->UCIE &= (uint8) ~UCTXIE;
}

//! PowerManager enable RX interrupt
template<typename Registers>
void USCI_UART<Registers>::enable() {
	if (!tx.isEnabled()) {
		// **Initialize USCI state machine**
		Registers()->UCCTL1 &= (uint8) ~UCSWRST;
	}
	// enable RX interrupt
	Registers()->UCIE |= UCRXIE;
}

//! PowerManager disable RX interrupt
template<typename Registers>
void USCI_UART<Registers>::disable() {
	if (!tx.isEnabled()) {
		// **Put state machine in reset**
		Registers()->UCCTL1 |= UCSWRST;
	}
	// disable RX interrupt
	Registers()->UCIE &= (uint8) ~UCRXIE;
}

} // usci
} // msp430x
} // reflex

#endif // USCI_UART_H
