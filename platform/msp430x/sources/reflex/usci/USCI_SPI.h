/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(ses):	 USCI_SPI
 *
 *	Author:	     Andre Sieber, Stefan Nuernberger
 *
 *	Description: SPI driver.
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and
 *    Operating Systems. All rights reserved.
 *
 *    Redistribution and use in source and binary forms, with or without
 *    modification, are permitted provided that the following conditions
 *    are met:
 *
 *       1. Redistributions of source code must retain the above copyright
 *          notice, this list of conditions and the following disclaimer.
 *
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the
 *          distribution.
 *
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *    The views and conclusions contained in the software and documentation
 *    are those of the authors and should not be interpreted as representing
 *    official policies, either expressed or implied, of BTU Cottbus,
 *    Department for Distributed Systems and Operating Systems.
 *
 **/

#ifndef REFLEX_MSP430X_USCI_SPI_H
#define REFLEX_MSP430X_USCI_SPI_H

#include "reflex/usci/Registers.h"
#include "reflex/interrupts/InterruptHandler.h"
#include "reflex/sinks/Queue.h"
#include "reflex/sinks/Sink.h"
#include "reflex/scheduling/Activity.h"
#include "reflex/scheduling/ActivityFunctor.h"
#include "reflex/memory/Buffer.h"
#include "reflex/pmc/Registers.h"
#include "reflex/io/Ports.h"

namespace reflex{
namespace msp430x {
namespace usci {

template<typename Registers>
class USCI_SPI : public Activity, public PowerManageAble
{
public:

    USCI_SPI ();

    //must be configured from outside.....
    enum PINS {
        PIN_CLK = 0x80,
        PIN_MISO = 0x40,
        PIN_MOSI = 0x20,
        PIN_CSN = 0x02
    };

    void enable();
    void disable();

    /**
    * Initialize the sender and the receiver, which use the serial
    *
    * @param receiver the object, which gets all receiving characters
    * @param sender gets notified if something was successfully sent
    */
    void init(Sink1<Buffer*>* receiver, Sink0* sender);

    /**
     * This method is called by the scheduler.
     * It starts sending the next buffer from the queue.
     */
    virtual void run();

    /**
     * All data waiting for transmission is stored in this queue
     * to get scheduled
     */
    Queue<Buffer*> input;


    uint8 send(uint8);

protected:
    /**
     * When the SCI receives data, it will be sent to the receiver
     */
    Sink1<Buffer*>* receiver;

    /**
     * The sender is notified, if last send request is finished
     */
    Sink0* sender;

    /**
     * holds the buffer, which is currently sending
     */
    Buffer* current;


    /**
     * the length of the data, which has to be sent
     */
    uint8 length;

    /**
     * the position of the byte, which is sent next
     */
    char* pos;
};

} // ns usci
} // ns msp430x
} // ns reflex

namespace reflex {
namespace msp430x {
namespace usci {

template<typename Registers>
USCI_SPI<Registers>::USCI_SPI() :
	PowerManageAble(PRIMARY)
{
	input.init(this);
	sender = 0;
	receiver = 0;
	current = 0;

	//set deepest allowed sleep mode
	this->setSleepMode(mcu::LPM3);
	Port1()->map(5, pmc::UCA0SOMI); // Map UCA0SOMI output to P1.5
	Port1()->map(6, pmc::UCA0SIMO); // Map UCA0SIMO output to P1.6
	Port1()->map(7, pmc::UCA0CLK); // Map UCA0CLK output to P1.7
	//configure pins for SPI
	Port1()->SEL |= (PIN_CLK | PIN_MISO | PIN_MOSI);
	Port1()->DIR |= (PIN_CLK | PIN_MOSI);
	Port1()->DIR &= ~PIN_MISO;

	//Configure CSN pin
	PortJ()->DIR |= PIN_CSN;
	PortJ()->OUT |= PIN_CSN;

	Registers()->UCIFG = 0;
	Registers()->UCCTL1 |= UCSWRST; ///< Reset State

	Registers()->UCCTL0 |= ( UCMSB | UCMST | UCSYNC| UCCKPH);

	Registers()->UCCTL1 |= UCSSELaclk;
	//Registers()->UCCTL1 |= UCSSELsmclk; //should be sm clock, but its easier to test it with aclock
}

template<typename Registers>
void USCI_SPI<Registers>::init(Sink1<Buffer*>* receiver, Sink0* sender)
{
	this->receiver = receiver;
	this->sender = sender;
}

template<typename Registers>
void USCI_SPI<Registers>::enable()
{
	Registers()->UCCTL1 &= ~UCSWRST; ///< Release State -> usci switch on
	PortJ()->OUT &= ~PIN_CSN;
}

template<typename Registers>
void USCI_SPI<Registers>::disable()
{
	Registers()->UCCTL1 |= UCSWRST; ///< Reset State;
	PortJ()->OUT |= PIN_CSN;
}

template<typename Registers>
void USCI_SPI<Registers>::run()
{
	current = input.get();
	length = current->getLength();
	pos = (char*)current->getStart();

	this->lock(); //lock run because we can handle only one send a time

	do {
		length--;

		uint8 temp = send(*pos);
		*pos++ = temp;
	} while (length);


}

template<typename Registers>
uint8 USCI_SPI<Registers>::send(uint8 ch)
{
	while(Registers()->UCSTAT&UCBUSY);	//hopefully usci isn't busy right now
	Registers()->UCTXBUF = ch;		//send byte
	while(Registers()->UCSTAT&UCBUSY);	//wait for completion
	return (Registers()->UCRXBUF);		//and read byte
}

} // usci
} // msp430x
} // reflex

#endif // REFLEX_MSP430X_USCI_SPI_H
