/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *

 *
 *    This file is part of REFLEX.
 *
 *    REFLEX is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as
 *    published by the Free Software Foundation, either version 3 of the
 *    License, or (at your option) any later version.
 *
 *    REFLEX is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with REFLEX.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include "FLASH.h"
#include "reflex/mcu/msp430x.h"
#include "Registers.h"
#include "reflex/sys/SYS.h"
#include <reflex/interrupts/InterruptLock.h>

using namespace reflex;
using namespace msp430x;
using namespace flash;

FLASH::FLASH()
{

}

bool FLASH::erase(caddr_t start, size_t size)
{

  _interruptsDisable();
  //align to full segment
  size = size + (SEGMENTSIZE - (size & (SEGMENTSIZE-1)));
  //write once on each segment to erase
  for(unsigned i = 0; i < size; i += SEGMENTSIZE){
    Registers()->FCTL3 = 0x00;  //unlock flash
    Registers()->FCTL1 = ERASE; //set to segment erase
    start[i] = 0;		//dummy write
    Registers()->FCTL1 = 0x00; //clr erase bit
    Registers()->FCTL3 = LOCK; // lock flash
  }
  _interruptsEnable();
  return true;

}

void FLASH::read(caddr_t dest, caddr_t src, size_t size)
{
  //memcpy(dest,src,size);
}

bool FLASH::write(caddr_t src, caddr_t dest, size_t size)
{
// 	  char* rpos = src;
// 	  char* wpos = dest;
// 	  while(rpos < src+size) {
	
// 	    if( !writeByte( wpos, *rpos ) ){
// 	      return false;
// 	    }
// 	    ++rpos;
// 	    ++wpos;
	    
// 	  }
// 	  return true;


	  char* rpos = src;
	  char* wpos = dest;
	  
	  uint16 alligned = 4- ((uint16) wpos % 4);

	  for (wpos; wpos < dest + alligned ; wpos++)
	    {
			  if( !writeByte( wpos, *rpos ) ){
			     return false;
			  }
			  ++rpos;
			  // ++wpos;
	    }

	  while(rpos < src+size) {
		  if (src+size - rpos < 4) 
		  {
			  if( !writeByte( wpos, *rpos ) ){
				  return false;
			  }
			  ++rpos;
			  ++wpos;
		  }
		  else
		  {
			  if( !writeLong( rpos,  wpos ) ){
					  return false;
				  }
				 rpos = rpos + 4;
				 wpos = wpos + 4;

		  }
	  }
	  return true;



}


bool FLASH::writeByte(char* pos,char value)
{

  //if value is initial value after erase do nothing
  //this prevents from multiple writing of 0xffff
  if(*pos == value){
    return true;
  }
  _interruptsDisable();

  Registers()->FCTL3 = 0x00; //clear Lock
  Registers()->FCTL1 = WRT; // set write bit
  *pos = value;
  Registers()->FCTL1 = 0x00;  //clr write bit
  Registers()->FCTL3 = LOCK; // Lock again
  _interruptsEnable();
  return ( *pos == value);
}




bool FLASH::writeWord(uint16* pos, uint16 value)
{

	  //if value is initial value after erase do nothing
	  //this prevents from multiple writing of 0xffff

	  _interruptsDisable();

	  Registers()->FCTL3 = 0x00; //clear Lock
	  Registers()->FCTL1 = WRT; // set write bit
	  *pos = value;
	  Registers()->FCTL1 = 0x00;  //clr write bit
	  Registers()->FCTL3 = LOCK; // Lock again
	  _interruptsEnable();
	  return ( *pos == value);


}

bool FLASH::writeLong(caddr_t src, caddr_t dest)
{
	  //if value is initial value after erase do nothing
	  //this prevents from multiple writing of 0xffff
	  _interruptsDisable();
	  if( (uint32)*src == (uint32)*dest){
	  	    return true;
	  	  }

	  Registers()->FCTL3 = 0x00; //clear Lock
	  Registers()->FCTL1 = BLKWRT; // set blockwrite bit
	  *dest = *src;
	  *(dest+1) = *(src+1);
	  *(dest+2) = *(src+2);
	  *(dest+3) = *(src+3);
	  Registers()->FCTL1 = 0x00;  //clr write bit
	  Registers()->FCTL3 = LOCK; // Lock again
	  _interruptsEnable();
	 return ( (uint32)*src == (uint32)*dest);
}


bool FLASH::copy(caddr_t src, caddr_t dest, size_t size)
{
  //write is 1->0 is only possible with erase of segment
  bool result = erase(dest,size);
  result &= write(src,dest,size);
  return result;
}
