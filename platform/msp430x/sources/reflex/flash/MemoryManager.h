#ifndef MEMORYMANAGER_H
#define MEMORYMANAGER_H
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(ses):	MemoryManager
 *	Author:		Richard Weickelt <richard@weickelt.de>, 
 *                      Andre Sieber <as@informatik.tu-cottbus.de>
 *
 *	Description: Implements alloc, free and memcopy for Flash and RAM
 *
 *    This file is part of REFLEX.
 *
 *    REFLEX is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as
 *    published by the Free Software Foundation, either version 3 of the
 *    License, or (at your option) any later version.
 *
 *    REFLEX is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with REFLEX.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "reflex/types.h"
#include "conf.h"

#include "reflex/mcu/msp430x.h"

#include "reflex/flash/Registers.h"
#include "reflex/interrupts/InterruptLock.h"

/* Defines for Memory Management */
#define MM_MAX_STACKSIZE 1024
#define MM_BEGINOF_RAM 0x1C00
#define MM_ENDOF_RAM 0x2Bff
#define MM_SIZEOF_SEGMENT 512


namespace reflex
{
  /* Only for proper declaration of operator new below */
  class MemoryManager;
}

/*
 * Operator new that uses system's memory manager
 */
void* operator new(size_t, reflex::MemoryManager&) throw ();
void operator delete(void* addr, reflex::MemoryManager&) throw ();

namespace reflex {
/**
 * A couple of bytes containing administrative data for MemoryManager
 */
struct Section
{
	uint16 size;
	Section* next;
};

/**
 * The memory Manager is responsible for Memory Management on the target system.
 *
 * It helps to allocate and free space in ROM and RAM dynamically.
 * It is also responsible for copying data between ROM and RAM
 *
 * Reflex' buffer system is not suitable for that, since it
 * provides only single siced buffers and doesn't allow memory fragmentation.
 *
 * Free memory is found by a first fit algorithm.
 * The current implementation uses a memory map for ROM. Instead a freelist could
 * be used, but would require ectensive flash operations on freeing.
 *
 * Although MemoryManager has only nonstatic members and should not need any space in RAM
 * an instance has to be created on system startup, to init the RAM section table.
 */
class MemoryManager
{
private:
	static caddr_t const BEGINOF_RAM;
	static caddr_t const ENDOF_RAM;

	enum
	{
		MAX_STACKSIZE = MM_MAX_STACKSIZE, SIZEOF_SEGMENT = MM_SIZEOF_SEGMENT
	};

	/*
	 * Writes one byte of data
	 */
	static bool writeByte(caddr_t addr, char data);

public:
	/**
	 * Constructor
	 */
	MemoryManager();

	/**
	 * Erases one specified segment
	 * Address alignment is uncritical.
	 *
	 * @param address iff
	 */
	static void erase(caddr_t addr);

	/**
	 * Allocates space in RAM or ROM. Memory is reserved until freeRam or
	 * freeRom is called.
	 *
	 * @param size space to allocate
	 * @param addr preferred memory address (optional)
	 * @return reserved address if successful or null
	 */
	static caddr_t allocRam(size_t size, const caddr_t addr = 0);

	/**
	 * Reserves a section in flash memory. According to its segments,
	 * the MemoryManager divides this memory into sections of fixed sizes.
	 * Every section has a minimum size of 512Bytes.
	 * If the user wants to reserve a special address addr, MemoryManager checks
	 * usage of the segment, where addr is located, is free.
	 * The important section member variable is size. When size is 0xffff, this
	 * means that it is not used (flash is erased there).
	 *
	 * @param size space to allocate
	 * @param addr preferred memory address (optional)
	 * @return reserved address if successful or null
	 */
	static caddr_t allocRom(size_t size, const caddr_t addr = 0);

	/**
	 * Allocates memory in RAM or ROM, strategy is decided by address.
	 * This function wraps freeRom or freeRam.
	 *
	 * @param size space to allocate
	 * @param addr preferred memory address (optional)
	 * @return reserved address if successful or null
	 */
	static caddr_t alloc(uint16 size, const caddr_t addr = 0);

	/**
	 * Sets RAM free, that was allocated by the complementing allocRam.
	 *
	 * @param addr start address of the allocated memory
	 */
	static void freeRam(const void* addr);

	/**
	 * Sets ROM free, that was allocated by the complementing allocRom.
	 *
	 * @param addr start address of the allocated memory
	 */
	static void freeRom(const void* addr, uint16 size);

	/**
	 * Copies a specified memory region. It can write to RAM and ROM.
	 *
	 * @param src address of source memory region
	 * @param dest address of destination memory region
	 * @param size of data to copy
	 * @param destination within flash
	 * @return true if success
	 */
	static bool memcopy(const void* src, const void* dest, uint16 size,
			bool destination_in_flash = false);

	/**
	 * Checks if two memory regions are identical
	 *
	 * @param source address of first memory region
	 * @param destination address of second memory region
	 * @param size of regions
	 * @return true if identical
	 */
	static bool isEqual(const void* source, const void* destination, uint16 size);

	/**
	 * Clears RAM table
	 */
	static void resetRam();


	/**
	 * Fills a RAM memory region with the desired value
	 *
	 * @param start address of memory region
	 * @param size of memory region
	 * @param value to write
	 */
	static void fill(const void* start, size_t size, uint8 value);
};
}
#endif /* MEMORYMANAGER_H */
