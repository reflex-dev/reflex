/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 **/

#ifndef REFLEX_LIB_DATA_TYPES_PARTIAL_H
#define REFLEX_LIB_DATA_TYPES_PARTIAL_H
#include "reflex/types.h"
#include "reflex/debug/StaticAssert.h"
#include "reflex/debug/SizeOf.h"
#include "reflex/data_types/Converter.h"
namespace reflex{
namespace data_types {

/*! \ingroup DataTypes @{ */

//! helper classed used within the implementation only
namespace helper {
	template<size_t TSize>
	class SubDataType;

	template<typename T>
	struct SubType;

	template<typename T>
	struct SubType<volatile T> {
		typedef volatile typename SubDataType<sizeof(T)>::value_type value_type;
	};
	template<typename T>
	struct SubType<const volatile T> {
		typedef const volatile typename SubDataType<sizeof(T)>::value_type value_type;
	};

	template<typename T>
	struct SubType {
		typedef typename SubDataType<sizeof(T)>::value_type value_type;
	};
}

//!	represents the a lower part of a Register with the sizeof(T).
/*!	The data value is places on the lowpart (lower addresses) and the
	higher part will not be touched by that type.
 */
template<typename T, typename TSub= typename helper::SubType<T>::value_type >
struct LowPart;

//!	represents the a higher part of a Register with the sizeof(T).
/*! The data value is places on the highpart (higher addresses)
	and the lower part will not be touched.
 */
template<typename T, typename TSub= typename helper::SubType<T>::value_type  >
struct HighPart;

//!
/*!
*/
template<typename T>
class Partial ;


//helper sub types
namespace helper {

	template<> 	struct SubDataType<1> { typedef uint8 value_type; };
	template<>  struct SubDataType<2> { typedef uint8 value_type; };
	template<>  struct SubDataType<4> { typedef uint16 value_type;};
	//template<>  struct SubDataType<5> { typedef uint8 value_type; };
	//template<>  struct SubDataType<6> { typedef uint8[3] value_type; };
	template<>  struct SubDataType<8> { typedef uint32 value_type;};
	template<>  struct SubDataType<16>{ typedef uint64 value_type;};

	template<typename T> class Padding { T:sizeof(T);}__attribute__ ((__packed__));


	//! presents the lower data part.
	template<typename T, size_t TSize = sizeof(T) >
	class LowSubDataType;


	template<typename T>
	class LowSubDataType<T,1> {
	protected:
		typedef  typename SubType<T>::value_type value_type;
	public://operations
		operator const value_type () const {return this->value;}
		LowSubDataType& operator=(const typename ClearVolatile<value_type>::type & value) {this->value=value; return *this;}
	protected://the data
		value_type value:4;
		value_type :4;
	}__attribute__ ((__packed__));

	template<typename T,size_t TSize>
	class LowSubDataType
		: public Partial< typename SubType<T>::value_type >
		, private Padding< typename SubType<T>::value_type >
	{
	protected:
		typedef typename SubType<T>::value_type value_type;
		enum {PADDING=8*sizeof(value_type)};
	}__attribute__ ((__packed__));




	//! presents the higher data part.
	template<typename T, size_t TSize = sizeof(T) >
	class HighSubDataType;


	template<typename T>
	class HighSubDataType<T,1> {
	protected:
		typedef  typename SubType<T>::value_type value_type;
	public:
		operator const value_type () const {return this->value;}
		HighSubDataType& operator=(const typename ClearVolatile<value_type>::type & value) {this->value=value; return *this;}

	protected:
		value_type :4;
		value_type value:4;
	}__attribute__ ((__packed__));

	template<typename T,size_t TSize>
	class HighSubDataType
		: private Padding< typename SubType<T>::value_type >
		, public Partial< typename SubType<T>::value_type >
	{
	protected:
		typedef typename SubType<T>::value_type value_type;
	}__attribute__ ((__packed__));

}







template<typename T, typename TSub>
class  LowPart
	: public helper::LowSubDataType<T>
{
public:
	//
	LowPart& operator=(const typename ClearVolatile<TSub>::type & value) {this->value=value; return *this;}
}__attribute__ ((__packed__));


template<typename T, typename TSub>
class  HighPart
	: public helper::HighSubDataType<T>
{
public:
	//
	HighPart& operator=(const typename ClearVolatile<TSub>::type & value) {this->value=value; return *this;}
}__attribute__ ((__packed__));

/*!
*/
template<typename T>
class Partial  {
public:
	typedef LowPart<T> LowType;
	typedef HighPart<T> HighType;
public:
//	operator T&() {return value;} // cannot bind packed field error
//	gcc seems to have problems with packed structs
	operator T&() {return *(getValueRef());}
	operator const T&() const { return *(getValueRef());}

	Partial& operator= (const T& value) {this->value=value; return *this;}

	T* getValueRef() {return &value;}
	const T* getValueRef() const {return &value;}
public:
	union {
		T			value;
		LowType		low;
		HighType	high;
	}__attribute__ ((__packed__));
} __attribute__ ((__packed__)) ;


/*! @} */

}}//ns typetraits,reflex




#endif // PARTIAL_H
