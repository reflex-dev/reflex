/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *
 *	Author:	 Sören Höckner
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */


#ifndef REFLEX_LIB_DATA_TYPES_WRITEONLY_H
#define REFLEX_LIB_DATA_TYPES_WRITEONLY_H

#include "reflex/debug/StaticAssert.h"

namespace reflex{

namespace data_types {
/*! \ingroup DataTypes @{ */

//!	This type is a wrapper to realize a write only type.
/*!	There is no posibility for that semantic in c++ The intention is to create structures
	for registerfiles that includes writeonly registers.
	\code
	struct Foo {
		unsigned tommys;
		const unsigned yanks;		// fails while compiling due to of lacking initialiation
		WriteOnly<unsigned> krauts;
	} *bar;
	...
	bar->krauts = 0x0815 //succeed
	read(bar->krauts); //fails as expected
	\endcode
*/
template<typename T>
struct WriteOnly;

template<typename T>
struct WriteOnly
{

	void operator= (const T& value) {check();this->value = value;}

protected:
	T value;

private:
	//permit the usage of that typeconversion operator
	template<typename TForbidden>
	operator const TForbidden () const;

	//why do you wanna use that
	void operator= (const WriteOnly&);

	void check() const; //! check for matching size of struct \sa ::Register::check()

} __attribute__ ((__packed__)); //be sure the structure isnt greater than expected
//FIXME: try to get rid of that unportable gnu attribute

template<typename T>
inline
void WriteOnly<T>::check() const {
	/* check whether the compiler does fill up structures. */
	STATIC_ASSERT(sizeof(WriteOnly<T>)==sizeof(T),Your_compiler_does_not_support_this_traits_of_type);
}


/*! @} */

} } //ns data_types, reflex
#endif // REFLEX_TYPETRAITS_WRITEONLY_H
