/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Author:	 Sören Höckner
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */


#ifndef REFLEX_LIB_DATA_TYPES_READONLY_H
#define REFLEX_LIB_DATA_TYPES_READONLY_H

#include "reflex/debug/StaticAssert.h"
#include "reflex/data_types/Register.h"
#include "reflex/data_types/Helper.h"
namespace reflex{

namespace data_types {
/*! \ingroup DataTypes @{ */

//!	This type is a wrapper to realize a read only type.
/*!	There is no chance to create a struct with const variables without
	to initialize them. The intention is to create structures for registerfiles
	that includes readonly registers.
	\code
	struct Foo {
		unsigned tommys;
		const unsigned yanks;		// fails while compiling due to of lacking initialization
		ReadOnly<unsigned> krauts;
	} *bar;
	...
	bar->krauts = 0x0815 //fails as expected
	read(bar->krauts); //succeed
	\endcode
*/
template<typename T>
class ReadOnly;

template<typename T>
class ReadOnly<Register<T> >
{
public:
	operator const T() const { check();return value;}
protected:
	Register<T> value;	
private:
	template<typename TForbidden>
	void operator = (TForbidden); //

	void check() const; //! check for matching size of struct \sa ::Register::check()

} __attribute__ ((__packed__));

template<typename T>
inline
void ReadOnly<Register<T> >::check() const{
	/* check whether the compiler does fill up structures. */
	STATIC_ASSERT(sizeof(ReadOnly<Register<T> >)==sizeof(T),Your_compiler_does_not_support_this_traits_of_type);
}


template<typename T>
class ReadOnly
{
public:
	operator const T () const { check();return value;}
protected:
	T value;

private:
	template<typename TForbidden>
	void operator = (TForbidden);

	void check() const; //! check for matching size of struct

} __attribute__ ((__packed__)); //be sure the structure isn't greater than expected
//FIXME: try to get rid of that unportable gnu attribute

template<typename T>
inline
void ReadOnly<T>::check() const {
	/* check whether the compiler does fill up structures. */
	STATIC_ASSERT(sizeof(ReadOnly<T>)==sizeof(T),Your_compiler_does_not_support_this_traits_of_type);
}

/*! @} */

} } //ns data_types reflex
#endif // REFLEX_TYPETRAITS_READONLY_H
