/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 **/

.global _interruptsDisable
.global _interruptsEnable
.global _interruptsCheck

#define GIE 0x0008

.section .notUpdateAble,"ax"


//disables maskable interrupts
_interruptsDisable:
	//return true, if Interrupts where enabled
	mov r2,r15
	dint
	and #GIE,r15
	rrc r15
	nop //delay
	ret


//enables maskable interrupts
_interruptsEnable:
	eint
	ret


//check if maskable interrupts are enabled
//flag is 0 when in interrupt handler or interrupts locked
_interruptsCheck:
	//return true, if Interrupts are enabled
	mov r2,r15
	and #GIE,r15
	rrc r15
	nop //delay
	ret


//.end .section notUpdateAble

.section .text

.macro wrapper p
.align 2
.global __wrapper_\p
__wrapper_\p:
	push r15 //r15 is used from gnu g++ as delivery register
	mov  #\p, r15
	br   #__wrapper_body

.endm

.irp	i,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17
wrapper \i
.endr

//generic interrupt wrapper, saves the volatile registers
//see Stefane Carrez sites for procedure conventions
.weak __wrapper_body
.func __wrapper_body
__wrapper_body:
	/*Saving the Register ...*/
	push r4
	push r5
	push r6
	push r7
	push r8
	push r9
	push r10
	push r11
	push r12
	push r13
	push r14
	//r15 is allready saved in wrapper_\p

	/*... Saving the Register */

	/**
	 * handle gets param a0 from wrapper_0
	 * call handle
	 */
	 call #handle
	/* restore register from stack ... */
	pop r14
	pop r13
	pop r12
	pop r11
	pop r10
	pop r9
	pop r8
	pop r7
	pop r6
	pop r5
	pop r4
	pop r15

	bic   #0xf0,0(r1) ; clears SCG1 SCG0 OSCOFF CPUOFF bit of SR
	reti
.endfunc

.extern _reset_vector__


# Define interrupt vectors in a dedicated section.
#
# Interrupt vectors are already defined by the linker e.g. in crtcc430x6137.o.
# They are defined as weak symbols and have to be overridden. There is
# currently no other way than redefining exported vector symbols. This saves
# us from hacking the linker script.
#
# a : memory is allocated for this section
# x : section is executable
# @progbits : section contains data
#
.section .vectors, "ax", @progbits
InterruptVectors:
    # Vectors 0xffda..0xfffe point to valid wrappers
    .global vector_ffda
    .set vector_ffda, __wrapper_0
    .global vector_ffdc
    .set vector_ffdc, __wrapper_1
    .global vector_ffde
    .set vector_ffde, __wrapper_2
    .global vector_ffe0
    .set vector_ffe0, __wrapper_3
    .global vector_ffe2
    .set vector_ffe2, __wrapper_4
    .global vector_ffe4
    .set vector_ffe4, __wrapper_5
    .global vector_ffe6
    .set vector_ffe6, __wrapper_6
    .global vector_ffe8
    .set vector_ffe8, __wrapper_7
    .global vector_ffea
    .set vector_ffea, __wrapper_8
    .global vector_ffec
    .set vector_ffec, __wrapper_9
    .global vector_ffee
    .set vector_ffee, __wrapper_10
    .global vector_fff0
    .set vector_fff0, __wrapper_11
    .global vector_fff2
    .set vector_fff2, __wrapper_12
    .global vector_fff4
    .set vector_fff4, __wrapper_13
    .global vector_fff6
    .set vector_fff6, __wrapper_14
    .global vector_fff8
    .set vector_fff8, __wrapper_15
    .global vector_fffa
    .set vector_fffa, __wrapper_16
    .global vector_fffc
    .set vector_fffc, __wrapper_17

    .global vector_fffe
    .set vector_fffe, _reset_vector__
