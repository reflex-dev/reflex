/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 **/

#ifndef REFLEX_SYSTEM_INTERRUPTDISPATCHER_H
#define REFLEX_SYSTEM_INTERRUPTDISPATCHER_H

#include "reflex/types.h"

#include "reflex/MachineDefinitions.h"

#include "reflex/interrupts/InterruptHandler.h"
#include "reflex/sinks/Sink.h"
#include "reflex/debug/Assert.h"
#include "reflex/data_types/Static.h"
#include "reflex/memory/memset.h"

namespace reflex {

//! \brief a logical interrupt table, that delegates interrupts events to sink0 objects.
/*!	Some platforms(more than one) maps multiple features to one interruptsource.
	This class provide a unique concept to delegate those interrupts to their
	dedicated handler routines.
	The user has to provide the size and a iterator type, which selects the
	handler given through the constructor to the iterator type.
	The port of the platform is responsible for the an efficient implementation
	of that iterator type.
	Mostly the such kind of informations were collected from a kind of vector
	register.
	In the initial state, the entries of the table are pointing to a static
	no-Operation Sink0 object.
	\note registered object are acting still within the interrupt context.

	\tparam TIntTraits	traits of the logical interrupt vector \see Interrupt_traits
	\tparam	TVecIT	the iterator type, which gives the handler to be delegated to.
					In the constructor it should accept an array of handlers
*/
template<typename TIntTraits, typename TIVecIt = mcu::interrupts::VecIterator<typename TIntTraits::VectorRef> >
class InterruptDispatcher
	: public InterruptHandler
	, protected data_types::Static<Sink0::NoOp>//! neutral noop sink, which we are using
{
	typedef Sink0* value_type;
	typedef Sink0::NoOp noop_type;
public:
	//! registers the interrupt handler and builds the initial state.
	/*! \param	vector the InterruptVector to be delegated from
	*/
	InterruptDispatcher(const mcu::InterruptVector vector= TIntTraits::globalVector() , const PowerManageAble::Priority priority=PowerManageAble::SECONDARY );

	//!	destruction
	/*! does nothing
	*/
	~InterruptDispatcher() {}

	//! access the various vectors.
	/*!	the user is responsible for checking the bounds and even for
		protecting agains pending interrupts of this vector.
		Normally the mcu should write a pointertype in one instruction
		so a protection should not be necessary.
		\return	reference to the handler
		\note registered object are acting still within the interrupt handle context.
	*/
	value_type& operator[](const typename TIntTraits::Vector& i) {return handlers[i];}

	//! fills the vector with a given value.
	/*! \param	val		the value written to each slot of the logical handlers
	*/
	inline void fill(const value_type&val) __attribute__((always_inline)) { memsetWord(handlers,val,TIntTraits::COUNT); }

	inline void clear(const word& i) { handlers[i]=&data_types::Static<Sink0::NoOp>::instance;}
protected:
	//! the primary interrupt handle method.
	virtual void handle();


	virtual void disable() {}
	virtual void enable() {}

protected:
	Sink0* handlers[TIntTraits::COUNT]; //! logical vector of handlers
};


template<typename TIntTraits, typename TIVecIt >
inline
InterruptDispatcher<TIntTraits,TIVecIt>::InterruptDispatcher(const mcu::InterruptVector vector, const PowerManageAble::Priority priority)
	: InterruptHandler (vector, priority )
{
	//fill array with default noop Sink so that calls to unrecorded slots will end there
	// assumtion: the static Sink0::NoOp instance is allready constructed before the first
	// interrupt delegation appears.
	fill(&data_types::Static<Sink0::NoOp>::instance);
}

template<typename TIntTraits, typename TIVecIt >
void InterruptDispatcher<TIntTraits,TIVecIt>::handle() {
	value_type handler = (*TIVecIt(handlers) ); //get the interrupt handler
	handler->notify(); // delegated the event when calling the notify method
}


/*! \class Interrupt_traits
	\brief defines properties needed by the InterruptDispatcher


	\code
	struct Interrupt_traits {
		//! the logical vectors to ensure typesafe accessing of them
		enum Vector {
			//name alias
			TIMER = mcu::interrupts::IV0 //the first vector
		};
		enum {	COUNT=10 //! the size of the Vector
		};

		typedef mcu::interrupts::IVRef<Registers> VectorRef; //the reference to the current Vector, mostly some register. will be dereferenced like *VectorRef()

		inline static const mcu::InterruptVector globalVector() {return mcu::interrupts::ADC12_A;}
	};

	\endcode
*/
//! helper class to determine invalid defined Interrupt_traits types
struct Invalid_Interrupt_traits {
	struct NotDefined{};
	typedef NotDefined Vector;

	enum Invalid {  COUNT };

	typedef NotDefined VectorRef;
	inline static mcu::InterruptVector globalVector() {return mcu::interrupts::INVALID; }
};
typedef Invalid_Interrupt_traits Interrupt_traits;

//}//ns core

} //ns reflex

#endif // INTERRUPTDISPATCHER_H
