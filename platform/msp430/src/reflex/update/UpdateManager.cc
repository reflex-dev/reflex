#include "reflex/update/UpdateManager.h"


#include "NodeConfiguration.h"

using namespace reflex;


UpdateManager::UpdateManager():
	
  forwardingFinishedFunctor(*this)
{

  input.init(this);
  forwardingFinished.init(&forwardingFinishedFunctor);
  outputUpdateManagerApplicationData = &ud.applicationInput;
  outputToUpdateDaemon = &ud.input;
}



void UpdateManager::init(Sink1<Buffer*>* ioDevice,
					Sink0* macTimerInput,
					Sink1<Buffer*>* outputUpdateManagerApplicationData,
					Pool* pool)
	{
	  
	  ud.init(&getApplication().flash,ioDevice,macTimerInput,outputUpdateManagerApplicationData,pool);
   
	}



void UpdateManager::run()
{
  
  packet = input.get();
  if (packet)
    {
      //outputToUpdateDaemon->assign(packet);
    ud.input.assign(packet);
      //packet->deleteBuf();
    }
}


void UpdateManager::forwardApplicationData()
{
  applicationPacket = applicationInput.get();
ud.applicationInput.assign(applicationPacket);
//  outputUpdateManagerApplicationData->assign(applicationPacket);
}


void UpdateManager::handleForwardingFinished()
{
  //outputToUpdateDaemonForward->notify();
  ud.forwardingFinished.notify();
}


