/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(ses):	 none
  
 *	Author:		 Carsten Schulze
 *
 *	Description: pseudo operators for gcc
 */

/**
 * pseudo implementation of the __cxa_pure_virtual function
 * which is used by the gcc
 * in that case, it does nothing
 */
extern "C" void __cxa_pure_virtual()
{
}
