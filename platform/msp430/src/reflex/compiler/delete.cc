/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(ses):	 none
  
 *	Author:		 Carsten Schulze
 *
 *	Description: pseudo delete operator
 */


/**
 * only a dummy function for the gcc-compiler,
 * witch needs a delete operator, but it should not be called
 * see initfini.c there are all constructors and destructors
 * @param p Pointer to object to delete
 */
void operator delete(void* p) {}

