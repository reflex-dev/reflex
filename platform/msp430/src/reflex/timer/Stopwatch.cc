/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
  
 *	Author:	Carsten Schulze
 */

#include "reflex/timer/Stopwatch.h"
#include "reflex/MachineDefinitions.h"

using namespace reflex;
using namespace timerB;

StopWatch::StopWatch()
{
	timerBRegisters = (Registers*)mcu::TIMERB_REGISTERS_BASE;

	//SPEED : SMCLK / 4 (should be near CPU cycles
	timerBRegisters->TBCTL = TBSSEL10 | MC10 ; //also disables interrupt, continue mode
	this->setSleepMode(mcu::LPM1);

/*	timerBRegisters->TBCTL = TBSSEL01 | MC10;//ID = 0, CNTL00
	timerBRegisters->TBCCTL0 = 0xffff;
	timerBRegisters->TBIV &= ~TBIVx; //reset pending interrupt

	timerBRegisters->TBR = 0;
	timerBRegisters->TBCCR0 = 0;
*/
}

void StopWatch::disable()
{
	timerBRegisters->TBCTL &= ~(MCx);
}

void StopWatch::enable()
{
	timerBRegisters->TBCTL |= MC10;
}
