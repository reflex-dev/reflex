/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
  
 *	Author:		 Carsten Schulze
 */

#include "reflex/timer/BasicClockModuleRNG.h"
#include "reflex/timer/TimerARegisters.h"
#include "reflex/MachineDefinitions.h"
#include "reflex/types.h"
#include "reflex/debug/Assert.h"

#include "NodeConfiguration.h"

using namespace reflex;
using namespace basicClockModule;

BasicClockModuleRNG::BasicClockModuleRNG()
{
	//volatile int* WDTCTL = (int*) 0x0120; //Init watchdog timer
	//*WDTCTL = (int) (0x5a00 | 0x0080) ; // WDTPW|WDTHOLD

	registers = (Registers*)mcu::BASIC_CLOCK_MODULE_BASE;
	timerARegisters = (timerA::Registers*) mcu::TIMERA_REGISTERS_BASE;

	registers->IE1 &= ~OFIE1; //disable oscilator fault interrupts

	//FIXME: don't know if neccessary here
	//timerARegisters->TAIV &= ~timerA::TAIVx; //reset pending interrupt

	//do not start the clocks here!
	//wait until a random number is requested
}

uint16 BasicClockModuleRNG::getRandomInt()
{
	timerARegisters->TAIV &= ~timerA::TAIVx; //reset pending interrupt
	//InterruptLock guard;
	registers->BCSCTL1 = 0x00;
	registers->BCSCTL1 |= XT2OFF;
	//use of external Resistor for Oscillator (ROSC), lower temperature sensibility
	//all other bits are set to 0, DCO/1 -> MCLK, DCO/1 -> SMCLK
	registers->BCSCTL2 = 0x00;
	registers->BCSCTL2 |= DCOR_VALUE;
	/* selecet SMCLK/1, continuous mode, clear*/
	timerARegisters->TACTL = 0x0000;
	timerARegisters->TACTL |= timerA::TASSEL10 | timerA::MC10;
	/* capture on rising edge | CCI2B(ACLK) as src | synchronize with timer | set capture mode */
	timerARegisters->TACCTL2 = 0x0000;
	timerARegisters->TACCTL2 |= timerA::CM01 | timerA::CCIS01 | timerA::SCS | timerA::CAP;

	register uint16 ret = 0;

	//calculate 16 bit by counting clock ticks during VLO ticks
	//the state of the bit is determined by the count. If it is odd
	//the bit is set otherwise it is not set. After all bits are set
	//return the resulting 16-bit integer value.
	for (register uint8 i=16; i > 0; i--) {
		//enhance uniformness of distribution by considering only
		//a majority vote out of 5 tests before determinig the
		//bit value
		register uint8 majorityVote = 0;
		for (register uint8 j=5; j > 0; j--) {
			while ( !(timerARegisters->TACCTL2 & timerA::CCIFG)) {
				//uint8 c[3] = {Led::RED, Led::GREEN, Led::BLUE};
				//getApplication().leds.turnOn(c[timerARegisters->TACCR0 % 3], true);
			}
			// clear capture flag
			timerARegisters->TACCTL2 &= ~timerA::CCIFG;
			// if LSB of capture count set (i.e. count is odd)
			// increase number of "ones" detected
			if (timerARegisters->TACCR2 & 0x01)
				majorityVote++;
		}
		ret >>= 1;
		//clear bit if less than 3 "ones" were detected
		if (majorityVote < 3)
			ret &= ~0x8000;
		//otherwise set it
		else
			ret |= 0x8000;
		//enhance randomness of the resulting value by the following means:
		//XOR the ACLK divider section of the BCSTL1 with the two LSB of ret
		register uint8 h = ret;
		h <<= 4;
		registers->BCSCTL1 ^= h & DIVAx;
		//add 5 to the RSEL bits to change DCO speed
		registers->BCSCTL1 += 5;
		//this clears the first two bits of BCSCTL1 assuring that the
		//oscilator stays switched on and operates in low frequency mode
		registers->BCSCTL1 &= 0x3F;
		// XOR the DCO Mod bits		
		registers->DCOCTL ^= MODx;
	}
	return ret;
}
