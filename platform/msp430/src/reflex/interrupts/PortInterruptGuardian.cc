/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(ses):	 InvalidPortInterruptHandler
  
 *	Author:	Stefan Nuernberger
 *
 *	Description: implementation of PortInterruptGuardian
 *	             and InvalidPortInterruptHandler
 */

#include "reflex/interrupts/PortInterruptGuardian.h"
#include "reflex/interrupts/PortInterruptHandler.h"
#include "reflex/debug/Assert.h"

using namespace reflex;

namespace reflex {
/**
 * InvalidPortInterruptHandler
 * dummy handler for uninitialized pins
 */
class InvalidPortInterruptHandler : public PortInterruptHandler {
public:
	InvalidPortInterruptHandler() :
		PortInterruptHandler(INVALID_PORT, INVALID_PIN,PowerManageAble::PRIMARY) {}

	void handle() { Assert(false); }
	void enable() {}
	void disable() {}
};

} // reflex

// dummy instance of invalid handler
InvalidPortInterruptHandler invalidPortInterruptHandler;

/**
 * constructor
 * @param p portnumber for interrupt capable port
 * @param iv interrupt vector for port
 */
PortInterruptGuardian::PortInterruptGuardian(Port::PortNumber p, mcu::InterruptVector iv):
		InterruptHandler(iv, PowerManageAble::SECONDARY), port(reinterpret_cast<ExtIntPort*>(p) ) {
	// interrupt is secondary - we do not need to be called by PM
	init();
	setSleepMode(mcu::DEEPESTSLEEPMODE); // deepest possible sleepmode
}

/**
 * init
 * initializes handlers with default
 */
void PortInterruptGuardian::init() {
	for (uint8 i = 0; i != PortInterruptHandler::MAX_PINS; ++i)
		handlers[i] = &invalidPortInterruptHandler;
}

/**
 * registerInterruptHandler
 * @param handler address of handler for interrupts
 * @param pin number of pin the handler is interested in
 */
void PortInterruptGuardian::registerInterruptHandler(
			PortInterruptHandler* handler,
			PortInterruptHandler::InterruptCapablePins pin)
{
	if ((pin > PortInterruptHandler::INVALID_PIN)
		&& (pin < PortInterruptHandler::MAX_PINS))
			handlers[pin] = handler;
}

/**
 * handle
 * implements InterruptHandler
 */
void PortInterruptGuardian::handle() {
	uint8 irq = (port->IFG & port->IE);
	// call all handlers with pending and enabled interrupts
	for (uint8 i = 0; i != PortInterruptHandler::MAX_PINS; ++i, irq >>= 1)
		if (irq & 0x1) handlers[i]->handle();
}

/**
 * powermanagement enable function
 */
void PortInterruptGuardian::enable() {
	// we don't need to call the registered handlers
	// since the PowerManager will do this for us
	// (PortInterruptHandlers are PowerManageAble!)
}

/**
 * powermanagement disable function
 */
void PortInterruptGuardian::disable() {
	// we don't need to call the registered handlers
	// since the PowerManager will do this for us
	// (PortInterruptHandlers are PowerManageAble!)
}

