/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(ses):	 InvalidInterruptHandler
  
 *	Author:	Karsten Walther, Carsten Schulze
 *
 *	Description: implementation of InterruptGuardian
 *	             and InvalidInterruptHandler
 */
#include "reflex/interrupts/InterruptGuardian.h"
#include "reflex/interrupts/InterruptVector.h"
#include "reflex/interrupts/InterruptHandler.h"
#include "reflex/debug/Assert.h"

using namespace reflex;


void InterruptGuardian::init()
{
		//all handler are initialized to invalidInterruptHandler
		for(unsigned int i = 0; i < mcu::interrupts::MAX_HANDLERS; i++){
				handlers[i] = &InterruptHandler::invalidInterruptHandler;
		}
}

void InterruptGuardian::registerInterruptHandler( InterruptHandler* handler, mcu::InterruptVector vector)
{
		Assert( vector > mcu::interrupts::INVALID_INTERRUPT && vector < mcu::interrupts::MAX_HANDLERS )
		// register handler in logical interrupt table
		handlers[vector] = handler;
}

