#ifndef PortInterruptHandler_h
#define PortInterruptHandler_h
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(ses):	 PortInterruptHandler
 *
 *	Author:	     Stefan Nuernberger
 *
 *	Description: PortInterruptHandler for interrupts on MSP430 ports
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 *
 * */

#include "reflex/powerManagement/PowerManageAble.h"
#include "reflex/io/Port.h"

namespace reflex {

/**
 * PortInterruptHandler
 * This class registers an InterruptHandler for a
 * single pin on an interrupt capable port.
 * Classes implementing PortInterruptHandler should not see
 * any difference to implementing a default InterruptHandler
 * except for the different name.
 */
class PortInterruptHandler : public PowerManageAble {
public:
	/**
	 * ports capable of interrupt generation
	 */
	enum InterruptCapablePorts {
		INVALID_PORT = 0x00,
		PORT1 = Port::PORT1,
		PORT2 = Port::PORT2
	};

	/**
	 * pins on port
	 */
	enum InterruptCapablePins {
		INVALID_PIN = -1, // invalid pin
		PIN0 = 0,
		PIN1 = 1,
		PIN2 = 2,
		PIN3 = 3,
		PIN4 = 4,
		PIN5 = 5,
		PIN6 = 6,
		PIN7 = 7,
		MAX_PINS = 8 // maximum number of pins
	};

	/**
	 * constructor
	 * @param port number of port to register
	 * @param pin bitmask of interrupt pin on port
	 * @param priority mark handler as secondary
	 */
	PortInterruptHandler(InterruptCapablePorts port, InterruptCapablePins pin,
						 const PowerManageAble::Priority priority);

protected:
	friend class PortInterruptGuardian;
	
	/**
	 * handle
	 * is called when an interrupt for the registered pin arrives
	 */
	virtual void handle() = 0;
};

}

#endif
