#ifndef REFLEX_MSP430_WDT_REGISTERS_H
#define REFLEX_MSP430_WDT_REGISTERS_H
#include "reflex/types.h"
#include "reflex/data_types.h"


namespace reflex {
namespace wdt_a {

	enum RegisterBITS {
		PW_VAL= 0x5A
		,PW_MASK=(0x5A<<8)	/* Watchdog timer password. Always read as 069h. Must be written as 05Ah, or a PUC will be generated */
		,HOLD=(0x1<<7)		/* Watchdog timer hold */
		,NMIES=(0x1<<6)		/* Watchdog NMI edge select */
		,NMI=(0x1<<5)		/* Watchdog NMI function select */

		,TMSEL=(1<<4)	/* Watchdog timer mode select */
		,CNTCL=(1<<3)	/* Watchdog timer counter clear */

		,S_SMCLK=(0x0<<2)	/* Watchdog timer clock source select SMCLK */
		,S_ACLK=(0x1<<2)	/* Watchdog timer clock source select ACLK */

		,IS_1=(0x1<<1)	/* Watchdog timer interval select */
		,IS_0=(0x1<<0)	/* Watchdog timer interval select */
	};

	enum ClockSource {
		 SMCLK = S_SMCLK
		,ACLK = S_ACLK
		,ClkSrcMASK = SMCLK|ACLK
	};

	enum Interval {
		 Ivl32k = 0
		,Ivl8192 = IS_0
		,Ivl512 = IS_1
		,Ivl64 = IS_0|IS_1
		,IntervalMASK=IS_0|IS_1
	};

	enum Address {
		__WDTCTL = 0x0120
	};

	/** Registerfile for the Watchdog Timer module
	 */
	struct Registers
	{
	public:
		//writes implicite the password on writeoperations
		data_types::PWProtectedRegister<uint16,wdt_a::PW_MASK> WDTCTL; /* watchdog timer control register */

		Registers(){}
		operator Registers*() {return operator->();}
		Registers* operator-> () {return reinterpret_cast<Registers*>(__WDTCTL);}
	};

}//ns wdt_a
}//ns reflex


#endif // REFLEX_MSP430_WDT_REGISTERS_H
