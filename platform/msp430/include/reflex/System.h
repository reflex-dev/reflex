#ifndef System_h
#define System_h
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(es):	System
 *
 *	Author:		Carsten Schulze, Karsten Walther
 *
 *	Description:	the base reflex system for MSP430
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */
#include "reflex/scheduling/Scheduler.h"
#include "reflex/interrupts/InterruptGuardian.h"
#include "reflex/interrupts/PortInterruptGuardian.h"
#include "reflex/timer/BasicClockModule.h"
#include "reflex/powerManagement/PowerManager.h"
#include "reflex/timer/TimerA.h"
#include "reflex/SystemStatusBlock.h"
#include "reflex/init/SystemInit.h"

/**
 * This class encapsulates all system objects.
 * There are several reasons for this:
 *   1. The order of construction of global objects is not defined,
 *      but the order of class initialization is (first the base
 *      classes in the order specified, then all members from
 *      top to bottom).
 *   2. It is possible to have multiple system environments by
 *      creating multiple instances. This is used in the guest
 *      platform.
 *
 * To use this class, the application programmer should derive
 * an own class containing additional system objects and drivers:
 * <code>
 * class NodeConfiguration :
 * public System
 * {
 *     I2C i2c;
 * };
 * NodeConfiguration system;
 * </code>
 */

namespace reflex {

struct System
{
	SystemInit init; // prepare the controller/platform for operation
	SystemStatusBlock status;
	PowerManager powerManager; //must be initialized before that guardian
	InterruptGuardian guardian;
	Scheduler scheduler;

	// interrupt distributor for ports
	PortInterruptGuardian port1;
	PortInterruptGuardian port2;

	BasicClockModule basicClockModule;

	TimerA timer;

	System() :
		port1(Port::PORT1, mcu::interrupts::P1),
		port2(Port::PORT2, mcu::interrupts::P2)
	{
	}
};

inline
System& getSystem()
{
	extern System system;
	return system;
}

} //reflex
#endif
