#ifndef FlashRegisters_h
#define FlashRegisters_h
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(ses):	FlashRegisters
 *
 *	Author:		Carsten Schulze, Karsten Walther
 *
 *	Description: Definition of flash memory register set layout and bits.
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 */

namespace reflex {

/**
 * Bitmasks which are used when accessing the registers
 * which deal with the flash
 */
enum FlashRegisterBits
{
	//Keys for writing and reading flash registers
	FRKEY = 0x9600,
	FWKEY = 0xA500,
	//FCTL1 bits
	BLKWRT = 0x0080,
	WRT = 0x0040,
	MERAS =   0x0004,
	ERASE =   0x0002,
	//FCTL2 bits
	FSSELx = 0x00c0,
	FSSEL00 = 0x0000, /// use ACLK
	FSSEL01 = 0x0040, ///use MCLK
	FSSEL10 = 0x0080, ///use SMCLK
	FSSEL11 = 0x00c0, ///use SMCLK
	//FCTL3 bits
	EMEX = 0x0020,
	LOCK =    0x0010,
	WAIT = 0x0008,
	ACCVIFG = 0x0004,
	KEYV = 0x0002,
	BUSY = 0x0001
};

/**
 * the struct is used to access the registers used for flash access
 * To use it it should be cast to FLASH_BASE (0x0000)
 */
struct FlashRegisters
{
	volatile unsigned char IE; ///interrupts enable register at adr. 0x0000
	volatile char dummy[0x126];
	volatile unsigned short FCTL1; ///0x0128 (initial value: 0x9600) see Dokumentation
	volatile unsigned short FCTL2; ///0x012a (initial value: 0x9642) see Dokumentation
	volatile unsigned short FCTL3; ///0x012c (initial value: 0x9618) see Dokumentation
};

}//reflex
#endif
