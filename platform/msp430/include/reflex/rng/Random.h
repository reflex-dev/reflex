#ifndef MIS_Random_h
#define MIS_Random_h

#include "reflex/timer/BasicClockModuleRNG.h"

class Random
{
public:
	Random(int seed)
	{
		//srandom(seed);
		this->seed = seed;
	}

	//FIXME: this uses the "HW"-generator of TI which is implemented
	//in BasicClockModuleRNG. This approach is not suitable for 
	//real applications since the module changes the frequency of the DCO
	//frequently. The DCO is the clock pulse used for the system clock and
	//crucial for timing. Applications that rely on proper timing behaviour
	//(almost all apps for WSN do) do not perform reliably when using this
	//approach. 
	//The BCMRNG module should only be used to generate seeds for some PRNG
	//which should be implemented here.
	//Another approach could be to generate a great set of random varibles
	//before the application start and store it in flash memory ...
	//FIXME: Find some better way to represent probability values than
	//floats between 0 and 1 ...
	float floatRand(); 
private:
	int seed;
	//BasicClockModuleRNG rng;
};
#endif

