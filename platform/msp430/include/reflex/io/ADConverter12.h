#ifndef ADConverter12_h
#define ADConverter12_h
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(ses):	ADConverter
 *
 *	Author:		Carsten Schulze, Karsten Walther
 *
 *	Description:	Driver for the 12bit analog digital converter
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */

#include "reflex/types.h"
#include "reflex/io/ADCRegisters.h"
#include "reflex/scheduling/Activity.h"
#include "reflex/interrupts/InterruptHandler.h"
#include "reflex/sinks/Sink.h"
#include "reflex/sinks/SingleValue.h"

namespace reflex {
/** The analog digital converter sample the channel with the number given
 *  by the input, and propagates the value to the output.
 *  If the converter is busy, when a new request arrives the new request
 *  is discarded, if the old request is not already started the old request
 *  is discarded. It is recommended to prevent these situations.
 */
class ADConverter12 
	: public Activity
	, public InterruptHandler
{
public:
	/** Initializes the register pointer and base InterruptHandler
	 */
    ADConverter12();

    /** Connects the converter to a subsequent component.
     */
      void init(Sink2<char,uint16>* output);

    /** starts the conversion of the channel given by the current value
     *  of input.
     *  Implements the Activity interface.
     */
    virtual void run();


    /** Reads the sampled value and propagates it to the output.
     */
    virtual void handle();

    /** Since device is enabled in run this method is empty so far.
     */
    virtual void enable();

    /** Disables the device for power down modes
     */
    virtual void disable();

    /** The trigger variable for the converter. The assigned value is the
     *  channel to sample.
     */
    SingleValue1<char> input;
	
	/**
	 * If this is called with keepOn == true, 
	 * the ADC12 does not switch off, after reading a value
	 * @param keepOn true -> does not switch off, false -> switch off
	 */
	 void keepOn(bool keep = true);

private:
	/** Pointer to the register set.
	 */
	ADC12Registers* registers;

	/** Holds the number of the currently sampled channel.
	 */
	char currentChannel;

	/** Pointer to an input of a subsequent component.
	 */
	  Sink2<char,uint16>* output;
	
	bool keep;
};

}//ns reflex
#endif
