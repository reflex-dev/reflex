#ifndef PORT_H
#define PORT_H

/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(ses):	Port
 *
 *	Author:	Carsten Schulze, Sören Höckner
 *
 *	Description:	Access to ports from the MSP430 and bitmasks
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */

#include "reflex/types.h"
#include "reflex/data_types.h"

namespace reflex {

/**Various constants for port handling on msp430.
 */
namespace Port {

	/**Defines the base addresses (address of first register) of each port
	 */
	enum PortNumber {
		PORT1 = 0x020,
		PORT2 = 0x028,
		PORT3 = 0x018,
		PORT4 = 0x01c,
		PORT5 = 0x030,
		PORT6 = 0x034
	};
	
	enum PORT1_BITMASKS {
		CTS_IN = 0x40,
		CTS_OUT = 0x80,
		IRDIODE_OUT = 0x01,
		LIGHT = 0x02,
		IRDIODE_IN = 0x04,
		PIR = 0x08,
		VIB = 0x10,
		PKT_INT    = 0x01,  // P1.0, CC2420_FIFOP
		RADIO_GIO0 = 0x08,  // P1.3, CC2420_FIFO
		RADIO_GIO1 = 0x10  // P1.4, CC2420_CCA
	};

	enum PORT2_BITMASKS {
		LEDS = 0x07,
		RED = 0x01,
		GREEN = 0x02,
		ORANGE = 0x04,
		BEEPER = 0x08,
		INC = 0x10,
		UD = 0x20,
		CS = 0x40,
		TASTER = 0x80
	};
	
	enum PORT3_BITMASKS {
		TX0_BIT = 0x10,
		RX0_BIT = 0x20,
		TX1_BIT = 0x40,
		RX1_BIT = 0x80,
		UART_BITS = 0x30,
		UART0_BITS = 0x30,
		UART1_BITS = 0xc0,
		SIMO0 = 0x02,
		SOMI0 = 0x04,
		RADIO_SCLK = 0x08,
		SPI0_BITS = 0x0E
	};
	
	enum PORT4_BITMASKS
	{
		RADIO_SFD     = 0x02,  // P4.1, CC2420 start frame delimiter
		RADIO_CS      = 0x04,  // P4.2, CC2420 chip select
		FLASH_PWR     = 0x08,  // P4.3, Flash turn on Power
		FLASH_CS      = 0x10,  // P4.4, Flash chip select
		RADIO_VREF_EN = 0x20,  // P4.5
		RADIO_RESET   = 0x40,  // P4.6
		FLASH_HOLD    = 0x80   // P4.7, Flash hold signal
	};
	
	enum PORT5_BITMASKS {
		P5SMCLK = 0x20,
		CNTRL0 = 0x40,
		CNTRL1 = 0x80
	};
	
	enum PORT6_BITMASKS {
		A5 = 0x20,
		MIC = 0x01,
		EXV = 0x08,
		BAT = 0x10
	};

}

/**	Registers for a port which has/has not interrupts.
 */
template<bool>
struct PortRegisters;

/**	Registers for a port which has interrupts.
 */
template<>
struct PortRegisters<true>
{
	data_types::ReadOnly<volatile uint8> IN; //read only
	//volatile const uint8 IN; //read only
	volatile uint8 OUT;
	volatile uint8 DIR;
	volatile uint8 IFG;
	volatile uint8 IES;
	volatile uint8 IE;
	volatile uint8 SEL;

	PortRegisters(){}
};

/**Registers for a port which has no interrupts.
 */
template<>
struct PortRegisters<false>
{
	data_types::ReadOnly<volatile uint8> IN; //read only
	//volatile uint8 IN; //read only
	volatile uint8 OUT;
	volatile uint8 DIR;
	volatile uint8 SEL;

	PortRegisters(){}
};


/**Base class of each Port.
 * For Port1 or Port2 this has the additional interrupt handle registers (IFG, IES IE).
 * Otherwise only registers IN OUT DIR and SEL are accessible. The register IN is a read only register.
 * Naturally you only can read from it here too.
 */
template<uint16 TPORTNumber>
struct PortBase
	: public PortRegisters< (TPORTNumber==Port::PORT1) || (TPORTNumber==Port::PORT2) > //TPortNumber is Port1 or Port2
{
	PortBase(){}
	/**Helper function to easily access a Port
	 * <CODE>
	 * 	Port1()->OUT &= 0x1;
	 * <\CODE>
	 */
	PortBase* operator->() {return reinterpret_cast<PortBase*>(TPORTNumber);}
};

struct ExtIntPort : public PortRegisters<true> {ExtIntPort(){}};

/**Structure to access port registers easily.
 * <code>
 * 	Port1()->OUT &= 0x1;
 * <\code>
 */
struct Port1 : public PortBase<Port::PORT1> {Port1(){} };
/**Structure to access port registers easily.
 * <code>
 * 	Port2()->OUT &= 0x1;
 * <\code>
 */
struct Port2 : public PortBase<Port::PORT2> { Port2(){}};
/**Structure to access port registers easily.
 * <code>
 * 	Port3()->OUT &= 0x1;
 * <\code>
 */
struct Port3 : public PortBase<Port::PORT3> { Port3(){}};
/**Structure to access port registers easily.
 * <code>
 * 	Port4()->OUT &= 0x1;
 * <\code>
 */
struct Port4 : public PortBase<Port::PORT4> { Port4(){} };
/**Structure to access port registers easily.
 * <code>
 * 	Port5()->OUT &= 0x1;
 * <\code>
 */
struct Port5 : public PortBase<Port::PORT5> { Port5(){} };

} //reflex
#endif
