#ifndef ADCRegisters_h
#define ADCRegisters_h

/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(ses):	ADCRegisters
 *
 *	Author:		Carsten Schulze
 *
 *	Description:	Registerlayout for analog digital converters 
 *	            	(12 bit and 10 bit)
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */

#include "reflex/MachineDefinitions.h"

namespace reflex {


/** 
 * The register set for the 12 bit analog digital converter.
 * Refer to manual for details.
 */
struct ADC12Registers
{
	/** the 16 ADC12 control registers (adr.: first 0x0080 - last 0x008F */
	volatile unsigned char ADC12MCTL[16];
	
	volatile unsigned char dummy0[0xB0]; ///do never write into these bytes
	
	/** the 16 ADC12 memory registers (adr.: first 0x0140 - last 0x015E */
	volatile unsigned short ADC12MEM[16];
	
	volatile unsigned char dummy1[0x40]; ///do never write into these bytes
	
	 /** ADC12 Controll 0 register (@ 0x01A0) */
	volatile unsigned short ADC12CTL0;
	
	/** ADC12 Controll 1 register (@ 0x01A2) */
	volatile unsigned short ADC12CTL1;
	
	 /** ADC12 interrupt flag register (@ 0x01A4) */
	volatile unsigned short ADC12IFG;
	
	/** ADC12 interrupt enable register (@ 0x01A6) */
	volatile unsigned short ADC12IE;
	
	/** ADC12 interrupt vector word register (@ 0x01A8) */
	volatile unsigned short ADC12IV;
};

/** 
 * The register set for the 10 bit analog digital converter.
 * Refer to manual for details.
 */
struct ADC10Registers
{
	/** ADC10 data transfer control register 0 (@ 0x0048) */
	volatile unsigned char ADC10DTC0;
	
	/** ADC10 data transfer control register 1 (@ 0x0049) */
	volatile unsigned char ADC10DTC1;
	
	/** ADC10 interrupt enable register (@ 0x004A) */
	volatile unsigned char ADC10IE;
	
	volatile unsigned char dummy0[0x165]; ///do never write into these bytes
	
	/** ADC10 control register 0 (@ 0x01B0) */
	volatile unsigned short ADC10CTL0;
	
	/** ADC10 control register 1 (@ 0x01B2) */
	volatile unsigned short ADC10CTL1;
	
	/** ADC10 memory (@ 0x01B4) */
	volatile unsigned short ADC10MEM;
	
	volatile unsigned char dummy1[0x6]; ///do never write into these bytes
	
	/** ADC10 data transfer start address (@ 0x01BC) */
	volatile unsigned short ADC10SA;
	
};

enum ADC12Bitmasks {
	//ADC12 control register 0
	SHT1x  = 0xf000, /// SHT1x Bits, just insert the ones needed
	SHT1_6 = 0x6000,
	
	SHT0x  = 0x0f00, /// SHT0x Bits, just insert the ones needed
	SHT0_0 = 0x0000,
	SHT0_6 = 0x0600,
	
	MSC        = 0x0080,
	REF2_5     = 0x0040,
	REFON      = 0x0020,
	ADC12ON    = 0x0010,
	ADC12OVIE  = 0x0008,
	ADC12TOVIE = 0x0004,
	ENC        = 0x0002,
	ADC12SC    = 0x0001,
	
	//ADC12 control register 1
	CSTARTADDx = 0xf000, ///CSTARTADDx Bits, just insert the ones needed
	CSTARTADD_0 = 0x0000,
	CSTARTADD_1 = 0x1000,
	CSTARTADD_2 = 0x2000,
	CSTARTADD_3 = 0x3000,
	SHSx = 0x0c00,
	SHS_00 = 0x0000,
	SHS_01 = 0x0400,
	SHS_10 = 0x0800,
	SHS_11 = 0x0c00,
	SHP  = 0x0200,
	ISSH = 0x0100,
	ADC12DIVx = 0x00e0,/// ADC12 DIvx Bits, insert only the ones needed
	ADC12SELx = 0x0018,
	ADC12SSEL01 = 0x0008,
	ADC12SSEL10 = 0x0010,
	ADC12SSEL11 = 0x0018,
	CONSEQ = 0x0006,
	CONSEQ_00 = 0x0000,
	CONSEQ_01 = 0x0002,
	CONSEQ_10 = 0x0004,//repeat single channel
	CONSEQ_11 = 0x0006,
	ADC12BUSY = 0x0001,
	
	// ADC Conversion memory control registers 
	// (can only be modified if ENC is 0)
	EOS = 0x80,
	SREFx= 0x70, /// select reference
	SREF_000 = 0x00, /// = AVcc & Vr- = AVss
	SREF_001 = 0x10,
	SREF_010 = 0x20,
	SREF_011 = 0x30,
	SREF_100 = 0x40,
	SREF_101 = 0x50,
	SREF_110 = 0x60,
	SREF_111 = 0x70,
	INCHx = 0x0f,
	INCH_0 = 0x00, /// A0
	INCH_1 = 0x01, /// A1
	INCH_2 = 0x02, /// A2
	INCH_3 = 0x03, /// A3
	INCH_4 = 0x04, /// A4
	INCH_5 = 0x05, /// A5
	INCH_6 = 0x06, /// A6
	INCH_7 = 0x07, /// A7
	INCH_8 = 0x08, /// V-eRef +
	INCH_9 = 0x09, /// Vref - / V-eRef -
	INCH_A = 0x0a, /// temperature
	INCH_B = 0x0b, /// (AVcc / AVss ) / 2
	INCH_C = 0x0c, /// (AVcc / AVss ) / 2
	INCH_D = 0x0d, /// (AVcc / AVss ) / 2
	INCH_E = 0x0e, /// (AVcc / AVss ) / 2
	INCH_F = 0x0f, /// (AVcc / AVss ) / 2
	V_E_REF = INCH_8, /// V-eRef + 
	V_D_REF_E = INCH_9, /// Vref - / V-eRef -
	INT_TEMP = INCH_A,/// temperature
	
	
	// ADC12 interrupt enable register
	ADC12IEx = 0xffff, ///interrupt enable bits, only insert the ones needed
	ADC12IE_0 = 0x0001,
	ADC12IE_1 = 0x0002,
	ADC12IE_2 = 0x0004,
	
	// ADC12 interrupt flag register
	ADC12IFGx = 0xffff, ///interrupt flag bits, only insert the ones needed
	ADC12IFG_0 = 0x0001,
	ADC12IFG_1 = 0x0002,
	ADC12IFG_2 = 0x0004,
	
	// ADC12 interrupt vector register
	ADC12IVx = 0x003e,
	/* the following values are for the complete register */
	ADC12IV_02 = 0x02, /// ADC12MEMx overflow 
	ADC12IV_04 = 0x04, /// conversion time overflow
	ADC12IV_06 = 0x06 /// ADC12MEM0 interrupt
};

} //namespace reflex

#endif
