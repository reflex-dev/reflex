#ifndef SerialRegisters_h
#define SerialRegisters_h

/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(ses):	SerialRegisters
 *
 *	Author:		Karsten Walther, Carsten Schulze
 *
 *	Description: Driver for serial interface.
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */

#include "reflex/timer/BasicClockRegisters.h"
#include "reflex/types.h"
/**
 * The struct SerialRegisters and the enums SerialBits and Baud
 * should facilitate the access to the registers
 * belonging to the serial interface
 * @author Carsten, Schulze
 */

namespace reflex {

/**
 * Gives access to the serial registers.
 * A pointer to this struct should be set to position
 * USART_BASE + USART-number
 * USART_BASE is defined in MachieneDefinitions.h
 * @see RegisterBase
 */
struct SerialRegisters{
	volatile uint8 UCTL;
	volatile uint8 UTCTL;
	volatile uint8 URCTL;
	volatile uint8 UMCTL;
	volatile uint8 UBR0;
	volatile uint8 UBR1;
	volatile uint8 URXBUF;
	volatile uint8 UTXBUF;

	/**
	 * Bitmasks for the serial interfaces
	 */
	enum SerialBits {
		//UCTL
		PENA   = 0x80,
		PEV    = 0x40,
		SPB    = 0x20,
		USART_CHAR   = 0x10,
		LISTEN = 0x08,
		SYNC   = 0x04,
		MM     = 0x02,
		SWRST  = 0x01,

		//TCTL
		CKPH    = 0x80,
		CKPL    = 0x40,
		SSEL00  = 0x00,
		SSEL01  = 0x10,
		SSEL10  = 0x20,
		SSEL11  = 0x30,
		URXSE   = 0x08,
		TXWAKE  = 0x04,
		STC     = 0x02,
		TXEPT   = 0x01,

		//URCTL
		FE      = 0x80,
		PE      = 0x40,
		OE      = 0x20,
		BRK     = 0x10,
		URXEIE  = 0x08,
		URXWIE  = 0x04,
		RXWAKE  = 0x02,
		RXERR   = 0x01,

		//ME
		UTXE0   = 0x80,
		URXE0   = 0x40,
		UTXE1   = 0x20,
		URXE1   = 0x10,
		USPIE0  = 0x40,
		USPIE1  = 0x10,

		//IE
		UTXIE0  = 0x80,
		URXIE0  = 0x40,
		UTXIE1  = 0x20,
		URXIE1  = 0x10,

		//IFG
		UTXIFG0 = 0x80,
		URXIFG0 = 0x40,
		UTXIFG1 = 0x20,
		URXIFG1 = 0x10
	};

	/**
	 * The available baud rates.
	 * The integer values are used by the serial to initialize the
	 * UBR0 and UBR1 registers.
	 * @see Serial
	 * @see ESBRadio
	 */

       enum Baud {
		SERIALCLOCK_HIGH = basicClockModule::SMCLKHZ,
		SERIALCLOCK_LOW = basicClockModule::ACLKHZ,///below 19200 Baud, stable on broken tmodes
		B300 = SERIALCLOCK_LOW/300, /// Baudrate = 300
		B600 = SERIALCLOCK_LOW/600,
		B1200 = SERIALCLOCK_LOW/1200,
		B2400 = SERIALCLOCK_LOW/2400,
		B4800 = SERIALCLOCK_LOW/4800,
		B9600 = SERIALCLOCK_LOW/9600,
		B19200 = SERIALCLOCK_HIGH/19200,/// Baudrate = 19200, initended to be default
		B31250 = SERIALCLOCK_HIGH/31250,
		B38400 = SERIALCLOCK_HIGH/38400,
		B56000 = SERIALCLOCK_HIGH/56000,
		B57600 = SERIALCLOCK_HIGH/57600,
		B115200 = SERIALCLOCK_HIGH/115200,/// Baudrate = 115200, seems to be to fast
		B128000 = SERIALCLOCK_HIGH/128000,
		B230400 = SERIALCLOCK_HIGH/230400,
		B460800 = SERIALCLOCK_HIGH/460800,
		B921600 = SERIALCLOCK_HIGH/921600,
		MCTLDEFAULT = 0xff /// used to initialise the UMCTL register
	};


};

struct SFRRegisters {
	volatile uint8 IE;
	volatile uint8 dummy1;
	volatile uint8 IFG;
	volatile uint8 dummy2;
	volatile uint8 ME;
};

} //end namespace reflex

#endif

