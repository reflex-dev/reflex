#ifndef BasicClockRegisters_h
#define BasicClockRegisters_h
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(ses):	BasicClockRegisters
 *
 *	Author:		Carsten Schulze
 *
 *	Description:	Usefull to access the Basic Clock Registers
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */
#include "reflex/timer/BasicClockModuleConfiguration.h"

namespace reflex {

namespace basicClockModule {

/**
 * The definitions of the basic clock modul registers
 * It should be used as a Pointer to null
 * @author Carsten Schulze
  
 */
struct Registers {
	volatile char IE1; /// interrupt enable register at adr 0x0000
	char dummy1; /// placeholder, do not write anything into it!
	volatile char IFG1; /// interrupt flag register at adr 0x0002
	char dummy[0x53]; /// placeholder, do not write anything into it!
	volatile char DCOCTL; /// DCO control register at adr 0x0056
	volatile char BCSCTL1; /// Basic clock system control 1 at 0x0057
	volatile char BCSCTL2; /// Basic clock system control 2 at 0x0058
};


/**
* Bitmasks for the basic clock modul registers
* @todo Insert the missing Bitmasks, which are not used till now
*/
enum ClockModus {
	DCO7 = 0xe0,
	MODx = 0x1f,
	MOD16= 0x10,
	XT2OFF= 0x80,
	XTS = 0x40,
	DIVAx = 0x30,
	DIVA1 = 0x00,
	DIVA2 = 0x10,
	DIVA4 = 0x20,
	DIVA8 = 0x30,
	RSELx= 0x7,
	RSEL1= 0x01,
	RSEL2= 0x02,
	RSEL3= 0x03,
	RSEL4= 0x04,
	RSEL5= 0x05,
	RSEL6= 0x06,
	RSEL7= 0x07,
	SELMDCOCLK = 0x40,
	SELMXT2CLK = 0x80,
	SELMLFXT1CLK = 0xc0,
	DIVMx = 0x30,
	DIVM1 = 0x00,
	DIVM2 = 0x10,
	DIVM4 = 0x20,
	DIVM8 = 0x30,
	SELS = 0x08,
	DIVSx = 0x06,
	DIVS1 = 0x00,
	DIVS2 = 0x02,
	DIVS4 = 0x04,
	DIVS8 = 0x06,
	DCOR = 0x01,
	OFIE1 = 0x02,
	OFIFG = 0x02,

	/**
	 * If you change the Basic Clock Module, please make sure,
	 * you insert here the correct values. Other modules(USART)
	 * also use the clocks. <b>The other modules need to know,
	 * how fast the basic Clocks are!</b>
	 */
	DCO_FREQUENCY = CLOCK_FREQUENCY,
	MCLKHZ  = CLOCK_FREQUENCY,
	SMCLKHZ = CLOCK_FREQUENCY,
	SMCLK_KHZ = SMCLKHZ/1000,
	ACLKHZ = EXT1_FREQUENCY,    /// intended to be 32768 Hz
	CALIBRATE_TICKS = CLOCK_FREQUENCY / EXT1_FREQUENCY

};

}	//basicClockModule

} //reflex
#endif
