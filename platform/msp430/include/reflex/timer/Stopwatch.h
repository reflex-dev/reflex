#ifndef StopWatch_H
#define StopWatch_H

/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(ses):	 Stopwatch
 *
 *	Author:	 Carsten Schulze, Karsten Walther
 *
 *	Description: This is a implementation for a Stopwatch.
 *              Therefor it uses the Timer B,
 *              it does not use interrupts
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */

#include "reflex/timer/TimerBRegisters.h"
#include "reflex/types.h"

//FIXME: this driver uses not the eventflow interface and compete ageinst the driver for the timerB

namespace reflex {

class StopWatch
{
public:

	/**
	 * Constructor of the Stopwatch, calls init
	 */
	StopWatch();

	/**
	 * Can be used to reset the timer B counter register
	 */
	void reset() {timerBRegisters->TBR = 0;}

	/**
	 * This function returns the timer B counting register.
	 * It is used inline, to be fast
	 * @return the timer B register
	 */
	Time getTime() {return timerBRegisters->TBR;}

private:

	/**
	 * disables the timer B
	 */
	void disable();

	/**
	 * enables the timer B
	 */
	void enable();

	/**
	 * pointer to the registers of timer B,
	 * which is used to access th registers
	 */
	volatile timerB::Registers* timerBRegisters;
};

} //namespace reflex

#endif
