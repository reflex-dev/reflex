#ifndef TimerA_h
#define TimerA_h
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(ses):	 TimerA
 *
 *	Author:	     Stefan Nuernberger
 *
 *	Description: This is used as system timer for the MSP430.
 *               It uses the TimerA with ACLK (32kHz) as input in
 *               continuous mode. Overflows can be reported to
 *               count local time. It also allows for a oneshot
 *               alarm notification through CCR 1. The other
 *               two CCRs (0 and 2) are not used.
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */
#include "reflex/types.h"
#include "reflex/sinks/Sink.h"
#include "reflex/timer/BasicClockRegisters.h"
#include "reflex/timer/TimerARegisters.h"
#include "reflex/interrupts/InterruptHandler.h"

namespace reflex {

/**
 * FIXME: The whole timer should be redesigned to allow the use
 * of the full capture/compare functionality with the 3 different
 * alarms. However, It is probably better to implement this only
 * for TimerB, since the system timer should better not be tampered
 * with by users. This timer is used by the HardwareTimerMilli
 * that is the base for all virtual timers on MSP430.
 */
class TimerA : public InterruptHandler
{
public:

		/** constructor
		 */
		TimerA();

		/** init
		 * @param alarm output to notify on alarm
		 * @param overflow output to notify on timer overflow
		 */
		void init(Sink0& alarm, Sink0& overflow);

		/** start
		 * start the counter. Alarm and overflow interrupts may
		 * occur after that.
		 */
		void start();

		/** stop
		 * stop the counter. No interrupts will happen after this
		 * was called.
		 */
		void stop();

		/** getTime
		 * get the current counter value
		 * @return uint16 current counter register value
		 */
		uint16 getTime();

		/** isOverflowPending
		 * @return bool true if an overflow interrupt is pending
		 */
		bool isOverflowPending();

		/** clearOverflow
		 * clear the overflow interrupt flag
		 */
		void clearOverflow();

		/** enableOverflow
		 * enable overflow interrupt notification
		 */
		void enableOverflow();

		/** disableOverflow
		 * disable overflow interrupt notification
		 */
		void disableOverflow();

		/** startAlarm
		 * start a oneshot alarm.
		 * @param time when the alarm should occur.
		 */
		void startAlarm(uint16 time);

		/** stopAlarm
		 * disable alarm notification.
		 */
		void stopAlarm();

		/**
		 * Timer handler called by the interrupt wrapper.
		 * It is responsible for resetting the hardware to allow
		 * further interrupts.
		 */
		virtual void handle();

		/** enable
		 * PowerManager function
		 * enables the TimerA
		 */
		virtual void enable();

		/** disable
		 * PowerManager function
		 * disables the TimerA
		 */
		virtual void disable();

private :
		volatile timerA::Registers* registers; // TimerA registers

		Sink0* alarm;	// handler for timer alarm
		Sink0* overflow;	// handler for counter overflow
};

} //reflex

#endif
