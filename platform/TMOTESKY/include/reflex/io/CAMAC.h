#ifndef CAMAC_H
#define CAMAC_H
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(ses):	CAMAC for the TmoteRadio cc2420.
 *					It is possible to use it as packetizer
 *
 *	Author:		Carsten Schulze
 *
 *	Description:	platform specific radio I/O 
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */

#include "reflex/scheduling/ActivityFunctor.h"
#include "reflex/memory/Buffer.h"
#include "reflex/sinks/Queue.h"
//#include "reflex/memory/SizedFifoBuffer.h"
#include "reflex/memory/Pool.h"
#include "reflex/io/TMoteRadio.h"
#include "reflex/io/Led.h"
#include "conf.h"

namespace reflex {

/**
 * Provides a simple collision avoidance MAC for the cc2420 Radio
 */
 
//class CAMAC : public Activity, public Sink0 /* , public Sink1<char> */
class CAMAC 
{

public:
	/**
	* 
	*/
	CAMAC(TMoteRadio* tmoteRadio);

	/**
	* Initialize the sender and the reciever, which use the serial
	*
	* @param receiver the object, which gets all receiving characters
	* @param sender gets notified if something was successfully sent
	*/
	void init(Sink1<char>* receiver, Sink0* sender);

	/**
	 * These method is called from the scheduler.
	 * It starts sending the next buffer from the queue.
	 */
	virtual void run();
	
	/**
	 * Implements notify for Sink0. 
	 * Used to recognise the clock signal!
	 * DO NOT USE THE MAC AS SENDER TO THE RADIO
	 */
	virtual void notify();
	
	/**
	 * to receive chars from the radio.
	 * can pack it into a buffer
	 * UNFINISHED: not implemented jet -> use Packetizer
	 * @param cRec charackter received
	 */
	// virtual void assign(char cRec); 
	
	/**
	 * used by the sending objects to store messages, which will be sent
	 */
	Queue<Buffer*> input;
	Event timerIn;
private:

	ActivityFunctor<CAMAC, &CAMAC::run> runFunctor;
	ActivityFunctor<CAMAC, &CAMAC::notify> notifyFunctor;

	/**
	 * When the SCI receives data, it will be sent to the receiver
	 */
	Sink1<char>* receiver;

	/**
	 * The sender is notified, if last send request is finished
	 */
	Sink0* sender;

	/**
	 * pointer to the queue of the radio do send data
	 */
	// Queue<Buffer*>* radioQueue;
	
	/**
	 * pointer to the radio
	 */
	TMoteRadio* tmoteRadio;
	
	/** The pool of buffers used by this channel. Uses IOBufferSize and
	 *  NrOfStdOutBuffers, which must be defined in conf.h .
	 * UNFINISHED : not used so far
	 * TODO : use global buffer
	 */
	//Pool *pool;
	
	/**
	 * Buffer to receive mesages. 
	 * UNFINISHED: Not implemented jet.
	 */
	// SizedFifoBuffer<IOBufferSize> *recBuff; perhaps later
	
	/** length of the msg to receive
	 * UNFINISHED: Not implemented jet.
	 */
	// uint8 recLength; // perhaps later
	
	/**
	 * shows if something is stored in RAM and waiting to be sent
	 */
	volatile bool waitToSend;
	
	/**
	 * ticks to wait
	 */
	volatile int ticksCount;
	volatile int ticks;
	
	/**
	 * calculates the time for the next tick
	 */
	void calcNextTime();
	 
	/**
	 * queue where the last received Msg is stored
	 * either it is used by the MAC itself or transfered to the stages above
	 */
	// Queue<Buffer*>* recMsgQue;
	
	// DEBUG only
	// Led led;
};


} //reflex

#endif
