#ifndef LED_h
#define LED_h
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(ses):	Led
 *
 *	Author:		Carsten Schulze
 *
 *	Description:	The LEDs should be used over this class
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */

#include "reflex/sinks/Sink.h"

namespace reflex {
/**
 * Initialise and controls the LEDs
 *
 * @author Carsten Schulze
  
 */
class Led : public Sink1<char> {
public:

	/**
	* constructor
	* Here is the initalization
	*/
	Led();

	enum Colour {
		BLUE = 0x40,/// blue LED
		GREEN = 0x20,/// green LED
		RED = 0x10,/// red LED
		ALL = 0x70 /// access all LEDs
	};

	/**
	* Turn the Light on
	* @param col Colour(s) which should be shown
	* @param only if true : all other LEDs will be turned off
	*/
	void turnOn(unsigned char col, bool only = false);

	/**
	* Turn the Light off
	* @param col Colour(s) which should be off
	*/
	void turnOff(unsigned char col);

	/**
	* Change the Lightstatus
	* @param col Colour(s) which should blink
	*/
	void blink(unsigned char col);

	/** This method sets the led to the wanted state.
	 *  The value is inverted for the user, to get the
	 *  more machine independet result.
	 *  Implements the Sink1 interface.
	 *  Uses numbers 0 to 7 instead Colour typ!
	 *  
	 *  @param value : 0 (led off) to 7 (all on)
	 */
	virtual void assign(char value);

};

} //reflex
#endif
