#ifndef TMoteRadio_h
#define TMoteRadio_h
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(ses):	TMoteRadio
 *
 *	Author:		Carsten Schulze
 *
 *	Description:	platform specific radio I/O
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */

#include "reflex/scheduling/Activity.h"
#include "reflex/memory/Pool.h"
#include "reflex/memory/Buffer.h"
#include "reflex/sinks/Queue.h"
#include "reflex/interrupts/PortInterruptFunctor.h"
#include "reflex/io/Port.h"
#include "reflex/io/SerialRegisters.h"
#include "reflex/io/CC2420Registers.h"
#include "reflex/MachineDefinitions.h"
#include "reflex/io/SPI.h"
#include "reflex/io/CC2420.h"
#include "reflex/types.h"
#include "reflex/sinks/Event.h"
#include "reflex/scheduling/ActivityFunctor.h"
#include "reflex/powerManagement/PowerManageAble.h"

enum {CRC_OK = 0x0080}; //bit 7 of last byte if auto crc is enabled = crc ok

namespace reflex {

/**
 * Provides send and receive operations
 * for the radio (CC2420) of the TMoteSky
 */

class TMoteRadio
	: public Activity
	, public PowerManageAble
{
public:
	enum {
		StdPowerLevel=31
		,StdChannel=11
	};
private:
	void handleRXI(); /// interrupt handling:receive buffer full
	void handleP1(); /// interrupt handling:receive Port1
	//conecting the interrupt handle routine with the Fuctions
	//handleTXI() and handleRXI(), which are used in these class

public:
	Pool& pool;
	SPI spi;
	CC2420 cc2420;

	/**Set register values, connect Sinks and initialise pool ref
	 * @param pool an pool of buffers
	*/
	TMoteRadio(Pool& pool);

	/**
	* Initialize the sender and the reciever, which use the serial
	*
	* @param receiver the object, which gets all receiving characters
	* @param sender gets notified if something was successfully sent
	*/
	void init(Sink1<Buffer*>* receiver, Sink0* sender);

	/** change channel of cc2420 module.
	 *	@param	channel that should the radio be changed to
	 */
	void changeChannel(uint8 channel){changeParameters(channel,this->powerlevel);}
	/** change powerlevel of cc2420 module.
	 *	@param	pwerlevel that should the radio be changed to
	 */
	void changePowerLevel(uint8 power) {changeParameters(this->channel,power);}
	/** change channel and powerlevel of cc2420 module.
	 *	@param	channel that should the radio be changed to
	 *	@param	pwerlevel that should the radio be changed to
	 */
	void changeParameters(uint8 chan,uint8 power);

	/**
	 * These method is called from the scheduler.
	 * It starts sending the next buffer from the queue.
	 */
	virtual void run();

	/**
	 * All data waiting for transmission is stored in this queue
	 * to get schedueled
	 */
	Queue<Buffer*> input;

	/**
	 * enables the radio
	 */
	virtual void enable();

	/**
	 * disables the radio
	 */
	virtual void disable();

	/**
	 * starts receiving
	 */
	void start_rx() {this->cc2420.start_rx();}


	/**
	 * stops receiving
	 */
	void stop_rx() {this->cc2420.stop_rx_tx();}

	/**
	 * returns the RSSI value from the last received packet
	 */
	uint8 getLastRSSI();

	void soft_reset();

public:
	PortInterruptFunctor<TMoteRadio,&TMoteRadio::handleP1,&TMoteRadio::enable,&TMoteRadio::disable> recvHandler;

protected:

	/**
	 * When the SCI receives data, it will be sent to the receiver
	 */
	Sink1<Buffer*>* receiver;

	/**
	 * The sender is notified, if last send request is finished
	 */
	Sink0* sender;

	/**
	 * holds the buffer, which is currently sending
	 */
	Buffer* current;

private:
	/**	perform some initialization to the radio
	 */
	void initialize();


	bool autoCRC;
	uint8 length;
	uint8* ramBuffer;
	uint8 channel;
	uint8 powerlevel;
	uint8 lastStatus;
	uint8 crc;
	uint8 rssi;

//	bool usedWithMac;
};

} //reflex

#endif
