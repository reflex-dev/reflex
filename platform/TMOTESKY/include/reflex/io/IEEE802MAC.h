#ifndef IEEE802MAC_h
#define IEEE802MAC_h
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(ses):	IEEE802 - MAC Header UNFINSHED, just a start
 *
 *	Author:		Carsten Schulze
 *
 *	Description:	platform specific radio I/O 
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */
#include "reflex/scheduling/Activity.h"
#include "reflex/memory/Buffer.h"
#include "reflex/sinks/Queue.h"
#include "reflex/memory/SizedFifoBuffer.h"
#include "reflex/memory/Pool.h"
#include "reflex/io/TMoteRadio.h"
#include "reflex/io/Led.h"
#include "conf.h"

namespace reflex {

/**
 * Provides a MAC impl. to receive and send valid IEEE802.15.4 data 
 * with the radio (CC2420) of the TMoteSky
 */

	/** non-beacon. 
	 * we also do not have a PAN-coordinator 
	 * ! CONFLICT WITH THE IEEE 802.15 STATNDARD
	 * we also do make not use of the CSMA-CA MAC ! 
	 * TODO ^^^^ :-)
	 */
class IEEE802MAC : public Activity, public Sink0, public Sink1<char>
{

public:
	/**
	* 
	*/
	IEEE802MAC(Queue<Buffer*>* radioQueue);

	/**
	* Initialize the sender and the reciever, which use the serial
	*
	* @param receiver the object, which gets all receiving characters
	* @param sender gets notified if something was successfully sent
	*/
	void init(Sink1<Buffer*>* receiver, Sink0* sender);

	/**
	 * These method is called from the scheduler.
	 * It starts sending the next buffer from the queue.
	 */
	virtual void run();
	
	/**
	 * Implements notify for Sink0. 
	 * Used to recognise the sending of a buffer
	 */
	virtual void notify();
	
	/**
	 * to receive the messages from the radio.
	 * @param cRec charackter received
	 */
	virtual void assign(char cRec); 
	
	Queue<Buffer*> input;
	
	
	
private:

	/**
	 * When the SCI receives data, it will be sent to the receiver
	 */
	Sink1<Buffer*>* receiver;

	/**
	 * The sender is notified, if last send request is finished
	 */
	Sink0* sender;
	
	/** current buffer waiting to send
	 */
	Buffer* current;

	Queue<Buffer*>* radioQueue;	
	
	
	/** The pool of buffers used by this channel. Uses IOBufferSize and
	 *  NrOfStdOutBuffers, which must be defined in conf.h .
	 * UNFINISHED 
	 * TODO : use global buffer
	 */
	Pool* pool;
	
	/**
	 * Buffer to receive mesages.
	 */
	Buffer *recBuff;
	
	/** length of the msg to receive
	 */
	uint8 recLength;
	
	/**
	 * queue where the last received Msg is stored
	 * either it is used by the MAC itself or transfered to the stages above
	 */
	Queue<Buffer*> recMsgQue;
	
	/**
	 * evaluates with a msg. Checks, wich kind of msg it is
	 * if it is a msg for the mac it recacts otherwise it 
	 * sends it to other the receiver
	 * @param msg the message to evaluate
	 */
	void processMsg (Buffer* msg);
	
	/** used to request an association with a pan
	 *  @param panID PANID, where to join
	 */
	void sendAssocRequ(uint16 panID);
	
	enum HEADER {
		BEACON = 0x0000,
		/*DATA = 0x2000,
		ACK = 0x4000,
		MAC_COMMAND = 0x6000,
		SECURITY = 0x1000,
		FRAME_PENDING = 0x0800,
		ACK_REQUEST = 0x0400,
		INTRA_PAN = 0x0200,
		DEST_NONE = 0x0000,
		DEST_16Bit = 0x0020,
		DEST_64Bit = 0x0030,
		SOURCE_NONE = 0x0000,
		SOURCE_16Bit = 0x0002, // bit-reihenfolge niocht klar
		SOURCE_64Bit = 0x0003
		
		*/ // reihenfolge unklar b0,b1,b2 ???
		DATA = 0x0100,
		ACK = 0x0200,
		MAC_COMMAND = 0x0300,
		FRAME_TYPE = 0x0700,
		SECURITY = 0x0800,
		FRAME_PENDING = 0x1000,
		ACK_REQUEST = 0x2000,
		INTRA_PAN = 0x4000,
		DEST_NONE = 0x0000,
		DEST_16Bit = 0x0008,
		DEST_64Bit = 0x000c,
		SOURCE_NONE = 0x0000,
		SOURCE_16Bit = 0x0080, // bit-reihenfolge niocht klar
		SOURCE_64Bit = 0x00c0
	};
	
	/// COPIERT ....
	
	// Frame format related definitions 
	enum
	{
		Frame_Type_Beacon = 0x00,
		Frame_Type_Data = 0x01,
		Frame_Type_Ack = 0x02,
		Frame_Type_Command = 0x03,
		Frame_Type_Field = 0x07,
		
		Security_Enabled_Field = 0x08,
		Frame_Pending_Field = 0x10,
		Ack_Request_Field = 0x20,
		PANId_Compression_Field = 0x40,
		Dest_Addr_Mode_Field = 0x0C,
		Dest_Addr_Mode_Missing = 0x00,
		Dest_Addr_Mode_Short = 0x08,
		Dest_Addr_Mode_Long = 0x0C,
		
		Src_Addr_Mode_Field = 0xC0,
		Src_Addr_Mode_Missing = 0x00,
		Src_Addr_Mode_Short = 0x80,
		Src_Addr_Mode_Long = 0xC0,
	};
	enum
	{
		KeyIdentifierMode_Field = 0x18, // bits 3-4
		KeyIdentifierMode_00 = 0x00,
		KeyIdentifierMode_01 = 0x08,
		KeyIdentifierMode_10 = 0x10,
		KeyIdentifierMode_11 = 0x18,
	};

	enum Command_Identifiers
	{
		ASSOCIATION_REQUEST_CMD = 0x01,
		ASSOCIATION_RESPONSE_CMD = 0x02,
		DISASSOCIATION_NOTIFICATION_CMD = 0x03,
		DATA_REQUEST_CMD = 0x04,
		PANID_CONFLICT_CMD = 0x05,
		ORPHAN_NOTIFICATION_CMD = 0x06,
		BEACON_REQUEST_CMD = 0x07,
		COORDINATOR_REALIGNMENT_CMD = 0x08,
		GTS_REQUEST_CMD = 0x09,
	};

	enum MAC_FRAME_POS // positions of data in the mac-frame
	{
		FRAME_CONTROL = 0, // byte 0 and 1
		SEQU_NR = 3,
		DEST_PANID = 4, // mybe 0 bytes long
		DEST_ADR = 5, // mybe 0 bytes long.. if no DEST_PANID = 4
		// other values not usefull, because depends on FrameControll
	};
  /// ... COPIERT
  
	uint8 sequenzNr;
	
	uint16 myPanId;
	uint16 my16BitId;
	// uint16 sourcePANID;
	// uint16 destPanId;
	
	Led led;
	bool waitingAssoc;
};


} //reflex

#endif
