#ifndef ExternalFlash_h
#define ExternalFlash_h

/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(ses):	ExternalFlash
 *
 *	Author:		Olaf Krause
 *
 *	Description:	The external 1024 Bytes flash memory
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */

#include "reflex/io/Port.h"
#include "reflex/types.h"
#include "reflex/io/SPI.h"

namespace reflex
{
  class ExternalFlash
  {
  private:
    SPI& spi;
    volatile Port p4;

    enum Flash_Instructions
    {
			WRITE_ENABLE			= 0x06,
			WRITE_DISABLE			= 0x04,
			READ_STATUS_REGISTER 	= 0x05,
			READ_DATA_BYTES			= 0x03,
			READ_DATA_BYTES_FAST	= 0x0B,
			PAGE_PROGRAM			= 0x02,
			SECTOR_ERASE			= 0xD8,
			BULK_ERASE				= 0xC7,
			DEEP_POWER_DOWN 		= 0xB9,
			RES						= 0xAB
    };

		enum Flash_Vars
		{
			WIP = 0x01, // write in progress bit from status word
			SECTORS	= 16,
			SECTOR_SIZE = 0x10000,

		};

		uint8 read_register();

		void write_enable();

		void waiting();

  public:

		/**
		 * constructor
		 * Here is the initalization
		 */
    ExternalFlash(SPI &spi_ref);

		/**
		 *	Executing the deep power down instruction
		 *	the only way to put the device in the lowest power consumption mode 
		 */
		void deep_power_down();

		/**
		 *	releases the Flash from deep power down mode
		 *  attention: this may take some time, see datasheet 
		 */
		void release_from_dpd();
		/**
	 	 * reads bytes from addr in flash to buf
		 * the size could be extended if it is needed
		 * the Flash can read "infinit" bytes
		 */
		void read(void* buf, uint8 bytes, flash_addr_t addr);

		/**
	 	 * writes bytes from buf to addr in flash
		 * programs one 256 bytes page from the Flash
		 *
		 */
		void write(const void* buf, uint8 bytes, flash_addr_t addr);

		/**
		 * erases a complete 64k sector (all bits are set to 1)
		 * note: any address inside a sector is a valid adress for the sector erase
		 * @param addr address in sector that shall be erased
		 */
		void sector_erase(flash_addr_t addr);	

		/**
		 * erases the complete flash
		 */	
		void bulk_erase();
  };

} // namespace reflex

#endif // ExternalFlash_h
