/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
  
 *	Author:		 Carsten Schulze, Olaf Krause
 *
 * design considerations: I think that an interrupt at fifo is NOT nessesary
 * (also sayed in the datasheed, only to determine that something is in the RXFIFO)
 *
 */

#include "conf.h"
#include "reflex/io/TMoteRadio.h"
#include "reflex/System.h"
//#include "NodeConfiguration.h"


using namespace reflex;

TMoteRadio::TMoteRadio(Pool& pool) :
	PowerManageAble(PowerManageAble::PRIMARY),
	pool(pool),
	spi(),
    cc2420(spi),
	recvHandler (PortInterruptHandler::PORT1,PortInterruptHandler::PIN0,*this,PowerManageAble::SECONDARY),
	channel(StdChannel),
	powerlevel(StdPowerLevel)
{
	//FIXME: the powermanagement of the rx part should be handled differently frome the tx part
	this->setSleepMode(mcu::LPM1);
	
	input.init(this);
	autoCRC = true; //enable, disable AUTO CRC -> if enable inc length+2
	sender = 0;
	receiver = 0;
	current = 0;
}

void TMoteRadio::initialize() // has to be called, when interrupts are enabled
{
	this->cc2420.hardware_reset();

	this->cc2420.enable_oscillator();

	//oscillator needs some time to get up, we give him 0.5ms @ 8MHz
	for (int i=0; i<1000;i++)
	{
		asm("nop;"); asm("nop;");asm("nop;"); asm("nop;");
	}

  this->cc2420.set_address_filter(false);

  this->cc2420.set_auto_crc(autoCRC); // ATTENTION: with AUTOCRC the MSG length increments by 2

  //set cor_thr to 20
  uint8 temp[2];

  this->cc2420.read_register_buffer(REG_MDMCTRL1, temp);
  temp[0] &= 0xF8; //clear corr+\_thr
  temp[0] |= 0x05;
  temp[1] &= 0x3F;
  this->cc2420.write_register_buffer(REG_MDMCTRL1, temp);

  this->cc2420.set_rxfifo_protection(false);

      //set RXBPPF_LOCUR to 1

  this->cc2420.read_register_buffer(REG_RXCTRL1, temp);
  temp[0] |= 0x20; //set bit 13
  this->cc2420.write_register_buffer(REG_RXCTRL1, temp);

  this->cc2420.set_fifo_threshold(127);

  this->cc2420.set_rf_channel(channel);

  this->cc2420.set_output_power_level(powerlevel);
    
  this->cc2420.start_rx();// done manualy

}


void TMoteRadio::init(Sink1<Buffer*>* receiver, Sink0* sender)
{
	this->receiver = receiver;
	this->sender = sender;
}

void TMoteRadio::changeParameters(uint8 chan,uint8 power)
{
	this->disable();
	channel=chan;
	powerlevel=power;
	this->enable();
}

void TMoteRadio::handleP1() // raises for PKT -> if a complete Frame was received or threshold is full
{

//   getApplication().serOut.write("INT");
//   getApplication().serOut.writeln();


  this->cc2420.clear_rx_interrupt_flag();

	if (!this->cc2420.get_fifo_in())
	{
		this->cc2420.flush_rx_fifo();
	}
  else //recieve
	{

       //get first byte
    this->cc2420.read_rx_fifo(&length, 1, false);

    if ((length >= 127) || (length <= 1))//invalid pkt, -> flush? RX_BUFFER_SIZE
		{
	  	this->cc2420.flush_rx_fifo();
		}
    else // get the rest of the pkt
	 	{
	   	current = new(&pool)  Buffer(&pool); // get a new buffer

        if(current==0)
        {
            this->cc2420.flush_rx_fifo();
        }
        else
        {

            current->initOffsets(0); //because cc2420 writes every byte and does not know where the header starts

                ramBuffer = (uint8*) current->getStart();

                if(autoCRC)
                {
                    length -= 2; //without FCS
                cc2420.read_rx_fifo(ramBuffer, length, false); //receive rx Buffer
                cc2420.read_rx_fifo(&rssi, 1, false); //recive RSSI, not yet needed
                cc2420.read_rx_fifo(&crc, 1, false); //recive CRC
                    if (crc & CRC_OK && receiver)
                    {
						 receiver->assign(current);
//     					getApplication().serOut.write("CRC OK");
//     					getApplication().serOut.writeln();
                    }
                    else //CRC not OK or receiver not connected
                    {
//     					getApplication().serOut.write("CRC NOT OK");
//     					getApplication().serOut.writeln();
                        current->downRef();
                    }
                }//if(autoCRC)
                else
                {
                    cc2420.read_rx_fifo(ramBuffer, length, false); //receive rx Buffer
                    receiver->assign(current);
                }
        }
		}//else
	}//else

  ramBuffer=0;

}//handleP1()



//already done in cc2420.cc
void TMoteRadio::enable()
{
	InterruptLock lock;
  spi.enable();

  this->cc2420.voltage_regulator_enable();
  initialize();
  this->cc2420.enable_rx_interrupt();
  this->unlock();
}

void TMoteRadio::disable()
{
this->lock();	
  //wait a little while for the radio to finish ongoing transmissions
  for (int i=0; i<1000;i++)

    {
      asm("nop;"); asm("nop;");asm("nop;"); asm("nop;");
    }

  this->cc2420.disable_rx_interrupt();
  this->cc2420.hardware_reset();

  this->cc2420.voltage_regulator_disable();

  spi.disable();
}
//#include "NodeConfiguration.h"

void TMoteRadio::run()
{

  Buffer* current = input.get();
  uint8* ramBuffer = 0;
  uint8 length=0;
  uint8 lastStatus=0;
  if ( current == 0) {
	return;
	
  }
  else
	{
      length = current->getLength();
		if ( length <= 0 )  // buffer not big enough
		{
			current->downRef();
			if ( sender) {
				sender->notify();
			}
			return;
		}

		uint8 lengthWithCRC;
		if (autoCRC) lengthWithCRC = length + 2;
		else lengthWithCRC = length;
		ramBuffer = (uint8*) current->getStart();
		cc2420.write_tx_fifo(&lengthWithCRC, 1);
		cc2420.write_tx_fifo(ramBuffer, length);
		lastStatus = cc2420.start_tx(false);

		if (lastStatus & CC2420::TX_UNDERFLOW)
		{
			cc2420.flush_tx_fifo();
		}
	  //here is an issue that the radio has not finished the transmisson, but we
      //notify the sender which could deactivate the radio hardware
      if(sender) sender->notify();
      current->downRef();
	}
}

uint8 TMoteRadio::getLastRSSI()
{
	return rssi;
}

void TMoteRadio::soft_reset()
{
  //	this->cc2420.software_reset();
}
