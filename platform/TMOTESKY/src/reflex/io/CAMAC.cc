/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
  
 *	Author:		 Carsten Schulze
 */

#include "reflex/System.h" //because of getSystem()
//#include "reflex/memory/SizedFifoBuffer.h"
#include "reflex/memory/Pool.h"
#include "reflex/io/CC2420Registers.h"
#include "reflex/io/TMoteRadio.h"
#include "reflex/io/CAMAC.h"

//test
#include "reflex/io/Led.h"

using namespace reflex;

CAMAC::CAMAC(TMoteRadio* tmoteRadio) : runFunctor(*this), notifyFunctor(*this)

{
	input.init(&runFunctor);
	timerIn.init(&notifyFunctor);
	this->tmoteRadio = tmoteRadio;
	//waitToSend = false;
	
	// be carefull : that means the radio must be constructed before
	//this->tmoteRadio->setMacUsed(true);
}

void CAMAC::init(Sink1<char>* receiver, Sink0* sender)
{
	this->receiver = receiver;
	this->sender = sender;
	// tmoteRadio->init(receiver,0); // possibly done in the NodeConfiguraion.h
}

/*
void CAMAC::assign(char cRec) // from radio,
{
	// the first sign is the length
	if ( recLength == 0 ) {
		recLength = cRec; // length of the next Msg. everything else done
		if ( recBuff ) {// that should not be ! -> delete
			recBuff->downRef();
		}
		recBuff = new (&pool) SizedFifoBuffer<IOBufferSize>(); // get a new Buffer to copy data there
		return;
	}
	
	recLength--;
	if ( recBuff == 0) { // If it was not enough space in pool.
		return;
	}
	recBuff->write(cRec);
	// add into to proceed queue
	if (recLength == 0) { // all received -> to receiver
		recMsgQue.assign(recBuff);
		recBuff=0;
	}
}
*/

void CAMAC::run()
{
	Buffer* nextToSend = input.get();
	if ( nextToSend == 0) {
		return;
	}
	runFunctor.lock();
	// something to send;
	tmoteRadio->input.assign(nextToSend); // will call downRef()
	//led.blink(Led::GREEN);
	waitToSend = true;
	calcNextTime();
}

void CAMAC::calcNextTime()
{
	// if TICK_PERIOD_MSEC is big then there is no need to wait some ticks
	// ticksCount must be minimum 1
	ticksCount = 1;
	if ( TICK_PERIOD_MSEC < 100 ) {
		ticksCount = ((ticks*ticks/3) & 0x000f);
	}
}

void CAMAC::notify() // notify from the clock signal
{
	// use tmoteRadio->sendWithCA() to determine if it was possible to send
	ticks++;
	ticks = ticks&0xff;
	ticksCount--;
	//if ( (waitToSend == true) /*&& (ticksCount <= 0)*/ ) {
	if ( (waitToSend == true) && (ticksCount <= 0) ) {
		//if ( tmoteRadio->sendWithCA() ) {
			waitToSend = false;
			runFunctor.unlock();
		//} else {
		//	calcNextTime();
		//}
	//	return; // no it is not useless
	}
	
}
