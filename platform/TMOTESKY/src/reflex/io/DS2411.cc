#include "reflex/io/DS2411.h"

#include "reflex/io/Port.h"

using namespace reflex;

/**
 * This class implements the driver for the 1-Wire DS2411 device.
 * @author Jonas Hartwig, Christian Hildebrand
 */

#define wait_us() {asm("nop");asm("nop");asm("nop");asm("nop");asm("nop");asm("nop");asm("nop");asm("nop");}//makro for waiting 1us
#define wait_tA() {wait_us();wait_us();wait_us();wait_us();wait_us();wait_us();}//wait ta in us, because of stack-time-lack

#define dir_inp() (Port2()->DIR &= ~PIN2_4)//set direction to input -> release bus
#define dir_outp() (Port2()->DIR |= PIN2_4)//set direction to output
#define getInp() (Port2()->IN & PIN2_4)//read input
 
void DS2411::init()	{

	Port2()->SEL &= ~PIN2_4;//set pin to I/O-Function
	Port2()->IE &= SETZERO;//disable interrupts
	Port2()->OUT &= ~PIN2_4;//out to 0
	dir_inp();//set to input --> line high
	for(int i = 0;i < IDSIZE;i++) {
		id[i] = 0x00;
	}
}

void DS2411::fetch() {

	sendResetPulse();//set 1-wire devices to listen
	writeByte(0x33);//send read rom
	for(int i = 0;i < 8;i++) {//read 8 byte 1 family- code, 6 ID, 1 CRC
			id[i] = readByte();
	}
}

unsigned int DS2411::wait(unsigned int us) { //1 us == 8 CPU-Takte on T-MoteSky

	asm("nop");//1 cycle,1word
	asm("nop");//1 cycle,1word
	asm("add #-1, R14");//dec r14 4 cycles,2word
	asm("jnz $-6");//reset plc 2 cycle,2word
}

void DS2411::writeByte(unsigned char b)	{

	for(int i = 1;i <= 8;i++) { //for 8 bit

		if(b & 0x01) {//if least bit is set brite a one
			dir_outp();//set dir to output
			wait_tA();//wait
			dir_inp();//set dir to input
			wait(tB);//wait
		}
		else {// write 0
			dir_outp();//set dir to output
			wait(tC);//wait
			dir_inp();//set dir to input
			wait(tD);//wait
		}
		b >>= 1;//shift byte by one for next bit to sent
	}
}

unsigned char DS2411::readByte() { //least bit first

	unsigned char b = 0;//create new byte
	for(int i = 1;i <= 8;i++) { //for 8 bit

		dir_outp();//set dir to output
		wait_tA();//wait
		dir_inp();//set dir to input
		wait(tE);//wait
		if(getInp()) {//read input, if is high write one into byte
			b |= 0x80;
		}
		wait(tF);//wait
		b >>= 1;//shift byte by one for next bit to sent
	}
	return b;//return read byte
}

void DS2411::sendResetPulse() {
	wait(tG);//wait
	dir_outp();//out is already 0 -> set to output -> write 0
	wait(tH);//wait
	dir_inp();//change dir to input -> resistor pull high -> DS2411 pull low
	wait(tI);//wait
	if(!getInp()){}//check if 1-wire is pulled down
	wait(tJ);//wait again
}
