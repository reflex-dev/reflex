/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
  
 *	Author:		 Olaf Krause, Soeren Hoeckner
 */


#include "reflex/io/UserButton.h"
#include "reflex/interrupts/InterruptVector.h"

using namespace reflex;

UserButton::UserButton()
	: InterruptHandler(mcu::interrupts::P2, PowerManageAble::PRIMARY)
{
	this->setSleepMode(mcu::LPM4);
	//this->enable();
}

void UserButton::init(Sink0 *rec)
{
	this->receiver = rec;
}

void UserButton::enable()
{
	// setting SEL to 0 enables I/O functionality
	Port2()->SEL &= ~Port::TASTER;
	Port2()->DIR &= ~Port::TASTER;
	Port2()->OUT &= ~Port::TASTER;

	Port2()->IE |= Port::TASTER;
}

void UserButton::disable()
{
	Port2()->IE &= ~Port::TASTER;
}


void UserButton::handle()
{
	// check if user button (P2.7) was pressed, i.e. P2IFG[7] is set
	if ((this->receiver != 0) && (Port2()->IFG & Port::TASTER))
	{
		receiver->notify();
	}

	// clear all P2 interrupts
	Port2()->IFG = 0x00;
}
