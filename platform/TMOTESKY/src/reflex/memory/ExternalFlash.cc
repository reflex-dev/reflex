#include "reflex/memory/ExternalFlash.h"
#include "reflex/interrupts/InterruptLock.h"
#include "NodeConfiguration.h"

using namespace reflex;

ExternalFlash::ExternalFlash(SPI &spi_ref) : spi(spi_ref), p4(Port::PORT4)
{
	// set IO functionality
	*p4.SEL &= ~(Port::FLASH_CS);

	p4.reg->OUT |= (Port::FLASH_CS | Port::FLASH_HOLD | Port::FLASH_PWR);

	// set chip select and hold pins as outputs
	p4.reg->DIR |= (Port::FLASH_CS | Port::FLASH_HOLD | Port::FLASH_PWR);


	this->spi.setProperties(InterruptHandler::SECONDARY);
}


void ExternalFlash::deep_power_down()
{
	uint8 rx_byte;

	InterruptLock lock();

	 p4.reg->OUT &= ~Port::FLASH_CS; // flash enable

	this->spi.flash_rx_byte(); //clear
	this->spi.flash_tx_byte(DEEP_POWER_DOWN);
	while (!this->spi.rx_interrupt());
	rx_byte = this->spi.flash_rx_byte();

	p4.reg->OUT |= Port::FLASH_CS; // flash disable
}//deep_power_down


uint8 ExternalFlash::read_register()
{
	uint8 rx_byte = 0;

	InterruptLock lock();

	p4.reg->OUT &= ~Port::FLASH_CS; // flash enable

	this->spi.flash_tx_byte(READ_STATUS_REGISTER);

	this->spi.flash_rx_byte(); //clear rx_buffer
	while (!this->spi.rx_interrupt());
	rx_byte = this->spi.flash_rx_byte();


	p4.reg->OUT |= Port::FLASH_CS; //flash disable

	return rx_byte;
}//read_register


void ExternalFlash::release_from_dpd()
{
	InterruptLock lock();

	p4.reg->OUT &= ~Port::FLASH_CS; // flash enable

	this->spi.flash_tx_byte(RES);

	p4.reg->OUT |= Port::FLASH_CS; //flash disable

}//enable


void ExternalFlash::write_enable()
{
	InterruptLock lock();

	p4.reg->OUT &= ~Port::FLASH_CS; // flash enable

	this->spi.flash_tx_byte(WRITE_ENABLE);

	p4.reg->OUT |= Port::FLASH_CS; //flash disable
}// write_enable

void ExternalFlash::waiting()
{
	uint8 reg;

	do {reg=read_register();}
	while(reg & WIP);

}//waiting

void ExternalFlash::read(void* buf, uint8 bytes, flash_addr_t addr)
{
	uint8* buffer = (uint8*)buf;

	waiting();
	InterruptLock lock();

	p4.reg->OUT &= ~Port::FLASH_CS; // flash enable

//	this->spi.flash_rx_byte();
	this->spi.flash_tx_byte(READ_DATA_BYTES);
	this->spi.flash_tx_byte(addr >> 16);
	this->spi.flash_tx_byte(addr >> 8);
	this->spi.flash_tx_byte(addr);

	this->spi.flash_rx_byte(); //clear rx_buffer	
	for (uint8 i=0; i<bytes; i++)
	{
		uint8 byte;
		while (!this->spi.rx_interrupt());
		byte = this->spi.flash_rx_byte();
		*buffer = ~byte;
		buffer++;
	}

  p4.reg->OUT |= Port::FLASH_CS; //flash disable

}//read

void ExternalFlash::write(const void* buf, uint8 bytes, flash_addr_t addr)
{
	uint8* buffer = (uint8*)buf;

	waiting();
	write_enable();
	InterruptLock lock();

	p4.reg->OUT &= ~Port::FLASH_CS; // flash enable
	
	this->spi.flash_tx_byte(PAGE_PROGRAM);
	this->spi.flash_tx_byte(addr >> 16);
	this->spi.flash_tx_byte(addr >> 8);
	this->spi.flash_tx_byte(addr);
	
	for (uint8 i=0; i<bytes; i++)
	{
		this->spi.flash_tx_byte(~*buffer);
		buffer++;
	}

  p4.reg->OUT |= Port::FLASH_CS; //flash disable

}//write

void ExternalFlash::sector_erase(flash_addr_t addr)
{
	waiting();
	write_enable();

	InterruptLock lock();

	p4.reg->OUT &= ~Port::FLASH_CS; // flash enable

	this->spi.flash_tx_byte(SECTOR_ERASE);
	this->spi.flash_tx_byte(addr >> 16);
	this->spi.flash_tx_byte(addr >> 8);
	this->spi.flash_tx_byte(addr);

  p4.reg->OUT |= Port::FLASH_CS; //flash disable
	
}//sector_erase

void ExternalFlash::bulk_erase()
{

	flash_addr_t addr = 0;
	for (uint8 i=0; i < SECTORS; i++)
	{
		sector_erase(addr);
		addr += SECTOR_SIZE;
	}
}//bulk_erase


