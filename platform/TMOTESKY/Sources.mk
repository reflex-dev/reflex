#/*
# *	REFLEX - Real-time Event FLow EXecutive
# *
# *	A lightweight operating system for deeply embedded systems.
# *
#
# *	Author:		 Carsten Schulze, Karsten Walther
# */


ifeq ($(DEBUG),1)
	CC_SOURCES_PLATFORM += \
		debug/PlatformAssert.cc
endif
