#/*
# *	REFLEX - Real-time Event FLow EXecutive
# *
# *	A lightweight operating system for deeply embedded systems.
# *
#  
# *	Author:		Karsten Walther
# */

CONTROLLER = MSP430
DEVICE = msp430x1611

include $(REFLEXPATH)/platform/$(PLATFORM)/Sources.mk


INCLUDES += -I$(REFLEXPATH)/platform/$(PLATFORM)/include

LDFLAGS += -T$(REFLEXPATH)/platform/$(PLATFORM)/LinkerScript -nostdlib -nostartfiles

ifdef BSL
DEFINED_REFLEX_VARS += "BSL = " $(BSL)
else
MISSING_REFLEX_VARS += "BSL - bsl.py script used for download"
endif


#support for darwin
#note that with darwin each tmote is connected to a device containing its id
#if no mote is specified by USB the first one is picked
BUILDPLATFORM = $(shell uname -s)
ifeq ($(BUILDPLATFORM), Darwin)
ifdef USB
ifneq ($(findstring usbserial,$USB), tty.usbserial)
TTYUSB = $(shell ls /dev/tty.usbserial* | sort | head -n1)
USB = $(TTYUSB)
endif
endif
endif

ifdef USB
DEFINED_REFLEX_VARS += "USB = " $(USB)
else
MISSING_REFLEX_VARS += "USB - USB device"
endif

TARGETS += download

download : all
	$(BSL) --telosb -er -c $(USB) $(EXECUTABLE)


