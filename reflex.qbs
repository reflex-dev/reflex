import qbs 1.0

/*
  Meta project file for the REFLEX framework and all of its official
  components.
*/
Project {
    qbsSearchPaths : "qbs"
    readonly property string reflex_platform : qbs.architecture

    property bool   buffer_enabled : true
    property string buffer_sizeType : "uint8"
    property int    buffer_maxPoolCount : 8
    property string buffer_bufferSizeType : "uint8"
    property bool   components_enabled : true
    property string core_schedulingScheme : "FIFO_SCHEDULING"
    property int    core_nrOfSchedulingSlots : undefined // deprecated
    property int    core_ticksPerRound : undefined // deprecated
    property bool   devices_enabled : true
    property int    msp430x_nodeinfoaddress : 0x1800
    property string msp430x_mcu : "cc430x6137"
    property bool   msp430x_bootloader_enabled : true
    property int    msp430x_bootloader_address : 0x8000
    property string msp430x_bootloader_channel : 0
    property bool   msp430x_update_enabled : true
    property int    msp430x_update_firmwareaddress : 0x8c00
    property int    omnetpp_nodeCount : 1
    property bool   protothread_enabled : true
    property bool   virtualtimer_enabled : true

    property bool   xml_enabled : true
    // Maximum amount of message requests in the output queue of XmlWriter.
    // This reflex in the size of XmlWriter::input_message.
    property int    xml_messageQueueSize : 1
    // Maximum length of an XML tag in the XML tag table (without \0)
    property int    xml_maxTagLength : 20

    references: [
        "buffer/buffer.qbs",
        "components/components.qbs",
        "core/core.qbs",
        "devices/devices.qbs",
        "platform/platform.qbs",
        "protothread/protothread.qbs",
        "virtualtimer/virtualtimer.qbs",
        "xml/xml.qbs"
    ]
}
