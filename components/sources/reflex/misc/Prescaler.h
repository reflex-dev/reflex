#ifndef REFLEX_LIB_PRESCALER_H
#define REFLEX_LIB_PRESCALER_H

/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *	Class(es):	Prescaler
 *
 *	Author:		Stefan Nuernberger
 *
 *	Description:	A prescaler, useful for scaling down logical
 *			clock ticks, etc.
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and
 *    Operating Systems. All rights reserved.
 *
 *    Redistribution and use in source and binary forms, with or without
 *    modification, are permitted provided that the following conditions
 *    are met:
 *
 *       1. Redistributions of source code must retain the above copyright
 *          notice, this list of conditions and the following disclaimer.
 *
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the
 *          distribution.
 *
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *    The views and conclusions contained in the software and documentation
 *    are those of the authors and should not be interpreted as representing
 *    official policies, either expressed or implied, of BTU Cottbus,
 *    Department for Distributed Systems and Operating Systems.
 *
 **/

#include "reflex/sinks/Sink.h"
#include "reflex/types.h"

namespace reflex {

/** This class is for prescaling of a tick, whenever the prescale value is
 *  reached the output is notified.
 */
class Prescaler : public Sink0 {
public:
	/** operation modes for prescaler */
	enum OperationMode {
		PERIODIC
		,ONESHOT
	};

	/** constructor
	 *  @param opmode : periodic or oneshot operation
	 */
	Prescaler(OperationMode opmode = PERIODIC);

	/** connect_output
	 *  set the subsequent sink
	 *  @param output event that will be triggered
	 */
	void connect_output(Sink0 *output);

	/** notify counts the ticks an notifies the subsequent sink if
	 *  prescale count is reached.
	 */
	virtual void notify();

	/** set the prescale value.
	 *  @param ticks: the prescale value, a 0 stops prescaling.
	 */
	void set(unsigned int ticks);

private:
	volatile unsigned prescaleValue; // the prescale value
	volatile unsigned counter; // the tick counter
	OperationMode opmode; // periodic or oneshot operation
	Sink0 *output; // will be triggered when prescale value is reached
};

} //namespace reflex

#endif // REFLEX_LIB_PRESCALER_H
