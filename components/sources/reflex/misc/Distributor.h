#ifndef Distributor_h
#define Distributor_h
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *	Class(ses):	Distributor, Distributor1
 *
 *	Author:		Karsten Walther
 *
 *	Description:	Event Distributors for Sink0 and Sink1<T> interface
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */
#include "reflex/sinks/Sink.h"
#include "reflex/interrupts/InterruptLock.h"

namespace reflex {

/** Distributes notify call to multiple subscribers. It get the maximal
 *  count as template parameter.
 *
 *	@author Karsten Walther
  
 */
template <const unsigned char maxElems>
class Distributor : public Sink0 {
public:
	/** Set subscriber number to 0.
	 */
	Distributor()
	{
		counter = 0;
	}

    /** distributing the event
     */
	virtual void notify();

	/** register subscribers
	 *
	 *  @param sink reference to a subsequent sink
	 */
	void registerSink(Sink0& sink);

	/** remove a subscriber and move up all following subscribers
	 *
	 *  @param sink Sink for removal
	 */
	void removeSink(Sink0& sink);

private:
	/** Array of subscribers
	 */
	Sink0* sinks[maxElems];

	/** count of current subscribers
	 */
	unsigned char  counter;
};

/** Distributes notify call to multiple subscribers. It get the maximal
 *  count as template parameter.
 *
 *	@author Karsten Walther
  
 */
template <typename T, const unsigned char maxElems>
class Distributor1 : public Sink1<T> {
public:
	/** Set subscriber number to 0.
	 */
	Distributor1()
	{
		counter = 0;
	}

    /** distributing the event
     */
	virtual void assign(T value);

	/** register subscribers
	 *
	 *  @param sink reference to a subsequent sink
	 */
	void registerSink(Sink1<T>* sink);

	/** remove a subscriber and move up all following subscribers
	 *
	 *  @param sink Sink for removal
	 */
	void removeSink(Sink1<T>* sink);

protected:
	/** Array of subscribers
	 */
	Sink1<T>* sinks[maxElems];

	/** count of current subscribers
	 */
	unsigned char  counter;
};

//Distributor0
template<const unsigned char maxElems>
void Distributor<maxElems>::notify()
{
	for(unsigned char i=0;i<counter;i++){
		sinks[i]->notify();
	}
}

template<const unsigned char maxElems>
void Distributor<maxElems>::registerSink(Sink0& sink)
{
	InterruptLock lock;
   	if(counter < maxElems){
   		sinks[counter] = &sink;
   		counter++;
   	}
}

template<const unsigned char maxElems>
void Distributor<maxElems>::removeSink(Sink0& sink)
{
	InterruptLock lock;
	for(int i=0; i<counter; i++) {
		if(sinks[i] == &sink) {
			sinks[i] = 0;
			for(int j=i; j<counter; j++) {  //move following pointers
				sinks[j] = sinks[j+1];
			}
			counter--;
			break;
		}
	}
}

// Distributor1
template <typename T, const unsigned char maxElems>
void Distributor1<T,maxElems>::assign(T value)
{
	for(unsigned char i=0;i<counter;i++){
		sinks[i]->assign(value);
	}
}

template <typename T, const unsigned char maxElems>
void Distributor1<T,maxElems>::registerSink(Sink1<T>* sink)
{
	InterruptLock lock;
   	if(counter < maxElems){
   		sinks[counter] = sink;
   		counter++;
   	}
}


template <typename T, const unsigned char maxElems>
void Distributor1<T,maxElems>::removeSink(Sink1<T>* sink)
{
	InterruptLock lock;
	for(int i=0; i<counter; i++) {
		if(sinks[i] == sink) {
			sinks[i] = 0;
			for(int j=i; j<counter; j++) {
				sinks[j] = sinks[j+1];
			}
			counter--;
			break;
		}
	}
}

} //namespace reflex

#endif

