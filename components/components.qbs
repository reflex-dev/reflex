import qbs 1.0
import ReflexPackage

/*
Build file for the REFLEX core.
*/
ReflexPackage {
    name: "components"

    Depends { name : "core" }
    Depends { name : "virtualtimer" }

    files: [
        "sources/reflex/misc/Prescaler.cc"
    ]

    Group {
        files: "sources/reflex/misc/*.h"
        qbs.install: true
        qbs.installDir: "include/reflex/misc"
    }
}
