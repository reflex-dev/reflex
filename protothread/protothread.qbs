import qbs 1.0
import ReflexPackage

ReflexPackage {
    name: "protothread"

    Depends { name : "core" }
    Depends { name : "platform" }

    files : "sources/reflex/scheduling/dummy.cc"

    Group {
        files: "sources/reflex/scheduling/*.h"
        qbs.install: true
        qbs.installDir: "include/reflex/scheduling"
    }
}
