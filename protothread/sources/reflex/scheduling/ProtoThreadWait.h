/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(es):	EDFActivity
 *
 *	Author:		Karsten Walther
 *
 *	Description:	An EDF-schedulable passive objects
 *
 *    This file is part of REFLEX.
 *
 *    REFLEX is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as 
 *    published by the Free Software Foundation, either version 3 of the 
 *    License, or (at your option) any later version.
 *
 *    REFLEX is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with REFLEX.  If not, see <http://www.gnu.org/licenses/>.
 * */

#ifndef ProtoThreadWait_h
#define ProtoThreadWait_h

namespace reflex {

/**
 * Block and wait until condition is true.
 *
 * This macro blocks the protothread until the specified condition is
 * true.
 *
 * \param condition The condition.
 *
 */
#define PT_WAIT_UNTIL_SCHEDULER(checkObject, object, method)    \
	do { \
		pt_wait_cond.activity=this; \
		pt_wait_cond.checkActivity=checkObject; \
		ProtothreadFunctor<object, method> pt_wait_functor(*this); \
		memcpy(pt_wait_memory, &pt_wait_functor, sizeof(pt_wait_functor)); \
		pt_wait_cond.functor=(ProtothreadFunctor<object, method>*) pt_wait_memory; \
		\
		LC_SET(lc); \
		if(!(pt_wait_cond.functor->get())) { \
			pt_condition(&pt_wait_cond); \
			setStatus(PT_WAITING); \
			return; \
		} \
	} while(0)

/**
 * Block and wait while condition is true.
 *
 * This function blocks and waits while condition is true. See
 * PT_WAIT_UNTIL().
 *
 * \param cond The condition.
 *
 */
//#define PT_WAIT_WHILE(cond) PT_WAIT_UNTIL(!(cond))

class ProtoThreadWait {

	public:
   
		ProtoThreadWait() {
		};

	protected:

		ProtothreadCondition pt_wait_cond;
		int pt_wait_memory[2];

};

} //namespace reflex

#endif /* ProtoThreadWait_h */

