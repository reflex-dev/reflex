/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(es):	EDFActivity
 *
 *	Author:		Karsten Walther
 *
 *	Description:	An EDF-schedulable passive objects
 *
 *    This file is part of REFLEX.
 *
 *    REFLEX is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as 
 *    published by the Free Software Foundation, either version 3 of the 
 *    License, or (at your option) any later version.
 *
 *    REFLEX is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with REFLEX.  If not, see <http://www.gnu.org/licenses/>.
 * */

#ifndef ProtoThreadTimer_h
#define ProtoThreadTimer_h

#include "reflex/sinks/Event.h"
#include "reflex/scheduling/ActivityFunctor.h"
#include "reflex/timer/VirtualTimer.h"

namespace reflex {

/**
 * Yield from the current protothread.
 *
 * This function will yield the protothread, thereby allowing other
 * processing to take place in the system.
 *
 */
#define PT_YIELD_TIMER(time, functor) \
	do { \
		pt_timer_event.init(&functor); \
		pt_timer.set(time); \
		PT_YIELD; \
	} while(0)

#define PT_WAIT_UNTIL_TIMER(condition, time, instruction) \
	{ \
		startTime = pt_timer.getSystemTime(); \
		LC_SET(lc); \
		if(!(condition)) { \
			if((pt_timer.getSystemTime() - startTime) >= time) instruction; \
			else { \
				setStatus(PT_WAITING); \
				return; \
			} \
		} \
	}

#define PT_WAIT_UNTIL_TIMER_SCHEDULE(condition, time, instruction) \
	{ \
		startTime = pt_timer.getSystemTime(); \
		LC_SET(lc); \
		if(!(condition)) { \
			if((pt_timer.getSystemTime() - startTime) >= time) instruction; \
			else { \
				if(!isSpawned()) trigger(); \
				setStatus(PT_WAITING); \
				return; \
			} \
		} \
	}


class ProtoThreadTimer {

    public:
   
		ProtoThreadTimer() : pt_timer(VirtualTimer::ONESHOT)
		{
			pt_timer.connect_output( &pt_timer_event );
		};

	protected:

		Time startTime;
		VirtualTimer pt_timer;
		Event pt_timer_event;

};

} //namespace reflex

#endif /* ProtoThreadTimer_h */

