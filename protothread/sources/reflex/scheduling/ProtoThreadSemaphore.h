/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *	Class(es):	none
 *
 *	Author:		Denny Berg
 *
 *	Description:	Provides typedefinition for the MetaType Activity
 *
 *    This file is part of REFLEX.
 *
 *    REFLEX is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as 
 *    published by the Free Software Foundation, either version 3 of the 
 *    License, or (at your option) any later version.
 *
 *    REFLEX is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with REFLEX.  If not, see <http://www.gnu.org/licenses/>.
 * */

/**
 * Counting semaphores implemented on protothreads
 *
 */

#ifndef ProtoThreadSemaphore_h
#define ProtoThreadSemaphore_h

#include "ProtoThread.h"

/**
 * Wait for a semaphore
 *
 * This macro carries out the "wait" operation on the semaphore. The
 * wait operation causes the protothread to block while the counter is
 * zero. When the counter reaches a value larger than zero, the
 * protothread will continue.
 *
 * \param s ProtoThreadSemaphore object
 *
 */
#define PT_SEM_WAIT(s)	\
  do { \
    PT_WAIT_UNTIL(s->getCounter() > 0); \
    s->decCounter(); \
  } while(0)

/**
 * Signal a semaphore
 *
 * This macro carries out the "signal" operation on the semaphore. The
 * signal operation increments the counter inside the semaphore, which
 * eventually will cause waiting protothreads to continue executing.
 *
 * \param s ProtoThreadSemaphore object
 *
 */
#define PT_SEM_SIGNAL(s) s->incCounter();

class ProtoThreadSemaphore {
    
    public:

	ProtoThreadSemaphore() {
	    count=0;
	};

	ProtoThreadSemaphore(unsigned int count) {
	    this->count=count;
	};

	int getCounter() {
	    return count;
	};

	void setCounter(unsigned int count) {
	    this->count=count;
	};

	void incCounter() {
	    ++count;
	};

	void decCounter() {
	    --count;
	};

    private:

	unsigned int count;

};

#endif /* ProtoThreadSemaphore_h */

