#ifndef REFLEX_SINK_H
#define REFLEX_SINK_H
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *	Class(es):	Sink0 - Sink3
 *
 *	Author:		Karsten Walther
 *
 *	Description:	A Callback implementation, for allowing driver
 *			object which can handle multiple interrupts.
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and
 *    Operating Systems. All rights reserved.
 *
 *    Redistribution and use in source and binary forms, with or without
 *    modification, are permitted provided that the following conditions
 *    are met:
 *
 *       1. Redistributions of source code must retain the above copyright
 *          notice, this list of conditions and the following disclaimer.
 *
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the
 *          distribution.
 *
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *    The views and conclusions contained in the software and documentation
 *    are those of the authors and should not be interpreted as representing
 *    official policies, either expressed or implied, of BTU Cottbus,
 *    Department for Distributed Systems and Operating Systems.
 *
 **/

#include "reflex/data_types/Static.h"

namespace reflex {

/**
 *  This is the interface for a dataless event channel.
 *
 *  @author Karsten Walther
 */
class Sink0 {
    class NoOp;
public:
    virtual void notify()=0;

};
/** no operation instance of a Sink0. Does nothing but callable.
 */
class Sink0::NoOp : public Sink0
{
    virtual void notify() {};
};

/**
 *  This is the generic interface for an event channel for a 1-tuple.
 *
 *  @author Karsten Walther
 */
template<typename T1>
class Sink1 {
public:
    virtual void assign(T1 v1)=0;
};

/**
 *  This is the generic interface for an event channel for a 2-tuple.
 *
 *  @author Karsten Walther
 */
template<typename T1, typename T2>
class Sink2 {
public:
    virtual void assign(T1 v1, T2 v2)=0;
};

/**
 *  This is the generic interface for an event channel for a 3-tuple.
 *
 *  @author Karsten Walther
 */
template<typename T1, typename T2, typename T3>
class Sink3 {
public:
    virtual void assign(T1 v1, T2 v2, T3 v3)=0;
};

} //namespace reflex

#endif
