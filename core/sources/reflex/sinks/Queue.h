#ifndef Queue_h
#define Queue_h

#include "reflex/scheduling/Activity.h"
#include "reflex/sinks/Trigger.h"
#include "reflex/sinks/Sink.h"

#include "reflex/interrupts/InterruptLock.h"

/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *	Class(es):	Queue
 *
 *	Author:		Karsten Walther
 *
 *	Description:	This is an active input, which schedules related
 *					activity in case of assignment. It can store elements
 *					which have a public next member.
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and
 *    Operating Systems. All rights reserved.
 *
 *    Redistribution and use in source and binary forms, with or without
 *    modification, are permitted provided that the following conditions
 *    are met:
 *
 *       1. Redistributions of source code must retain the above copyright
 *          notice, this list of conditions and the following disclaimer.
 *
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the
 *          distribution.
 *
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *    The views and conclusions contained in the software and documentation
 *    are those of the authors and should not be interpreted as representing
 *    official policies, either expressed or implied, of BTU Cottbus,
 *    Department for Distributed Systems and Operating Systems.
 *
 * */
#include "reflex/data_types/LinkedList.h"
#include "reflex/debug/StaticAssert.h"

namespace reflex {

/**
 *  A Queue is a buffer for chainable elements. Therefore it can store
 *	an infinit amount of elements. Note, that T must be a pointer to a type
 *	which has a public next member.
 */
template<class T>
class Queue
{
    STATIC_ASSERT(true, Queue_can_only_be_used_with_pointers_to_ChainLink_objects);
};

template<class T>
class Queue<T*>
    : public Trigger
    , public Sink1<T*>
    , public data_types::LinkedList<T*>
{
public:
    /** Initializes queue structure and Trigger base.
     *
     *  @param act pointer to the related activity
     */
    Queue() {}

    /**
     *	This method puts an element into the queue, it will not be checked,
     *	if the parameter is valid or if it is already in another queue.
     *
     *	@param pointer to a object which has a pointer to an element
     *  of the same type called next
     */
    virtual void assign(T* item);

    /** returns the next element from the queue or 0
     */
    T* get();
};


}// end namespace reflex

template <typename T>
void reflex::Queue<T*>::assign(T* item)
{
    reflex::InterruptLock lock;
    this->append(item);

    trigger();
}

template <typename T>
T* reflex::Queue<T*>::get()
{
    reflex::InterruptLock lock;
    return this->takeFirst();
}
#endif
