#ifndef Fifo_h
#define Fifo_h

/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *	Class(es):	Fifo1, Fifo2
 *
 *	Author:		Karsten Walther
 *
 *	Description:	Bounded Event Flow Buffer
 *
 *	(c) Karsten Walther, BTU Cottbus 2005
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and
 *    Operating Systems. All rights reserved.
 *
 *    Redistribution and use in source and binary forms, with or without
 *    modification, are permitted provided that the following conditions
 *    are met:
 *
 *       1. Redistributions of source code must retain the above copyright
 *          notice, this list of conditions and the following disclaimer.
 *
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the
 *          distribution.
 *
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *    The views and conclusions contained in the software and documentation
 *    are those of the authors and should not be interpreted as representing
 *    official policies, either expressed or implied, of BTU Cottbus,
 *    Department for Distributed Systems and Operating Systems.
 *
 * */

#include "reflex/interrupts/InterruptLock.h"
#include "reflex/scheduling/Activity.h"
#include "reflex/sinks/Trigger.h"
#include "reflex/sinks/Sink.h"
#include "reflex/types.h"

namespace reflex {

/** Bounded Event Flow Buffer
 *
 *  @author Karsten Walther

 */
template<typename T1, uint8 n>
class Fifo1 : public Trigger, public Sink1<T1>
{
public:
	Fifo1()
	{
        readPos = 0;
        count = 0;
	}

    Fifo1(Activity* act)
    {
        readPos = 0;
        count = 0;
        Trigger::init(act);
    }

    /** implements Sink1
     */
    virtual void assign(T1 value);

    /** retrieve value */
    T1 get();

    //! Returns true if no elements are stored
    bool isEmpty() const;

private:
    /** Bufferspace
     */
    T1 vs1[n];

    unsigned char readPos;

    /** elements currently in buffer
     */
    uint8 count;
};

/** Bounded Event Flow Buffer for tuples
 *
 *  @author Karsten Walther

 */
template<typename T1, typename T2, uint8 n>
class Fifo2 : public Trigger, public Sink2<T1,T2>
{
public:
	Fifo2()
	{
        readPos = 0;
        count = 0;
	}

    Fifo2(Activity* act)
    {
        readPos = 0;
        count = 0;
        Trigger::init(act);
    }

    /** implements Sink2
     */
    virtual void assign(T1 v1, T2 v2);

    /** retrieve values */
    void get(T1& r1, T2& r2);

    //! Returns true if no elements are stored
    bool isEmpty() const;

private:
    /** Bufferspace
     */
    T1 vs1[n];
    T2 vs2[n];

    unsigned char readPos;

    /** Elements currently in buffer
     */
    uint8 count;
};

template <typename T1, uint8 n>
void Fifo1<T1,n>::assign(T1 value)
{
    InterruptLock lock;
    if(count<n){

        vs1[(readPos + count) % n] = value;
        count++;

        trigger();
    }
}

template <typename T1, uint8 n>
T1 Fifo1<T1,n>::get()
{
    InterruptLock lock;
    T1 retVal = vs1[readPos];
    if(count){
        count--;
        readPos = (readPos+1) % n;
    }
    return retVal;
}

/*!
 \brief Returns true if no elements are stored.
 */
template <typename T1, uint8 n>
bool Fifo1<T1,n>::isEmpty() const
{
    return count == 0;
}

template <typename T1, typename T2, uint8 n>
void Fifo2<T1,T2,n>::assign(T1 v1, T2 v2)
{
    InterruptLock lock;
    if(count<n){
        uint8 pos = (readPos + count) % n;
        vs1[pos] = v1;
        vs2[pos] = v2;
        count++;
        trigger();
    }
}

template <typename T1, typename T2, uint8 n>
void Fifo2<T1,T2,n>::get(T1& r1, T2& r2)
{
    InterruptLock lock;

    if(count){
        r1 = vs1[readPos];
        r2 = vs2[readPos];
        count--;
        readPos = (readPos + 1) % n;
    }
}

/*!
 \brief Returns true if no elements are stored.
 */
template <typename T1, typename T2, uint8 n>
bool Fifo2<T1,T2,n>::isEmpty() const
{
    return count == 0;
}
}// end namespace reflex
#endif
