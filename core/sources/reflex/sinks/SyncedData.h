#ifndef SyncedData_h
#define SyncedData_h
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *	Class(es):	SyncedData
 *
 *	Author:		Karsten Walther
 *
 *	Description:	This is a generic data container, which notifies a
 * 					subscriber, if value has changed.
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */

#include "reflex/interrupts/InterruptLock.h"
#include "reflex/sinks/Sink.h"

namespace reflex {

/** This class implements a syncronized data type, which can be used over
 *  component borders. It is useful in situation, where a producer provides
 *  data, which are of interest for many subscribers, which only want to be
 *  informed if new data are available. Further it can be used to implement
 *  an OR-semantic of different inputs of an component, where something has
 *  to be done no matter, which input value changed.
 *	When the datatype is read it provides the current value. Note that no
 *  buffers, which must be freed, should be used with this class.
 */
template<typename T>
class SyncedData : public Sink1<T>{
public:
	SyncedData()
	{
		subscriber = 0;
	}

	void bind(Sink0* subscriber)
	{
		this->subscriber = subscriber;
	}

	virtual void assign (T value);

	void get(T& target) const;

private:
	/** the data element
	 */
	T data;

	Sink0* subscriber;
};

template <typename T>
void SyncedData<T>::assign(T value)
{
	InterruptLock lock;
	data = value;
	if(subscriber){
		subscriber->notify();
	}
}

template <typename T>
void SyncedData<T>::get(T& target) const
{
	InterruptLock lock;
	target = data;
}

} //namespace reflex

#endif
