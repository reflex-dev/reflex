#ifndef InterruptGuardian_h
#define InterruptGuardian_h
/*
 *    REFLEX - Real-time Event FLow EXecutive
 *
 *    A lightweight operating system for deeply embedded systems.
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2006-2014 BTU Cottbus, Department for Distributed Systems and
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */
#include <reflex/MachineDefinitions.h>
#include <reflex/types.h>

// Entry function for assembler.
extern "C" void handle(uint8 index);

namespace reflex {

class InterruptHandler;

/*!
\brief Provides the infrastructure for handling interrupt tables.

InterruptGuardian provides a default implementation for platforms that use
interrupt vector tables to identify interrupts. An application can register
InterruptHandler objects via the registerInterruptHandler() method to one or
more interrupt vectors.

The low-level interrupt handling is often implemented in assembler using
the ::handle() function as an entry point to high-level code.

For the usage of InterruptGuardian have a look into posix::CoreApplication
or atmega::CoreApplication. Even though most platforms use the default
implementation, it is possible to replace it entirely by a custom one.

\sa InterruptHandler
 */
class InterruptGuardian
{
    friend class InterruptHandler;
    friend void ::handle(uint8);

public:
    InterruptGuardian(InterruptHandler** table, uint8 size);

protected:
    void handle(uint8 index);
    void registerInterruptHandler(InterruptHandler* handler,
            mcu::InterruptVector index);
    void unregisterInterruptHandler(InterruptHandler* handler,
            mcu::InterruptVector index);

private:
    InterruptHandler** _handlers;
    uint8 _size;
};

} //namespace reflex

#endif
