/*
 *    REFLEX - Real-time Event FLow EXecutive
 *
 *    A lightweight operating system for deeply embedded systems.
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2006-2014 BTU Cottbus, Department for Distributed Systems and
 *    Operating Systems. All rights reserved.
 *
 *    Redistribution and use in source and binary forms, with or without
 *    modification, are permitted provided that the following conditions
 *    are met:
 *
 *       1. Redistributions of source code must retain the above copyright
 *          notice, this list of conditions and the following disclaimer.
 *
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the
 *          distribution.
 *
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *    The views and conclusions contained in the software and documentation
 *    are those of the authors and should not be interpreted as representing
 *    official policies, either expressed or implied, of BTU Cottbus,
 *    Department for Distributed Systems and Operating Systems.
 *
 * */

#include "reflex/interrupts/InterruptGuardian.h"
#include "reflex/CoreApplication.h"
#include "reflex/interrupts/InterruptHandler.h"

using namespace reflex;

/*!
\brief Creates an interrupt guardian object and initializes the handler table.

The handler table is considered to be an array of pointers to InterruptHandler
objects. Its beginning is marked with \a table and the length is considered to
be \a size.

All entries in the handler table are initially set to
InterruptHandler::invalidInterruptHandler.

*/
InterruptGuardian::InterruptGuardian(InterruptHandler** table, uint8 size)
{
    _handlers = table;
    _size = size;
    for (uint8 i = 0; i < _size; i++)
    {
        _handlers[i] = &InterruptHandler::invalidInterruptHandler;
    }
}

/*!
\brief Adds an InterruptHandler object to the handler table.

This method registers the interrupt handler \a handler at position \a index
in the handler table. When the entry at \a index is already occupied by
another handler, this method will fail.
*/
void InterruptGuardian::registerInterruptHandler(InterruptHandler* handler,
        mcu::InterruptVector index)
{
    Assert(index < _size);
    Assert(_handlers[index] == InterruptHandler::invalidInterruptHandler);
    _handlers[index] = handler;
}

/*!
\brief Removes a registered \a handler from the interrupt vector table.

This method removes \a handler from \a index in the handler table. On success
the entry points to InterruptHandler::invalidInterruptHandler. This method
will fail when trying to remove a \a handler that has not been registered
to \a index.
*/
void InterruptGuardian::unregisterInterruptHandler(InterruptHandler* handler,
        mcu::InterruptVector index)
{
    Assert(vector < _size)
    Assert(handlers[index] == handler)
    _handlers[index] = &InterruptHandler::invalidInterruptHandler;
}

/*!
\brief Calls the specific interrupt handler.

This method looks for a registered interrupt handler in the lookup table and
dispatches it. When using a preemptive scheduling scheme, it is also
responsible to notify the scheduler about interrupts.
 */
void InterruptGuardian::handle(uint8 index)
{
#if defined PRIORITY_SCHEDULING \
        || defined PRIORITY_SCHEDULING_SIMPLE \
        || defined EDF_SCHEDULING \
        || defined EDF_SCHEDULING_SIMPLE
    mcu::CoreApplication::instance()->scheduler()->enterScheduling();
#endif

    _handlers[index]->handle();

#if defined PRIORITY_SCHEDULING \
        || defined PRIORITY_SCHEDULING_SIMPLE \
        || defined EDF_SCHEDULING \
        || defined EDF_SCHEDULING_SIMPLE
    mcu::CoreApplication::instance()->scheduler()->leaveScheduling();
#endif
}

/*!
\brief Forwards the interrupt handler call from assembler to C++

This function serves as an entry point from low-level assembler to high-level
code. It forwards an interrupt request of interrupt \a index via a global
InterruptGuardian object to an appropriate InterruptHandler object.

\relates reflex::InterruptGuardian
*/
extern "C" void handle(uint8 index)
{
    mcu::CoreApplication::instance()->guardian()->handle(index);
}
