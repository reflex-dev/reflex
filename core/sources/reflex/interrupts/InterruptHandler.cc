/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
  
 *	Author:		 Karsten Walther
 *
 *	(c) Karsten Walther, BTU Cottbus 2005
 */

#include "reflex/interrupts/InterruptHandler.h"
#include "reflex/CoreApplication.h"

using namespace reflex;


InterruptHandler::InvalidInterruptHandler InterruptHandler::invalidInterruptHandler;

InterruptHandler::InterruptHandler(mcu::InterruptVector vector, const reflex::PowerManageAble::Priority priority)
	//: PowerManageAble(priority) //see below COMMENT #1
	: PowerManageAble(PowerManageAble::Priority(static_cast<bool>(priority)) )
{
	mcu::CoreApplication::instance()->guardian()->registerInterruptHandler(this, vector);
}

void InterruptHandler::registerTo(mcu::InterruptVector index)
{
    mcu::CoreApplication::instance()->guardian()->registerInterruptHandler(
                this, index);
}


void InterruptHandler::unregisterFrom(mcu::InterruptVector index)
{
    mcu::CoreApplication::instance()->guardian()->unregisterInterruptHandler(
                this, index);
}

//FIXME:
//COMMENT #1
/* Due to an internal compiler error in the m68hc12-elf-g++ we have to walk a longer
   way. That unnecessary bool conversion prevent the usage of that buggy part of the compiler.
   MHH that port of the gcc will not longer be supported
   may that bug  will be fixed when switching to m68k-elf target.
compiler msg:
./system/src/reflex/interrupts/InterruptHandler.cc:23: error: insn does not satisfy its constraints:
(insn:HI 16 15 18 2 (set (mem/s:HI (mem/c:HI (plus:HI (reg/f:HI 3 sp)
(const_int 2 [0x2])) [37 this+0 S2 A8]) [20 <variable>.D.1374._vptr.PowerManageAble+0 S2 A8])
(const:HI (plus:HI (symbol_ref/i:HI ("_ZTVN6reflex16InterruptHandlerE") <var_decl 0x401cf630 _ZTVN6reflex16InterruptHandlerE>)
(const_int 4 [0x4])))) 25 {*movhi_68hc12} (nil)
(nil))
../../../../../system/src/reflex/interrupts/InterruptHandler.cc:23: internal compiler error: in reload_cse_simplify_operands, at postreload.c:392
Please submit a full bug report,
with preprocessed source if appropriate.

*/
