#ifndef InterruptFunctor_h
#define InterruptFunctor_h
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *	Class(es):	InterruptFunctor
 *
 *	Author:		Karsten Walther
 *
 *	Description:	A Callback implementation, for allowing driver
 *			object which can handle multiple interrupts.
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */

#include "reflex/interrupts/InterruptHandler.h"
#include "reflex/MachineDefinitions.h"
namespace reflex {

/** This class represents an interrupt handler, which calls a statically
 *  determined method on a given driver object. This allows a driver to handle
 *  multiple interrupts with different handle methods.
 */
template <typename T,void (T::*MF)(), void (T::*MF2)(), void (T::*MF3)()>
class InterruptFunctor : public InterruptHandler {
public:
	/**
	 *  The constructor is responsible for registration of handler and
	 *  initialization of reference to driver object.
	 *
	 *  @param vector Is the physical interruptvector
	 *  @param obj Is the corresponding driver object
	 */
	InterruptFunctor(const mcu::InterruptVector vector, T& obj, const PowerManageAble::Priority priority) :
			InterruptHandler(vector, priority),
			obj(obj)
	{}

	/**
	 *  This is the method called by the guardian, itself it calls
	 *  the specified method on the given object.
	 */
	virtual void handle()
	{
		(obj.*MF)();
	}

	virtual void enable()
	{
		(obj.*MF2)();
	}

	virtual void disable()
	{
		(obj.*MF3)();
	}


private:
	T& obj;
};

} //namespace reflex

#endif

