/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
  
 *	Author:		 Karsten Walther
 */

#include <reflex/MachineDefinitions.h>
#include "reflex/powerManagement/PowerManager.h"
#include "reflex/powerManagement/PowerManageAble.h"
#include "reflex/interrupts/InterruptLock.h"

extern "C" void _interruptsEnable();

using namespace reflex;

namespace {
    uint8 useCounts[mcu::NrOfPowerModes];
}

PowerManager::PowerManager(PowerDownFunction powerFuntions[])
{
    this->modes = powerFuntions;
    currentMode = NOTHING;
}

void PowerManager::enableObject(PowerManageAble* object)
{
	if( !object->enabled ){
		object->enable();
		object->enabled=true;
		useCounts[object->deepestAllowedSleepMode]++;
	}
}

void PowerManager::disableObject(PowerManageAble* object)
{
	if(object->enabled){
		object->disable();
		object->enabled=false;
		useCounts[object->deepestAllowedSleepMode]--;
	}
}

void PowerManager::switchMode(powerManagement::PowerGroups value, powerManagement::PowerGroups mask)
{
	InterruptLock lock;
	currentMode = (value | (currentMode & ~mask));

	//FIXME: an iteratorconcept for that queue is needed;
	PowerManageAble* it = queue.front();

	while(it) 
	{
		if( !it->isSecondary)
		{
			if(it->groups & currentMode)
			{
				enableObject(it);
			}else
			{
				disableObject(it);
			}
		}
		it = static_cast<PowerManageAble*>(it->next);
	}
}

void reflex::PowerManager::powerDown() const
{
	int i = 0;
	while(!useCounts[i] && i<(mcu::NrOfPowerModes-1)) //look for deepest possible powerdown method
	{
		i++;
	}
	(*modes[i])();	//call powerdown method  interrupts will be enabled inside this method
}

void _active() {
	_interruptsEnable();
	return;
}
