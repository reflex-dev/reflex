#include "reflex/powerManagement/PowerManageAble.h"
#include "reflex/powerManagement/PowerManager.h"
#include "reflex/CoreApplication.h"

using namespace reflex;

PowerManageAble::PowerManageAble()
	: enabled(0),isSecondary(true)
	,deepestAllowedSleepMode(mcu::ACTIVE) //set the higest available sleepmode by default
	,groups(powerManagement::NOTHING)
{
}

PowerManageAble::PowerManageAble(const PowerManageAble::Priority isSecondary)
	: enabled(0),isSecondary(isSecondary)
	,deepestAllowedSleepMode(mcu::ACTIVE) //set the higest available sleepmode by default
	,groups(powerManagement::NOTHING)
{
    mcu::CoreApplication::instance()->powerManager()->registerObject(this);
}

void PowerManageAble::switchOn()
{
	mcu::CoreApplication::instance()->powerManager()->enableObject(this);
}

void PowerManageAble::switchOff()
{
	mcu::CoreApplication::instance()->powerManager()->disableObject(this);
}
