#ifndef HardwareTimer_h
#define HardwareTimer_h
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(ses):	HardwareTimer
 *
 *	Author:		Stefan Nuernberger
 *
 *	Description:	Definition of a hardware timer that can be virtualized.
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */

#include "reflex/types.h"
#include "reflex/sinks/Sink.h"

namespace reflex {

/** HardwareTimer abstraction
 */
class HardwareTimer {
public:
	/** constructor */
	HardwareTimer() {}

	/**
	 * init
	 * connect subsequent output
	 */
	void init(Sink0 *output) {
		this->output = output;
	}

	/** getNow
     * @return current counter value in Time width
	 */
	virtual Time getNow() = 0;
	
    /** getTimestamp
     * @return 64 bit counter value in hardware precision
     */
    virtual uint64 getTimestamp() = 0;

	/** start
	 * set a timeout until next interrupt and start the timer
	 * @param ticks number of ticks until timer event
	 */
	virtual void start(Time /*ticks*/) = 0;

	/**
	 * startAt
	 * set a new time interrupt to occur ticks after t0
	 * @param t0 time from where to set the timer
	 * @param ticks number of ticks after t0 when the timer should fire
	 */
	virtual void startAt(Time /*t0*/, Time /*ticks*/) = 0;

	/** stop
	 * stop the hardware timer
	 */
	virtual void stop() = 0;

protected:
	Sink0 *output;
};

/**
 * HardwareTimerDummy
 * Dummy class for unimplemented HardwareTimers
 */
class HardwareTimerDummy : public HardwareTimer {
public:
	HardwareTimerDummy(){}
	Time getNow(){ return 0; }
    uint64 getTimestamp() { return 0; }
    void start(Time ticks) { (void)ticks; }
	void startAt(Time t0, Time ticks) { (void) t0; (void)ticks; }
	void stop() {}
};

} //namespace reflex

#endif
