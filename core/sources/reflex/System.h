#ifndef SYSTEM_H
#define SYSTEM_H
/*
 *    This file is part of REFLEX.
 *
 *    Copyright 2014 BTU Cottbus, Department for Distributed Systems and
 *    Operating Systems. All rights reserved.
 *
 *    Redistribution and use in source and binary forms, with or without
 *    modification, are permitted provided that the following conditions
 *    are met:
 *
 *       1. Redistributions of source code must retain the above copyright
 *          notice, this list of conditions and the following disclaimer.
 *
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the
 *          distribution.
 *
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *    The views and conclusions contained in the software and documentation
 *    are those of the authors and should not be interpreted as representing
 *    official policies, either expressed or implied, of BTU Cottbus,
 *    Department for Distributed Systems and Operating Systems.
 *
 * */
#include <reflex/CoreApplication.h>

namespace reflex {

/*!
\brief Alias for mcu::CoreApplication

This class is a legacy class to keep old code compatible with the new
mcu::CoreApplication interface class. It is still possible to use
System as a base class for applications and to retrieve the singleton
object using getSystem().

This class is deprecated and its use is discouraged.

\deprecated

*/
class System : public mcu::CoreApplication
{
public:
    System();

    InterruptGuardian& guardian;
    PowerManager& powerManager;
    Scheduler& scheduler;
};

inline System::System() :
    guardian(*mcu::CoreApplication::guardian()),
    powerManager(*mcu::CoreApplication::powerManager()),
    scheduler(*mcu::CoreApplication::scheduler())
{

}

/*!
\brief Get the current application object.

This function is deprecated. Use mcu::CoreApplication::instance() instead.

\sa atmega::CoreApplication, posix::CoreApplication
\relates System
\deprecated
*/
inline System& getSystem()
{
    return *static_cast<System*>(mcu::CoreApplication::instance());
}


/*!
\class reflex::mcu::CoreApplication
\brief Platform-dependent Core Application

Dependending for which platform REFLEX was compiled, this class maps to

- atmega::CoreApplication
- msp430x::CoreApplication
- omnetpp::CoreApplication
- posix::CoreApplication

*/

} //namespace reflex

#endif
