#ifndef TimeTriggeredActivity_h
#define TimeTriggeredActivity_h
/*
 *    This file is part of REFLEX.
 *
 *    Copyright 2014 BTU Cottbus, Department for Distributed Systems and
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */
#include "reflex/types.h"
#include "reflex/data_types/ChainLink.h"

namespace reflex {

/*!
\brief Activity class for time triggered scheduling.
 */
class TimeTriggeredActivity : public data_types::ChainLink {

    friend class TimeTriggeredScheduler;

public:
	TimeTriggeredActivity();
    ~TimeTriggeredActivity();

	/** The run method is called by the scheduler and implements the
	 *  functionality of the activity.
	 */
	virtual void run()=0;

    void trigger();
    bool islocked() const;
    void lock();
    void unlock();

private:
	bool locked;
    uint8 rescheduleCount;
};

/*!
\brief Brings this activity into scheduling.

Triggering an activity does not necessarily mean, that it is executed.
It has to be registered with the scheduler by calling
TimeTriggeredScheduler::registerActivity() first.
 */
inline void TimeTriggeredActivity::trigger()
{
    rescheduleCount++;
}

/*!
\brief Returns true if the activity is locked.

 */
inline bool TimeTriggeredActivity::islocked() const
{
    return locked;
}

/*!
\brief Prevents the activity from being executed.
 */
inline void TimeTriggeredActivity::lock()
{
    locked=true;
}

/*!
\brief Marks a locked activity executable again.

This method does not have any effect on locked activities.
*/
inline 	void TimeTriggeredActivity::unlock()
{
    locked = false;
}

} //namespace reflex

#endif
