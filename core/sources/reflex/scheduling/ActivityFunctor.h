#ifndef ActivityFunctor_h
#define ActivityFunctor_h
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2014 BTU Cottbus, Department for Distributed Systems and
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */
#include <reflex/scheduling/Activity.h>

namespace reflex {

/*!
Wraps a component member method of \a T to become an activity.

Activities are methods inside components which operate on component
members. One component may define many activities. From the scheduler's
viewpoint, an activity is an object of the class reflex::Activity which simply
defines a ``run()``-method. ActivityFunctor now acts as a proxy between these
two: It takes a pointer to a component and to one of it's member methods and
and forwards the scheduler request to this particular method.

The following code snippet shows the usage of ActivityFunctor:

\rst
.. code-block:: cpp

    #include <reflex/scheduling/ActivityFunctor.h>

    // A component with one activity
    class MyComponent
    {

        MyComponent();

        // ... event-channels and nested components

    private:
        // The component's activity method. It must be defined before
        // act_doSomething.
        void doSomething();

        // The activity functor
        reflex::ActivityFunctor<MyComponent, &MyComponent::doSomething>
                act_doSomething;

    };

    // The constructor initializes the activity functor.
    MyComponent::MyComponent() :
        act_doSomething(*this)
    {
        // ... further initialization
    }

\endrst

MyComponent may define many activity functors and of course event channel
in- and outputs. The resulting call-chain behind the scene is shown in the
following figure.

\verbatim

 ┌─────────┐       ┌──────────────────────────┐               ┌───────────┐
 │Scheduler│       │ActivityFunctor           │               │MyComponent│
 └────┬────┘       │<MyComponent,             │               └─────┬─────┘
      │            │&MyComponent::doSomething>│                     │
      │            └────────────┬─────────────┘                     │
      │                         │     act_doSomething(*this)        │
      │                         │<──────────────────────────────────│
      │                         │─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ >│
      =                         =                                   =
      │          run()          │                                   │
      │────────────────────────>│           doSomething()           │
      │                         │──────────────────────────────────>│
      │                         │                                   =
      │                         │< ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─│
      │< ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─│                                   │
      │                         │                                   │
\endverbatim

 */
template <typename T, void (T::* MemFn)()>
class ActivityFunctor : public Activity {
public:
    /*!
    Creates and initializes the ActivityFunctor object.

    The parameter \a pointerToComponent points to the wrapped component
    object.
    */
    ActivityFunctor(T& pointerToComponent) :
            pObj(pointerToComponent)
    {
    }

    /*!
    In run method the specific memberfunction of the related object
    is called.
    */
    virtual void run()
    {
        (pObj.*MemFn)();
    }

private:
    // Reference to the related object.
    T& pObj;
};

} //namespace reflex

#endif

