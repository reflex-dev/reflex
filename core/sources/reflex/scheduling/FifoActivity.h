#ifndef FifoActivity_h
#define FifoActivity_h
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(ses):	Activity
 *
 *	Author:		Karsten Walther
 *
 *	Description:	Abstract description of passive objects
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */
#include "reflex/scheduling/FifoScheduler.h"
#include "reflex/data_types/ChainLink.h"
namespace reflex {

/** Simplest form of an passive object
 */
class FifoActivity
	: public data_types::ChainLink
{
public:
	FifoActivity();

	/** The run method is called by the scheduler and implements the
	 *  functionality of the activity.
	 */
	virtual void run()=0;

	/** This method brings this activity into scheduling
	 */
	void trigger();

	/** This prevents activity to be activated, scheduling is still possible
	 *  If the activity is already scheduled but not activated, the effect
	 *  is delayed until activity is completed the next time.
	 */
	void lock();

	/** Notification that the activity can be activated again. If there were
	 *  scheduling request during lock time, the activity is scheduled now.
	 */
	void unlock();
	
	bool islocked() const {return locked;}

	/**
	 * @return	the count this activity was triggered
	*/
	unsigned char triggered() {return rescheduleCount;}

private:
	/** The friend declaration allows direct manipulation of activity members
	 *  only at defined points.
	 */
	friend class FifoScheduler;

	enum Status
	{
		SCHEDULED ,     //ready to run or running
		IDLE            //not used until now or in stand by
	};

	/** this is a workaround for a bug in hcs12 compiler, which
	 * cannot deal with volatile pointers */
	FifoActivity* dummy();

	/** Scheduling status of the activity
	 */
	Status status;

	/** This flag determines if an activity can be putted on the readyList.
	 */
	bool locked;

	/** This count determines how often the activity must be activated
	 *  currently.
	 */
	unsigned char rescheduleCount;
};

} //namespace reflex

#endif


