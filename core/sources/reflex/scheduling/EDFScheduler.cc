/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
  
 *	Author:		 Karsten Walther
 */
#include "reflex/scheduling/EDFScheduler.h"
#include "reflex/interrupts/InterruptLock.h"
#include "reflex/powerManagement/PowerManager.h"
#include "reflex/CoreApplication.h"

using namespace reflex;

EDFScheduler::EDFScheduler()
{
	stackedSchedules = 0;
	enterScheduling();
}

//loops infinitely, brings the system to sleep whenever nothing is to do
void EDFScheduler::start()
{
	leaveScheduling();

	_interruptsEnable();
	for (;;)
	{
		mcu::CoreApplication::instance()->powerManager()->powerDown();
	}
}

// This is the dispatching routine, which empties the readyList.
// It is called with disabled interrupts
void EDFScheduler::dispatch()
{
	//run Activities from head of list until an
	//interrupted one is in front or the list is empty
	EDFActivity* first = (EDFActivity*)(readyList.first());

	while( (first !=0 ) && (first->status != EDFActivity::INTERRUPTED) ){

		first->status = EDFActivity::RUNNING;	// mark the activity as running

		_interruptsEnable();	    //interrupts are enabled during execution
		first->run();               //invocation of the activity
		_interruptsDisable();       //disable interrupts for running system code

		readyList.takeFirst();          //remove activity from the ready-list,
		first->status = EDFActivity::IDLE; //because invocation is finished

		//reschedule activity if needed
		if((first->rescheduleCount > 0) && (!first->locked)){

			//prepare Activity for rescheduling
			first->rescheduleCount--;
			first->updateDeadline();
			first->status = EDFActivity::SCHEDULED;
			toSchedule.append(first);

			//reserve scheduling monitor, and update readyList
			stackedSchedules++;
			updateSchedule();
			stackedSchedules--;
		}

		//select next element in the readyList
		first = (EDFActivity*)(readyList.first());
	}

	//if there is an interrupted element set it Running again,
	//because this Stack-instance of Scheduler has done its work
	if(first && (first->status == EDFActivity::INTERRUPTED) ){
		first->status = EDFActivity::RUNNING;
	}
}

// This function brings given activity into the scheduling process.
void EDFScheduler::schedule(EDFActivity* act)
{
	//Interrupts must be disabled now, at the end they must be enabled
	//when call was softwareinitiated and must stay disabled when schedule
	//was called by an interrupthandler
	InterruptLock lock;

	//if the activity is already in scheduling process only increment the
	//reschedule counter
	if(	(act->status != EDFActivity::IDLE ) ||
		act->locked) {

		act->rescheduleCount++;

	}else{

		enterScheduling();      //use scheduling monitor
		act->updateDeadline();		//prepare activity
		toSchedule.append(act);  //add activity to monitorlist
		act->status = EDFActivity::SCHEDULED; //set status
		leaveScheduling();     //use scheduling monitor

	}
}

void EDFScheduler::unlock(EDFActivity* act)
{
	InterruptLock lock; //protect this method from interrupts

	act->locked = false; //remove lock

	//if this activity is NOT in scheduling process
	if(	act->status == EDFActivity::IDLE ) {

		//if it must be scheduled
		if(act->rescheduleCount){

			//schedule it as usual
			act->rescheduleCount--;
			enterScheduling();
			act->updateDeadline();
			toSchedule.append(act);
			act->status = EDFActivity::SCHEDULED;
			leaveScheduling();

		}
	}
}

// Sorts pending activities into readyList. It is guaranteed that this method
// does not overlap with itself.
void EDFScheduler::updateSchedule()
{
	while(toSchedule.first()){
		EDFActivity* current = (EDFActivity*)(toSchedule.takeFirst());

		_interruptsEnable();	//enable interrupts because insert can
								//take a long time

		readyList.insert(current);

		_interruptsDisable();	//disable interrupts since access to
								//toSchedule must be atomic
	}
}

// Signals a starting scheduling process.
// Must be executed with disabled Interrupts
void EDFScheduler::enterScheduling()
{
	//signaling for scheduling monitor
	stackedSchedules++;

	//If there is a running Activity mark it INTERRUPTED.
	EDFActivity* first = (EDFActivity*)(readyList.first());
	if( (first!=0) && (first->status == EDFActivity::RUNNING) ){
		first->status = EDFActivity::INTERRUPTED;
	}
}

// If no other schedulings are in process, update of readyList is performed.
// Must be executed with disabled Interrupts
void EDFScheduler::leaveScheduling()
{
	if(stackedSchedules==1){ //no other schedule operation in progress
		updateSchedule();    //this is the function must be executed monitored
		stackedSchedules--;  //release schedule monitor
		dispatch();          //start dispatching
	}else{					 //other schedule operation in progress
		stackedSchedules--;
	}
}

