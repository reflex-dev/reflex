/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
  
 *	Author:		 Karsten Walther
 *
 *	(c) Karsten Walther, BTU Cottbus 2005
 */
#include "reflex/scheduling/Activity.h"

using namespace reflex;

/** This file makes a signature check on the public elements of the choosen
 *	activity type.
 */

class CheckActivity : public Activity {
public:
	virtual void run(){};
};

static void check()
{
	CheckActivity act;
	act.trigger();
	act.run();
	act.lock();
	act.unlock();
}

