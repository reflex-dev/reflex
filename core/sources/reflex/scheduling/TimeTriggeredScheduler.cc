/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *	Author:	Karsten Walther, Stefan Nuernberger
 */
#include "reflex/scheduling/TimeTriggeredScheduler.h"
#include "reflex/interrupts/InterruptLock.h"
#include "reflex/powerManagement/PowerManager.h"
#include "reflex/CoreApplication.h"

using namespace reflex;

TimeTriggeredScheduler::TimeTriggeredScheduler()
{
    // Book keeping of available scheduler frames
    for (unsigned i = 0; i < NR_OF_SCHEDULING_SLOTS; i++)
    {
        availableFrames.append(&frames[i]);
    }
    currentFrame = 0;
}

void TimeTriggeredScheduler::start()
{
    currentFrame = usedFrames.first();

    while(1)
    {
        _interruptsDisable();
        for(;
            (currentFrame != 0) && (currentFrame->startTime <= currentTick);
            currentFrame = static_cast<Frame*>(currentFrame->next))
        {
            TimeTriggeredActivity* act = currentFrame->act;
            if (act->rescheduleCount == 0)
            {
                continue;
            }
            if (act->islocked())
            {
                continue;
            }

            act->rescheduleCount--;
            _interruptsEnable();
            act->run();
            _interruptsDisable();
        }
//        _interruptsEnable();
        mcu::CoreApplication::instance()->powerManager()->powerDown();
    }
}

/*!
\brief Switches to the next time slot.

If the last time slot has been reached, the scheduling cycle is reset to the
beginning. By that time, every activity must have been finished.

This method is synchronously handled.
 */
void TimeTriggeredScheduler::notify()
{
    currentTick++;
    if (currentTick == TICKS_PER_ROUND)
    {
        currentTick = 0;

        // Todo: Check if last activity has been finished
        //       and raise error if not.
        currentFrame = usedFrames.first();
    }
}

/*!
\brief Schedules \a act to be executed at \a startTime.

Call this method for each activity at least once. This is typically done
on startup. Otherwise the activity won't run.

Registering fails, if \act is 0 or no time slot is available. Check
NrOfSchedulingSlots in conf.h in that case.
 */
void TimeTriggeredScheduler::registerActivity(TimeTriggeredActivity* act, Time startTime)
{
    Assert(act != 0);
    Assert(!availableFrames.isEmpty());
    Frame* emptyFrame = availableFrames.takeFirst();
    emptyFrame->act = act;
    emptyFrame->startTime = startTime;
    usedFrames.insert(emptyFrame);
}

/*!
\brief Removes activity from every registered time slot.
 */
void TimeTriggeredScheduler::removeActivity(TimeTriggeredActivity* act)
{
    Assert(act != 0);
    Frame* frame = usedFrames.first();
    while (frame != 0)
    {
        Frame* next = static_cast<Frame*>(frame->next);
        if (frame->act == act)
        {
            usedFrames.remove(frame);
            availableFrames.append(frame);
        }
        frame = next;
    }
}
