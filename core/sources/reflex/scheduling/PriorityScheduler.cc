/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
  
 *	Author:		 Karsten Walther
 */
#include "reflex/scheduling/PriorityScheduler.h"
#include "reflex/interrupts/InterruptLock.h"
#include "reflex/powerManagement/PowerManager.h"
#include "reflex/CoreApplication.h"

using namespace reflex;

void PriorityScheduler::start()
{
	// Dispatch pending activities first
	leaveScheduling();

	_interruptsEnable();
	while(1){
		mcu::CoreApplication::instance()->powerManager()->powerDown();
	}
}

void PriorityScheduler::dispatch()
{
	//run Activities from head of list until an interrupted one
	//is in front or the list is empty
	PriorityActivity* first = readyList.first();
	while( (first !=0 ) && (first->status != PriorityActivity::INTERRUPTED) ){

	    first->status = PriorityActivity::RUNNING;

		//run activity
	    _interruptsEnable();
    	first->run();
	    _interruptsDisable();

	    readyList.takeFirst();

	   	first->status = PriorityActivity::IDLE;

		//rescheduling if needed
	    if((first->rescheduleCount > 0) && (!first->locked)){

    		first->rescheduleCount--;
			first->status = PriorityActivity::SCHEDULED;
			toSchedule.append(first);

			//reserve scheduling monitor, and update readyList
			stackedSchedules++;
			updateSchedule();
			stackedSchedules--;
		}

	    first = readyList.first();
	}

	//if there is an interrupted element set it Running again,
	//because this Stack-instance of PriorityScheduler has done its work
	if(first){
		if(first->status == PriorityActivity::INTERRUPTED)
		first->status = PriorityActivity::RUNNING;
	}
}

void PriorityScheduler::schedule(PriorityActivity* act)
{
	//Interrupts must be disabled now, at the end they must be enabled
	//when call was softwareinitiated and must stay disabled when schedule
	//was called by an interrupthandler
	InterruptLock lock;

	if(	(act->status != PriorityActivity::IDLE) ||
		act->locked) {

		act->rescheduleCount++;
	}else{

		enterScheduling();
		toSchedule.append(act);
		act->status = PriorityActivity::SCHEDULED;
		leaveScheduling();

	}
}

void PriorityScheduler::unlock(PriorityActivity* act)
{
	InterruptLock lock;

	act->locked = false;

	if(	act->status == PriorityActivity::IDLE ) {

		if(act->rescheduleCount){
			act->rescheduleCount--;
			enterScheduling();
			toSchedule.append(act);
			act->status = PriorityActivity::SCHEDULED;
			leaveScheduling();
		}
	}
}

void PriorityScheduler::updateSchedule()
{
    while(!toSchedule.isEmpty()){
		PriorityActivity* current = toSchedule.takeFirst();

	    _interruptsEnable();

	    readyList.insert(current);

    	_interruptsDisable();
    }
}

void PriorityScheduler::enterScheduling()
{
    stackedSchedules++;

    //If there is a running PriorityActivity mark it INTERRUPTED.
    PriorityActivity* first = (PriorityActivity*)(readyList.first());
    if( (first!=0) && (first->status == PriorityActivity::RUNNING) ){
		first->status = PriorityActivity::INTERRUPTED;
    }
}

void PriorityScheduler::leaveScheduling()
{
	if(stackedSchedules==1){
		updateSchedule();
		stackedSchedules--;
		dispatch();
	}else{
		stackedSchedules--;
	}
}

