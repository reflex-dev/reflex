/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
  
 *	Author:		 Karsten Walther
 */
#include "reflex/scheduling/PrioritySchedulerNonPreemptive.h"
#include "reflex/interrupts/InterruptLock.h"
#include "reflex/powerManagement/PowerManager.h"
#include "reflex/CoreApplication.h"

using namespace reflex;

void PriorityScheduler::start()
{
	while(1){

	    running = readyList.takeFirst();
		while(!running){
			mcu::CoreApplication::instance()->powerManager()->powerDown();
			_interruptsDisable();
			running = readyList.takeFirst();
		}

	    //run activity
	    _interruptsEnable();
    	running->run();
	    _interruptsDisable();

	    running->status = PriorityActivity::IDLE;

	    if((running->rescheduleCount > 0) && (!running->locked)){
    		running->rescheduleCount--;
			readyList.insert(running);
			running->status = PriorityActivity::SCHEDULED;
		}
	}
}

void PriorityScheduler::schedule(PriorityActivity* act)
{
	//Interrupts must be disabled now, at the end they must be enabled
	//when call was softwareinitiated and must stay disabled when schedule
	//was called by an interrupthandler
	InterruptLock lock;

	if(	(act->status != PriorityActivity::IDLE ) ||
		act->locked) {

		act->rescheduleCount++;
	}else{
		readyList.insert(act);
		act->status = PriorityActivity::SCHEDULED;
	}

}

void PriorityScheduler::unlock(PriorityActivity* act)
{
	InterruptLock lock;

	act->locked = false;

	if(	act->status == PriorityActivity::IDLE ) {

		if(act->rescheduleCount){
			act->rescheduleCount--;
			readyList.insert(act);
			act->status = PriorityActivity::SCHEDULED;
		}
	}
}
