/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
  
 *	Author:		 Karsten Walther
 */

#include "reflex/scheduling/TimeTriggeredActivity.h"
#include "reflex/CoreApplication.h"

using namespace reflex;

TimeTriggeredActivity::TimeTriggeredActivity()
{
    rescheduleCount = 0;
    locked = false;
}

TimeTriggeredActivity::~TimeTriggeredActivity()
{
    mcu::CoreApplication::instance()->scheduler()->removeActivity(this);
}
