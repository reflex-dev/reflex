/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
  
 *	Author:		 Karsten Walther
 */
#include "reflex/scheduling/FifoActivity.h"
#include "reflex/CoreApplication.h"

using namespace reflex;

FifoActivity::FifoActivity()
{
    status = IDLE;
    next = 0;
    rescheduleCount = 0;
    locked = false;
}

void FifoActivity::lock()
{
	locked = true;
	mcu::CoreApplication::instance()->scheduler()->lockCount++;
}


void FifoActivity::unlock()
{
	mcu::CoreApplication::instance()->scheduler()->unlock(this);
	mcu::CoreApplication::instance()->scheduler()->lockCount--;
}

void FifoActivity::trigger()
{
	mcu::CoreApplication::instance()->scheduler()->schedule(this);
}

FifoActivity* FifoActivity::dummy()
{
	return this;
}


