#ifndef EDFActivity_h
#define EDFActivity_h
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */
#include "reflex/types.h"
#include "reflex/data_types/ChainLink.h"

namespace reflex {

/*!
\brief A deadline-aware activity.
*/
class EDFActivity : public data_types::ChainLink
{
public:
    EDFActivity();

    void lock();
    bool isLocked() const;
    void unlock();

    bool isScheduled() const;
    uint8 triggered() const;

    Time responseTime() const;
    void setResponseTime(Time ticks);

    bool lessEqual(EDFActivity& right);

protected:
    virtual void run() = 0;

private:
    friend class EDFScheduler;
    friend class Trigger;

    enum Status
    {
        RUNNING ,
        INTERRUPTED ,
        SCHEDULED,
        IDLE
    };

    void updateDeadline();
    void trigger();

    Time _responseTime;
    Time deadline;
    Status status;
    bool locked;
    uint8 rescheduleCount;
};

inline bool EDFActivity::isLocked() const
{
    return locked;
}

/*!
Returns true if the activity is scheduled for execution.

Once an activity is scheduled, it will run, regardless of its locking state.

\see lock(), unlock()
 */
inline bool EDFActivity::isScheduled() const
{
    return status == SCHEDULED;
}

/*!
Returns, how many times this activity has been triggered.
*/
inline uint8 EDFActivity::triggered() const
{
    return rescheduleCount;
}

/*!
\brief Returns the current response time of the activity.
 */
inline Time EDFActivity::responseTime() const
{
    return _responseTime;
}


/*!
\brief Sets the response time to \a ticks.

The response time is the maximum delay time after triggering the activity.
This value is typically set during application startup and must NOT change
after the activity has been scheduled.

When calling this method, make sure, that the activity has not been scheduled
by calling isScheduled() (with the kernel locked).
 */
inline void EDFActivity::setResponseTime(Time ticks)
{
    this->_responseTime = ticks;
}

} //namespace reflex

#endif

