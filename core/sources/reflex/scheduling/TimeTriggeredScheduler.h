#ifndef TimeTriggeredScheduler_h
#define TimeTriggeredScheduler_h

/*
 *    This file is part of REFLEX.
 *
 *    Copyright 2014 BTU Cottbus, Department for Distributed Systems and
 *    Operating Systems. All rights reserved.
 *
 *    Redistribution and use in source and binary forms, with or without
 *    modification, are permitted provided that the following conditions
 *    are met:
 *
 *       1. Redistributions of source code must retain the above copyright
 *          notice, this list of conditions and the following disclaimer.
 *
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the
 *          distribution.
 *
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *    The views and conclusions contained in the software and documentation
 *    are those of the authors and should not be interpreted as representing
 *    official policies, either expressed or implied, of BTU Cottbus,
 *    Department for Distributed Systems and Operating Systems.
 *
 * */

#include "reflex/data_types/ChainLink.h"
#include "reflex/data_types/LinkedList.h"
#include "reflex/scheduling/TimeTriggeredActivity.h"
#include "reflex/sinks/Sink.h"

namespace reflex {

class TimeTriggeredScheduler : private Sink0
{
    friend class TimeTriggeredActivity;

public:
    TimeTriggeredScheduler();
    Sink0* in_tick();
    void registerActivity(TimeTriggeredActivity* act, Time startTime);
    void removeActivity(TimeTriggeredActivity* act);
    void start();

protected:
    void schedule(TimeTriggeredActivity* act);

private:
    virtual void notify();

    struct Frame : public data_types::ChainLink
    {
        TimeTriggeredActivity* act;
        Time startTime;

        bool lessEqual(Frame& right) const;
    };

    data_types::LinkedList<Frame*> availableFrames;
    data_types::LinkedList<Frame*> usedFrames;

    Frame frames[NR_OF_SCHEDULING_SLOTS];
    Frame* currentFrame;
    Time currentTick;
};


inline Sink0* TimeTriggeredScheduler::in_tick()
{
    return this;
}


inline void TimeTriggeredScheduler::schedule(TimeTriggeredActivity* act)
{
    act->rescheduleCount++;
}

inline bool TimeTriggeredScheduler::Frame::lessEqual(Frame& right) const
{
    return (startTime <= right.startTime);
}


} //namespace reflex

#endif
