/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
  
 *	Author:		 Karsten Walther
 */
#include "reflex/scheduling/PriorityActivity.h"
#include "reflex/CoreApplication.h"

using namespace reflex;

PriorityActivity::PriorityActivity()
{
    status = IDLE;
    next = 0;
    rescheduleCount = 0;
    locked = false;
    priority = DefaultPriority;
}

void PriorityActivity::lock()
{
	locked = true;
	mcu::CoreApplication::instance()->scheduler()->lockCount++;
}

void PriorityActivity::unlock()
{

	mcu::CoreApplication::instance()->scheduler()->unlock(this);
}

void PriorityActivity::trigger()
{
	mcu::CoreApplication::instance()->scheduler()->schedule(this);
}

