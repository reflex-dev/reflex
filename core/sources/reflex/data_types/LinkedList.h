#ifndef LINKEDLIST_H
#define LINKEDLIST_H
/*
 * REFLEX - Real-time Event FLow EXecutive
 *
 * A lightweight operating system for deeply embedded systems.
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2014 BTU Cottbus, Department for Distributed Systems and
 *    Operating Systems. All rights reserved.
 *
 *    Redistribution and use in source and binary forms, with or without
 *    modification, are permitted provided that the following conditions
 *    are met:
 *
 *       1. Redistributions of source code must retain the above copyright
 *          notice, this list of conditions and the following disclaimer.
 *
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the
 *          distribution.
 *
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED
 *    SYSTEMS AND OPERATION SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND
 *    OPERATION SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *    The views and conclusions contained in the software and documentation
 *    are those of the authors and should not be interpreted as representing
 *    official policies, either expressed or implied, of BTU Cottbus,
 *    Department for Distributed Systems and Operating Systems.
 *
 * */

#include <reflex/data_types/ChainLink.h>
#include <reflex/debug/Assert.h>

namespace reflex {
namespace data_types {

//! \cond
namespace internal {

class LinkedList
{
public:
    LinkedList();

    const ChainLink* first() const { return _head.next; }
    ChainLink* first() { return _head.next; }
    bool isEmpty() const;
    const ChainLink* last() const { return isEmpty() ? 0 : _tail; }
    ChainLink* last() { return isEmpty() ? 0 : _tail; }

    void append(ChainLink* item);
    void insertBefore(ChainLink* before, ChainLink* item);
    void remove(ChainLink* item);
    ChainLink* takeFirst();

protected:
    ChainLink _head;
    ChainLink* _tail;
};

inline LinkedList::LinkedList()
{
    _tail = &this->_head;
}

inline bool LinkedList::isEmpty() const
{
    return _tail == &_head;
}

inline void LinkedList::append(ChainLink* item)
{
    Assert(item);
    Assert(!item->isLinked());

    _tail->next = item;
    _tail = item;
}

inline void LinkedList::insertBefore(ChainLink* before, ChainLink* item)
{
    if (!before || isEmpty())
    {
        append(item);
    }
    // Search for position with O(n)
    else
    {
        Assert(item);
        Assert(!item->isLinked());
        Assert(!isEmpty());
        ChainLink* i = &_head;
        while(i->next != before)
        {
            i = i->next;
            Assert(i);
        }
        item->next = before->next;
        before->next = item;
    }
}

/*
- O(1) for first item
- O(n) for an arbitrary item
 */
inline void LinkedList::remove(ChainLink* item)
{
    Assert(item);
    Assert(!isEmpty());

    ChainLink* i = &_head;
    while (i->next != item)
    {
        i = i->next;
        Assert(i);
    }
    i->next = item->next;
    if (item == _tail)
    {
        _tail = i;
    }
    item->unlink();
}

inline ChainLink* LinkedList::takeFirst()
{
    ChainLink* item = _head.next;
    if (item != 0)
    {
        _head.next = item->next;
        if (_tail == item)
        {
            _tail = &_head;
        }
        item->unlink();
    }
    return item;
}

}

template<typename T>
class LinkedList
{
private:
    LinkedList();
};

//! \endcond


/*!
\brief Provides forward-linked lists of ChainLink objects.

LinkedList is a generic template class which provides forward-linked lists
of ChainLink objects. It offers constant-time insertions and removals and fast
access to both ends, which makes it ideal for queues. LinkedList can even be
used for sorted lists through its sorted insert() method.

Though being a template, LinkedList's value type must be pointers to ChainLink
objects or objects of classes deriving from ChainLink. The following snippet
gives an example for such a custom list.

\code
// A custom node type...
struct DataNode : public reflex::data_types::ChainLink
{
    int data;
}

// ... and a list for it
reflex::data_types::LinkedList<DataNode*> dataList;

// This does not compile
reflex::data_types::LinkedList<int> integerList;
\endcode

That is, because LinkedList heavily depends on ChainLink to build up its
internal link structure. It does not take ownership of the items, nor does
it manage physical storage for them. Instead, the user is responsible for
destroying removed items. Items can not shared between different lists.
One item must always belong into a single list at a time. Therefore,
copy constructor and assignment operator are disabled.

Since LinkedList and ChainLink form a forward-linked structure, insertion and
removal at the front can be done in constant time. Additionally, appending
items at the back has also O(1), which makes LinkedList ideal for queues.
Insertion and removal of items at arbitrary positions has O(n).

Iterating over a LinkedList is straight forward:
\code
// Using DataNode and dataList from the example above
for (ChainLink* item = dataList.first(); item->hasNext(); item = item->next())
{
    static_cast<DataNode*>(item)->data = 47;
}

ChainLink* item = dataList.first();
while (item->hasNext())
{
    static_cast<DataNode*>(item)->data = 11;
    item = item->next();
}
\endcode

\todo C++11 range-based iteration could make the code more-readable.
\todo Think about a more flexible solution for sorted insertions

\see ChainLink
 */
template<typename T>
class LinkedList<T*> : private internal::LinkedList
{
public:
    LinkedList();
    const T* back() const { return last(); }
    T* back() { return last(); }
    const T* first() const;
    T* first();
    const T* front() const { return first(); }
    T* front() { return first(); }
    const T* last() const;
    T* last();

    bool isEmpty() const;

    void append(T* item);
    void insert(T* item);
    void insertBefore(T* before, T* item);
    void remove(T* item);
    T* takeFirst();

private:
    LinkedList(const LinkedList&);
    LinkedList& operator=(const LinkedList&);
};


/*!
\brief Constructs an empty list.
 */
template<typename T>
LinkedList<T*>::LinkedList() {}


/*!
\brief Inserts \a item at the end of the list.

After this method, last() points to \a item.
 */
template<typename T>
void LinkedList<T*>::append(T* item)
{
    internal::LinkedList::append(item);
}

/*!
\brief Returns a reference to the first item in the list.

If the list is empty, a null-pointer is returned. This avoids the need to check
isEmpty() before retrieving the first item.

\sa last(), isEmpty().
 */
template<typename T>
const T* LinkedList<T*>::first() const
{
    return static_cast<T*>(internal::LinkedList::first());
}

/*!
\overload
 */
template<typename T>
T* LinkedList<T*>::first()
{
    return static_cast<T*>(internal::LinkedList::first());
}

/*!
\brief Returns a reference to the last item in the list.

If the list is empty, a null-pointer is returned. This saves the need to check
isEmpty() before retrieving the last item.

\sa first(), isEmpty().
 */
template<typename T>
const T* LinkedList<T*>::last() const
{
    return static_cast<T*>(internal::LinkedList::last());
}

/*!
\overload
 */
template<typename T>
T* LinkedList<T*>::last()
{
    return static_cast<T*>(internal::LinkedList::last());
}

/*!
\brief Inserts \a item at the first fitting position assuming ascending order.

This method has O(1) if \a item is bigger than the maximum and O(n) for
arbitrary items.
 */
template<typename T>
inline void LinkedList<T*>::insert(T* item)
{
    if (isEmpty() || (last()->lessEqual(*item)))
    {
        append(item);
    }
    else
    {
        Assert(item);
        Assert(!item->isLinked());

        ChainLink* i = &_head;
        while (static_cast<T*>(i->next)->lessEqual(*item) && (i->next != 0))
        {
            i = i->next;
        }
        item->next = i->next;
        i->next = item;
    }
}

/*!
\brief Inserts \a item in front of the item pointed to by \a before.

If \a before is a null-pointer or the list is empty, the \a item is
appended to the list.

This method has O(n).
 */
template<typename T>
void LinkedList<T*>::insertBefore(T* before, T* item)
{
    internal::LinkedList::insertBefore(before, item);
}

/*!
\brief Returns true if the list contains no items; otherwise returns false.
 */
template<typename T>
bool LinkedList<T*>::isEmpty() const
{
    return internal::LinkedList::isEmpty();
}

/*!
\brief Removes \a item from the list.

This method assumes \a item being a member of the list and has O(n).
 */
template<typename T>
void LinkedList<T*>::remove(T* item)
{
    internal::LinkedList::remove(item);
}

/*!
\brief Removes and returns the first item from the list.

If empty, a null-pointer is returned.
 */
template<typename T>
T* LinkedList<T*>::takeFirst()
{
    return static_cast<T*>(internal::LinkedList::takeFirst());
}

}

}

#endif // LINKEDLIST_H
