import qbs 1.0
import ReflexPackage

/*
Build file for the REFLEX core.
*/
ReflexPackage {
    name: "core"

    Depends { name : "platform-essentials" }

    Group {
        prefix : "sources/reflex/"
        files: [
            "interrupts/InterruptGuardian.cc",
            "interrupts/InterruptHandler.cc",
            "powerManagement/PowerManageAble.cc",
            "powerManagement/PowerManager.cc",
            "timer/Clock.cc"
        ]
    }

    cpp.defines : {
        var defines = [];
        defines.push(project.core_schedulingScheme);
        if (project.core_schedulingScheme == "TIME_TRIGGERED_SCHEDULING")
        {
            if (project.core_nrOfSchedulingSlots == undefined)
            {
                throw "project.core_nrOfSchedulingSlots is not set.";
            }
            defines.push("NR_OF_SCHEDULING_SLOTS=" + project.core_nrOfSchedulingSlots);

            if (project.core_ticksPerRound == undefined)
            {
                throw "project.core_ticksPerRound is not set.";
            }
            defines.push("TICKS_PER_ROUND=" + project.core_ticksPerRound);
        }
        return defines;
    }

    Group {
        files : {
            return  {
                EDF_SCHEDULING : [
                    "sources/reflex/scheduling/EDFScheduler.cc",
                    "sources/reflex/scheduling/EDFActivity.cc"
                ],
                FIFO_SCHEDULING : [
                    "sources/reflex/scheduling/FifoScheduler.cc",
                    "sources/reflex/scheduling/FifoActivity.cc"
                ],
                EDF_SCHEDULING_SIMPLE : [
                    "sources/reflex/scheduling/EDFScheduler_simple.cc",
                    "sources/reflex/scheduling/EDFActivity.cc"
                ],
                EDF_SCHEDULING_NONPREEMPTIVE : [
                    "sources/reflex/scheduling/EDFSchedulerNonPreemptive.cc",
                    "sources/reflex/scheduling/EDFActivity.cc"
                ],
                PRIORITY_SCHEDULING : [
                    "sources/reflex/scheduling/PriorityScheduler.cc",
                    "sources/reflex/scheduling/PriorityActivity.cc"
                ],
                PRIORITY_SCHEDULING_SIMPLE : [
                    "sources/reflex/scheduling/PriorityScheduler_simple.cc",
                    "sources/reflex/scheduling/PriorityActivity.cc"
                ],
                PRIORITY_SCHEDULING_NONPREEMPTIVE : [
                    "sources/reflex/scheduling/PrioritySchedulerNonPreemptive.cc",
                    "sources/reflex/scheduling/PriorityActivity.cc"
                ],
                TIME_TRIGGERED_SCHEDULING : [
                    "sources/reflex/scheduling/TimeTriggeredScheduler.cc",
                    "sources/reflex/scheduling/TimeTriggeredActivity.cc"
                ]
            }[project.core_schedulingScheme];
        }
    }

    Group {
        files: "sources/reflex/scheduling/*.h"
        qbs.install: true
        qbs.installDir: "include/reflex/scheduling"
    }

    Group {
        files: "sources/reflex/sinks/*.h"
        qbs.install: true
        qbs.installDir: "include/reflex/sinks"
    }

    Group {
        files: "sources/reflex/interrupts/*.h"
        qbs.install: true
        qbs.installDir: "include/reflex/interrupts"
    }

    Group {
        files: "sources/reflex/powerManagement/*.h"
        qbs.install: true
        qbs.installDir: "include/reflex/powerManagement"
    }

    Group {
        files: "sources/reflex/memory/*.h"
        qbs.install: true
        qbs.installDir: "include/reflex/memory"
    }

    Group {
        files: "sources/reflex/timer/*.h"
        qbs.install: true
        qbs.installDir: "include/reflex/timer"
    }

    Group {
        files: "sources/reflex/debug/*.h"
        qbs.install: true
        qbs.installDir: "include/reflex/debug"
    }

    Group {
        files: "sources/reflex/data_types/*.h"
        qbs.install: true
        qbs.installDir: "include/reflex/data_types"
    }

    Group {
        prefix : "sources/reflex/"
        files: [
            "System.h"
        ]
        qbs.install: true
        qbs.installDir: "include/reflex"
    }

    Export {
        Depends { name : "cpp" }
        Depends { name : "platform-essentials" }

        cpp.defines : {
            var defines = [];
            defines.push(project.core_schedulingScheme);
            if (project.core_schedulingScheme == "TIME_TRIGGERED_SCHEDULING")
            {
                defines.push("NR_OF_SCHEDULING_SLOTS=" + project.core_nrOfSchedulingSlots);
                defines.push("TICKS_PER_ROUND=" + project.core_ticksPerRound);
            }
            return defines;
        }

    }
}
