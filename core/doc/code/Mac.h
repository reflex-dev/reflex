//A task is derived from base class Task
class Converter : public Task {
public:
	//No explicit action needed for
	//base class
	Converter()
	{
		//initialization stuff
	}

	//this method is called by the scheduler
	virtual void run()
	{
		//do the conversion with an input value
		//and assign it to the output
		output->assign(convert(input.get()));
	}

	//The conversion logic
	Temperature convert(unsigned);

	//Buffer for the raw values
	Fifo<unsigned,AFIFOSIZE>

	//Output for converted values
	Sink<Temperature>* output;
};
