bool EDFActivity::lessEqual(EDFActivity& right)
{
	Time currentSlot = clock.getTime() & TimeMSB;
	Time leftTime = deadline xor currentSlot;
	Time rightTime =
			right.deadline xor currentSlot;
	return (leftTime <= rightTime);
}
