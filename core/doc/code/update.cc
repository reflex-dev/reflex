void EDFScheduler::updateSchedule()
{
	//as long as there are elements in 2nd list
	while(toSchedule.first()){
		//take an element from the second list (toSchedule)
		EDFActivity* current = toSchedule.deque();
		//and put it into the ready-list with enabled interrupts
		_interruptsEnable();
		readyList.insert(current);
		_interruptsDisable();
	}
}
