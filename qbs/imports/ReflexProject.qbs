import qbs 1.0
import qbs.FileInfo
import qbs.ModUtils

/*!
\brief Default Qbs item for REFLEX Projects.

This Qbs item serves as a default root item for REFLEX projects. It controls,
which REFLEX packages are built and contains configuration properties them.
This makes application development much more convenient.

\sa ReflexApplication

*/
Project {
    readonly property string reflex_path : path + "/../../"
    readonly property string reflex_platform : qbs.architecture

    property bool   buffer_enabled : true
    property string buffer_sizeType : "uint8"
    property int    buffer_maxPoolCount : 8
    property string buffer_bufferSizeType : "uint8"
    property bool   components_enabled : false
    property string core_schedulingScheme : "FIFO_SCHEDULING"
    property int    core_nrOfSchedulingSlots : undefined // deprecated
    property int    core_ticksPerRound : undefined // deprecated
    property bool   devices_enabled : true
    property int    msp430x_nodeinfoaddress : 0x1800
    property string msp430x_mcu : "cc430x6137"
    property bool   msp430x_bootloader_enabled : false
    property int    msp430x_bootloader_address : 0x8000
    property string msp430x_bootloader_channel : 0
    property bool   msp430x_update_enabled : false
    property int    msp430x_update_firmwareaddress : 0x8c00
    property int    omnetpp_nodeCount : 1
    property bool   protothread_enabled : true
    property bool   virtualtimer_enabled : true

    property bool   xml_enabled : false
    // Maximum amount of message requests in the output queue of XmlWriter.
    // This reflex in the size of XmlWriter::input_message.
    property int    xml_messageQueueSize : 1
    // Maximum length of an XML tag in the XML tag table (without \0)
    property int    xml_maxTagLength : 20

    references : {
        var pck = [
            reflex_path + "/core/core.qbs",
            reflex_path + "/platform/platform.qbs"
        ];

        if (buffer_enabled) pck.push(reflex_path + "/buffer/buffer.qbs");
        if (components_enabled) pck.push(reflex_path + "/components/components.qbs");
        if (devices_enabled) pck.push(reflex_path + "/devices/devices.qbs");
        if (protothread_enabled) pck.push(reflex_path + "/protothread/protothread.qbs");
        if (xml_enabled) pck.push(reflex_path + "/xml/xml.qbs");
        if (virtualtimer_enabled) pck.push(reflex_path + "/virtualtimer/virtualtimer.qbs");

        return pck;
    }

}
