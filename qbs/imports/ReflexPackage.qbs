import qbs 1.0

// Default QBS File for REFLEX library packages
Product {
    type: "staticlibrary"

    Depends { name: "cpp" }

    cpp.includePaths : "sources"

    Export {
        Depends { name: "cpp" }
        cpp.includePaths : "sources"
    }

    Group {
        fileTagsFilter: "staticlibrary"
        qbs.install: true
        qbs.installDir: "lib"
    }
}
