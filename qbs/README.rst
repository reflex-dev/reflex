==================
Build System Files
==================

This folder contains modules and items for building REFLEX itself and REFLEX
applications.

============ =================================================================
Subdirectory Content
============ =================================================================
config       Configuration `profiles` for the Qt Build Suite. ``example.conf``
             is recommended as a starting point.
imports      QBS `items` for convenience. They are necessary to define
             `products`.
modules      QBS modules that help to build `products`.
============ =================================================================

Read the documentation for more details.
