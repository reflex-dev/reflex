import qbs 1.0
import ReflexPackage

ReflexPackage {
    name: "xml"

    Depends { name: "buffer" }

    Group {
        prefix : "sources/reflex/xml/"
        files : [
            "XmlReader.cc",
            "XmlTagTable.cc",
            "XmlWriter.cc"
        ]
    }

    cpp.defines : [
        "XML_MESSAGE_QUEUE_SIZE=" + project.xml_messageQueueSize,
        "XML_MAX_TAG_LENGTH=" + project.xml_maxTagLength
    ]


    Depends { name : "core" }

    Group {
        files: "sources/reflex/xml/*.h"
        qbs.install: true
        qbs.installDir: "include/reflex/xml"
    }

    Export {
        cpp.defines : [
            "XML_MESSAGE_QUEUE_SIZE=" + project.xml_messageQueueSize,
            "XML_MAX_TAG_LENGTH=" + project.xml_maxTagLength
        ]
    }
}
