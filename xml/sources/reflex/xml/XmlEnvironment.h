#ifndef XMLENVIRONMENT_H_
#define XMLENVIRONMENT_H_
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *
 *	Author:		Richard Weickelt <richard@weickelt.de>
 *
 *	Class(es):	XmlEnvironment
 *
 *	Description: The XML environment
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2011 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATION SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATION SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */

#include "reflex/types.h"
#include "reflex/memory/SizedPool.h"
#include "reflex/xml/XmlChannel.h"
#include "reflex/xml/XmlContainer.h"
#include "reflex/xml/XmlConverterEnum.h"
#include "reflex/xml/XmlConverterInteger.h"
#include "reflex/xml/XmlReader.h"
#include "reflex/xml/XmlTagTable.h"
#include "reflex/xml/XmlWriter.h"

namespace reflex
{

/**
 * The XML environment integrates XML communication into the Reflex event flow.
 *
 * @sa XmlEnvironment as point to start.
 * @sa examples/XmlBlinker as an example application how to use XML in Your application.
 */
namespace xml
{

/**
 * Convenience class that includes all necessary XML components.
 *
 * Mostly, a XML application includes the basic classes XmlReader,
 * XmlWriter, XmlTagTable, a Pool and connects those objects.
 * This class serves as an example how to use them and is suitable for
 * most applications.
 *
 * \code
 * // Header file Application.h
 * class Application {
 * public:
 * 	Application();
 *
 * private:
 *	reflex::xml::XmlEnvironment xml;
 *	static const reflex::xml::XmlTagTable::LookupEntry tags[];
 * };
 *
 * // Source file Application.cc
 * using namespace reflex;
 * using namespace xml;
 * namespace reflex
 * 	{
 *		extern reflex::NodeConfiguration system;
 *	}
 *
 * const XmlTagTable::LookupEntry Application::tags[] IN_FLASH =
 *	{
 *		{ "state", &system.application.xml_stateHandler },
 *		{ "commandy", &system.application.xml_commandHandler },
 *		// More handlers/channels whatever...
 *		{ "", 0 }
 *	};
 *
 * Application::Application(): xml(&tags[0])
 * {
 * 	// Do not forget to connect the inputs and outputs of the xml environment
 * 	// to the communication interface.
 * }
 *
 * \endcode
 *
 */
struct XmlEnvironment
{

	enum
	{
		/**
		 * Each Buffer must be able to contain the tag string and <, >.
		 */
		BufferSize = XmlTagTable::LookupEntry::MaxTagLength + 2,
		/**
		 * XmlReader and XmlWriter need exactly 2 buffers for each to work.
		 * They can not benefit from more at the moment.
		 */
		PoolSize = 4
	};

	/**
	 * XML parsers and related utilities.
	 */
	reflex::SizedPool<BufferSize, PoolSize> pool; // pool for XML communication
	reflex::xml::XmlReader reader; // incoming XML data on serial port --> event flow
	reflex::xml::XmlWriter writer; // event flow --> outgoing XML data
	reflex::xml::XmlTagTable tagTable; // tag table object to identify XML handlers

	XmlEnvironment(const XmlTagTable::LookupEntry* tags) :
		pool(0), tagTable(tags)
	{
		this->reader.init(&tagTable, &pool);
		this->writer.init(&tagTable, &pool);
		this->reader.output_pollRequest = &this->writer.input_pollRequest;
	}
};
}
}

#endif /* XML_H_ */
