#include "reflex/xml/MemoryPolicy.h"
#include "reflex/xml/XmlHandler.h"
#include "reflex/xml/XmlTagTable.h"

using namespace reflex;
using namespace xml;

XmlTagTable::XmlTagTable(const XmlTagTable::LookupEntry* const first) :
	first(first), last(findLast(first))
{
}


XmlHandler* XmlTagTable::handlerByName(const char* tag) const
{
	for (const XmlTagTable::LookupEntry* entry = first; entry <= last; entry++)
	{
		if (equal(tag, &entry->tag[0]))
		{
			return MemoryPolicy::read(&entry->handler);
		}
	}
	return 0;
}


const XmlTagTable::LookupEntry* XmlTagTable::entryByName(const char* tag) const
{
	for (const XmlTagTable::LookupEntry* entry = first; entry <= last; entry++)
	{
		if (equal(tag, &entry->tag[0]))
		{
			return entry;
		}
	}
	return 0;
}


const XmlTagTable::LookupEntry* XmlTagTable::findLast(const XmlTagTable::LookupEntry* first)
{
	if (MemoryPolicy::read(&first->tag[0]) == '\0')
	{
		return first;
	}

	while (true)
	{
		if (MemoryPolicy::read(&first->tag[0]) == '\0')
		{
			return first - 1;
		}
		else
		{
			first++;
		}
	}
}


bool XmlTagTable::equal(const char* ram, const char* rom)
{
	while (*ram != '\0')
	{
		if (*ram != MemoryPolicy::read(rom))
		{
			return false;
		}
		ram++;
		rom++;
	}
	return (MemoryPolicy::read(rom) == '\0');
}
