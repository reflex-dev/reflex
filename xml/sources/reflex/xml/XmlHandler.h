#ifndef XMLHANDLER_H_
#define XMLHANDLER_H_
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *
 *	Author:		Richard Weickelt <richard@weickelt.de>
 *
 *	Class(es):	XmlHandler
 *
 *	Description: Serves as interface and default/empty XML handling object.
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2011 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATION SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATION SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */
#include "reflex/types.h"
#include "reflex/data_types/Flags.h"
namespace reflex {
class Buffer;
namespace xml {

/**
 * Base class and interface for all XML handler implementations.
 * This class handles events that occur in XmlReader and XmlWriter while parsing
 * a XML stream.
 * Derive from XmlHandler and implement handleReceiveEvent() and handleSendEvent().
 *
 * @sa XmlChannel and XmlContainer as implementation examples
 * @sa XmlReader and XmlWriter
 */
class XmlHandler {
public:
	/**
	 * Flags that set the behavior of XmlHandler.
	 * It is used for data access from the XML point of view.
	 * For example when XmlHandler::WriteEnabled is not activated, the handler implementation
	 * should ignore incoming XML data.
	 *
	 */
	enum AccessFlag {
		ReadEnabled = 1 << 0, /**< Reading from system via XML is allowed */
		WriteEnabled = 1 << 1, /**< Writing to system via XML is allowed. */
		ReplaceSystemEvents = 1 << 2	/**< Replaces system events by XML data, but does not generate events.  */
	};

	DECLARE_FLAGS(AccessFlags, AccessFlag)

	/**
	 * Events emitted by the parser, so that the XmlHandler knows what to do.
	 */
	enum XmlEvent
	{
		XmlAfterStartElement = 1 << 0,
		XmlBeforeEndElement = 1 << 1,
		XmlAfterEndElement = 1 << 2,
		XmlAfterEmptyElement = 1 << 3,
		XmlContent = 1 << 4
	};

	/**
	 * Feedback provided by XmlHandler for XmlReader/XmlWriter after handleReceiveEvent()
	 * or handleSendEvent() has been called.
	 */
	enum XmlAction
	{
		XmlNoAction = 0x00, /**< Default case, Nothing to do. */
		XmlMoreBuffers = 0x01, /**< The data was handled, but one more iteration with another buffer is needed for completion. */
		XmlRepeatPreviousTag = 0x02, /**< Causes XmlWriter go back one tag in the message skeleton. */
		XmlSkipNextTag = 0x04 /**< Causes XmlWriter continue after the following tag. */
	};

	inline XmlHandler();

	/**
	 * Implement this method for parser events of XmlReader.
	 * This method is called when XmlReader has found a tag and identified this XmlHandler
	 * through XmlTagTable. XmlReader does not handle the content of this tag, but rather
	 * forward the data 1:1.
	 *
	 * @param event parser-event flags described in XmlEvent.
	 * @param buffer the XML-ASCII data when \a event contains XmlHandler::XmlContent.
	 * 		  Otherwise a null-pointer. The buffer is not null-terminated!
	 * 		  Do not store it elsewhere!
	 */
	virtual void handleReceiveEvent(XmlEvent event, Buffer* content) = 0;

	/**
	 * Implement this method to handle parser events of XmlWriter.
	 * This method is called when XmlWriter has found a tag and identified this XmlHandler
	 * through XmlTagTable.XmlWriter does not modify the content buffer. It will be sent
	 * 1:1.
	 *
	 * @param event parser-event flags described in XmlEvent.
	 * @param buffer an empty buffer when \a event contains XmlHandler::XmlContent.
	 * 		  Otherwise a null-pointer. Do not store the buffer!
	 * 		  Do not terminate its content by a null-char.
	 * @return an XmLHandler::XmlAction.
	 */
	virtual uint8 handleSendEvent(XmlEvent event, Buffer* content) = 0;

	/**
	 * Sets the handler's configuration flags defined in XmlHandler::AccessFlag.
	 */
	void setConfig(XmlHandler::AccessFlags config)
	{
		this->config = config;
	}

	/**
	 * Reads the handler's configuration flags defined in XmlHandler::AccessFlag.
	 */
	XmlHandler::AccessFlags getConfig() const
	{
		return this->config;
	}

protected:
	AccessFlags config;

};

DECLARE_OPERATORS_FOR_FLAGS(XmlHandler::AccessFlags)

XmlHandler::XmlHandler():config(ReadEnabled | WriteEnabled)
{
}


}
}

#endif /* XMLHANDLER_H_ */
