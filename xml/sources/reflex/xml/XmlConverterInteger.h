#ifndef XMLINTEGERCONVERTER_H_
#define XMLINTEGERCONVERTER_H_
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *
 *	Author:		Richard Weickelt <richard@weickelt.de>
 *
 *	Class(es):	XmlConverterInteger
 *
 *	Description: Helper class for conversion between integer and string
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2011 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATION SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATION SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */
#include "reflex/types.h"
#include "reflex/memory/Buffer.h"
#include "reflex/data_types/Logarithm.h"
#include "reflex/xml/XmlHandler.h"

namespace reflex
{
namespace xml
{

/**
 * Conversion between integer and string.
 *
 * @tparam DataT The data type to convert.
 * @tparam base The base number system. Currently implemented are 2 (binary), 10 (decimal) and 16 (hex).
 *
 * This is a policy class for XmlHandler implementations like XmlChannel or XmlContainer,
 * but can also be used directly.
 *
 * It is able to deal with fixed-point-integer values.
 */
template<typename DataT, uint8 base = 10>
class XmlConverterInteger
{
public:
	enum Base
	{
		Binary = 2, Decimal = 10, Hex = 16
	};

	enum
	{
		Separator = '.'
	};

	void setDecimalPoint(uint8 decimalPoint = 0);

	/**
	 * Converts integer data into a string.
	 */
	bool rawToString(DataT data, Buffer* buffer);

	/**
	 * Converts a string into an integer and ignores any
	 * prefixes (0x, 0b)
	 */
	bool stringToRaw(char* string, DataT& result);

private:
	/**
	 * Converts one digit of a binary value into the character representation.
	 */
	char toChar(uint8 value);

	uint8 decimalPoint; ///< digits after comma
};

/**
 * Helper class for XmlConverterInteger to determine the
 * maximum length of a type when being expressed with
 * different base.
 */
template<typename T, uint8 base> struct TypeInfo;

template<uint8 base>
struct TypeInfo<uint8, base>
{
	enum
	{
		Digits = (base == 2) ? 8 : (base == 10) ? 3 : (base == 16) ? 2 : 0
	};
};

template<uint8 base>
struct TypeInfo<int8, base>
{
	enum
	{
		Digits = TypeInfo<uint8, base>::Digits
	};
};

template<uint8 base>
struct TypeInfo<uint16, base>
{
	enum
	{
		Digits = (base == 2) ? 16 : (base == 10) ? 5 : (base == 16) ? 4 : 0
	};
};

template<uint8 base>
struct TypeInfo<int16, base>
{
	enum
	{
		Digits = TypeInfo<uint16, base>::Digits
	};
};

template<uint8 base>
struct TypeInfo<uint32, base>
{
	enum
	{
		Digits = (base == 2) ? 32 : (base == 10) ? 10 : (base == 16) ? 8 : 0
	};
};

template<uint8 base>
struct TypeInfo<int32, base>
{
	enum
	{
		Digits = TypeInfo<uint32, base>::Digits
	};
};

template<typename DataT, uint8 base>
void XmlConverterInteger<DataT, base>::setDecimalPoint(uint8 decimalPoint)
{
	this->decimalPoint = decimalPoint;
}

/**
 * This method contains many switch statements which are all optimized
 * by the compiler.
 */
template<typename DataT, uint8 base>
bool XmlConverterInteger<DataT, base>::rawToString(DataT data, Buffer* buffer)
{
	/* Assume maximum string length for this data type and base system. */
	const uint8 digits = xml::TypeInfo<DataT, base>::Digits;

	/* Write sign if needed */
	if (data < 0)
	{
		buffer->write('-');
		data = -data;
	}

	/* Write prefix for convenience reasons */
	switch (base)
	{
	case Binary:
		buffer->write('0');
		buffer->write('b');
		break;
	case Hex:
		buffer->write('0');
		buffer->write('x');
		break;
	}

	char reverse[digits + 1]; // 1 extra for the separator
	uint8 i = 0;
	/* Write digits after decimal point first */
	while (i < decimalPoint)
	{
		reverse[i] = toChar(data % base);
		switch (base)
		{
		case Binary:
		case Hex:
			data = data >> Logarithm<base, 2>::value; // 1@Binary, 4@Hex
			break;
		default:
			data /= base;
		}
		i++;
	}

	/* Write the separator if needed */
	if (decimalPoint > 0)
	{
		reverse[i] = Separator;
		i++;
	}

	/* Write digits before separator if any left */
	switch (base)
	{
	case Binary:
	case Hex:
		/* Always fill all digits */
		while (i < digits)
		{
			reverse[i] = toChar(data % base);
			data = data >> Logarithm<base, 2>::value;
			i++;
		}
		break;
	default:
		/* Write at least one digit */
		do
		{
			reverse[i] = toChar(data % base);
			data /= 10;
			i++;
		} while (data > 0);
	}

	/* Finally reverse the string */
	while (i > 0)
	{
		i--;
		buffer->write(reverse[i]);
	}
	return true;
}

template<typename DataT, uint8 base>
bool XmlConverterInteger<DataT, base>::stringToRaw(char* string, DataT& data)
{
	bool negative = false;

	if (*string == '-')
	{
		negative = true;
		string++;
	}

	/* Handle digits before separator */
	for (; ((*string != '\0') && (*string != Separator)); string++)
	{
		if ((base == Hex) && (*string >= 'A') && (*string <= 'F'))
		{
			data = (data << Logarithm<Hex, 2>::value) + (*string - 'A' + 10);
		}
		else if ((*string >= '0') && (*string <= '9'))
		{
			data *= base;
			data += *string - '0';
		}
	}

	/* Ignore separator */
	if (*string == Separator)
	{
		string++;
	}

	/* Handle digits after separator.
	 * Only expected digits will be read. Missing digits
	 * will be interpreted as 0. */
	for (uint8 afterSeparator = decimalPoint; afterSeparator > 0; afterSeparator--)
	{
		data *= base;
		if ((*string >= '0') && (*string <= '9'))
		{
			data += (*string - '0');
			string++;
		}
	}

	if (negative == true)
	{
		data = -data;
	}

	return true;
}

template<typename DataT, uint8 base>
char XmlConverterInteger<DataT, base>::toChar(uint8 value)
{
	if (value < 10)
	{
		return ('0' + value);
	}
	else
	{
		return ('A' - 10 + value);
	}
}

}
}

#endif /* XMLINTEGERCONVERTER_H_ */
