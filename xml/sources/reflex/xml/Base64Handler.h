#ifndef BASE64HANDLER_H
#define BASE64HANDLER_H
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *
 *	Author:		Richard Weickelt <richard@weickelt.de>
 *
 *	Class(es):	Base64Handler
 *
 *	Description: Converts a stream of Base64 encoded buffers into a stream of raw buffers.
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2011 BTU Cottbus, Department for Distributed Systems and
 *    Operating Systems. All rights reserved.
 *
 *    Redistribution and use in source and binary forms, with or without
 *    modification, are permitted provided that the following conditions
 *    are met:
 *
 *       1. Redistributions of source code must retain the above copyright
 *          notice, this list of conditions and the following disclaimer.
 *
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the
 *          distribution.
 *
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED
 *    SYSTEMS AND OPERATION SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND
 *    OPERATION SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *    The views and conclusions contained in the software and documentation
 *    are those of the authors and should not be interpreted as representing
 *    official policies, either expressed or implied, of BTU Cottbus,
 *    Department for Distributed Systems and Operating Systems.
 *
 * */
#include "reflex/sinks/Sink.h"
#include "reflex/types.h"
#include "reflex/xml/XmlHandler.h"
namespace reflex
{

class Buffer;
class Pool;

namespace xml
{

/**
 * Converts a stream of Base64 encoded buffers (from XmlReader) into a stream of
 * raw buffers (to the application).
 *
 *
 * This XmlHandler is write only and can neither be used in outgoing messages nor can
 * it be polled. It will ignore all events related to sending.
 */
class Base64Handler: public XmlHandler
{
public:

	Base64Handler();

	/**
	 * Initializes the pool, from which binary buffers will be created.
	 * @param pool
	 */
	void init(Pool* pool);

	/**
	 * Does the conversion.
	 *
	 * @param event
	 * @param content
	 */
	void handleReceiveEvent(uint8 event, Buffer* content);

	/**
	 * Events related to sending are completely ignored.
	 */
	uint8 handleSendEvent(uint8 event, Buffer* content);

	Sink1<Buffer*>* output_binaryData;

private:

	Pool* pool;
	Buffer* binary;
	uint8 bytesLeft;
	uint8 unfinished[3];
};
}
}

#endif /* BASE64HANDLER_H */
