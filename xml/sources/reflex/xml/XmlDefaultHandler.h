#ifndef XMLDEFAULTHANDLER_H_
#define XMLDEFAULTHANDLER_H_
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *
 *	Author:		Richard Weickelt <richard@weickelt.de>
 *
 *	Class(es):	XmlDefaultHandler
 *
 *	Description: Default handler for values that allow linear conversion between XML and raw.
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2011 BTU Cottbus, Department for Distributed Systems and
 *    Operating Systems. All rights reserved.
 *
 *    Redistribution and use in source and binary forms, with or without
 *    modification, are permitted provided that the following conditions
 *    are met:
 *
 *       1. Redistributions of source code must retain the above copyright
 *          notice, this list of conditions and the following disclaimer.
 *
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the
 *          distribution.
 *
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED
 *    SYSTEMS AND OPERATION SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND
 *    OPERATION SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *    The views and conclusions contained in the software and documentation
 *    are those of the authors and should not be interpreted as representing
 *    official policies, either expressed or implied, of BTU Cottbus,
 *    Department for Distributed Systems and Operating Systems.
 *
 * */

#include "reflex/types.h"
#include "reflex/memory/Buffer.h"
#include "reflex/xml/XmlHandler.h"
namespace reflex
{
namespace xml
{
/**
 * Generic XML-to-value converter.
 *
 * It uses the curious recurring template pattern to gain access to
 * stringToRaw(), rawToString(), get() and set() which are to be
 * implemented in derived classes.
 *
 * Do not create instances of this class directly, but derive from it.
 * See XmlChannel, XmlContainer for further examples.
 *
 */
template<class DataT, class DerivedT>
class XmlDefaultHandler: public XmlHandler
{
public:
	/**
	 * Implements the receive functionality of XmlHandler.
	 */
	void handleReceiveEvent(XmlEvent event, Buffer* content);

	/**
	 * Implements the send functionality of XmlHandler.
	 */
	uint8 handleSendEvent(XmlEvent event, Buffer* content);
};

template<class DataT, class DerivedT>
void XmlDefaultHandler<DataT, DerivedT>::handleReceiveEvent(XmlHandler::XmlEvent event, Buffer* content)
{
	if ((event & XmlContent) && (config & WriteEnabled))
	{
		content->write('\0');
		DataT result = 0;
		const bool success = static_cast<DerivedT*> (this)->stringToRaw(reinterpret_cast<char*> (content->getStart()),
				result);
		if (success)
		{
			static_cast<DerivedT*> (this)->set(result);
		}
	}
}

template<class DataT, class DerivedT>
uint8 XmlDefaultHandler<DataT, DerivedT>::handleSendEvent(XmlHandler::XmlEvent event, Buffer* content)
{
	if ((event & XmlAfterStartElement) && (config & XmlHandler::ReadEnabled))
	{
		const DataT data = static_cast<DerivedT*> (this)->get();
		static_cast<DerivedT*> (this)->rawToString(data, content);
	}
	return XmlHandler::XmlNoAction;
}

}
}

#endif /* XMLCONVERTER_H_ */
