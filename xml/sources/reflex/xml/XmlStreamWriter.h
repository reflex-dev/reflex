#ifndef XMLSTREAMWRITER_H_
#define XMLSTREAMWRITER_H_
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *
 *	Author:  Richard Weickelt <richard@weickelt.de>
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATION SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATION SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */
#include "reflex/types.h"
namespace reflex
{
class Buffer;

namespace xml
{

/**
 * \brief Lightweight stream-oriented xml-writer.
 *
 * It's intended to be used in low-level activities that fill
 * a buffer with a complete XML message. XmlStreamWriter does
 * not prevent the user from generating invalid messages.
 *
 * \note This class is not finished, yet and needs more work!
 */
class XmlStreamWriter
{
public:
	XmlStreamWriter(Buffer* buffer);
	void writeStartDocument();
	void writeEndDocument();
	void writeStartElement(const char* name);
	void writeAttributeName(const char* name);
	void writeAttributeData(const char* data);
	void writeAttribute(const char* name, const char* data);
	void writeCharacters(const char* characters);
	void writeEndElement(const char* name);

	Buffer* getBuffer();

private:
	enum State
	{
		Idle, StartDocument, StartElement, StartAttribute, Characters, EndElement, EndDocument
	};

	Buffer* buffer;
	uint8 state;
};
}
}

#endif /* XMLSTREAMWRITER_H_ */
