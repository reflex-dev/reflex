#ifndef XMLCHANNEL_H_
#define XMLCHANNEL_H_
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *
 *	Author:		Richard Weickelt <richard@weickelt.de>
 *
 *	Class(es):	XmlChannel
 *
 *	Description: An induction-coil-like wrapper for event channels
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2011 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATION SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATION SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */
#include "reflex/interrupts/InterruptLock.h"
#include "reflex/sinks/Sink.h"
#include "reflex/xml/XmlDefaultHandler.h"
namespace reflex
{
namespace xml
{
/**
 * An induction coil that can be plugged between Sink1 event channels.
 *
 * @tparam DataT The data type of the event channel.
 * @tparam ConversionPolicy a class that converts raw data from and into XML data.
 *
 * This component enables the user to read and write XML data from/into an
 * event channel. It behaves like an inductor coil that always stores the
 * last event being sent. Like any other XmlHandler it can be configured for
 * read and write access via XmlHandler::setConfig. Additionally, XmlChannel
 * provides the configuration option XmlHandler::ReplaceSystemEvents, which will
 * not initiate, but replace system event data on occurrence. This option
 * is useful for system tests, where external sensor values might be emulated
 * and therefore replaced by XML data.
 *
 * This class is implicitly synchronized.
 *
 * @see XmlHandler for configuration examples.
 * @see XmlConverterInteger and XmlConverterEnum for @a ConversionPolicy examples.
 *
 */
template<class DataT, class ConversionPolicy>
class XmlChannel: public Sink1<DataT> ,
		public XmlDefaultHandler<DataT, XmlChannel<DataT, ConversionPolicy> > ,
		public ConversionPolicy
{
public:

	Sink1<DataT>* output; /**< Link to target event channel */

	/**
	 * Implements the Sink1 interface.
	 */
	void assign(DataT data);

	/**
	 * Caches the data internally.
	 * This method is needed by XmlDefaultHandler to forward raw data
	 * after conversion.
	 */
	void set(DataT data);

	/**
	 * Loads the data from cache.
	 * This method is needed by XmlDefaultHandler before conversion.
	 */
	DataT get() const;

	/**
	 * @see get().
	 */
	void get(DataT& data) const;

	DataT data; /**< Data cache */

};

template<class DataT, class ConversionPolicy>
void XmlChannel<DataT, ConversionPolicy>::assign(DataT data)
{
	if (this->config & XmlHandler::ReplaceSystemEvents)
	{
		data = this->get();
		if (this->output)
		{
			this->output->assign(data);
		}
	}
	else
	{
		this->set(data);
	}
}

template<class DataT, class ConversionPolicy>
void XmlChannel<DataT, ConversionPolicy>::get(DataT& data) const
{
	data = this->get();
}

template<class DataT, class ConversionPolicy>
void XmlChannel<DataT, ConversionPolicy>::set(DataT data)
{
	if ((this->output) && !(this->config & XmlHandler::ReplaceSystemEvents))
	{
		this->output->assign(data);
	}
	InterruptLock lock;
	this->data = data;
}

template<class DataT, class ConversionPolicy>
DataT XmlChannel<DataT, ConversionPolicy>::get() const
{
	InterruptLock lock;
	return this->data;
}


}
}

#endif /* XMLCHANNEL_H_ */
